<?php
   include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Meeting Dashboard | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    

    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
     <link rel="stylesheet" href="../../assets/global/vendor/multi-select/multi-select.css">
      <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-select/bootstrap-select.css">
           <link rel="stylesheet" href="../../assets/global/vendor/select2/select2.css">
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>

      <link rel="stylesheet" href="../../assets/css/customised-crm.css">

        <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
          <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
         
          <div class="col-xl-12 col-md-12">
            <!-- Widget Linearea One-->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-5 pt-5">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <?php  

                                                        $pname=mysqli_query($dbc,"select `name` from `team` where email='$id' ");
                                                        while ($pfet=mysqli_fetch_assoc($pname)) {
                                                            $pfname=$pfet['name'];
                                                        }

                                                     ?>
                  <i class="icon md-account grey-600 font-size-20 vertical-align mr-2" style="text-align: right;"></i>                   <b>Profile:</b> &nbsp;<?php  echo $pfname; ?>             
                  </div>
  
                </div>
               
                
              </div>
            </div>
            <!-- End Widget Linearea One -->
          </div>


          <div class="col-xl-3 col-md-6" data-target="#resourcewisemodal" data-toggle="modal">
            <!-- Widget Linearea One-->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-account grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Resource Wise Review
                  </div>
  
                </div>
               
                
              </div>
            </div>
            <!-- End Widget Linearea One -->
          </div>

          <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="resourcewisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Resource Wise Review</h4>
                          </div>
                          <div class="modal-body">
                            <form action="" method="get" novalidate="novalidate" autocomplete="off" >
                              <div>
                             <label class="form-control-label" >Department:</label>
                                    <select class="form-control" id="Rankd" name="Rank"  data-plugin="select2" required="required" >
                                     <option value="">Select Department</option>
                                              <option value="sales">Sales</option>
                                              <option value="technical">Technical</option>
                                    </select>
                                    </div>
                                      <div class="containersd">
                                             <div class="sales">
                                                <label class="form-control-label" >Employee:</label>
                                                <div>
                                                   <select name="project"   data-plugin="select2" class="form-control" onchange="redirect(this.value);" required >
                                                  <?php
                                                  $project=mysqli_query($dbc,"select * from `team` where `Department`='Sales'");
                                                  echo '<option >Select</option>';
                                                  while($row=mysqli_fetch_assoc($project))
                                                  {
                                                 //$pro=$row['ProjectName'];
                                             
                                                  echo '<option  value="'.$row['email'].'">'.$row['name'].'</option>';
                                                 
                                                 }
                                                 ?>
                                                </select>
                                                </div>
                                             </div>
                                             <div class="technical">
                                                <label class="form-control-label" >Employee:</label>
                                                <div>
                                                   <select class="form-control" data-plugin="select2"  name="project" id="ms_example6" onchange="redirect(this.value);" required >
                                                  <?php
                                                    $project=mysqli_query($dbc,"select * from `team` where `Department`='Technical'");
                                                    echo '<option >Select</option>';
                                                  while($row=mysqli_fetch_assoc($project))
                                                  {
                                                    //$pro=$row['ProjectName'];
                                             
                                                  echo '<option  value="'.$row['email'].'">'.$row['name'].'</option>';
                                                 
                                                  }
                                                  ?>
                                                  </select>
                                                </div>
                                             </div>
                                          </div>
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


          <div class="col-xl-3 col-md-6" data-target="#periodwisemodal" data-toggle="modal">
            <!-- Widget Linearea Two -->
            <div class="card card-shadow" id="widgetLineareaTwo">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-flash grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Period Wise Review
                  </div>
                 
                </div>
              </div>
            </div>
            <!-- End Widget Linearea Two -->
          </div>

          <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="periodwisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Period Wise Review</h4>
                          </div>
                          <div class="modal-body">
                             <form action="" method="get" class="form-horizontal" novalidate="novalidate"  autocomplete="off">
                              <div class="form-group">
                                  <label class="form-control-label" >Review Period:</label>
                                 <div>
                                    <select size="1"  data-plugin="select2" class="form-control Ranka populate" title="" name="Rank">
                                       <option value="Annual">Select</option>
                                       <option value="ryear">Yearly</option>
                                       <option value="rhalfyear">Half Yearly</option>
                                       <option value="rquater">Quarterly</option>
                                       <option value="amonth">Monthly</option>
                                       <!-- <option value="rweek">Weekly</option> -->
                                       <option value="adate">Date Range</option>
                                    </select>
                                 </div>
                                 <div class="containers">
                                    <div class="ryear">
                                       <label class="form-control-label">Year:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project"  id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <?php
                                                $curyear=date('Y');
                                                
                                                ?>
                                             <option value="<?php echo $curyear; ?>"><?php echo $curyear; ?>-<?php echo $curyear+1; ?></option>
                                             <option value="<?php echo $curyear-1; ?>"><?php echo $curyear-1; ?>-<?php echo $curyear; ?></option>
                                             <option value="<?php echo $curyear-2; ?>"><?php echo $curyear-2; ?>-<?php echo $curyear-1; ?></option>
                                             <option value="<?php echo $curyear-3; ?>"><?php echo $curyear-3; ?>-<?php echo $curyear-2; ?></option>
                                             <option value="<?php echo $curyear-4; ?>"><?php echo $curyear-4; ?>-<?php echo $curyear-3; ?></option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rhalfyear">
                                        <label class="form-control-label">Half Year:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <option value="1sty">1st</option>
                                             <option value="2ndy">2nd</option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rquater">
                                      
                                        <label class="form-control-label">Quarter:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <option value="1stq">1st</option>
                                             <option value="2ndq">2nd</option>
                                             <option value="3rdq">3rd</option>
                                             <option value="4thq">4th</option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rweek">
                                        <label class="form-control-label">Week:</label>
                                       <div>
                                          <select name="project"  data-plugin="select2" class="form-control populate" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <?php
                                                $val=1;
                                                while($val<53)
                                                {
                                                   echo '<option value="'.$val.'">'.$val.'</option>'; 
                                                   $val=$val+1;
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="amonth">
                                        <label class="form-control-label">Month:</label>
                                       <div>
                                          <select name="project"  data-plugin="select2" class="form-control populate" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <option value="04">Apr</option>
                                             <option value="05">May</option>
                                             <option value="06">Jun</option>
                                             <option value="07">Jul</option>
                                             <option value="08">Aug</option>
                                             <option value="09">Sep</option>
                                             <option value="10">Oct</option>
                                             <option value="11">Nov</option>
                                             <option value="12">Dec</option>
                                             <option value="01">Jan</option>
                                             <option value="02">Feb</option>
                                             <option value="03">Mar</option>
                                          </select>
                                       </div>
                                    </div>
                                    <!-- </div>
                                       <div class="container"> -->
                                    <div class="adate">
                                       <label class="form-control-label">Date:</label>
                                       <div>
                                          <input type="text" name="fdate" data-plugin="datepicker" class="form-control" onchange="redirectfd(this.value);" placeholder="From Date" required >
                                          <br>
                                          <input type="text" name="tdate" data-plugin="datepicker" class="form-control" onchange="redirectat(this.value);" placeholder="To Date" required >
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </form>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


          <div class="col-xl-3 col-md-6" data-target="#productwisemodal" data-toggle="modal">
            <!-- Widget Linearea Three -->
            <div class="card card-shadow" id="widgetLineareaThree">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-chart grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Product Wise Review
                  </div>
                </div>
                
              </div>
            </div>
            <!-- End Widget Linearea Three -->
          </div>

           <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="productwisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Product Wise Review</h4>
                          </div>
                          <div class="modal-body">
                            <form action="" method="get" class="form-horizontal" novalidate="novalidate" autocomplete="off" >
                                       <div class="form-group">
                                           <label class="form-control-label">Review For:</label>
                                          <div>
                                             <select size="1" id="Rankarf" data-plugin="select2" class="form-control populate" title="" name="Rank">
                                                <option value="">Select</option>
                                                <option value="rproduct">Product</option>
                                                <!-- <option value="ram">Account Manager</option> -->
                                                <!--  <option value="rregion">Region</option>
                                                   <option value="rsbu">SBU</option> -->
                                                <!-- <option value="rsbu">SBU</option> -->
                                                <!-- <option value="week">Weekly</option>
                                                   <option value="adate">Date Range</option> -->
                                             </select>
                                          </div>
                                          <div class="containersrf">
                                             <div class="rproduct">
                                                <label class="form-control-label">Product:</label>
                                                <div>
                                                   <select name="project"  data-plugin="select2" class="form-control populate" id="ms_example6" onchange="redirectmn(this.value);" required >
                                                      <option value="">Select</option>
                                                      <?php
                                                         $resfet=mysqli_query($dbc,"select distinct(ProductName) from products");
                                                         while($valrw=mysqli_fetch_assoc($resfet))
                                                         {
                                                             $pnam=$valrw['ProductName'];
                                                            echo '<option value="'.$pnam.'">'.$pnam.'</option>'; 
                                                            
                                                         }
                                                         ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


          <div class="col-xl-3 col-md-6" data-target="#customerwisemodal" data-toggle="modal">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaFour">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-view-list grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Customer Wise Review
                  </div>
                  
                </div>
                
              </div>
            </div>
            <!-- End Widget Linearea Four -->
          </div>

          <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="customerwisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Customer Wise Review</h4>
                          </div>
                          <div class="modal-body">
                           
                             <form action="" method="get" class="form-horizontal" novalidate="novalidate" autocomplete="off" >
                                       <div class="form-group">
                                          <!-- <label class="col-sm-2">&nbsp;</label>
                                          <label class="col-sm-2 control-label"  for="w4-username"><b>Review For:</b></label>
                                          <div class="col-sm-8" style="margin-left: 30px" >
                                             <select size="1" id="Rankcust" class="form-control" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' title="" name="Rank">
                                                <option value="">Select</option>
                                                <option value="rcustomer">Customers</option>
                                                
                                             </select>
                                          </div> -->
                                          <div class="containercust">
                                             <div class="rcustomer">
                                                <label class="form control-label">Customer Name:</label>
                                                <div>
                                                   <select data-plugin="select2" class="form-control populate" name="project" id="ms_example6" onchange="redirectcusts(this.value);" required >
                                                      <option value="">Select</option>
                                                      <?php
                                                         $resfet=mysqli_query($dbc,"select distinct(Company) from funnel where Stage='Won'");
                                                         while($valrw=mysqli_fetch_assoc($resfet))
                                                         {
                                                             $cname=$valrw['Company'];
                                                            echo '<option value="'.$cname.'">'.$cname.'</option>'; 
                                                            
                                                         }
                                                         ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
           <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-input-antenna grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Prospects
                  </div>
                   <?php 
                                                            $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign` ");
                                                            $rc=mysqli_fetch_assoc($cust);
                                                            $res=$rc['count_column'];
                                                            
                                                        ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example-modal-lg" data-toggle="modal" id="campaignedemodal">Campaigned</button>
                  
                </div>
              </div>
            </div>

            <!-- Modal -->
                    <div class="modal fade example-modal-lg" id="campaignedemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Campaigned Prospects</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                                /*$fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail,Date,CampaignType from `campaign` group by Company ");*/
                                                $fetfunnel=mysqli_query($dbc,"select distinct(t.Company), t.ContactPerson, t.Designation, t.Mobile, t.Mail, t.Date, t.CampaignType from (SELECT * FROM campaign ORDER BY ModificationDetail DESC) as t group by Company order by `Date` desc");
                                                
                                                
                                                //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                        
                                                echo '<table class="table table-striped table-responsive table-bordered example" >';
                                                /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>Campaign Type</th>';
                                                            echo '<th>Status</th>';
                                                            echo '<th>CampaignDate</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $cci=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $cci;
                                                            $cci=$cci+1;
                                                            $company=$frow['Company'];
                                                            $contactperson=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $campaigneddate=$frow['Date'];

                                                            $st=mysqli_query($dbc,"select Stage from funnel where Company='$company' and MeetingDate='$campaigneddate' ");
                                                            while ($str=mysqli_fetch_assoc($st)) {
                                                                $status=$str['Stage'];
                                                            }
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$cci."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['CampaignType']}</td>";
                                                            echo "<td>".$status."</td>";
                                                            echo "<td>{$frow['Date']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Meetings
                  </div>
                  <?php 
                                                            $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` ");
                                                            $rc=mysqli_fetch_assoc($cust);
                                                            $res=$rc['count_column'];
                                                            
                                                        ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example1-modal-lg" data-toggle="modal" id="totalmeetingsmodal">Total</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example2-modal-lg" data-toggle="modal" id="monthlymeetingsmodal">Monthly</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example3-modal-lg" data-toggle="modal" id="annuallymeetingsmodal">Annually</button>
                </div>
              </div>
            </div>

            <!-- Total Meetings Modal -->
 <!-- Modal -->
                    <div class="modal fade example1-modal-lg" id="totalmeetingsmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Meetings</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                                    select Company, max(Date) as MaxDate
                                                    from meeting 
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company order by `Date` desc");
                                        
                                                echo '<table class="table table-striped table-responsive table-bordered example" >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            /*echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';*/
                                                            echo '<th>Last Meeting</th>';
                                                            echo '<th>Status</th>';
                                                            echo '<th>Requirement</th>';

                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tmi=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tmi;
                                                            $tmi=$tmi+1;
                                                            $company=$frow['Company'];
                                                            $contactperson=$frow['ContactPerson'];
                                                            
                                                            $mobile=$frow['Mobile'];
                                                            $mailid=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                            $pro=$frow['ProjectName'];
                                                            $campaigntype=$frow['CampaignType'];
                                                            $requirement=$frow['Requirement'];
                                                            $sector=$frow['Sector'];
                                                            
                                                            $companytype=$frow['CompanyType'];
                                                            $designation=$frow['Designation'];


                                                            $st=mysqli_query($dbc,"select Stage from funnel where Company='$company' and MeetingDate='$lastmeeting' ");
                                                            while ($str=mysqli_fetch_assoc($st)) {
                                                                $status=$str['Stage'];
                                                            }

                                                            if($status == null)
                                                            {
                                                                echo '<tr>';
                                                            echo "<td>".$tmi."</td>";
                                                            /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                            echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="leadins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                                        <a href="proscat.php?c='.$company.'">View Profile</a>
                                                                    </div>
                                                                </div></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";*/
                                                            echo "<td>{$frow['Date']}</td>";
                                                            echo "<td>Meeting Stage</td>";
                                                            echo '<td><span class="more">'.$requirement.'</span></td>';
                                                            
                                                            echo '</tr>';
                                                            $status=null;

                                                            }
                                                            else
                                                            {

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tmi."</td>";
                                                            /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                            echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="leadins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                                        <a href="proscat.php?c='.$company.'">View Profile</a>
                                                                    </div>
                                                                </div></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                           /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";*/
                                                            echo "<td>{$frow['Date']}</td>";
                                                            echo "<td>".$status."</td>";
                                                            echo '<td><span class="more">'.$requirement.'</span></td>';
                                                            
                                                        
                                                        
                                                            echo '</tr>';
                                                            $status=null;
                                                            }
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

<!-- Monthly Meetings Modal -->
 <!-- Modal -->
                    <div class="modal fade example2-modal-lg" id="monthlymeetingsmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Monthly Meetings</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                                    select Company, max(Date) as MaxDate
                                                    from meeting where MONTH(Date) = MONTH(CURRENT_DATE()) AND YEAR(Date) = YEAR(CURRENT_DATE())
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company order by `Date` desc");
                                        
                                                echo '<table class="table table-striped table-responsive table-bordered example" >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            /*echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';*/
                                                            echo '<th>Last Meeting</th>';
                                                            echo '<th>Status</th>';
                                                            echo '<th>Requirement</th>';

                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tmi=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tmi;
                                                            $tmi=$tmi+1;
                                                            $company=$frow['Company'];
                                                            $contactperson=$frow['ContactPerson'];
                                                            
                                                            $mobile=$frow['Mobile'];
                                                            $mailid=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                            $pro=$frow['ProjectName'];
                                                            $campaigntype=$frow['CampaignType'];
                                                            $requirement=$frow['Requirement'];
                                                            $sector=$frow['Sector'];
                                                            
                                                            $companytype=$frow['CompanyType'];
                                                            $designation=$frow['Designation'];


                                                            $st=mysqli_query($dbc,"select Stage from funnel where Company='$company' and MeetingDate='$lastmeeting' ");
                                                            while ($str=mysqli_fetch_assoc($st)) {
                                                                $status=$str['Stage'];
                                                            }

                                                            if($status == null)
                                                            {
                                                                echo '<tr>';
                                                            echo "<td>".$tmi."</td>";
                                                            /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                            echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                                        <a href="proscat.php?c='.$company.'">View Profile</a>
                                                                    </div>
                                                                </div></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";*/
                                                            echo "<td>{$frow['Date']}</td>";
                                                            echo "<td>Meeting Stage</td>";
                                                            echo '<td><span class="more">'.$requirement.'</span></td>';
                                                            
                                                            echo '</tr>';
                                                            $status=null;

                                                            }
                                                            else
                                                            {

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tmi."</td>";
                                                            /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                            echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                                        <a href="proscat.php?c='.$company.'">View Profile</a>
                                                                    </div>
                                                                </div></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                           /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";*/
                                                            echo "<td>{$frow['Date']}</td>";
                                                            echo "<td>".$status."</td>";
                                                            echo '<td><span class="more">'.$requirement.'</span></td>';
                                                            
                                                        
                                                        
                                                            echo '</tr>';
                                                            $status=null;
                                                            }
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                    <!-- Annually Meetings Modal -->
 <!-- Modal -->
                    <div class="modal fade example3-modal-lg" id="annuallymeetingsmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Annual Meetings</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                    $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                        select Company, max(Date) as MaxDate
                                        from meeting where  YEAR(Date) = YEAR(CURRENT_DATE())
                                        group by Company
                                    ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company order by `ModificationDetail` desc");
                                    
                                    echo '<table class="table table-striped table-responsive table-bordered example" >';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>ContactPerson</th>';
                                                echo '<th>Designation</th>';
                                                /*echo '<th>Mobile</th>';
                                                echo '<th>Mail</th>';*/
                                                echo '<th>Last Meeting</th>';
                                                echo '<th>Status</th>';
                                                echo '<th>Requirement</th>';
                                    
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $tmi=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $tmi;
                                                $tmi=$tmi+1;
                                                $company=$frow['Company'];
                                                $contactperson=$frow['ContactPerson'];
                                                
                                                $mobile=$frow['Mobile'];
                                                $mailid=$frow['Mail'];
                                                $lastmeeting=$frow['Date'];
                                                $pro=$frow['ProjectName'];
                                                $campaigntype=$frow['CampaignType'];
                                                $requirement=$frow['Requirement'];
                                                $sector=$frow['Sector'];
                                                
                                                $companytype=$frow['CompanyType'];
                                                $designation=$frow['Designation'];
                                    
                                    
                                                $st=mysqli_query($dbc,"select Stage from funnel where Company='$company' and MeetingDate='$lastmeeting' ");
                                                while ($str=mysqli_fetch_assoc($st)) {
                                                    $status=$str['Stage'];
                                                }
                                    
                                                if($status == null)
                                                {
                                                    echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                                /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>Meeting Stage</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                                echo '</tr>';
                                                $status=null;
                                    
                                                }
                                                else
                                                {
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                               /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>".$status."</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                            
                                            
                                                echo '</tr>';
                                                $status=null;
                                                }
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Prospect Response
                  </div>
                  <?php 
                                                                $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` where `RMail`='$id' and `Response`='Positive' ");
                                                                $rc=mysqli_fetch_assoc($cust);
                                                                $res=$rc['count_column'];
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example4-modal-lg" data-toggle="modal" id="annualesponsemodal">Annually</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example5-modal-lg" data-toggle="modal" id="monthlyresponsemodal">Monthly</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example8-modal-lg" data-toggle="modal" id="totalresponsemodal">Total</button>
                </div>
              </div>
            </div>

             <!-- Annually Orders Modal -->
 <!-- Modal -->
                    <div class="modal fade example4-modal-lg" id="annualesponsemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Annual response</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.Company, m.ContactPerson, m.Designation, m.Mobile, m.Mail, m.Date   from `meeting` m inner join (
                                                    select Company, max(date) as MaxDate
                                                    from meeting where  `Response`='Positive' and YEAR(ModificationDetail) = YEAR(CURRENT_DATE())
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate order by `Date` desc ");
                                        
                                                echo '<table class="table table-striped table-responsive table-bordered example">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>Last Meeting</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tcrai=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tcrai;
                                                            $tcrai=$tcrai+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tcrai."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['Date']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                     <!-- Monthly Orders Modal -->
 <!-- Modal -->
                    <div class="modal fade example5-modal-lg" id="monthlyresponsemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Monthly Response</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.Company, m.ContactPerson, m.Designation, m.Mobile, m.Mail, m.Date   from `meeting` m inner join (
                                                    select Company, max(date) as MaxDate
                                                    from meeting where  `Response`='Positive' and MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND YEAR(ModificationDetail) = YEAR(CURRENT_DATE())
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate  order by `Date` desc");
                                        
                                                echo '<table class="table table-striped table-responsive-md table-bordered example">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>Last Meeting</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tcrmi=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tcrmi;
                                                            $tcrmi=$tcrmi+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tcrmi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['Date']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->




<!-- Today Orders Modal -->
 <!-- Modal -->
                    <div class="modal fade example8-modal-lg" id="totalresponsemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Response</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.Company, m.ContactPerson, m.Designation, m.Mobile, m.Mail, m.Date   from `meeting` m inner join (
                                                    select Company, max(date) as MaxDate
                                                    from meeting where  `Response`='Positive'
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate  order by `Date` desc");
                                        
                                                echo '<table class="table table-striped table-responsive table-bordered example">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>Last Meeting</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tcri=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tcri;
                                                            $tcri=$tcri+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tcri."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['Date']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>

                         
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-wifi-alt-2 grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Follow Up
                  </div>
                  <?php 
                                                                $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` where `RMail`='$id' and `FollowUp`='Yes' ");
                                                                $rc=mysqli_fetch_assoc($cust);
                                                                $res=$rc['count_column'];
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example6-modal-lg" data-toggle="modal" id="totalfollowupmodal">Total</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example12-modal-lg" data-toggle="modal" id="todayfollowupmodal">Today</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example11-modal-lg" data-toggle="modal" id="tomorrowfollowupmodal">Tomorrow</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example10-modal-lg" data-toggle="modal" id="weekfollowupmodal">Week</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example9-modal-lg" data-toggle="modal" id="monthfollowupmodal">Month</button>
                </div>
              </div>
            </div>

            <!-- Annual Funnel Modal -->
 <!-- Modal -->
                    <div class="modal fade example6-modal-lg" id="totalfollowupmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Follow Up</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                            echo '<table class="table table-striped table-responsive table-bordered example">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>FollowUp Date</th>';
                                                        echo '<th>FollowUp Time</th>';
                                                        echo '<th>Project</th>';
                                                        echo '<th>Requirement</th>';
                                                        echo '<th>Remarks</th>';
                                                        echo '<th>Infrastructure</th>';
                                                        echo '<th>Contact Person</th>';
                                                        echo '<th>Designation</th>';
                                                        echo '<th>Mobile</th>';
                                                        echo '<th>Mail</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $tfi=0;
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        global $tfi;
                                                        $tfi=$tfi+1;
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        

                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));
                                                        if($followupdate == $today)
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$tfi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['FollowUpDate']}";
                                                            echo '</div>';
                                                            echo '</div></td>';
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            /*echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";*/
                                                            echo '<td> <span class="more">'.$requirement.'</span></td>';
                                                            echo '<td> <span class="more">'.$remark.'</span></td>';
                                                            echo '<td> <span class="more">'.$infrastructure.'</span></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } else
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$tfi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['FollowUpDate']}</td>";
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } 
                                                        

                                                    }
                                                echo '</tbody>';
                                            echo '</table>';
                                        ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
<div class="modal fade example9-modal-lg" id="monthfollowupmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">This Month's Follow Up</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                            echo '<table class="table table-striped table-responsive table-bordered example">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>FollowUp Date</th>';
                                                        echo '<th>FollowUp Time</th>';
                                                        echo '<th>Project</th>';
                                                        echo '<th>Requirement</th>';
                                                        echo '<th>Remarks</th>';
                                                        echo '<th>Infrastructure</th>';
                                                        echo '<th>Contact Person</th>';
                                                        echo '<th>Designation</th>';
                                                        echo '<th>Mobile</th>';
                                                        echo '<th>Mail</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $tmfui=0;
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        global $tmfui;
                                                        $tmfui=$tmfui+1;
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        

                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        $fyear = date('Y', strtotime($followupdate));
                                                        $fmonth = date('F', strtotime($followupdate));

                                                        $cyear = date('Y', strtotime($today));
                                                        $cmonth = date('F', strtotime($today));
                                                        
                                                        if($fyear == $cyear and $fmonth == $cmonth)
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$tmfui."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['FollowUpDate']}";
                                                            echo '</div>';
                                                            echo '</div></td>';
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['FollowUpDate']}</td>";
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                echo '</tbody>';
                                            echo '</table>';
                                        ?>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                     <!-- Monthly Funnel Modal -->
 <!-- Modal -->
                    <div class="modal fade example12-modal-lg" id="todayfollowupmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Today's Follow Up</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                            echo '<table class="table table-striped table-responsive table-bordered example">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>FollowUp Date</th>';
                                                        echo '<th>FollowUp Time</th>';
                                                        echo '<th>Project</th>';
                                                        echo '<th>Requirement</th>';
                                                        echo '<th>Remarks</th>';
                                                        echo '<th>Infrastructure</th>';
                                                        echo '<th>Contact Person</th>';
                                                        echo '<th>Designation</th>';
                                                        echo '<th>Mobile</th>';
                                                        echo '<th>Mail</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $tdfi=0;
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        global $tdfi;
                                                        $tdfi=$tdfi+1;
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($followupdate == $today)
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$tdfi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['FollowUpDate']}";
                                                            echo '</div>';
                                                            echo '</div></td>';
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    /*$datetime = new DateTime($date);
                                                        $datetime->add(new DateInterval("P2D"));
                                                            echo $datetime->format('Y-m-d H:i:s');*/
                                            
                                                echo '</tbody>';
                                            echo '</table>';
                                        ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                     <!-- weekly Funnel Modal -->
 <!-- Modal -->
                    <div class="modal fade example11-modal-lg" id="tomorrowfollowupmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Tomorrow's Follow Up</h4>
                          </div>
                          <div class="modal-body">
                            <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                            echo '<table class="table table-striped table-responsive table-bordered example">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>FollowUp Date</th>';
                                                        echo '<th>FollowUp Time</th>';
                                                        echo '<th>Project</th>';
                                                        echo '<th>Requirement</th>';
                                                        echo '<th>Remarks</th>';
                                                        echo '<th>Infrastructure</th>';
                                                        echo '<th>Contact Person</th>';
                                                        echo '<th>Designation</th>';
                                                        echo '<th>Mobile</th>';
                                                        echo '<th>Mail</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $tmfi=0;
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        global $tmfi;
                                                        $tmfi=$tmfi+1;
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        

                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        $tomorrowdate=(new DateTime($today))->add(new DateInterval("P1D"))->format('Y-m-d');
                                                        //echo $tomorrowdate;
                                                        if($followupdate == $tomorrowdate)
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$tmfi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['FollowUpDate']}";
                                                            echo '</div>';
                                                            echo '</div></td>';
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['FollowUpDate']}</td>";
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }

                                                echo '</tbody>';
                                            echo '</table>';
                                        ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                     <!-- Today Funnel Modal -->
 <!-- Modal -->
                    <div class="modal fade example10-modal-lg" id="weekfollowupmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">This Week's Follow Up</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                            echo '<table class="table table-striped table-responsive table-bordered example">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>FollowUp Date</th>';
                                                        echo '<th>FollowUp Time</th>';
                                                        echo '<th>Project</th>';
                                                        echo '<th>Requirement</th>';
                                                        echo '<th>Remarks</th>';
                                                        echo '<th>Infrastructure</th>';
                                                        echo '<th>Contact Person</th>';
                                                        echo '<th>Designation</th>';
                                                        echo '<th>Mobile</th>';
                                                        echo '<th>Mail</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $twfi=0;
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        global $twfi;
                                                        $twfi=$twfi+1;
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        

                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        $monday = strtotime("last monday");
                                                        $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;

                                                        $satday = strtotime(date("Y-m-d",$monday)." +5 days");

                                                        $this_week_sd = date("Y-m-d",$monday);
                                                        $this_week_ed = date("Y-m-d",$satday);

                                                        if($followupdate >=$this_week_sd and $followupdate<=$this_week_ed)
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$twfi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['FollowUpDate']}";
                                                            echo '</div>';
                                                            echo '</div></td>';
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['FollowUpDate']}</td>";
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                echo '</tbody>';
                                            echo '</table>';
                                        ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
   


            <!-- End Widget Linearea Four -->
          </div>
         
           

        


         

          
         

         

        

        </div>
      </div>
    </div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>
    <!-- Core  -->

    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>

        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
            <script src="../../assets/global/vendor/multi-select/jquery.multi-select.js"></script>
                 <script src="../../assets/global/js/Plugin/multi-select.js"></script>
                  <script src="../../assets/global/js/Plugin/bootstrap-select.js"></script>
                     <script src="../../assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

                  <script >
          $(document).ready(function() {
    $('.example').DataTable();
} );
        </script>
<script>
            $(document).ready(function() {
    $('.example1').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example2').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example3').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example4').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example5').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example6').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example7').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example8').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example9').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example10').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example11').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example12').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example13').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example14').DataTable();
} );
        </script>
   <script src="select-option-meetingjs.js"></script>
  </body>
</html>
