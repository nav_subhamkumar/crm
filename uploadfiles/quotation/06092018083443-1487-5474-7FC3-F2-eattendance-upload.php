<?php
   include "session_handler.php";
   ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="bootstrap material admin template">
      <meta name="author" content="">
      <title>Attendance Upload | Bizapp CRM</title>
      <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
      <link rel="shortcut icon" href="../../assets/images/favicon.ico">
      <!-- Stylesheets -->
      <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
      <link rel="stylesheet" href="../../assets/css/site.min.css">
      <!-- Plugins -->
      <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
      <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
      <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
      <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
      <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
      <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
      <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
      <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
      <!-- Fonts -->
      <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
      <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
      <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- table set -->
      <link rel='stylesheet' href='../../assets/css/customised-crm.css'>
      <?php include "includes/css/select.php"; ?>
      <?php include "includes/css/tables.php"; ?>
      
      <script src="../../assets/js/customised-crm.js"></script>
      <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
      <!--[if lt IE 9]>
      <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
      <![endif]-->
      <!--[if lt IE 10]>
      <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
      <script src="../../assets/global/vendor/respond/respond.min.js"></script>
      <![endif]-->
      <!-- Scripts -->
      <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
      <script>
         Breakpoints();
      </script>
   </head>
   <body class="animsition site-navbar-small dashboard">
      <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <?php include "navbar-header.php"; ?>
      <?php include "side-navigation.php"; ?>   
      <!-- Page -->
      <div class="page">
         <div class="page-content" >
            <!-- Panel Basic -->
            <!-- upload start -->
            <div class="panel">
               <div class="panel-body container-fluid">
                  <div class="row row-lg">
                     <div class="col-md-12">
                        <div class="example-wrap">
                           <h4 class="example-title">Upload Customers Detail</h4>
                           <div class="example">
                              <form action="../auth/hr/employee/eattendance.php" method="post"   >
                                 <div class="row">
                                    <div class="form-group  col-md-6">
                                       <label class="form-control-label" >Date Of Attendance</label>
                                        
                                        <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text">
                                        <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                        </div>
               <input type="text" name="doa" class="form-control date" data-plugin="datepicker">
                                      </div>                                 
                                    </div>

                                    <div class="form-group  col-md-6">
                                       <label class="form-control-label" for="inputBasicFirstName">Upload</label>
                                       <input type="file" class="form-control"  name="file"  />
                                    </div>
                                    
                                    <div class="form-group col-md-9">
                                       <!-- <input type="submit" name="upload_submit" value="Submit" class="btn btn-success" > -->
                                       <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                    <div class="form-group col-md-2">
                                       <h5 class="pull-right"><a href="../../assets/data-format/eattendance.csv">Download Sample File (.csv)</a></h5>
                                       <h5 ><font color="red">*</font> Upload Maximum 500 Row</h5>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                        <!-- End Example Basic Form -->
                     </div>
                  </div>
               </div>
            </div>
            <!-- upload end -->
            <!-- input customer details  start -->
            <div class="panel">
               <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                  <h4 class="example-title">Attendance List</h4>
                  <div class="example-wrap">
                  <!--  basic start -->
                  <?php

                                        $today=date();
                                        $yesterday=$today-1;

                                        $fetdetail=mysqli_query($dbc,"select * from `eattendance`  order by `Date` desc ");



                                        /*echo '<table class="table table-striped mb-none" id="datatable-tabletools">';*/
                                        echo '<table class="table table-striped table-responsive-md table-bordered examplee">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Date</th>';
                                                    echo '<th>Employee ID</th>';
                                                    echo '<th>Employee Name</th>';
                                                    echo '<th>Shift</th>';
                                                    echo '<th>In Time</th>';
                                                    echo '<th>Out Time</th>';
                                                    echo '<th>Shift Hours</th>';
                                                    echo '<th>Work Hours</th>';
                                                    echo '<th>OT Hours</th>';
                                                    echo '<th>Work Status</th>';
                                                    echo '<th>Remarks</th>';
                                                    echo '<th>Uploaded On</th>';
                                                   /* echo '<th>View</th>';
                                                    echo '<th>Update</th>';
                                                    echo '<th>Remove</th>';*/
                                                    
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $cp=0;
                                            
                                                while($frow=mysqli_fetch_assoc($fetdetail))
                                                {
                                                    global $cp;
                                                    $cp=$cp+1;
                                                    $quotno=$frow['EmployeeId'];
                                                    $company=$frow['EmployeeName'];
                                                    $product=$frow['Shift'];
                                                    $service=$frow['InTime'];
                                                    $grandtotal=$frow['OutTime'];
                                                    $modificationdetail=$frow['ShiftHours'];
                                                    $modificationdetail=$frow['WorkHours'];
                                                    $modificationdetail=$frow['OTHours'];
                                                    $modificationdetail=$frow['WorkStatus'];
                                                    $modificationdetail=$frow['Remarks'];
                                                    $modificationdetail=$frow['ModificationDetail'];
                                               

                                                        $fetfunnelrev=mysqli_query($dbc,"select * from `eattendance` where `Date`='$yesterday' order by `Date` desc ");
                                                       

                                                        while($result=mysqli_fetch_assoc($fetfunnelrev))
                                                        {
                                                            
                                                            $probability=$result['Probability'];

                                                        }

                                                        
                                                        echo '<tr>';
                                                        echo "<td>".$cp."</td>";
                                                        echo "<td>{$frow['Date']}</td>";
                                                        echo "<td>{$frow['EmployeeId']}</td>";
                                                        echo "<td>{$frow['EmployeeName']}</td>";
                                                        echo "<td>{$frow['Shift']}</td>";
                                                        echo "<td>{$frow['InTime']}</td>";
                                                        echo "<td>{$frow['OutTime']}</td>";
                                                        echo "<td>{$frow['ShiftHours']}</td>";
                                                        echo "<td>{$frow['WorkHours']}</td>";
                                                        echo "<td>{$frow['OTHours']}</td>";
                                                        echo "<td>{$frow['WorkStatus']}</td>";
                                                        echo "<td>{$frow['Remarks']}</td>";
                                                        echo "<td>{$frow['ModificationDetail']}</td>";
                                                        // echo "<td>".$quotdate."</td>";
                                                        // echo "<td>".$probability."</td>";
                                                        /*echo '<td><a class="btn btn-primary" href="quotation.php?n='.$quotno.'">View</a></td>';*/
                                                        /*echo '<td><a href="quotation.php?q='.$quotno.'" target="_blank" class="btn btn-primary">View</a></td>';

                                                        echo '<td><form action="quotupdate.php" method="post">
                                                        <input type="hidden" name="quotno" value="'.$quotno.'">
                                                        <input type="submit" name="update" value="Update" class="btn btn-warning"></form> </td>';

                                                        echo '<td><form action="quotation.php" method="post">
                                                        <input type="hidden" name="quotno" value="'.$quotno.'">
                                                        <input type="submit" name="delete" value="Delete" class="btn btn-danger"></form> </td>';
*/
                                                       
    
                                                        
                                                }


                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>
                  <!-- input customer details end -->
                <!-- End Panel Basic -->
             </div>
         </div>
      </div></div></div>
      <!-- End Page -->
      <!-- Footer -->
      <?php include "includes/footer.php"; ?>

      
      <!-- Core  -->
      <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
      <script src="../../assets/global/vendor/jquery/jquery.js"></script>
      <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
      <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
      <script src="../../assets/global/vendor/animsition/animsition.js"></script>
      <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
      <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
      <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
      <script src="../../assets/global/vendor/waves/waves.js"></script>
      <!-- Plugins -->
      <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
      <script src="../../assets/global/vendor/switchery/switchery.js"></script>
      <script src="../../assets/global/vendor/intro-js/intro.js"></script>
      <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
      <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
      <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
      <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
      <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
      <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
      <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
      <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
      <!-- Scripts -->
      <script src="../../assets/global/js/Component.js"></script>
      <script src="../../assets/global/js/Plugin.js"></script>
      <script src="../../assets/global/js/Base.js"></script>
      <script src="../../assets/global/js/Config.js"></script>
      <script src="section/Menubar.js"></script>
      <script src="section/Sidebar.js"></script>
      <script src="section/PageAside.js"></script>
      <script src="section/GridMenu.js"></script>
      <!-- Config -->
      <script src="../../assets/global/js/config/colors.js"></script>
      <script src="../../assets/js/config/tour.js"></script>
      <script>Config.set('assets', '../assets');</script>
      <!-- Page -->
      <script src="../../assets/js/Site.js"></script>
      <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
      <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
      <script src="../../assets/global/js/Plugin/switchery.js"></script>
      <script src="../../assets/global/js/Plugin/matchheight.js"></script>
      <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
      <script src="../../assets/global/js/Plugin/peity.js"></script>
      <script src="../../assets/examples/js/dashboard/v1.js"></script>
      
      <!-- date picker -->
        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="../../assets/global/js/Plugin/bootstrap-datepicker.js"></script>

      <?php include "includes/js/select.php"; ?>
      <?php include "includes/js/dynamicaddition.php"; ?>
      <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
   
      <script >
          $(document).ready(function() {
    $('.examplee').DataTable();
} );
        </script>
   </body>
</html>