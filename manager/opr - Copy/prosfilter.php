<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Prospect Filter | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
      <link rel="stylesheet" href="../../assets/css/site.min.css">
      <!-- Plugins -->
      <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
      <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
      <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
      <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
      <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
      <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
      <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
      <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
      <!-- Fonts -->
      <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
      <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
      <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- table set -->
      <link rel='stylesheet' href='../../assets/css/customised-crm.css'>
      <script src="../../assets/js/customised-crm.js"></script>

      <?php include "includes/css/select.php"; ?>

      <?php include "includes/css/datepicker.php"; ?>
      <?php include "includes/css/tables.php" ?>

      <!-- <link rel="stylesheet" href="../../assets/examples/css/forms/advanced.css">
<link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css"> -->
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
    
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <!-- write body content here -->

      <div class="page-content">
      
        <!-- Panel Select 2 -->
        <div class="panel">
               <div class="panel-body container-fluid">
                  <div class="row row-lg">
                     <div class="col-md-12">
                        <div class="example-wrap">
                           <h4 class="example-title">Prospect Search</h4>
                           <div class="example">
                              <form action="" method="post" autocomplete="off">
                                 <div class="row">
                                    
                                    <div class="form-group  col-md-6">
                                       <label class="form-control-label" >Sector</label>
                                       <select name="sector[]" class="form-control " multiple data-plugin="select2" required>
                                                                    <?php

                                                                    $comp=$_POST['c'];
                                                                    $project=mysqli_query($dbc,"select distinct(SectorName) from `sector`  ");
                                                                    
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                        //$pro=$row['ProjectName'];


                                                        
                                                                         echo '<option  value="'.$row['SectorName'].'">'.$row['SectorName'].'</option>';
                                                                        
                                                                    }
                                                                   ?>
                      
                                    </select>
                                    </div>
                                    <div class="form-group  col-md-6">
                                      <label class="form-control-label" >Designation</label>
                                       <select name="designation" class="form-control "  data-plugin="select2" >
                                                                    <?php
                                  $project=mysqli_query($dbc,"select distinct(Designation) from `customers`");
                                  echo '<option value="">Select</option>';
                                  while($row=mysqli_fetch_assoc($project))
                                  {
                                    //$pro=$row['ProjectName'];
                            
                                     echo '<option  value="'.$row['Designation'].'">'.$row['Designation'].'</option>';
                                    
                                  }
                                ?>
                      
                                    </select>
                                    </div>
                                    </div>

                                    <div class="row">
                                    <div class="form-group  col-md-6">
                                        <label class="form-control-label" >Department</label>
                                        <select name="department" class="form-control maxlength" data-plugin="select2">
                                          <?php
                                  $project=mysqli_query($dbc,"select distinct(Dept) from `customers`");
                                  echo '<option value="">Select</option>';
                                  while($row=mysqli_fetch_assoc($project))
                                  {
                                    //$pro=$row['ProjectName'];
                            
                                     echo '<option  value="'.$row['Dept'].'">'.$row['Dept'].'</option>';
                                    
                                  }
                                ?>
                      
                                        </select>
                                    </div>
                                    <div class="form-group  col-md-6">
                                       <label class="form-control-label" >Company Type</label>
                                       <select name="companytype" class="form-control " data-plugin="select2">
                                          <option value="">Select</option>
                                          <option value="SMB">SMB</option>
                                          <option value="Enterprise">Enterprise</option>
                                          <option value="Large">Large</option>
                      
                                        </select>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="form-group  col-md-6">
                                        <label class="form-control-label" >No of Employees</label>
                                        <select name="noofemployees" class="form-control maxlength" data-plugin="select2">
                                          <option value="">Select</option>
                                                              <option value="0-50">0-50</option>
                                                              <option value="51-250">51-250</option>
                                                              <option value="251-500">251-500</option>
                                                              <option value="501-1000">501-1000</option>
                                                              <option value="Above 1000">Above 1000</option>
                      
                                        </select>
                                    </div>
                                    <div class="form-group  col-md-6">
                                       <label class="form-control-label" >Location</label>
                                       <select name="location" class="form-control "  data-plugin="select2">
                                          <?php
                                  $project=mysqli_query($dbc,"select distinct(SubLocation) from `customers`");
                                  echo '<option value="" >Select</option>';
                                  while($row=mysqli_fetch_assoc($project))
                                  {
                                    //$pro=$row['ProjectName'];
                            
                                     echo '<option  value="'.$row['SubLocation'].'">'.$row['SubLocation'].'</option>';
                                    
                                  }
                                ?>
                      
                                        </select>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="form-group  col-md-6">
                                        <label class="form-control-label" >Prospects Added</label>
                                        <select name="prosadded" class="form-control maxlength Ranks"  data-plugin="select2">
                                          <option value="">Select</option>
                                                                <option value="Today">Today</option>
                                                                <option value="ThisWeek">This Week</option>
                                                                <option value="ThisMonth">This Month</option>
                                                                <option value="ThisYear">This Year</option>
                                                                <!-- <option value="DateRange">Date Range</option> -->
                      
                                        </select>
                                    </div>
                                    <!-- <div class="containers">

                                                        <div class="DateRange">
                                    <div class="form-group  col-md-6">
                                      
                                                            <label class="form-control-label" >Date</label>
                                                                                           
                                                            
                                                            <input type="text" name="fdate" data-plugin="datepicker" class="form-control "  placeholder="From Date"  >

                                                            <input type="text" name="tdate" data-plugin="datepicker" class="form-control "   placeholder="To Date"  >
                                                          </div>
                                                        </div>
                                                            
                                                        </div> -->
                                                    
                                   
                                    </div>
                                    
                                    <div class="form-group ">
                                       <!-- <input type="submit" name="upload_submit" value="Submit" class="btn btn-success" > -->
                                       <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                    </form>
                                 </div>
                              
                           </div>
                        </div>
                        <!-- End Example Basic Form -->
                     </div>
                  </div>
               </div>


               <script type="text/javascript">
    
                                                 $(document).ready(function() {
                                                $('#Rank').bind('change', function() {
                                                var elements = $('div.containers').children().hide(); // hide all the elements
                                                /*var elements = $('div.container').children().hide();*/
                                                var value = $(this).val();

                                                if (value.length) { // if somethings' selected
                                                    elements.filter('.' + value).show(); // show the ones we want
                                                }
                                                }).trigger('change');
                                                });
                                                </script>


            
            <!-- panel end -->
        </div>
  
<div class="page-content" style="margin-top: -40px">
<!-- Panel Basic -->
        <div class="panel">
          <header class="panel-heading">
            
            <h3 class="panel-title font-size-18">Customer List</h3>
          </header>
          <div class="panel-body">
            <?php
                                    $sector=implode(',', $_POST['sector']);
                                    $designation=$_POST['designation'];
                    $department=$_POST['department'];
                    $companytype=$_POST['companytype'];
                    $noofemployees=$_POST['noofemployees'];
                    $location=$_POST['location'];
                                        $company=$_POST['c'];
                                        $prosadded=$_POST['prosadded'];
                                        $fdate=$_POST['fdate'];
                                        $tdate=$_POST['tdate'];

                                  
                               

                                ?>
           
              <?php
                    /*$fetdetail=mysqli_query($dbc,"select * from `$pro` where `Sector`='$item' or SubLocation='$location' or NoOfEmployees='$noofemployees' ");*/

                    echo '<table class="table table-striped table-responsive table-bordered" id="example">';
                      echo '<thead>';
                        echo '<tr>';
                          echo '<th>Sl No.</th>';
                                                    echo '<th>Company</th>';
                                                    echo '<th>Sector</th>';
                                                    echo '<th>Contact-Person</th>';
                                                    echo '<th>Designation</th>';
                                                    echo '<th>Department</th>';
                                                    echo '<th>Mobile-No</th>';
                                                    echo '<th>Mail-ID</th>';
                                                    echo '<th>Address</th>';
                                                    echo '<th>Location</th>';
                                                    echo '<th>Sub-Location</th>';
                                                    echo '<th>No Of Employees</th>';
                                                    echo '<th>Company Type</th>';
                                                    echo '<th>Profile</th>';
                                                    echo '<th>Update</th>';
                                                    echo '<th>Remove</th>';
                        echo '</tr>';
                      echo '</thead>';
                      echo '<tbody>';
                       $pcount=0;
                  if(isset($_POST['submit']) or isset($_POST['c']))
                  {
                                        if(isset($_POST['c']))
                                        {
                                            $sector=$_POST['sector'];
                                        }
                                        else
                                        {
                                            $sector=implode(',', $_POST['sector']);
                                        }
                    
                    
                    $designation=$_POST['designation'];
                    $department=$_POST['department'];
                    $companytype=$_POST['companytype'];
                    $noofemployees=$_POST['noofemployees'];
                    $location=$_POST['location'];
                                        $company=$_POST['c'];
                                        $prosadded=$_POST['prosadded'];
                                        $fdate=$_POST['fdate'];
                                        $tdate=$_POST['tdate'];

                    $array = explode(',', $sector);

                    if(!empty($sector))
                    {
                      


                    foreach ($array as $item) {

                    if(!empty($designation) and !empty($department) and !empty($companytype) and !empty($noofemployees) and !empty($location) )
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  and SubLocation='$location' and NoOfEmployees='$noofemployees'  and Dept='$department' and CompanyType='$companytype' and Designation='$designation' ");

                    }
                    elseif (!empty($department) and !empty($companytype) and !empty($noofemployees) and !empty($location)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  and SubLocation='$location' and NoOfEmployees='$noofemployees'  and Dept='$department' and CompanyType='$companytype' ");
                    }
                    elseif ( !empty($companytype) and !empty($noofemployees) and !empty($location)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  and SubLocation='$location' and NoOfEmployees='$noofemployees' and CompanyType='$companytype' ");
                    }
                    elseif ( !empty($noofemployees) and !empty($location)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  and SubLocation='$location' and NoOfEmployees='$noofemployees' ");
                    }
                    elseif (!empty($location)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  and SubLocation='$location'  ");
                    }
                    elseif (!empty($noofemployees)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  and NoOfEmployees='$noofemployees'   ");
                    }
                    elseif (!empty($department)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  and Dept='$department'  ");
                    }
                    elseif (!empty($designation)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  and Designation='$designation'  ");
                    }
                    elseif (!empty($companytype)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  and CompanyType='$companytype'  ");
                    }
                                        elseif (!empty($item)) 
                                        {
                                            $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'");
                                        }
                                        elseif($company)
                                        {
                                            $fetdetail=mysqli_query($dbc,"select * from `customers` where `Company`='$company'");

                                        }
                                        elseif ($prosadded) {
                                            if($prosadded == "Today")
                                            {
                                                $fetdetail=mysqli_query($dbc,"select * from `customers` where DATE(ModificationDetail)= DATE(CURRENT_DATE)");

                                            }
                                            elseif($prosadded == "ThisWeek")
                                            {
                                                $fetdetail=mysqli_query($dbc,"select * from `customers` where WEEKOFYEAR(ModificationDetail)=WEEKOFYEAR(NOW())");
                                            }
                                            elseif($prosadded == "ThisMonth")
                                            {
                                                $fetdetail=mysqli_query($dbc,"select * from `customers` where MONTH(ModificationDetail)= MONTH(CURRENT_DATE)");
                                            }
                                            elseif($prosadded == "ThisYear")
                                            {
                                                $fetdetail=mysqli_query($dbc,"select * from `customers` where YEAR(ModificationDetail)= YEAR(CURRENT_DATE)");
                                            }
                                            elseif($prosadded == "DateRange")
                                            {
                                                $fetdetail=mysqli_query($dbc,"select * from `customers` where DATE(ModificationDetail) Between '$fdate' and '$tdate' order by ModificationDetail ");
                                            }
                                            
                                        }
                    /*elseif (!empty($department1) or !empty($companytype1) or !empty($noofemployees1) or !empty($location1) )
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location'  or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");
                    }
                    elseif (empty($department1) or empty($companytype1) or empty($noofemployees1) or empty($location1) )
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location'  or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");
                    }
                    else
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location' or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");

                    }*/
                    
                    //$fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location' or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");

                      

                    
                              $count=0;
                        while($frow=mysqli_fetch_assoc($fetdetail))
                        {
                          global $count;
                          $count=$count+1;
                                                    $cid=$frow['cid'];
                          $company=$frow['Company'];
                                                    $sector=$frow['Sector'];
                                                    $firstname=$frow['FirstName'];
                                                    $lastname=$frow['LastName'];
                                                    $designation=$frow['Designation'];
                                                    $department=$frow['Dept'];
                                                    $mobile=$frow['Mobile'];
                                                    $mailid=$frow['Mail'];
                                                    $address=$frow['Address'];
                                                    $location=$frow['Location'];
                                                    $sublocation=$frow['SubLocation'];
                                                    $noofemployees=$frow['NoOfEmployees'];
                                                    $companytype=$frow['CompanyType'];
                          
                          
                            echo '<tr>';
                            echo "<td>".$count."</td>";
                                                        /*echo "<td>{$frow['Company']}</td>";*/
                                                        echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="customerprofile.php?c='.$company.'">Add Profile</a>
                                                                        <a href="custprofiledash.php?u='.$company.'&v='.$cid.'">View Profile</a>
                                                                        <a href="../auth/meeting/ins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'&fun=funnel&sect='.$sector.'&mob='.$mobile.'&deg='.$designation.'">Send To Funnel</a>
                                                                        
                                                                    </div>
                                                                </div></td>';
                                                        echo "<td>{$frow['Sector']}</td>";
                                                        echo "<td>{$frow['FirstName']} {$frow['LastName']}</td>";
                                                        echo "<td>{$frow['Designation']}</td>";
                                                        echo "<td>{$frow['Dept']}</td>";
                                                        echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                        echo "<td>{$frow['Mail']}</td>";
                                                        /*echo "<td>{$frow['Address']}</td>";*/
                                                        echo '<td> <span class="more">'.$address.'</span></td>';
                                                        echo "<td>{$frow['Location']}</td>";
                                                        echo "<td>{$frow['SubLocation']}</td>";
                                                        echo "<td>{$frow['NoOfEmployees']}</td>";
                                                        echo "<td>{$frow['CompanyType']}</td>";
                                                        echo "<td><a  href='custprofiledash.php?u=$company&v=$cid' class='text-muted text-uppercase btn btn-info'  ><font color='white'>Profile</font></a></td>";
                                                        echo "<td><a  href='editcust.php?u=$cid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        echo "<td><a  href='../auth/customer/delcust.php?u=$cid' class='delete text-muted text-uppercase btn btn-danger' ><font color='white'>Delete</font></a></td>";


                            
                          }
                        } 
                        
                      }
                      else
                      {
                        /*$fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location'  or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");*/
                        /*if(!empty($department1) and empty($companytype) and empty($noofemployees) and empty($location) )
                        {
                          $fetdetail=mysqli_query($dbc,"select * from `customers` ");
                        }*/

                        if(!empty($designation) and !empty($department) and !empty($companytype) and !empty($noofemployees) and !empty($location) )
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where  SubLocation='$location' and NoOfEmployees='$noofemployees'  and Dept='$department' and CompanyType='$companytype' and Designation='$designation' ");

                    }
                    elseif (!empty($department) and !empty($companytype) and !empty($noofemployees) and !empty($location)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where  SubLocation='$location' and NoOfEmployees='$noofemployees'  and Dept='$department' and CompanyType='$companytype' ");
                    }
                    elseif ( !empty($companytype) and !empty($noofemployees) and !empty($location)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where  SubLocation='$location' and NoOfEmployees='$noofemployees' and CompanyType='$companytype' ");
                    }
                    elseif ( !empty($noofemployees) and !empty($location)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where  SubLocation='$location' and NoOfEmployees='$noofemployees' ");
                    }
                    elseif (!empty($location)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where  SubLocation='$location'  ");
                    }
                    elseif (!empty($noofemployees)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where  NoOfEmployees='$noofemployees'   ");
                    }
                    elseif (!empty($department)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where  Dept='$department'  ");
                    }
                    elseif (!empty($designation)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where  Designation='$designation'  ");
                    }
                    elseif (!empty($companytype)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where  CompanyType='$companytype'  ");
                    }
                                        elseif($company)
                                        {
                                            $fetdetail=mysqli_query($dbc,"select * from `customers` where `Company`='$company'");

                                        }
                                        elseif ($prosadded) {
                                            if($prosadded == "Today")
                                            {
                                                $fetdetail=mysqli_query($dbc,"select * from `customers` where DATE(ModificationDetail)= DATE(CURRENT_DATE)");

                                            }
                                            elseif($prosadded == "ThisWeek")
                                            {
                                                $fetdetail=mysqli_query($dbc,"select * from `customers` where WEEKOFYEAR(ModificationDetail)=WEEKOFYEAR(NOW())");
                                            }
                                            elseif($prosadded == "ThisMonth")
                                            {
                                                $fetdetail=mysqli_query($dbc,"select * from `customers` where MONTH(ModificationDetail)= MONTH(CURRENT_DATE)");
                                            }
                                            elseif($prosadded == "ThisYear")
                                            {
                                                $fetdetail=mysqli_query($dbc,"select * from `customers` where YEAR(ModificationDetail)= YEAR(CURRENT_DATE)");
                                            }
                                            elseif($prosadded == "DateRange")
                                            {
                                                $fetdetail=mysqli_query($dbc,"select * from `customers` where DATE(ModificationDetail) Between '$fdate' and '$tdate' order by ModificationDetail ");
                                            }
                                        }
                      

                        $count=0;
                        while($frow=mysqli_fetch_assoc($fetdetail))
                        {
                          global $count;
                          $count=$count+1;
                                                    $cid=$frow['cid'];
                          $company=$frow['Company'];
                                                    $sector=$frow['Sector'];
                                                    $firstname=$frow['FirstName'];
                                                    $lastname=$frow['LastName'];
                                                    $designation=$frow['Designation'];
                                                    $department=$frow['Dept'];
                                                    $mobile=$frow['Mobile'];
                                                    $mailid=$frow['Mail'];
                                                    $address=$frow['Address'];
                                                    $location=$frow['Location'];
                                                    $sublocation=$frow['SubLocation'];
                                                    $noofemployees=$frow['NoOfEmployees'];
                                                    $companytype=$frow['CompanyType'];
                          
                          
                            echo '<tr>';
                            echo "<td>".$count."</td>";
                                                        /*echo "<td>{$frow['Company']}</td>";*/
                                                        echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="customerprofile.php?c='.$company.'">Add Profile</a>
                                                                        <a href="custprofiledash.php?u='.$company.'&v='.$cid.'">View Profile</a>
                                                                        
                                                                    </div>
                                                                </div></td>';
                                                        echo "<td>{$frow['Sector']}</td>";
                                                        echo "<td>{$frow['FirstName']} {$frow['LastName']}</td>";
                                                        echo "<td>{$frow['Designation']}</td>";
                                                        echo "<td>{$frow['Dept']}</td>";
                                                        echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                        echo "<td>{$frow['Mail']}</td>";
                                                        /*echo "<td>{$frow['Address']}</td>";*/
                                                        echo '<td> <span class="more">'.$address.'</span></td>';
                                                        echo "<td>{$frow['Location']}</td>";
                                                        echo "<td>{$frow['SubLocation']}</td>";
                                                        echo "<td>{$frow['NoOfEmployees']}</td>";
                                                        echo "<td>{$frow['CompanyType']}</td>";
                                                        echo "<td><a  href='custprofiledash.php?u=$company&v=$cid' class='text-muted text-uppercase btn btn-info'  ><font color='white'>Profile</font></a></td>";
                                                        echo "<td><a  href='editcust.php?u=$cid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        echo "<td><a  href='../auth/customer/delcust.php?u=$cid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";


                            }
                          } 

                      }
                    
                      echo '</tbody>';
                    echo '</table>';
                  ?>
          
          </div>
        </div>
        <!-- End Panel Basic -->
      </div>

  </div>
    <!-- End Page -->


     <!-- Footer -->
    <?php include "includes/footer.php"; ?>
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
      <script src="../../assets/global/vendor/jquery/jquery.js"></script>
      <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
      <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
      <script src="../../assets/global/vendor/animsition/animsition.js"></script>
      <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
      <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
      <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
      <script src="../../assets/global/vendor/waves/waves.js"></script>
      <!-- Plugins -->
      <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
      <script src="../../assets/global/vendor/switchery/switchery.js"></script>
      <script src="../../assets/global/vendor/intro-js/intro.js"></script>
      <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
      <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
      <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
      <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
      <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
      <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
      <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
      <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
      <!-- Scripts -->
      <script src="../../assets/global/js/Component.js"></script>
      <script src="../../assets/global/js/Plugin.js"></script>
      <script src="../../assets/global/js/Base.js"></script>
      <script src="../../assets/global/js/Config.js"></script>
      <script src="section/Menubar.js"></script>
      <script src="section/Sidebar.js"></script>
      <script src="section/PageAside.js"></script>
      <script src="section/GridMenu.js"></script>
      <!-- Config -->
      <script src="../../assets/global/js/config/colors.js"></script>
      <script src="../../assets/js/config/tour.js"></script>
      <script>Config.set('assets', '../assets');</script>
      <!-- Page -->
      <script src="../../assets/js/Site.js"></script>
      <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
      <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
      <script src="../../assets/global/js/Plugin/switchery.js"></script>
      <script src="../../assets/global/js/Plugin/matchheight.js"></script>
      <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
      <script src="../../assets/global/js/Plugin/peity.js"></script>
      <script src="../../assets/examples/js/dashboard/v1.js"></script>
      
      <?php include "includes/js/select.php"; ?>
      <?php include "includes/js/dynamicaddition.php"; ?>

      <?php include "includes/js/datepicker.php"; ?>
      <script src="../../assets/global/js/Plugin/datatables.js"></script>
    
        <script src="../../assets/examples/js/tables/datatable.js"></script>



        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script >
          $(document).ready(function() {
    $('#example').DataTable();
} );
        </script>

        <!-- <script src="../../assets/examples/js/forms/advanced.css"></script>
<script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script src="../../assets/global/js/Plugin/bootstrap-datepicker.js"></script> -->


    
  </body>
</html>



