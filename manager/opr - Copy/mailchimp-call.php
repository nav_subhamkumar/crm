<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Telecalling Campaign | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>

    <link rel='stylesheet' href="../../assets/css/customised-crm.css" >
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
    
  </head>
  <body class="animsition site-navbar-small dashboard">
    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <!-- write body content here -->
<div class="page-content">
  <div class="panel">
               <div class="panel-body container-fluid">
                  <div class="row row-lg">
                     <div class="col-md-12">
                        <div class="example-wrap">
                           <h4 class="example-title">Telecalling Campaign</h4>
                           <div class="example">
                              <form action="" method="get" enctype="multipart/form-data" autocomplete="off" novalidate="novalidate">
                                 <div class="row">
                                    <div class="form-group  col-md-6">
                                       <label class="form-control-label" >Projects</label>
                  
                            
                                 <select name="project" class="form-control maxlength" data-plugin="select2">
                                 <?php
                                  $project=mysqli_query($dbc,"select * from `campaign-projects`  ");
                                  echo '<option value="" >Select</option>';
                                  while($row=mysqli_fetch_assoc($project))
                                  {
                                    //$pro=$row['ProjectName'];
                            
                                     echo '<option  value="'.$row['ProjectName'].'">'.$row['ProjectName'].'</option>';
                                    
                                  }
                                ?>
                                </select>
                              
                
                  <label class="form-control-label">Clicks</label>
              
                                 <select name="clicks" class="form-control maxlength" data-plugin="select2">
                                 <?php
                                                                $project=mysqli_query($dbc,"select distinct(Clicked) from `mailchimpdata`");
                                                                
                                                                while($row=mysqli_fetch_assoc($project))
                                                                {
                                                                    //$pro=$row['ProjectName'];
                                                            
                                                                     echo '<option  value="'.$row['Clicked'].'">'.$row['Clicked'].'</option>';
                                                            
                                                                }
                                                            ?>
                      
                                 </select>
        
                                  
                <!-- End Example Basic -->
              </div>
              <div class="form-group  col-md-6">
                  <label class="form-control-label" >Opens</label>
                  
              
                    <select name="opens" class="form-control maxlength" data-plugin="select2">
                              <?php
                                $project=mysqli_query($dbc,"select distinct(Opened) from `mailchimpdata`");
                                
                                while($row=mysqli_fetch_assoc($project))
                                {
                                  //$pro=$row['ProjectName'];
                              
                                   echo '<option  value="'.$row['Opened'].'">'.$row['Opened'].'</option>';
                              
                                }
                              ?>
                    </select>
              
              
                <!-- End Example Basic -->
              </div></div>
             
             <button type="submit" name="submit" class="btn-primary btn">Submit</button>
              <button type="submit" class="btn-default btn-pure btn">Cancel</button>
        </form>
        </div> 
      </div>
  </div></div></div></div></div>
<div class="page-content" style="margin-top: -40px">
<!-- Panel Basic -->
        <div class="panel">
          <header class="panel-heading">
            
            <h3 class="panel-title font-size-18">Customer List</h3>
          </header>
          <div class="panel-body">
           
              <?php
                    /*$fetdetail=mysqli_query($dbc,"select * from `$pro` where `Sector`='$item' or SubLocation='$location' or NoOfEmployees='$noofemployees' ");*/

                    echo '<table class="table table-striped table-responsive table-bordered" id="example">';
                      echo '<thead>';
                        echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Company</th>';
                                                    echo '<th>Sector</th>';
                                                    echo '<th>Contact-Person</th>';
                                                    echo '<th>Designation</th>';
                                                    echo '<th>Department</th>';
                                                    echo '<th>Mobile-No</th>';
                                                    echo '<th>Mail-ID</th>';
                                                    echo '<th>Address</th>';
                                                    echo '<th>Location</th>';
                                                    echo '<th>Sub-Location</th>';
                                                    echo '<th>No Of Employees</th>';
                                                    echo '<th>CompanyType</th>';
                                                    echo '<th>Profile</th>';
                                                    echo '<th>Update</th>';
                                                    echo '<th>Remove</th>';
                        echo '</tr>';
                      echo '</thead>';
                      echo '<tbody>';
                      if(isset($_GET['submit']))
                  {
                    
                    $sector=implode(',', $_GET['sector']);
                    $pro=$_GET['project'];
                    $opens=$_GET['opens'];
                    $clicks=$_GET['clicks'];

                                        $fetmail=mysqli_query($dbc,"select * from mailchimpdata where RMail='$id' and Opened='$opens' or Clicked='$clicks' ");
                                        while ($fm=mysqli_fetch_assoc($fetmail)) {
                                            $customermail=$fm['CustomerMail'];
                                        
                    
                                        $fetdetail=mysqli_query($dbc,"select distinct(Company),`Sector`,`SubSector`,`FirstName`,`LastName`,`Level`,`Dept`,`Designation`,`Mobile`,`Mail`,`ContactPerson2`,`ContactNumber`,`EmailID`,`Url`,`Address`,`Location`,`SubLocation`,`City`,`Pin`,`State`,`StdCode`,`LandlineNo`,`FaxNo`,`NoOfEmployees`,`CompanyType`,`GSTNo` from `$pro` where `RMail`='$id' and Mail='$customermail' ");
                                        
                    /*elseif (!empty($department1) or !empty($companytype1) or !empty($noofemployees1) or !empty($location1) )
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location'  or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");
                    }
                    elseif (empty($department1) or empty($companytype1) or empty($noofemployees1) or empty($location1) )
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location'  or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");
                    }
                    else
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location' or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");

                    }*/
                    
                    //$fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location' or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");


                    
                    $count=0;
                        while($frow=mysqli_fetch_assoc($fetdetail))
                        {
                          global $count;
                          $count=$count+1;
                                                    $cid=$frow['cid'];
                                                    $company=$frow['Company'];
                                                    $sector=$frow['Sector'];
                                                    $firstname=$frow['FirstName'];
                                                    $lastname=$frow['LastName'];
                                                    $designation=$frow['Designation'];
                                                    $department=$frow['Dept'];
                                                    $mobile=$frow['Mobile'];
                                                    $mailid=$frow['Mail'];
                                                    $address=$frow['Address'];
                                                    $location=$frow['Location'];
                                                    $sublocation=$frow['SubLocation'];
                                                    $noofemployees=$frow['NoOfEmployees'];
                                                    $companytype=$frow['CompanyType'];
                          
                          
                            echo '<tr>';
                            echo "<td>".$count."</td>";
                                                        /*echo "<td>{$frow['Company']}</td>";*/
                                                        echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                      <a href="calldetailins.php?c='.$company.'&f='.$firstnamename.'&l='.$lastname.'&e='.$mailid.'&ctype=Telecalling&t='.$pro.'" >Update Detail</a>
                                                                        <a href="customerprofile.php?c='.$company.'">Add Profile</a>
                                                                        <a href="custprofiledash.php?u='.$company.'&v='.$cid.'">View Profile</a>
                                                                        
                                                                    </div>
                                                                </div></td>';
                                                        echo "<td>{$frow['Sector']}</td>";
                                                        echo "<td>{$frow['FirstName']} {$frow['LastName']}</td>";
                                                        echo "<td>{$frow['Designation']}</td>";
                                                        echo "<td>{$frow['Dept']}</td>";
                                                        echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                        echo "<td>{$frow['Mail']}</td>";
                                                        /*echo "<td>{$frow['Address']}</td>";*/
                                                        echo '<td> <span class="more">'.$address.'</span></td>';
                                                        echo "<td>{$frow['Location']}</td>";
                                                        echo "<td>{$frow['SubLocation']}</td>";
                                                        echo "<td>{$frow['NoOfEmployees']}</td>";
                                                        echo "<td>{$frow['CompanyType']}</td>";
                                                        echo "<td><a  href='custprofiledash.php?u=$company&v=$cid' class='text-muted text-uppercase btn btn-info'  ><font color='white'>Profile</font></a></td>";
                                                        echo "<td><a  href='editcust.php?u=$cid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        echo "<td><a  href='../auth/customer/delcust.php?u=$cid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";


                            
                          }
                        } 
                      }
                                        
                      echo '</tbody>';
                    echo '</table>';
                  ?>
          
          </div>
        </div>
        <!-- End Panel Basic -->
      </div>

  </div>
    <!-- End Page -->


     <!-- Footer -->
    <?php include "includes/footer.php"; ?>


    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>

        <?php include "includes/css/select.php"; ?>
      <?php include "includes/js/select.php"; ?>


        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script >
          $(document).ready(function() {
    $('#example').DataTable();
} );
        </script>


    
  </body>
</html>
