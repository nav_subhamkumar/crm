<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Change Incentive | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    <!--  date picker  -->
    <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
    
  </head>
  <body class="animsition site-navbar-small dashboard">
    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <!-- write body content here -->
      <div class="page-content">

        <div class="row">
        <div class="col-xl-3 col-md-6">
            <!-- Widget Linearea One-->
            <button class="btn btn-primary" data-target="#exampleFormModal" data-toggle="modal" type="button">Add </button>
            
            <!-- End Widget Linearea One -->
          </div>
          
                    
                    <!-- Modal -->
                    <div class="modal fade" id="exampleFormModal" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <form class="modal-content" action="../auth/incentive/ins.php" method="post" autocomplete="off" >
                        
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFormModalLabel">Add Percentage</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              
                              <div class="col-xl-12 form-group">
                                <label class="form-control-label">Incentive Name</label>
                                <input type="text" class="form-control" name="incentivename"  required>
                              </div>
                              <div class="col-xl-12 form-group">
                                <label class="form-control-label">Criteria (Percent respect to Target)</label>
                                <input type="text" class="form-control" name="criteria"  required>
                              </div>
                              <div class="col-xl-12 form-group">
                                <label class="form-control-label">Incentive Percentage</label>
                                <input type="text" class="form-control" name="incentivepercentage"  required>
                              </div>
                              <div class="col-md-12 float-right">
                        <button class="btn btn-primary"  type="submit" name="incentive">Submit</button>
                       <button class="btn btn-default" data-dismiss="modal" type="reset" >Close</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <!-- End Modal -->
                </div>
                <br>
        <!-- Panel Basic -->
        <div class="panel">
          <header class="panel-heading">
            
            <h3 class="panel-title example-title">Incentive List</h3>
          </header>
          <div class="panel-body">
            
              <?php
                                        $fetdetail=mysqli_query($dbc,"select * from `incentive` ");



                                        echo '<table class="table table-striped table-responsive-md table-bordered example">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Incentive Name</th>';
                                                    echo '<th>Incentive Percentage</th>';
                                                    echo '<th>Criteria (Percentage >= Target)</th>';
                                                    echo '<th>Delete</th>';
                                                    
                                                    
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $cp=0;
                                                while($frow=mysqli_fetch_assoc($fetdetail))
                                                {
                                                    global $cp;
                                                    $cp=$cp+1;
                                                    $incid=$frow['id'];
                                                    $incame=$frow['IncentiveName'];
                                                    $incpercent=$frow['IncentivePercent'];
                                                    $criteria=$frow['Criteria'];
                                                   

                                                       
                                                        echo '<tr>';
                                                        echo "<td>".$cp."</td>";
                                                        echo "<td>{$frow['IncentiveName']}</td>";
                                                        echo "<td>{$frow['IncentivePercentage']}</td>";
                                                        echo "<td>{$frow['Criteria']}</td>";
                                                        
                                                       

                                                        echo "<td><a  href='../auth/incentive/del.php?u=$incid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";


                                                        
                                                        
                                                }
                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>
                                    
          
          </div>
        </div>
        <!-- End Panel Basic -->
      </div>
      
</div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>

    
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>
        <!-- date picker -->
        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="../../assets/global/js/Plugin/bootstrap-datepicker.js"></script>

        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script >
          $(document).ready(function() {
          $('.example').DataTable();
           } );
        </script>


    
  </body>
</html>
