<?php
   include "session_handler.php";
   ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="bootstrap material admin template">
      <meta name="author" content="">
      <title>Edit Customer | Bizapp CRM</title>
      <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
      <link rel="shortcut icon" href="../../assets/images/favicon.ico">
      <!-- Stylesheets -->
      <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
      <link rel="stylesheet" href="../../assets/css/site.min.css">
      <!-- Plugins -->
      <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
      <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
      <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
      <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
      <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
      <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
      <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
      <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
      <!-- Fonts -->
      <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
      <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
      <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- table set -->
      <link rel='stylesheet' href='../../assets/css/customised-crm.css'>
      <?php include "includes/css/select.php"; ?>
      
      <script src="../../assets/js/customised-crm.js"></script>
      <!--[if lt IE 9]>
      <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
      <![endif]-->
      <!--[if lt IE 10]>
      <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
      <script src="../../assets/global/vendor/respond/respond.min.js"></script>
      <![endif]-->
      <!-- Scripts -->
      <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
      <script>
         Breakpoints();
      </script>
   </head>
   <body class="animsition site-navbar-small dashboard">
      <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <?php include "navbar-header.php"; ?>
      <?php include "side-navigation.php"; ?>   
                                 <?php
                                          
                                        

                                            $c=$_GET['u'];
                                            
                                            $query=mysqli_query($dbc,"select * from customers where   `id`='$c'");
                                            while($row=mysqli_fetch_array($query))
                                            {
                                             $company=$row['Company'];
                                             $sector=$row['Sector'];
                                             $subsector=$row['SubSector'];
                                             $firstname=$row['FirstName'];
                                             $lastname=$row['LastName'];
                                             $level=$row['Level'];
                                             $department=$row['Dept'];
                                             $designation=$row['Designation'];
                                             $mobile=$row['Mobile'];
                                             $email=$row['Mail'];
                                             $url=$row['Url'];
                                             $address=$row['Address'];
                                             $location=$row['Location'];
                                             $city=$row['City'];
                                             $stdcode=$row['StdCode'];
                                             $state=$row['State'];
                                             $landlineno=$row['LandlineNo'];
                                             $faxno=$row['FaxNo'];
                                             $noofemployees=$row['NoOfEmployees'];
                                             $companytype=$row['CompanyType'];
                                             $gstno=$row['GSTNo'];

                                            }
                                 ?>
      <!-- Page -->
      <div class="page">
         <div class="page-content" >
            <!-- Panel Basic -->
           
            <!-- input customer details  start -->
            <div class="panel">
               <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                  <h4 class="example-title">Input Customer Details</h4>
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                     <li class="nav-item" role="presentation">
                        <a class="active nav-link" data-toggle="tab" href="#basic" aria-controls="basic" role="tab">Basic</a>
                     </li>
                     <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#advance" aria-controls="advance" role="tab">Advance</a>
                     </li>
                  </ul>
                  <!--  basic start -->
                  <div class="tab-content">
                     <div class="tab-pane active animation-slide-left" id="basic" role="tabpanel">
                        <div class="example">
                           <form action="../auth/customer/icmp.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              <h5 class="mb-lg">Company Information</h5>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Company Name</label>
                                    <input type="text" class="form-control" name="company" placeholder="Company Name" onkeypress="return blockSpecialChar(event)" required="required" value="<?php echo $company; ?>" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Sector</label>
                                    <select class="form-control" name="sector"  data-plugin="select2" required="required" >
                                    <?php
                                       $comp=$_GET['c'];
                                       $project=mysqli_query($dbc,"select distinct(SectorName) from `sector`  ");
                                       echo '<option value="<?php echo $sector; ?>"><?php echo $sector; ?></option>';
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                         echo '<option  value="'.$row['SectorName'].'">'.$row['SectorName'].'</option>';
                                                                                 
                                       }
                                       ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >City</label>
                                    <input type="text" class="form-control" name="city" placeholder="City"  required="required" value="<?php echo $city; ?>" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >State</label>
                                    <input type="text" class="form-control" name="state" placeholder="State"  required="required" value="<?php echo $state; ?>">
                                 </div>
                              </div>
                              <h5 class="mb-lg">Contact Information</h5>
                              <div class="controls-basic">
                                 <div role="form" class="entry">
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >First Name</label>
                                          <input type="text" class="form-control" name="firstname" id="w4-f" onKeyUp="fname()" placeholder="First Name" required="required" value="<?php echo $firstname; ?>">
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Last Name</label>
                                          <input type="text" class="form-control" name="lastname" id="w4-l" onKeyUp="lname()" placeholder="Last Name" required="required" value="<?php echo $lastname; ?>" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Email</label>
                                          <input type="email" class="form-control" name="email" placeholder="Email" required="required" value="<?php echo $email; ?>" >
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Mobile</label>
                                          <input type="text" class="form-control" name="mobile" id="w4-mob" placeholder="Mobile" onKeyUp="mob()" maxlength="12" required="required" value="<?php echo $mobile; ?>">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Designation</label>
                                          <input type="text" class="form-control" name="designation" placeholder="Designation" required="required" value="<?php echo $designation; ?>">
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button">
                                          <span class="icon md-plus" ></span>
                                          </button>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group ">
                                 <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                 <button type="reset"  class="btn btn-default">Reset</button>
                              </div>
                           </form>
                        </div>
                     </div>
                     <!--  basic end -->
                     <!--  advance start -->
                     <div class="tab-pane animation-slide-left" id="advance" role="tabpanel">
                        <div class="example">
                           <form action="../auth/customer/icmp.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              <h5 class="mb-lg">Company Information</h5>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Company Name</label>
                                    <input type="text" class="form-control" name="company" placeholder="Company Name" onkeypress="return blockSpecialChar(event)" required="required" value="<?php echo $company; ?>">
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Sector</label>
                                    <select class="form-control" name="sector"  data-plugin="select2" required="required" >
                                    <?php
                                       $comp=$_GET['c'];
                                       $project=mysqli_query($dbc,"select distinct(SectorName) from `sector`  ");
                                       echo '<option value="<?php echo $sector; ?>"><?php echo $sector; ?></option>';
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                         echo '<option  value="'.$row['SectorName'].'">'.$row['SectorName'].'</option>';
                                                                                 
                                       }
                                       ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >City</label>
                                    <input type="text" class="form-control" name="city" placeholder="City"  required="required" value="<?php echo $city; ?>" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >State</label>
                                    <input type="text" class="form-control" name="state" placeholder="State"  required="required" value="<?php echo $state; ?>" >
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Url</label>
                                    <input type="text" class="form-control" name="url" placeholder="URL" value="<?php echo $url; ?>" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Sub Sector</label>
                                    <input type="text" class="form-control" name="subsector" placeholder="Sub Sector" value="<?php echo $subsector; ?>" >
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Address</label>
                                    <input type="text" class="form-control" name="address" placeholder="Address" value="<?php echo $address; ?>" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Location</label>
                                    <input type="text" class="form-control" name="location" placeholder="Location" value="<?php echo $location; ?>" >
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Std Code</label>
                                    <input type="text" class="form-control" name="stdcode" id="w4-std" onKeyUp="stdc()" placeholder="Std Code" value="<?php echo $stdcode; ?>" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Landline No</label>
                                    <input type="text" class="form-control" name="landlineno" id="w4-land" onKeyUp="landl()" placeholder="Landline No" value="<?php echo $landlineno; ?>" >
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Fax No</label>
                                    <input type="text" class="form-control" name="faxno" placeholder="Fax No" value="<?php echo $faxno; ?>" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >No of Employees</label>
                                    <select class="form-control" name="noofemployees" data-plugin="select2" >
                                       <option value="<?php echo $noofemployees; ?>"><?php echo $noofemployees; ?></option>
                                       <option selected value="0-50">0-50</option>
                                       <option value="51-250">51-250</option>
                                       <option value="251-500">251-500</option>
                                       <option value="501-1000">501-1000</option>
                                       <option value="Above 1000">Above 1000</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Company Type</label>
                                    <select class="form-control" name="companytype" data-plugin="select2"  >
                                       <option value="<?php echo $companytype; ?>"><?php echo $companytype; ?></option>
                                       <option selected value="SMB">SMB</option>
                                       <option value="Enterprise">Enterprise</option>
                                       <option value="Large">Large</option>
                                    </select>
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >GST No</label>
                                    <input type="text" class="form-control" name="gstno" placeholder="GST No" value="<?php echo $gstno; ?>" >
                                 </div>
                              </div>
                              <h5 class="mb-lg">Contact Information</h5>
                              <div class="controls">
                                 <div role="form" class="entry">
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >First Name</label>
                                          <input type="text" class="form-control" name="firstname" id="w4-f" onKeyUp="fname()" placeholder="First Name" required="required" value="<?php echo $firstname; ?>" >
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Last Name</label>
                                          <input type="text" class="form-control" name="lastname" id="w4-l" onKeyUp="lname()" placeholder="Last Name" required="required" value="<?php echo $lastname; ?>" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Level</label>
                                          <input type="text" class="form-control" name="level" placeholder="Level" value="<?php echo $level; ?>" >
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Department</label>
                                          <input type="text" class="form-control" name="department" placeholder="Department" value="<?php echo $department; ?>" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Designation</label>
                                          <input type="text" class="form-control" name="designation" placeholder="Designation" value="<?php echo $designation; ?>" >
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Mobile</label>
                                          <input type="text" class="form-control" name="mobile" id="w4-mob" onKeyUp="mob()" placeholder="Mobile" maxlength="12" required="required" value="<?php echo $mobile; ?>" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Email</label>
                                          <input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $email; ?>" >
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add" type="button">
                                          <span class="icon md-plus" ></span>
                                          </button>
                                          </span>
                                       </div>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="form-group ">
                                       <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                       <button type="reset"  class="btn btn-default">Reset</button>
                                    </div>
                           </form>
                           </div>
                           </div>
                           <!--  advance end -->
                        </div>
                     </div>
                  </div>
                  <!-- input customer details end -->
                <!-- End Panel Basic -->
         </div>
      </div>
      <!-- End Page -->
      <!-- Footer -->
      <?php include "includes/footer.php"; ?>

      
      <!-- Core  -->
      <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
      <script src="../../assets/global/vendor/jquery/jquery.js"></script>
      <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
      <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
      <script src="../../assets/global/vendor/animsition/animsition.js"></script>
      <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
      <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
      <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
      <script src="../../assets/global/vendor/waves/waves.js"></script>
      <!-- Plugins -->
      <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
      <script src="../../assets/global/vendor/switchery/switchery.js"></script>
      <script src="../../assets/global/vendor/intro-js/intro.js"></script>
      <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
      <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
      <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
      <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
      <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
      <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
      <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
      <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
      <!-- Scripts -->
      <script src="../../assets/global/js/Component.js"></script>
      <script src="../../assets/global/js/Plugin.js"></script>
      <script src="../../assets/global/js/Base.js"></script>
      <script src="../../assets/global/js/Config.js"></script>
      <script src="section/Menubar.js"></script>
      <script src="section/Sidebar.js"></script>
      <script src="section/PageAside.js"></script>
      <script src="section/GridMenu.js"></script>
      <!-- Config -->
      <script src="../../assets/global/js/config/colors.js"></script>
      <script src="../../assets/js/config/tour.js"></script>
      <script>Config.set('assets', '../assets');</script>
      <!-- Page -->
      <script src="../../assets/js/Site.js"></script>
      <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
      <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
      <script src="../../assets/global/js/Plugin/switchery.js"></script>
      <script src="../../assets/global/js/Plugin/matchheight.js"></script>
      <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
      <script src="../../assets/global/js/Plugin/peity.js"></script>
      <script src="../../assets/examples/js/dashboard/v1.js"></script>
      
      <?php include "includes/js/select.php"; ?>
      <?php include "includes/js/dynamicaddition.php"; ?>
   </body>
</html>