<?php
   include "session_handler.php";
   ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="bootstrap material admin template">
      <meta name="author" content="">
      <title>Add Prospects | Bizapp CRM</title>
      <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
      <link rel="shortcut icon" href="../../assets/images/favicon.ico">
      <!-- Stylesheets -->
      <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
      <link rel="stylesheet" href="../../assets/css/site.min.css">
      <!-- Plugins -->
      <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
      <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
      <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
      <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
      <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
      <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
      <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
      <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
      <!-- Fonts -->
      <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
      <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
      <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- table set -->
      <link rel='stylesheet' href='../../assets/css/customised-crm.css'>
      <script src="../../assets/js/customised-crm.js"></script>

      <?php include "includes/css/select.php"; ?>

      <?php include "includes/css/datepicker.php"; ?>
      <!--[if lt IE 9]>
      <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
      <![endif]-->
      <!--[if lt IE 10]>
      <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
      <script src="../../assets/global/vendor/respond/respond.min.js"></script>
      <![endif]-->
      <!-- Scripts -->
      <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
      <script>
         Breakpoints();
      </script>
   </head>
   <body class="animsition site-navbar-small dashboard">
      <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <?php include "navbar-header.php"; ?>
      <?php include "side-navigation.php"; ?>   
      <!-- Page -->
      <div class="page">
         <div class="page-content" >
            <!-- Panel Basic -->
            
            <!-- input customer details  start -->
            <div class="panel">
               <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                  <h4 class="example-title">Input Customer Profile Details</h4>
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                     <li class="nav-item col-md-2" role="presentation">

                        <a class="active nav-link" data-toggle="tab" href="#infrastructure" aria-controls="infrastructure" role="tab"><i class="icon md-cloud" aria-hidden="true"></i>Infrastructure</a>
                     </li>
                     <li class="nav-item col-md-2" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#security" aria-controls="security" role="tab"><i class="icon md-shield-security" aria-hidden="true"></i>Security</a>
                     </li>
                     <li class="nav-item col-md-2" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#devtest" aria-controls="devtest" role="tab"><i class="icon md-developer-board" aria-hidden="true"></i>Development & Testing</a>
                     </li>
                     <li class="nav-item col-md-2" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#digtransfer" aria-controls="digtransfer" role="tab"><i class="icon md-transform" aria-hidden="true"></i>Digital Transformation</a>
                     </li>
                     <li class="nav-item col-md-2" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#analytics" aria-controls="analytics" role="tab"><i class="icon md-chart" aria-hidden="true"></i>Analytics</a>
                     </li>

                  </ul>
                  <!--  basic start -->
                  <div class="tab-content">
                     <div class="tab-pane active animation-slide-left" id="infrastructure" role="tabpanel">
                        <div class="example">
                           <form action="../auth/customer/icmpprof.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              
                              <div class="row">
                      <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="sector"  data-plugin="select2" required="required" >
                                    <?php

                                                                $comp=$_GET['c'];
                                                                    $project=mysqli_query($dbc,"select distinct(Company) from `customers`  ");
                                                                    /*echo '<option  value="'.$comp.'">'.$comp.'</option>';*/
                                                                    echo '<option  value="">Select</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                       
                                                                         echo '<option  value="'.$row['Company'].'">'.$row['Company'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                    </select>
                                 </div>
                               </div>
                              <div class="row">

                        
                        <label class="form-control-label col-md-12" style="font-weight: bold;">SERVER</label>
                      
                      </div>
                      <div class="row">
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >No of Server</label>
                            <input type="text" class="form-control" name="noofserver"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="serveroem"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Age</label>
                            <input type="text" class="form-control" name="serverage"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Configuration</label>
                        <input type="text" class="form-control" name="serverconfig"  >
                      </div>
                      </div>

                      <div class="row">
                        
                        <label class="form-control-label col-md-12" style="font-weight: bold;">STORAGE</label>
                      
                      </div>
                      <div class="row">
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Storage Type</label>
                            <select class="form-control" name="storagetype" data-plugin="select2"  >
                              
                              <option value="">Select</option>
                              <option value="SAN">SAN</option>
                              <option value="NAS">NAS</option>
                              <option value="Tape">Tape</option>
                              <option value="HardDisk">Hard Disk</option>
                              
                            
                            </select>
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="storageoem"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >No of Storage Box</label>
                            <input type="text" class="form-control" name="noofstoragebox"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Data Size</label>
                        <input type="text" class="form-control" name="storagedatasize"  >
                      </div>
                      </div>

                      <div class="row">
                        
                        <label class="form-control-label col-md-12" style="font-weight: bold;">OS</label>
                      
                      </div>
                      <div class="row">
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >OS Name</label>
                            <select class="form-control" name="osname" data-plugin="select2"  >
                              
                              <option value="">Select</option>
                              <option value="Windows">Windows</option>
                              <option value="Open Source">Open Source</option>
                              
                            
                            </select>
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >OS Detail</label>
                        <input type="text" class="form-control" name="osdetail"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >OEM</label>
                            <input type="text" class="form-control" name="osoem"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >OS Version</label>
                        <input type="text" class="form-control" name="osversion"  >
                      </div>
                      </div>

                      <div class="row">
                        
                        <label class="form-control-label col-md-12" style="font-weight: bold;">VIRTUALIZATION</label>
                      
                      </div>
                      <div class="row">
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >OEM</label>
                            <input type="text" name="virtualizationoem" class="form-control">
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Version</label>
                        <input type="text" class="form-control" name="virtualizationversion"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Renewal Date</label>
                            
                      
                      <input type="text" class="form-control" name="virtualizationrenewaldate" data-plugin="datepicker">
                      </div>
                      
                      </div>

                      <div class="row">
                        
                        <label class="form-control-label col-md-12" style="font-weight: bold;">BACKUP</label>
                      
                      </div>
                      <div class="row">
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Data Size</label>
                            <input type="text" name="backupdatasize" class="form-control">
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >OEM</label>
                            <input type="text" name="backupoem" class="form-control">
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Version</label>
                        <input type="text" class="form-control" name="backupversion"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Renewal Date</label>
                            
                      
                      <input type="text" class="form-control" name="backuprenewaldate" data-plugin="datepicker">
                      </div>
                      
                      </div>

                      <div class="row">
                        
                        <label class="form-control-label col-md-12" style="font-weight: bold;">CLOUD</label>
                      
                      </div>
                      <div class="row">
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >OEM</label>
                            <input type="text" name="cloudoem" class="form-control">
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Use Case</label>
                            <input type="text" name="cloudusecase" class="form-control">
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Monthly Billing</label>
                        <input type="text" class="form-control" name="cloudmonthlybilling"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Managed By</label>
                            
                      <input type="text" class="form-control" name="cloudmanagedby"  >
                      </div>
                      
                      </div>

                      

                      

                      <div class="row">
                        <label class="form-control-label col-md-12" style="font-weight: bold;">REMOTE DESKTOP</label>
                      </div>
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Using Remote Desktop</label>
                        <select class="form-control" name="usingremotedesktop" data-plugin="select2"  >
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="remotedesktopoem"  >
                      </div>
                      </div>

                      <div class="row">
                        <label class="form-control-label col-md-12" style="font-weight: bold;">MAIL</label>
                      </div>
                      <div class="row">
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >No of Users</label>
                            <input type="text" name="mailnoofusers" class="form-control">
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="mailoem"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Renewal Date</label>
                      <input type="text" class="form-control" name="mailrenewaldate" data-plugin="datepicker">
                      </div>
                      </div>

                      <div class="row">
                        <label class="form-control-label col-md-12" style="font-weight: bold;">AD</label>
                      </div>
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Using AD</label>
                        <select class="form-control" name="usingad" data-plugin="select2"  >
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="adoem"  >
                      </div>
                      </div>

                      

                      <div class="row">
                        <label class="form-control-label col-md-12" style="font-weight: bold;">ISO 27001</label>
                      </div>
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Status</label>
                        <select class="form-control" name="iso27001status" data-plugin="select2"  >
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Plan</label>
                        <input type="text" class="form-control" name="iso27001plan"  >
                      </div>
                      </div>
                                 
                              
                              <div class="form-group ">
                                 <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                 <button type="reset"  class="btn btn-default">Reset</button>
                              </div>
                           </form>
                        </div>
                     </div>
                     <!--  infrastructure end -->
                     <!--  security start -->
                     <div class="tab-pane animation-slide-left" id="security" role="tabpanel">
                        <div class="example">
                           <form action="../auth/customer/icmpprof.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              
                              <div class="row">
                              <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="sector"  data-plugin="select2" required="required" >
                                    <?php

                                                                $comp=$_GET['c'];
                                                                    $project=mysqli_query($dbc,"select distinct(Company) from `customers`  ");
                                                                    /*echo '<option  value="'.$comp.'">'.$comp.'</option>';*/
                                                                    echo '<option  value="">Select</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                       
                                                                         echo '<option  value="'.$row['Company'].'">'.$row['Company'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                    </select>
                                 </div>
                                  </div>

                                  <div class="row">
                        
                        <label class="form-control-label col-md-12" style="font-weight: bold;">ENDPOINT MANAGEMENT</label>
                      
                      </div>
                      <div class="row">
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >No of Endpoints</label>
                            <input type="text" name="noofendpoints" class="form-control">
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="endpointoem"  >
                      </div>
                      <div class="form-group col-md-3">
                        <label class="form-control-label" >Renewal Date</label>
                            
                      
                      <input type="text" class="form-control" name="endpointrenewaldate" data-plugin="datepicker">
                      </div>
                      
                      </div>

                      <div class="row">
                        
                        <label class="form-control-label col-md-12" style="font-weight: bold;">FIREWALL</label>
                      
                      </div>
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Age</label>
                            <input type="text" name="firewallage" class="form-control">
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >OEM</label>
                            <input type="text" name="firewalloem" class="form-control">
                      </div>

                    </div>
                    <div class="row">
                        
                        <label class="form-control-label col-md-12" style="font-weight: bold;">DLP</label>
                      
                      </div>
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="dlpoem"  >
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Renewal Date</label>
                            
                      
                      <input type="text" class="form-control" name="dlprenewaldate" data-plugin="datepicker">
                      </div>
                      
                      </div>

                      <div class="row">
                        
                        <label class="form-control-label col-md-12" style="font-weight: bold;">MOBILE SECURITY</label>
                      
                      </div>
                      <div class="row">

                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Using Security</label>
                            
                      
                      <select class="form-control" name="mobilesecurity" data-plugin="select2"  >
                              
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                              
                              
                            </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="mobilesecurityoem"  >
                      </div>

                      
                      </div>

                      <div class="row">
                        <label class="form-control-label col-md-12" style="font-weight: bold;">VAPT</label>
                      </div>
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Status</label>
                        <select class="form-control" name="vaptstatus" data-plugin="select2"  >
                              <option value="">Select</option>
                              <option value="Done">Done</option>
                              <option value="Not Done">Not Done</option>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Plan</label>
                        <input type="text" class="form-control" name="vaptplan"  >
                      </div>
                      </div>
                      
                                    <div class="form-group ">
                                       <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                       <button type="reset"  class="btn btn-default">Reset</button>
                                    </div>
                                  
                           </form>
                           </div>
                           </div>
                           <!--  security end -->
                           <!--  development and testing start -->
                     <div class="tab-pane animation-slide-left" id="devtest" role="tabpanel">
                        <div class="example">
                           <form action="../auth/customer/icmpprof.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              
                              <div class="row">
                              <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="sector"  data-plugin="select2" required="required" >
                                    <?php

                                                                $comp=$_GET['c'];
                                                                    $project=mysqli_query($dbc,"select distinct(Company) from `customers`  ");
                                                                    /*echo '<option  value="'.$comp.'">'.$comp.'</option>';*/
                                                                    echo '<option  value="">Select</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                       
                                                                         echo '<option  value="'.$row['Company'].'">'.$row['Company'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                    </select>
                                 </div>
                                  </div>
                              
                                    
                                    
                                    <div class="form-group ">
                                       <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                       <button type="reset"  class="btn btn-default">Reset</button>
                                    </div>
                                  
                           </form>
                           </div>
                           </div>
                           <!--  development and testing end -->
                           <!--  digital transformaton start -->
                     <div class="tab-pane animation-slide-left" id="digtransfer" role="tabpanel">
                        <div class="example">
                           <form action="../auth/customer/icmpprof.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              
                              <div class="row">
                              <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="sector"  data-plugin="select2" required="required" >
                                    <?php

                                                                $comp=$_GET['c'];
                                                                    $project=mysqli_query($dbc,"select distinct(Company) from `customers`  ");
                                                                    /*echo '<option  value="'.$comp.'">'.$comp.'</option>';*/
                                                                    echo '<option  value="">Select</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                       
                                                                         echo '<option  value="'.$row['Company'].'">'.$row['Company'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                    </select>
                                 </div>
                                  </div>

                                  <div class="row">
                        <label class="form-control-label col-md-12" style="font-weight: bold;">CRM</label>
                      </div>
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Using CRM</label>
                        <select class="form-control" name="usingcrm" data-plugin="select2"  >
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="crmoem"  >
                      </div>
                      </div>

                      <div class="row">
                        <label class="form-control-label col-md-12" style="font-weight: bold;">HELP DESK</label>
                      </div>
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Using Help Desk</label>
                        <select class="form-control" name="usinghelpdesk" data-plugin="select2"  >
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="helpdeskoem"  >
                      </div>
                      </div>

                      <div class="row">
                        <label class="form-control-label col-md-12" style="font-weight: bold;">ERP</label>
                      </div>
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Using ERP</label>
                        <select class="form-control" name="usingerp" data-plugin="select2"  >
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="erpoem"  >
                      </div>
                      </div>

                      <div class="row">
                        <label class="form-control-label col-md-12" style="font-weight: bold;">AUTOMATION</label>
                      </div>
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Using Automation</label>
                        <select class="form-control" name="usingautomation" data-plugin="select2"  >
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="automationoem"  >
                      </div>
                      </div>

                      <div class="row">
                        <label class="form-control-label col-md-12" style="font-weight: bold;">ITIL</label>
                      </div>
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Status</label>
                        <select class="form-control" name="itilstatus" data-plugin="select2"  >
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Plan</label>
                        <input type="text" class="form-control" name="itilplan"  >
                      </div>
                      </div>
                              
                                   
                                    <div class="form-group ">
                                       <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                       <button type="reset"  class="btn btn-default">Reset</button>
                                    </div>
                                  
                           </form>
                           </div>
                           </div>
                           <!--  digital transformation end -->
                           <!--  analytics start -->
                     <div class="tab-pane animation-slide-left" id="analytics" role="tabpanel">
                        <div class="example">
                           <form action="../auth/customer/icmpprof.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              
                              <div class="row">
                              <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="sector"  data-plugin="select2" required="required" >
                                    <?php

                                                                $comp=$_GET['c'];
                                                                    $project=mysqli_query($dbc,"select distinct(Company) from `customers`  ");
                                                                    /*echo '<option  value="'.$comp.'">'.$comp.'</option>';*/
                                                                    echo '<option  value="">Select</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                       
                                                                         echo '<option  value="'.$row['Company'].'">'.$row['Company'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                    </select>
                                 </div>
                                  </div>
                                  <div class="row">
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >Using Tool</label>
                        <select class="form-control" name="usinganalyticstool" data-plugin="select2"  >
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label class="form-control-label" >OEM</label>
                        <input type="text" class="form-control" name="analyticstooloem"  >
                      </div>
                      </div>
                              
                                    <div class="form-group ">
                                       <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                       <button type="reset"  class="btn btn-default">Reset</button>
                                    </div>
                           </form>
                           </div>
                           </div>
                           <!--  analytics end -->
                        </div>
                     </div>
                  </div>
                  <!-- input customer details end -->
                <!-- End Panel Basic -->
         </div>
      </div>
      <!-- End Page -->
      <!-- Footer -->
      <?php include "includes/footer.php"; ?>
      <!-- Core  -->
      <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
      <script src="../../assets/global/vendor/jquery/jquery.js"></script>
      <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
      <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
      <script src="../../assets/global/vendor/animsition/animsition.js"></script>
      <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
      <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
      <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
      <script src="../../assets/global/vendor/waves/waves.js"></script>
      <!-- Plugins -->
      <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
      <script src="../../assets/global/vendor/switchery/switchery.js"></script>
      <script src="../../assets/global/vendor/intro-js/intro.js"></script>
      <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
      <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
      <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
      <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
      <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
      <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
      <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
      <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
      <!-- Scripts -->
      <script src="../../assets/global/js/Component.js"></script>
      <script src="../../assets/global/js/Plugin.js"></script>
      <script src="../../assets/global/js/Base.js"></script>
      <script src="../../assets/global/js/Config.js"></script>
      <script src="section/Menubar.js"></script>
      <script src="section/Sidebar.js"></script>
      <script src="section/PageAside.js"></script>
      <script src="section/GridMenu.js"></script>
      <!-- Config -->
      <script src="../../assets/global/js/config/colors.js"></script>
      <script src="../../assets/js/config/tour.js"></script>
      <script>Config.set('assets', '../assets');</script>
      <!-- Page -->
      <script src="../../assets/js/Site.js"></script>
      <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
      <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
      <script src="../../assets/global/js/Plugin/switchery.js"></script>
      <script src="../../assets/global/js/Plugin/matchheight.js"></script>
      <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
      <script src="../../assets/global/js/Plugin/peity.js"></script>
      <script src="../../assets/examples/js/dashboard/v1.js"></script>
      
      <?php include "includes/js/select.php"; ?>
      <?php include "includes/js/dynamicaddition.php"; ?>

      <?php include "includes/js/datepicker.php"; ?>


   </body>
</html>