<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Dashboard | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
<link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v2.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  


  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>    
    <?php include "side-navigation.php"; ?>   
    <?php 
                        $res=mysqli_query($dbc,"select * from `team` where `email`='$id' ");
                        while($r=mysqli_fetch_assoc($res))
                        {
                            $empid=$r['empid'];
                            $name=$r['name'];
                            $email=$r['email'];
                            $password=$r['password'];
                            $contact=$r['contact'];
                            $designation=$r['desig'];
                            $doj=$r['doj'];
                            $employeetype=$r['EmployeeType'];
                            $salescertification=$r['SalesCertification'];
                            $technicalcertification=$r['TechnicalCertification'];
                            $demo=$r['Demo'];
                            $poc=$r['POC'];

                        }

                        

                    ?>


    <!-- Page -->
    <div class="page">
      <!-- write body content here -->

      <div class="page-content container-fluid">
        <div class="row">
          <div class="col-lg-3">
            <!-- Page Widget -->
            <div class="card card-shadow text-center">
              <div class="card-block">
                <div class="thumb-info mb-md">
                 <div  data-target="#ProfilePicture" data-toggle="modal"><img height="200" width="200" src="<?php if(!empty($profileimage)){ echo '../../uploadfiles/profileimage/'.$profileimage; }else{ echo '../assets/images/!logged-user.jpg'; }  ?>" class="rounded img-responsive" alt="<?php echo $name; ?>"></div>
                                        <div class="thumb-info-title">
                                      <h3><span class="thumb-info-inner"><?php echo $name; ?></span></h3><br>
                                      <h4><span class="thumb-info-type"><?php echo $role;  ?></span></h4>
                                      <h4><span class="thumb-info-inner">Employee ID: <?php echo $empid; ?></span></h4>
                                        </div>
                                    </div>
                <div class="profile-social">
                  <a class="icon bd-twitter" href="javascript:void(0)"></a>
                  <a class="icon bd-facebook" href="javascript:void(0)"></a>
                  <a class="icon bd-dribbble" href="javascript:void(0)"></a>
                  <a class="icon bd-github" href="javascript:void(0)"></a>
                </div>
                <button type="button" class="btn btn-primary">Follow</button>
              </div>
              
            </div>
            <!-- End Page Widget -->

                    <!-- Modal -->
                    <div class="modal fade" id="ProfilePicture" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <form class="modal-content" action="../auth/profile/upload.php" method="post" enctype="multipart/form-data">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFormModalLabel">Upload Your Image</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="form-group">
                                <input type="file" class="form-control" name="uplfile" >
                              </div>
                              
                              <div class="col-md-12 float-right">
                          <button class="btn btn-primary"  name="submit" type="submit">Upload</button>
                          </div></div></div></form></div></div></div>

          <div class="col-lg-9">
            <!-- Panel -->
            <div class="panel">
              <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                  <li class="nav-item" role="presentation"><a class="active nav-link" data-toggle="tab" href="#overview"
                      aria-controls="activities" role="tab">Overview </a></li>
                  <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#skill" aria-controls="profile"role="tab">Skill</a></li>
                  <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#personalstats" aria-controls="settings" role="tab">Personal Stats</a></li>
                  
                </ul>

                <div class="tab-content">
                  <div class="tab-pane active animation-slide-left" id="overview" role="tabpanel">
                   <div class="panel">

          <h3 class="panel-title font-size-18">Add An Employee</h3>

          <form action="../auth/profile/ins.php" method="post"> 

          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-6 col-xl-6">
                <!-- Example Basic -->
                <div class="example-wrap m-sm-0">
                  <h4 class="example-title">Full Name</h4>
                  
                  <div class="example">
                    <input type="text" class=" form-control" value="<?php echo $name; ?>" data-plugin="maxlength" data-placement="bottom-right-inside" name="name" >
                  </div>
                </div>
                <div class="example-wrap m-sm-0">
                  <h4 class="example-title">Password</h4>
                  
                  <div class="example">
                    <input type="Password" name="password" class=" form-control" data-plugin="maxlength"
                      data-placement="bottom-right-inside" value="<?php echo $password; ?>" >
                  </div>
                </div>
                <div class="example-wrap m-sm-0">
                  <h4 class="example-title">Confirm Password</h4>
                  
                  <div class="example">
                    <input type="Password" name="cpassword" class="form-control" data-plugin="maxlength"
                      data-placement="bottom-right-inside" value="<?php echo $password; ?>" >
                  </div>
                </div>
                <div class="example-wrap m-sm-0">
                  <h4 class="example-title">Date of Birth</h4>
                  
                  <div class="example">
                    
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="icon wb-calendar" aria-hidden="true"></i>
                      </span>
                      <input type="text" class="form-control datepair-date datepair-end date" date-plugin="maxlength"  name="dob" data-plugin="datepicker" value="<?php echo $doj; ?>" disabled>
                    </div>
                  </div>
                <!-- End Example Basic -->
              </div>
            </div>
              
              <div class="col-md-6 col-xl-6">
                <!-- Example Basic -->
                <div class="example-wrap m-sm-0">
                  <h4 class="example-title">Contact No</h4>
                  
                  <div class="example">
                    <input type="value" name="contactno" class=" form-control" data-plugin="maxlength" value="<?php echo $contact; ?>" data-placement="bottom-right-inside" maxlength="10">

                  </div>
                </div>
                <div class="example-wrap m-sm-0">
                  <h4 class="example-title">Email</h4>
                  
                  <div class="example">
                    <input type="email" name="email" class=" form-control" data-plugin="maxlength" value="<?php echo $email; ?>" data-placement="bottom-right-inside" disabled>
                    <input type="hidden" name="email" value="<?php echo $email; ?>">
                  </div>
                </div>
                <div class="example-wrap m-sm-0">
                  <h4 class="example-title">Date of Joining</h4>
                  
                  <div class="example">
                
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="icon wb-calendar" aria-hidden="true"></i>
                      </span>
                      <input type="text" class="form-control datepair-date datepair-end date"  name="doj" data-plugin="datepicker" value="<?php echo $doj; ?>" disabled >
                    </div>
                  </div>
                </div>
                
 
                <!-- End Example Basic -->
              </div>
              <button type="submit" name="submit" class="btn-primary btn">Update</button>
              <button type="submit" class="btn-default btn-pure btn">Cancel</button>
              </div>
             </div></form>
          </div>
        
                  </div>

                  <div class="tab-pane animation-slide-left" id="skill" role="tabpanel">
                    
                    
                  </div>
                  
                  <div class="tab-pane animation-slide-left" id="personalstats" role="tabpanel">
                    <h4>Personal Stats</h4>
                   <div class="row" data-plugin="matchHeight" data-by-row="true">
                    
          <div class="col-xl-3 col-md-6">
            <!-- Widget Linearea One-->
            <div class="card card-shadow">
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-account grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Sales Certifications
                  </div><br>
                  <span class="grey-700 font-size-30 float-left">1,253</span>
                </div>
                
                
              </div>
            </div>
            <!-- End Widget Linearea One -->
          </div>
          <div class="col-xl-3 col-md-6">
            <!-- Widget Linearea Two -->
            <div class="card card-shadow">
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-flash grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Technical Certifications 
                  </div><br>
                  <span class="grey-700 font-size-30 float-left">2,425</span>
                </div>
                
              </div>
            </div>
            <!-- End Widget Linearea Two -->
          </div>
          <div class="col-xl-3 col-md-6">
            <!-- Widget Linearea Three -->
            <div class="card card-shadow">
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Demo
                  </div><br>
                  <span class="grey-700 font-size-30 float-left">1,864</span>
                </div>
                
              </div>
            </div>
            <!-- End Widget Linearea Three -->
          </div>
          <div class="col-xl-3 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" >
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-view-list grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    POC
                  </div><br>
                  <span class="grey-700 font-size-30 float-left" >845</span>
                </div>
                
              </div>
            </div>
            <!-- End Widget Linearea Four -->
          </div>
        </div>
              </div>
            </div>
            <!-- End Panel -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>    
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>

        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="../../assets/global/vendor/timepicker/jquery.timepicker.min.js"></script>
        <script src="../../assets/global/vendor/datepair/datepair.min.js"></script>
        <script src="../../assets/global/vendor/datepair/jquery.datepair.min.js"></script>


        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script >
          $(document).ready(function() {
    $('#example').DataTable();
} );
        </script>


    
  </body>
</html>
