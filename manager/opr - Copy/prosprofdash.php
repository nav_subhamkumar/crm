<?php
   include "session_handler.php";
   ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="bootstrap material admin template">
      <meta name="author" content="">
      <title>Prospects Profile Dashboard | Bizapp CRM</title>
      <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
      <link rel="shortcut icon" href="../../assets/images/favicon.ico">
      <!-- Stylesheets -->
      <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    <!-- <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'> -->
    <?php include "includes/css/tables.php"; ?>
      <!-- <script src="../../assets/js/customised-crm.js"></script> -->
      <!--[if lt IE 9]>
      <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
      <![endif]-->
      <!--[if lt IE 10]>
      <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
      <script src="../../assets/global/vendor/respond/respond.min.js"></script>
      <![endif]-->
      <!-- Scripts -->
      <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
      <script>
         Breakpoints();
      </script>
   </head>
   <body class="animsition site-navbar-small dashboard">
      <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <?php include "navbar-header.php"; ?>
      <?php include "side-navigation.php"; ?>   
      <!-- Page -->
      <div class="page">
         <div class="page-content" >
            <!-- Panel Basic -->
            <!-- input customer details  start -->
            <div class="panel">
               <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                  <h4 class="example-title">Customer Profile Details</h4>
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                     <li class="nav-item col-md-2" role="presentation">
                        <a class="active nav-link" data-toggle="tab" href="#infrastructure" aria-controls="infrastructure" role="tab"><i class="icon md-cloud" aria-hidden="true"></i>Infrastructure</a>
                     </li>
                     <li class="nav-item col-md-2" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#security" aria-controls="security" role="tab"><i class="icon md-shield-security" aria-hidden="true"></i>Security</a>
                     </li>
                     <li class="nav-item col-md-2" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#devtest" aria-controls="devtest" role="tab"><i class="icon md-developer-board" aria-hidden="true"></i>Development & Testing</a>
                     </li>
                     <li class="nav-item col-md-2" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#digtransfer" aria-controls="digtransfer" role="tab"><i class="icon md-transform" aria-hidden="true"></i>Digital Transformation</a>
                     </li>
                     <li class="nav-item col-md-2" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#analytics" aria-controls="analytics" role="tab"><i class="icon md-chart" aria-hidden="true"></i>Analytics</a>
                     </li>
                  </ul>
                  <!--  basic start -->
                  <div class="tab-content">
                     <div class="tab-pane active animation-slide-left" id="infrastructure" role="tabpanel">
                        <div class="example">
                           <?php
                              $cname=$_GET['u'];
                              $cid=$_GET['v'];
                                  /*$fetdetail=mysqli_query($dbc,"select * from `customersprofile` where `Company`='$cname' ");*/
                                  $fetdetail=mysqli_query($dbc,"select * from `customersprofile`  ");
                              
                                  /*echo '<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                  echo '<table  class="table table-striped dataTable table-responsive table-bordered "  data-plugin="dataTable">';

                                      echo '<thead>';
                                          echo '<tr>';
                                              echo '<th></th>';
                                              echo '<th></th>';
                                              echo '<th colspan="4">Server</th>';
                                              echo '<th colspan="4">Storage</th>';
                                              echo '<th colspan="4">OS</th>';
                                              echo '<th colspan="3">Virtualization</th>';
                                              echo '<th colspan="4">Backup</th>';
                                              echo '<th colspan="4">Cloud</th>';
                                              /*security*/
                                              echo '<th colspan="3">Endpoint Management</th>';
                                              echo '<th colspan="2">Firewall</th>';
                                              echo '<th colspan="2">DLP</th>';
                                              echo '<th colspan="2">Mobile Security</th>';
                                              echo '<th colspan="2">VAPT</th>';
                                              /*dig*/
                                              echo '<th colspan="2">Remote Desktop</th>';
                                              echo '<th colspan="2">Help Desk</th>';
                                              echo '<th colspan="2">ERP</th>';
                                              echo '<th colspan="2">CRM</th>';
                                              echo '<th colspan="2">Automation</th>';
                                              echo '<th colspan="3">Mail</th>';
                                              echo '<th colspan="2">AD</th>';
                                              echo '<th colspan="2">Analytics Tool</th>';
                                              echo '<th colspan="2">ISO 27001</th>';
                                              echo '<th colspan="2">ITIL</th>';
                                              
                                              echo '<th>Update</th>';
                                              
                                          echo '</tr>';
                                          echo '<tr>';
                                              echo '<th>Sl No.</th>';
                                              echo '<th>Company</th>';
                              
                                              echo '<th>No of Server</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Age</th>';
                                              echo '<th>Configuration</th>';
                              
                                              echo '<th>Storage Type</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>No of Storage Box</th>';
                                              echo '<th>Data Size</th>';
                              
                                              echo '<th>OS Name</th>';
                                              echo '<th>OS Detail</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>OS Version</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>Version</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Data Size</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Version</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>Use Case</th>';
                                              echo '<th>Monthly Billing</th>';
                                              echo '<th>Managed By</th>';
                              
                                              /*security*/
                              
                                              echo '<th>No of Endpoints</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Age</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Renewal Date</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Using Remote Desktop</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Help Desk</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using ERP</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using CRM</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Automation</th>';
                                              echo '<th>OEM</th>';
                                              
                                              echo '<th>No of Users</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Using AD</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Tool</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Update</th>';
                                              
                                          echo '</tr>';
                                      echo '</thead>';
                                      echo '<tbody>';

                                      $count=0;
                                          while($frow=mysqli_fetch_assoc($fetdetail))
                                          { 
                                            ?>
                                            <?php
                                              global $count;
                                              $count=$count+1;
                                              $ccid=$frow['id'];
                                              $cpid=$frow['cid'];
                                              $company=$frow['Company'];
                                              $noofserver=$frow['NoOfServer'];
                                              $serveroem=$frow['ServerOEM'];
                                              $serverage=$frow['ServerAge'];
                                              $serverconfig=$frow['ServerConfig'];
                                              $storagetype=$frow['StorageType'];
                                              $storageoem=$frow['StorageOEM'];
                                              $noofstoragebox=$frow['NoOfStorageBox'];
                                              $storagedatasize=$frow['StorageDataSize'];
                                              $osname=$frow['OSName'];
                                              $osdetail=$frow['OSDetail'];
                                              $osoem=$frow['OSOEM'];
                                              $osversion=$frow['OSVersion'];
                                              $virtualizationoem=$frow['VirtualizationOEM'];
                                              $virtualizationversion=$frow['VirtualizationVersion'];
                                              $virtualizationrenewaldate=$frow['VirtualizationRenewalDate'];
                                              $backupdatasize=$frow['BackupDataSize'];
                                              $backupoem=$frow['BackupOEM'];
                                              $backupversion=$frow['BackupVersion'];
                                              $backuprenewaldate=$frow['BackupRenewalDate'];
                                              $cloudoem=$frow['CloudOEM'];
                                              $cloudusecase=$frow['CloudUseCase'];
                                              $cloudmonthlybilling=$frow['CloudMonthlyBilling'];
                                              $cloudmanagedby=$frow['CloudManagedBy'];
                                              $noofendpoints=$frow['NoOfEndpoints'];
                                              $endpointoem=$frow['EndpointOEM'];
                                              $endpointrenewaldate=$frow['EndpointRenewalDate'];
                                              $firewallage=$frow['FirewallAge'];
                                              $firewalloem=$frow['FirewallOEM'];
                                              $dlprenewaldate=$frow['DLPRenewalDate'];
                                              $dlpoem=$frow['DLPOEM'];
                                              $mobilesecurity=$frow['MobileSecurity'];
                                              $mobilesecurityoem=$frow['MobileSecurityOEM'];
                                              $vaptstatus=$frow['VAPTStatus'];
                                              $vaptplan=$frow['VAPTPlan'];
                                              $usingremotedesktop=$frow['UsingRemoteDesktop'];
                                              $remotedesktopoem=$frow['RemoteDesktopOEM'];
                                              $usinghelpdesk=$frow['UsingHelpDesk'];
                                              $helpdeskoem=$frow['HelpDeskOEM'];
                                              $usingerp=$frow['UsingERP'];
                                              $erpoem=$frow['ERPOEM'];
                                              $usingcrm=$frow['UsingCRM'];
                                              $crmoem=$frow['CRMOEM'];
                                              $usingautomation=$frow['UsingAutomation'];
                                              $automationoem=$frow['AutomationOEM'];
                                              $mailnoofusers=$frow['MailNoOfUsers'];
                                              $mailoem=$frow['MailOEM'];
                                              $mailrenewaldate=$frow['MailRenewalDate'];
                                              $usingad=$frow['UsingAD'];
                                              $adoem=$frow['ADOEM'];
                                              $usinganalyticstool=$frow['UsingAnalyticsTool'];
                                              $analyticstooloem=$frow['AnalyticsToolOEM'];
                                              $iso27001status=$frow['ISO27001Status'];
                                              $iso27001plan=$frow['ISO27001Plan'];
                                              $itilstatus=$frow['ITILStatus'];
                                              $itilplan=$frow['ITILPlan'];
                              
                                                
                                              
                                                  echo '<tr>';
                                                  echo "<td>".$count."</td>";
                                                  echo "<td>".$company."</td>";
                                                  echo "<td>".$noofserver."</td>";
                                                  echo "<td>".$serveroem."</td>";
                                                  echo "<td>".$serverage."</td>";
                                                  echo "<td>".$serverconfig."</td>";
                                                  echo "<td>".$storagetype."</td>";
                                                  echo "<td>".$storageoem."</td>";
                                                  echo "<td>".$noofstoragebox."</td>";
                                                  echo "<td>".$storagedatasize."</td>";
                                                  echo "<td>".$osname."</td>";
                                                  echo "<td>".$osdetail."</td>";
                                                  echo "<td>".$osoem."</td>";
                                                  echo "<td>".$osversion."</td>";
                                                  echo "<td>".$virtualizationoem."</td>";
                                                  echo "<td>".$virtualizationversion."</td>";
                                                  echo "<td>".$virtualizationrenewaldate."</td>";
                                                  echo "<td>".$backupdatasize."</td>";
                                                  echo "<td>".$backupoem."</td>";
                                                  echo "<td>".$backupversion."</td>";
                                                  echo "<td>".$backuprenewaldate."</td>";
                                                  echo "<td>".$cloudoem."</td>";
                                                  echo "<td>".$cloudusecase."</td>";
                                                  echo "<td>".$cloudmonthlybilling."</td>";
                                                  echo "<td>".$cloudmanagedby."</td>";
                                                  echo "<td>".$noofendpoints."</td>";
                                                  echo "<td>".$endpointoem."</td>";
                                                  echo "<td>".$endpointrenewaldate."</td>";
                                                  echo "<td>".$firewallage."</td>";
                                                  echo "<td>".$firewalloem."</td>";
                                                  echo "<td>".$dlprenewaldate."</td>";
                                                  echo "<td>".$dlpoem."</td>";
                                                  echo "<td>".$mobilesecurity."</td>";
                                                  echo "<td>".$mobilesecurityoem."</td>";
                                                  echo "<td>".$vaptstatus."</td>";
                                                  echo "<td>".$vaptplan."</td>";
                                                  echo "<td>".$usingremotedesktop."</td>";
                                                  echo "<td>".$remotedesktopoem."</td>";
                                                  echo "<td>".$usinghelpdesk."</td>";
                                                  echo "<td>".$helpdeskoem."</td>";
                                                  echo "<td>{$frow['UsingERP']}</td>";
                                                  echo "<td>{$frow['ERPOEM']}</td>";
                                                  echo "<td>{$frow['UsingCRM']}</td>";
                                                  echo "<td>{$frow['CRMOEM']}</td>";
                                                  echo "<td>{$frow['UsingAutomation']}</td>";
                                                  echo "<td>{$frow['AutomationOEM']}</td>";
                                                  echo "<td>{$frow['MailNoOfUsers']}</td>";
                                                  echo "<td>{$frow['MailOEM']}</td>";
                                                  echo "<td>{$frow['MailRenewalDate']}</td>";
                                                  echo "<td>{$frow['UsingAD']}</td>";
                                                  echo "<td>{$frow['ADOEM']}</td>";
                                                  echo "<td>{$frow['UsingAnalyticsTool']}</td>";
                                                  echo "<td>{$frow['AnalyticsToolOEM']}</td>";
                                                  echo "<td>{$frow['ISO27001Status']}</td>";
                                                  echo "<td>{$frow['ISO27001Plan']}</td>";
                                                  echo "<td>{$frow['ITILStatus']}</td>";
                                                  echo "<td>{$frow['ITILPlan']}</td>";
                              
                              
                                                  
                                                  echo "<td><a  href='editcustprofile.php?u=$ccid&v=$cid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                  /*echo "<td><a  href='../auth/customer/delcust.php?u=$cid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/
                              
                              
                                                 echo "</tr>" ;
                                                 ?>
                                                 <?php
                                                  
                                          }
                                      echo '</tbody>';
                                  echo '</table>';
                              ?>
                        </div>
                     </div>
                     <!--  infrastructure end -->
                     <!--  security start -->
                     <div class="tab-pane animation-slide-left" id="security" role="tabpanel">
                        <div class="example">
                           <?php
                              $cname=$_GET['u'];
                              $cid=$_GET['v'];
                                  /*$fetdetail=mysqli_query($dbc,"select * from `customersprofile` where `Company`='$cname' ");*/
                                  $fetdetail=mysqli_query($dbc,"select * from `customersprofile`  ");
                              
                                  /*echo '<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                  echo '<table  class="table table-striped dataTable table-responsive table-bordered "  data-plugin="dataTable">';
                                      echo '<thead>';
                                          echo '<tr>';
                                              echo '<th></th>';
                                              echo '<th></th>';
                                              echo '<th colspan="4">Server</th>';
                                              echo '<th colspan="4">Storage</th>';
                                              echo '<th colspan="4">OS</th>';
                                              echo '<th colspan="3">Virtualization</th>';
                                              echo '<th colspan="4">Backup</th>';
                                              echo '<th colspan="4">Cloud</th>';
                                              /*security*/
                                              echo '<th colspan="3">Endpoint Management</th>';
                                              echo '<th colspan="2">Firewall</th>';
                                              echo '<th colspan="2">DLP</th>';
                                              echo '<th colspan="2">Mobile Security</th>';
                                              echo '<th colspan="2">VAPT</th>';
                                              /*dig*/
                                              echo '<th colspan="2">Remote Desktop</th>';
                                              echo '<th colspan="2">Help Desk</th>';
                                              echo '<th colspan="2">ERP</th>';
                                              echo '<th colspan="2">CRM</th>';
                                              echo '<th colspan="2">Automation</th>';
                                              echo '<th colspan="3">Mail</th>';
                                              echo '<th colspan="2">AD</th>';
                                              echo '<th colspan="2">Analytics Tool</th>';
                                              echo '<th colspan="2">ISO 27001</th>';
                                              echo '<th colspan="2">ITIL</th>';
                                              
                                              echo '<th>Update</th>';
                                              
                                          echo '</tr>';
                                          echo '<tr>';
                                              echo '<th>Sl No.</th>';
                                              echo '<th>Company</th>';
                              
                                              echo '<th>No of Server</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Age</th>';
                                              echo '<th>Configuration</th>';
                              
                                              echo '<th>Storage Type</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>No of Storage Box</th>';
                                              echo '<th>Data Size</th>';
                              
                                              echo '<th>OS Name</th>';
                                              echo '<th>OS Detail</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>OS Version</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>Version</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Data Size</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Version</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>Use Case</th>';
                                              echo '<th>Monthly Billing</th>';
                                              echo '<th>Managed By</th>';
                              
                                              /*security*/
                              
                                              echo '<th>No of Endpoints</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Age</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Renewal Date</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Using Remote Desktop</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Help Desk</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using ERP</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using CRM</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Automation</th>';
                                              echo '<th>OEM</th>';
                                              
                                              echo '<th>No of Users</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Using AD</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Tool</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Update</th>';
                                              
                                          echo '</tr>';
                                      echo '</thead>';
                                      echo '<tbody>';

                                      $count=0;
                                          while($frow=mysqli_fetch_assoc($fetdetail))
                                          { 
                                            ?>
                                            <?php
                                              global $count;
                                              $count=$count+1;
                                              $ccid=$frow['id'];
                                              $cpid=$frow['cid'];
                                              $company=$frow['Company'];
                                              $noofserver=$frow['NoOfServer'];
                                              $serveroem=$frow['ServerOEM'];
                                              $serverage=$frow['ServerAge'];
                                              $serverconfig=$frow['ServerConfig'];
                                              $storagetype=$frow['StorageType'];
                                              $storageoem=$frow['StorageOEM'];
                                              $noofstoragebox=$frow['NoOfStorageBox'];
                                              $storagedatasize=$frow['StorageDataSize'];
                                              $osname=$frow['OSName'];
                                              $osdetail=$frow['OSDetail'];
                                              $osoem=$frow['OSOEM'];
                                              $osversion=$frow['OSVersion'];
                                              $virtualizationoem=$frow['VirtualizationOEM'];
                                              $virtualizationversion=$frow['VirtualizationVersion'];
                                              $virtualizationrenewaldate=$frow['VirtualizationRenewalDate'];
                                              $backupdatasize=$frow['BackupDataSize'];
                                              $backupoem=$frow['BackupOEM'];
                                              $backupversion=$frow['BackupVersion'];
                                              $backuprenewaldate=$frow['BackupRenewalDate'];
                                              $cloudoem=$frow['CloudOEM'];
                                              $cloudusecase=$frow['CloudUseCase'];
                                              $cloudmonthlybilling=$frow['CloudMonthlyBilling'];
                                              $cloudmanagedby=$frow['CloudManagedBy'];
                                              $noofendpoints=$frow['NoOfEndpoints'];
                                              $endpointoem=$frow['EndpointOEM'];
                                              $endpointrenewaldate=$frow['EndpointRenewalDate'];
                                              $firewallage=$frow['FirewallAge'];
                                              $firewalloem=$frow['FirewallOEM'];
                                              $dlprenewaldate=$frow['DLPRenewalDate'];
                                              $dlpoem=$frow['DLPOEM'];
                                              $mobilesecurity=$frow['MobileSecurity'];
                                              $mobilesecurityoem=$frow['MobileSecurityOEM'];
                                              $vaptstatus=$frow['VAPTStatus'];
                                              $vaptplan=$frow['VAPTPlan'];
                                              $usingremotedesktop=$frow['UsingRemoteDesktop'];
                                              $remotedesktopoem=$frow['RemoteDesktopOEM'];
                                              $usinghelpdesk=$frow['UsingHelpDesk'];
                                              $helpdeskoem=$frow['HelpDeskOEM'];
                                              $usingerp=$frow['UsingERP'];
                                              $erpoem=$frow['ERPOEM'];
                                              $usingcrm=$frow['UsingCRM'];
                                              $crmoem=$frow['CRMOEM'];
                                              $usingautomation=$frow['UsingAutomation'];
                                              $automationoem=$frow['AutomationOEM'];
                                              $mailnoofusers=$frow['MailNoOfUsers'];
                                              $mailoem=$frow['MailOEM'];
                                              $mailrenewaldate=$frow['MailRenewalDate'];
                                              $usingad=$frow['UsingAD'];
                                              $adoem=$frow['ADOEM'];
                                              $usinganalyticstool=$frow['UsingAnalyticsTool'];
                                              $analyticstooloem=$frow['AnalyticsToolOEM'];
                                              $iso27001status=$frow['ISO27001Status'];
                                              $iso27001plan=$frow['ISO27001Plan'];
                                              $itilstatus=$frow['ITILStatus'];
                                              $itilplan=$frow['ITILPlan'];
                              
                                                
                                              
                                                  echo '<tr>';
                                                  echo "<td>".$count."</td>";
                                                  echo "<td>".$company."</td>";
                                                  echo "<td>".$noofserver."</td>";
                                                  echo "<td>".$serveroem."</td>";
                                                  echo "<td>".$serverage."</td>";
                                                  echo "<td>".$serverconfig."</td>";
                                                  echo "<td>".$storagetype."</td>";
                                                  echo "<td>".$storageoem."</td>";
                                                  echo "<td>".$noofstoragebox."</td>";
                                                  echo "<td>".$storagedatasize."</td>";
                                                  echo "<td>".$osname."</td>";
                                                  echo "<td>".$osdetail."</td>";
                                                  echo "<td>".$osoem."</td>";
                                                  echo "<td>".$osversion."</td>";
                                                  echo "<td>".$virtualizationoem."</td>";
                                                  echo "<td>".$virtualizationversion."</td>";
                                                  echo "<td>".$virtualizationrenewaldate."</td>";
                                                  echo "<td>".$backupdatasize."</td>";
                                                  echo "<td>".$backupoem."</td>";
                                                  echo "<td>".$backupversion."</td>";
                                                  echo "<td>".$backuprenewaldate."</td>";
                                                  echo "<td>".$cloudoem."</td>";
                                                  echo "<td>".$cloudusecase."</td>";
                                                  echo "<td>".$cloudmonthlybilling."</td>";
                                                  echo "<td>".$cloudmanagedby."</td>";
                                                  echo "<td>".$noofendpoints."</td>";
                                                  echo "<td>".$endpointoem."</td>";
                                                  echo "<td>".$endpointrenewaldate."</td>";
                                                  echo "<td>".$firewallage."</td>";
                                                  echo "<td>".$firewalloem."</td>";
                                                  echo "<td>".$dlprenewaldate."</td>";
                                                  echo "<td>".$dlpoem."</td>";
                                                  echo "<td>".$mobilesecurity."</td>";
                                                  echo "<td>".$mobilesecurityoem."</td>";
                                                  echo "<td>".$vaptstatus."</td>";
                                                  echo "<td>".$vaptplan."</td>";
                                                  echo "<td>".$usingremotedesktop."</td>";
                                                  echo "<td>".$remotedesktopoem."</td>";
                                                  echo "<td>".$usinghelpdesk."</td>";
                                                  echo "<td>".$helpdeskoem."</td>";
                                                  echo "<td>{$frow['UsingERP']}</td>";
                                                  echo "<td>{$frow['ERPOEM']}</td>";
                                                  echo "<td>{$frow['UsingCRM']}</td>";
                                                  echo "<td>{$frow['CRMOEM']}</td>";
                                                  echo "<td>{$frow['UsingAutomation']}</td>";
                                                  echo "<td>{$frow['AutomationOEM']}</td>";
                                                  echo "<td>{$frow['MailNoOfUsers']}</td>";
                                                  echo "<td>{$frow['MailOEM']}</td>";
                                                  echo "<td>{$frow['MailRenewalDate']}</td>";
                                                  echo "<td>{$frow['UsingAD']}</td>";
                                                  echo "<td>{$frow['ADOEM']}</td>";
                                                  echo "<td>{$frow['UsingAnalyticsTool']}</td>";
                                                  echo "<td>{$frow['AnalyticsToolOEM']}</td>";
                                                  echo "<td>{$frow['ISO27001Status']}</td>";
                                                  echo "<td>{$frow['ISO27001Plan']}</td>";
                                                  echo "<td>{$frow['ITILStatus']}</td>";
                                                  echo "<td>{$frow['ITILPlan']}</td>";
                              
                              
                                                  
                                                  echo "<td><a  href='editcustprofile.php?u=$ccid&v=$cid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                  /*echo "<td><a  href='../auth/customer/delcust.php?u=$cid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/
                              
                              
                                                 echo "</tr>" ;
                                                 ?>
                                                 <?php
                                                  
                                          }
                                      echo '</tbody>';
                                  echo '</table>';
                              ?>
                        </div>
                     </div>
                     <!--  security end -->
                     <!--  development and testing start -->
                     <div class="tab-pane animation-slide-left" id="devtest" role="tabpanel">
                        <div class="example">
                           <?php
                              $cname=$_GET['u'];
                              $cid=$_GET['v'];
                                  /*$fetdetail=mysqli_query($dbc,"select * from `customersprofile` where `Company`='$cname' ");*/
                                  $fetdetail=mysqli_query($dbc,"select * from `customersprofile`  ");
                              
                                  /*echo '<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                  echo '<table  class="table table-striped dataTable table-responsive table-bordered "  data-plugin="dataTable">';
                                      echo '<thead>';
                                          echo '<tr>';
                                              echo '<th></th>';
                                              echo '<th></th>';
                                              echo '<th colspan="4">Server</th>';
                                              echo '<th colspan="4">Storage</th>';
                                              echo '<th colspan="4">OS</th>';
                                              echo '<th colspan="3">Virtualization</th>';
                                              echo '<th colspan="4">Backup</th>';
                                              echo '<th colspan="4">Cloud</th>';
                                              /*security*/
                                              echo '<th colspan="3">Endpoint Management</th>';
                                              echo '<th colspan="2">Firewall</th>';
                                              echo '<th colspan="2">DLP</th>';
                                              echo '<th colspan="2">Mobile Security</th>';
                                              echo '<th colspan="2">VAPT</th>';
                                              /*dig*/
                                              echo '<th colspan="2">Remote Desktop</th>';
                                              echo '<th colspan="2">Help Desk</th>';
                                              echo '<th colspan="2">ERP</th>';
                                              echo '<th colspan="2">CRM</th>';
                                              echo '<th colspan="2">Automation</th>';
                                              echo '<th colspan="3">Mail</th>';
                                              echo '<th colspan="2">AD</th>';
                                              echo '<th colspan="2">Analytics Tool</th>';
                                              echo '<th colspan="2">ISO 27001</th>';
                                              echo '<th colspan="2">ITIL</th>';
                                              
                                              echo '<th>Update</th>';
                                              
                                          echo '</tr>';
                                          echo '<tr>';
                                              echo '<th>Sl No.</th>';
                                              echo '<th>Company</th>';
                              
                                              echo '<th>No of Server</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Age</th>';
                                              echo '<th>Configuration</th>';
                              
                                              echo '<th>Storage Type</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>No of Storage Box</th>';
                                              echo '<th>Data Size</th>';
                              
                                              echo '<th>OS Name</th>';
                                              echo '<th>OS Detail</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>OS Version</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>Version</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Data Size</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Version</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>Use Case</th>';
                                              echo '<th>Monthly Billing</th>';
                                              echo '<th>Managed By</th>';
                              
                                              /*security*/
                              
                                              echo '<th>No of Endpoints</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Age</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Renewal Date</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Using Remote Desktop</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Help Desk</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using ERP</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using CRM</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Automation</th>';
                                              echo '<th>OEM</th>';
                                              
                                              echo '<th>No of Users</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Using AD</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Tool</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Update</th>';
                                              
                                          echo '</tr>';
                                      echo '</thead>';
                                      echo '<tbody>';

                                      $count=0;
                                          while($frow=mysqli_fetch_assoc($fetdetail))
                                          { 
                                            ?>
                                            <?php
                                              global $count;
                                              $count=$count+1;
                                              $ccid=$frow['id'];
                                              $cpid=$frow['cid'];
                                              $company=$frow['Company'];
                                              $noofserver=$frow['NoOfServer'];
                                              $serveroem=$frow['ServerOEM'];
                                              $serverage=$frow['ServerAge'];
                                              $serverconfig=$frow['ServerConfig'];
                                              $storagetype=$frow['StorageType'];
                                              $storageoem=$frow['StorageOEM'];
                                              $noofstoragebox=$frow['NoOfStorageBox'];
                                              $storagedatasize=$frow['StorageDataSize'];
                                              $osname=$frow['OSName'];
                                              $osdetail=$frow['OSDetail'];
                                              $osoem=$frow['OSOEM'];
                                              $osversion=$frow['OSVersion'];
                                              $virtualizationoem=$frow['VirtualizationOEM'];
                                              $virtualizationversion=$frow['VirtualizationVersion'];
                                              $virtualizationrenewaldate=$frow['VirtualizationRenewalDate'];
                                              $backupdatasize=$frow['BackupDataSize'];
                                              $backupoem=$frow['BackupOEM'];
                                              $backupversion=$frow['BackupVersion'];
                                              $backuprenewaldate=$frow['BackupRenewalDate'];
                                              $cloudoem=$frow['CloudOEM'];
                                              $cloudusecase=$frow['CloudUseCase'];
                                              $cloudmonthlybilling=$frow['CloudMonthlyBilling'];
                                              $cloudmanagedby=$frow['CloudManagedBy'];
                                              $noofendpoints=$frow['NoOfEndpoints'];
                                              $endpointoem=$frow['EndpointOEM'];
                                              $endpointrenewaldate=$frow['EndpointRenewalDate'];
                                              $firewallage=$frow['FirewallAge'];
                                              $firewalloem=$frow['FirewallOEM'];
                                              $dlprenewaldate=$frow['DLPRenewalDate'];
                                              $dlpoem=$frow['DLPOEM'];
                                              $mobilesecurity=$frow['MobileSecurity'];
                                              $mobilesecurityoem=$frow['MobileSecurityOEM'];
                                              $vaptstatus=$frow['VAPTStatus'];
                                              $vaptplan=$frow['VAPTPlan'];
                                              $usingremotedesktop=$frow['UsingRemoteDesktop'];
                                              $remotedesktopoem=$frow['RemoteDesktopOEM'];
                                              $usinghelpdesk=$frow['UsingHelpDesk'];
                                              $helpdeskoem=$frow['HelpDeskOEM'];
                                              $usingerp=$frow['UsingERP'];
                                              $erpoem=$frow['ERPOEM'];
                                              $usingcrm=$frow['UsingCRM'];
                                              $crmoem=$frow['CRMOEM'];
                                              $usingautomation=$frow['UsingAutomation'];
                                              $automationoem=$frow['AutomationOEM'];
                                              $mailnoofusers=$frow['MailNoOfUsers'];
                                              $mailoem=$frow['MailOEM'];
                                              $mailrenewaldate=$frow['MailRenewalDate'];
                                              $usingad=$frow['UsingAD'];
                                              $adoem=$frow['ADOEM'];
                                              $usinganalyticstool=$frow['UsingAnalyticsTool'];
                                              $analyticstooloem=$frow['AnalyticsToolOEM'];
                                              $iso27001status=$frow['ISO27001Status'];
                                              $iso27001plan=$frow['ISO27001Plan'];
                                              $itilstatus=$frow['ITILStatus'];
                                              $itilplan=$frow['ITILPlan'];
                              
                                                
                                              
                                                  echo '<tr>';
                                                  echo "<td>".$count."</td>";
                                                  echo "<td>".$company."</td>";
                                                  echo "<td>".$noofserver."</td>";
                                                  echo "<td>".$serveroem."</td>";
                                                  echo "<td>".$serverage."</td>";
                                                  echo "<td>".$serverconfig."</td>";
                                                  echo "<td>".$storagetype."</td>";
                                                  echo "<td>".$storageoem."</td>";
                                                  echo "<td>".$noofstoragebox."</td>";
                                                  echo "<td>".$storagedatasize."</td>";
                                                  echo "<td>".$osname."</td>";
                                                  echo "<td>".$osdetail."</td>";
                                                  echo "<td>".$osoem."</td>";
                                                  echo "<td>".$osversion."</td>";
                                                  echo "<td>".$virtualizationoem."</td>";
                                                  echo "<td>".$virtualizationversion."</td>";
                                                  echo "<td>".$virtualizationrenewaldate."</td>";
                                                  echo "<td>".$backupdatasize."</td>";
                                                  echo "<td>".$backupoem."</td>";
                                                  echo "<td>".$backupversion."</td>";
                                                  echo "<td>".$backuprenewaldate."</td>";
                                                  echo "<td>".$cloudoem."</td>";
                                                  echo "<td>".$cloudusecase."</td>";
                                                  echo "<td>".$cloudmonthlybilling."</td>";
                                                  echo "<td>".$cloudmanagedby."</td>";
                                                  echo "<td>".$noofendpoints."</td>";
                                                  echo "<td>".$endpointoem."</td>";
                                                  echo "<td>".$endpointrenewaldate."</td>";
                                                  echo "<td>".$firewallage."</td>";
                                                  echo "<td>".$firewalloem."</td>";
                                                  echo "<td>".$dlprenewaldate."</td>";
                                                  echo "<td>".$dlpoem."</td>";
                                                  echo "<td>".$mobilesecurity."</td>";
                                                  echo "<td>".$mobilesecurityoem."</td>";
                                                  echo "<td>".$vaptstatus."</td>";
                                                  echo "<td>".$vaptplan."</td>";
                                                  echo "<td>".$usingremotedesktop."</td>";
                                                  echo "<td>".$remotedesktopoem."</td>";
                                                  echo "<td>".$usinghelpdesk."</td>";
                                                  echo "<td>".$helpdeskoem."</td>";
                                                  echo "<td>{$frow['UsingERP']}</td>";
                                                  echo "<td>{$frow['ERPOEM']}</td>";
                                                  echo "<td>{$frow['UsingCRM']}</td>";
                                                  echo "<td>{$frow['CRMOEM']}</td>";
                                                  echo "<td>{$frow['UsingAutomation']}</td>";
                                                  echo "<td>{$frow['AutomationOEM']}</td>";
                                                  echo "<td>{$frow['MailNoOfUsers']}</td>";
                                                  echo "<td>{$frow['MailOEM']}</td>";
                                                  echo "<td>{$frow['MailRenewalDate']}</td>";
                                                  echo "<td>{$frow['UsingAD']}</td>";
                                                  echo "<td>{$frow['ADOEM']}</td>";
                                                  echo "<td>{$frow['UsingAnalyticsTool']}</td>";
                                                  echo "<td>{$frow['AnalyticsToolOEM']}</td>";
                                                  echo "<td>{$frow['ISO27001Status']}</td>";
                                                  echo "<td>{$frow['ISO27001Plan']}</td>";
                                                  echo "<td>{$frow['ITILStatus']}</td>";
                                                  echo "<td>{$frow['ITILPlan']}</td>";
                              
                              
                                                  
                                                  echo "<td><a  href='editcustprofile.php?u=$ccid&v=$cid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                  /*echo "<td><a  href='../auth/customer/delcust.php?u=$cid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/
                              
                              
                                                 echo "</tr>" ;
                                                 ?>
                                                 <?php
                                                  
                                          }
                                      echo '</tbody>';
                                  echo '</table>';
                              ?>
                        </div>
                     </div>
                     <!--  development and testing end -->
                     <!--  digital transformaton start -->
                     <div class="tab-pane animation-slide-left" id="digtransfer" role="tabpanel">
                        <div class="example">
                           <?php
                              $cname=$_GET['u'];
                              $cid=$_GET['v'];
                                  /*$fetdetail=mysqli_query($dbc,"select * from `customersprofile` where `Company`='$cname' ");*/
                                  $fetdetail=mysqli_query($dbc,"select * from `customersprofile`  ");
                              
                                  /*echo '<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                  echo '<table  class="table table-striped dataTable table-responsive table-bordered "  data-plugin="dataTable">';
                                      echo '<thead>';
                                          echo '<tr>';
                                              echo '<th></th>';
                                              echo '<th></th>';
                                              echo '<th colspan="4">Server</th>';
                                              echo '<th colspan="4">Storage</th>';
                                              echo '<th colspan="4">OS</th>';
                                              echo '<th colspan="3">Virtualization</th>';
                                              echo '<th colspan="4">Backup</th>';
                                              echo '<th colspan="4">Cloud</th>';
                                              /*security*/
                                              echo '<th colspan="3">Endpoint Management</th>';
                                              echo '<th colspan="2">Firewall</th>';
                                              echo '<th colspan="2">DLP</th>';
                                              echo '<th colspan="2">Mobile Security</th>';
                                              echo '<th colspan="2">VAPT</th>';
                                              /*dig*/
                                              echo '<th colspan="2">Remote Desktop</th>';
                                              echo '<th colspan="2">Help Desk</th>';
                                              echo '<th colspan="2">ERP</th>';
                                              echo '<th colspan="2">CRM</th>';
                                              echo '<th colspan="2">Automation</th>';
                                              echo '<th colspan="3">Mail</th>';
                                              echo '<th colspan="2">AD</th>';
                                              echo '<th colspan="2">Analytics Tool</th>';
                                              echo '<th colspan="2">ISO 27001</th>';
                                              echo '<th colspan="2">ITIL</th>';
                                              
                                              echo '<th>Update</th>';
                                              
                                          echo '</tr>';
                                          echo '<tr>';
                                              echo '<th>Sl No.</th>';
                                              echo '<th>Company</th>';
                              
                                              echo '<th>No of Server</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Age</th>';
                                              echo '<th>Configuration</th>';
                              
                                              echo '<th>Storage Type</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>No of Storage Box</th>';
                                              echo '<th>Data Size</th>';
                              
                                              echo '<th>OS Name</th>';
                                              echo '<th>OS Detail</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>OS Version</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>Version</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Data Size</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Version</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>Use Case</th>';
                                              echo '<th>Monthly Billing</th>';
                                              echo '<th>Managed By</th>';
                              
                                              /*security*/
                              
                                              echo '<th>No of Endpoints</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Age</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Renewal Date</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Using Remote Desktop</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Help Desk</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using ERP</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using CRM</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Automation</th>';
                                              echo '<th>OEM</th>';
                                              
                                              echo '<th>No of Users</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Using AD</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Tool</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Update</th>';
                                              
                                          echo '</tr>';
                                      echo '</thead>';
                                      echo '<tbody>';

                                      $count=0;
                                          while($frow=mysqli_fetch_assoc($fetdetail))
                                          { 
                                            ?>
                                            <?php
                                              global $count;
                                              $count=$count+1;
                                              $ccid=$frow['id'];
                                              $cpid=$frow['cid'];
                                              $company=$frow['Company'];
                                              $noofserver=$frow['NoOfServer'];
                                              $serveroem=$frow['ServerOEM'];
                                              $serverage=$frow['ServerAge'];
                                              $serverconfig=$frow['ServerConfig'];
                                              $storagetype=$frow['StorageType'];
                                              $storageoem=$frow['StorageOEM'];
                                              $noofstoragebox=$frow['NoOfStorageBox'];
                                              $storagedatasize=$frow['StorageDataSize'];
                                              $osname=$frow['OSName'];
                                              $osdetail=$frow['OSDetail'];
                                              $osoem=$frow['OSOEM'];
                                              $osversion=$frow['OSVersion'];
                                              $virtualizationoem=$frow['VirtualizationOEM'];
                                              $virtualizationversion=$frow['VirtualizationVersion'];
                                              $virtualizationrenewaldate=$frow['VirtualizationRenewalDate'];
                                              $backupdatasize=$frow['BackupDataSize'];
                                              $backupoem=$frow['BackupOEM'];
                                              $backupversion=$frow['BackupVersion'];
                                              $backuprenewaldate=$frow['BackupRenewalDate'];
                                              $cloudoem=$frow['CloudOEM'];
                                              $cloudusecase=$frow['CloudUseCase'];
                                              $cloudmonthlybilling=$frow['CloudMonthlyBilling'];
                                              $cloudmanagedby=$frow['CloudManagedBy'];
                                              $noofendpoints=$frow['NoOfEndpoints'];
                                              $endpointoem=$frow['EndpointOEM'];
                                              $endpointrenewaldate=$frow['EndpointRenewalDate'];
                                              $firewallage=$frow['FirewallAge'];
                                              $firewalloem=$frow['FirewallOEM'];
                                              $dlprenewaldate=$frow['DLPRenewalDate'];
                                              $dlpoem=$frow['DLPOEM'];
                                              $mobilesecurity=$frow['MobileSecurity'];
                                              $mobilesecurityoem=$frow['MobileSecurityOEM'];
                                              $vaptstatus=$frow['VAPTStatus'];
                                              $vaptplan=$frow['VAPTPlan'];
                                              $usingremotedesktop=$frow['UsingRemoteDesktop'];
                                              $remotedesktopoem=$frow['RemoteDesktopOEM'];
                                              $usinghelpdesk=$frow['UsingHelpDesk'];
                                              $helpdeskoem=$frow['HelpDeskOEM'];
                                              $usingerp=$frow['UsingERP'];
                                              $erpoem=$frow['ERPOEM'];
                                              $usingcrm=$frow['UsingCRM'];
                                              $crmoem=$frow['CRMOEM'];
                                              $usingautomation=$frow['UsingAutomation'];
                                              $automationoem=$frow['AutomationOEM'];
                                              $mailnoofusers=$frow['MailNoOfUsers'];
                                              $mailoem=$frow['MailOEM'];
                                              $mailrenewaldate=$frow['MailRenewalDate'];
                                              $usingad=$frow['UsingAD'];
                                              $adoem=$frow['ADOEM'];
                                              $usinganalyticstool=$frow['UsingAnalyticsTool'];
                                              $analyticstooloem=$frow['AnalyticsToolOEM'];
                                              $iso27001status=$frow['ISO27001Status'];
                                              $iso27001plan=$frow['ISO27001Plan'];
                                              $itilstatus=$frow['ITILStatus'];
                                              $itilplan=$frow['ITILPlan'];
                              
                                                
                                              
                                                  echo '<tr>';
                                                  echo "<td>".$count."</td>";
                                                  echo "<td>".$company."</td>";
                                                  echo "<td>".$noofserver."</td>";
                                                  echo "<td>".$serveroem."</td>";
                                                  echo "<td>".$serverage."</td>";
                                                  echo "<td>".$serverconfig."</td>";
                                                  echo "<td>".$storagetype."</td>";
                                                  echo "<td>".$storageoem."</td>";
                                                  echo "<td>".$noofstoragebox."</td>";
                                                  echo "<td>".$storagedatasize."</td>";
                                                  echo "<td>".$osname."</td>";
                                                  echo "<td>".$osdetail."</td>";
                                                  echo "<td>".$osoem."</td>";
                                                  echo "<td>".$osversion."</td>";
                                                  echo "<td>".$virtualizationoem."</td>";
                                                  echo "<td>".$virtualizationversion."</td>";
                                                  echo "<td>".$virtualizationrenewaldate."</td>";
                                                  echo "<td>".$backupdatasize."</td>";
                                                  echo "<td>".$backupoem."</td>";
                                                  echo "<td>".$backupversion."</td>";
                                                  echo "<td>".$backuprenewaldate."</td>";
                                                  echo "<td>".$cloudoem."</td>";
                                                  echo "<td>".$cloudusecase."</td>";
                                                  echo "<td>".$cloudmonthlybilling."</td>";
                                                  echo "<td>".$cloudmanagedby."</td>";
                                                  echo "<td>".$noofendpoints."</td>";
                                                  echo "<td>".$endpointoem."</td>";
                                                  echo "<td>".$endpointrenewaldate."</td>";
                                                  echo "<td>".$firewallage."</td>";
                                                  echo "<td>".$firewalloem."</td>";
                                                  echo "<td>".$dlprenewaldate."</td>";
                                                  echo "<td>".$dlpoem."</td>";
                                                  echo "<td>".$mobilesecurity."</td>";
                                                  echo "<td>".$mobilesecurityoem."</td>";
                                                  echo "<td>".$vaptstatus."</td>";
                                                  echo "<td>".$vaptplan."</td>";
                                                  echo "<td>".$usingremotedesktop."</td>";
                                                  echo "<td>".$remotedesktopoem."</td>";
                                                  echo "<td>".$usinghelpdesk."</td>";
                                                  echo "<td>".$helpdeskoem."</td>";
                                                  echo "<td>{$frow['UsingERP']}</td>";
                                                  echo "<td>{$frow['ERPOEM']}</td>";
                                                  echo "<td>{$frow['UsingCRM']}</td>";
                                                  echo "<td>{$frow['CRMOEM']}</td>";
                                                  echo "<td>{$frow['UsingAutomation']}</td>";
                                                  echo "<td>{$frow['AutomationOEM']}</td>";
                                                  echo "<td>{$frow['MailNoOfUsers']}</td>";
                                                  echo "<td>{$frow['MailOEM']}</td>";
                                                  echo "<td>{$frow['MailRenewalDate']}</td>";
                                                  echo "<td>{$frow['UsingAD']}</td>";
                                                  echo "<td>{$frow['ADOEM']}</td>";
                                                  echo "<td>{$frow['UsingAnalyticsTool']}</td>";
                                                  echo "<td>{$frow['AnalyticsToolOEM']}</td>";
                                                  echo "<td>{$frow['ISO27001Status']}</td>";
                                                  echo "<td>{$frow['ISO27001Plan']}</td>";
                                                  echo "<td>{$frow['ITILStatus']}</td>";
                                                  echo "<td>{$frow['ITILPlan']}</td>";
                              
                              
                                                  
                                                  echo "<td><a  href='editcustprofile.php?u=$ccid&v=$cid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                  /*echo "<td><a  href='../auth/customer/delcust.php?u=$cid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/
                              
                              
                                                 echo "</tr>" ;
                                                 ?>
                                                 <?php
                                                  
                                          }
                                      echo '</tbody>';
                                  echo '</table>';
                              ?>
                        </div>
                     </div>
                     <!--  digital transformation end -->
                     <!--  analytics start -->
                     <div class="tab-pane animation-slide-left" id="analytics" role="tabpanel">
                        <div class="example">
                           <?php
                              $cname=$_GET['u'];
                              $cid=$_GET['v'];
                                  /*$fetdetail=mysqli_query($dbc,"select * from `customersprofile` where `Company`='$cname' ");*/
                                  $fetdetail=mysqli_query($dbc,"select * from `customersprofile`  ");
                              
                                  /*echo '<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                  echo '<table  class="table table-striped dataTable table-responsive table-bordered "  data-plugin="dataTable">';
                                      echo '<thead>';
                                          echo '<tr>';
                                              echo '<th></th>';
                                              echo '<th></th>';
                                              echo '<th colspan="4">Server</th>';
                                              echo '<th colspan="4">Storage</th>';
                                              echo '<th colspan="4">OS</th>';
                                              echo '<th colspan="3">Virtualization</th>';
                                              echo '<th colspan="4">Backup</th>';
                                              echo '<th colspan="4">Cloud</th>';
                                              /*security*/
                                              echo '<th colspan="3">Endpoint Management</th>';
                                              echo '<th colspan="2">Firewall</th>';
                                              echo '<th colspan="2">DLP</th>';
                                              echo '<th colspan="2">Mobile Security</th>';
                                              echo '<th colspan="2">VAPT</th>';
                                              /*dig*/
                                              echo '<th colspan="2">Remote Desktop</th>';
                                              echo '<th colspan="2">Help Desk</th>';
                                              echo '<th colspan="2">ERP</th>';
                                              echo '<th colspan="2">CRM</th>';
                                              echo '<th colspan="2">Automation</th>';
                                              echo '<th colspan="3">Mail</th>';
                                              echo '<th colspan="2">AD</th>';
                                              echo '<th colspan="2">Analytics Tool</th>';
                                              echo '<th colspan="2">ISO 27001</th>';
                                              echo '<th colspan="2">ITIL</th>';
                                              
                                              echo '<th>Update</th>';
                                              
                                          echo '</tr>';
                                          echo '<tr>';
                                              echo '<th>Sl No.</th>';
                                              echo '<th>Company</th>';
                              
                                              echo '<th>No of Server</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Age</th>';
                                              echo '<th>Configuration</th>';
                              
                                              echo '<th>Storage Type</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>No of Storage Box</th>';
                                              echo '<th>Data Size</th>';
                              
                                              echo '<th>OS Name</th>';
                                              echo '<th>OS Detail</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>OS Version</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>Version</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Data Size</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Version</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>Use Case</th>';
                                              echo '<th>Monthly Billing</th>';
                                              echo '<th>Managed By</th>';
                              
                                              /*security*/
                              
                                              echo '<th>No of Endpoints</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Age</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Renewal Date</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>OEM</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Using Remote Desktop</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Help Desk</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using ERP</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using CRM</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Automation</th>';
                                              echo '<th>OEM</th>';
                                              
                                              echo '<th>No of Users</th>';
                                              echo '<th>OEM</th>';
                                              echo '<th>Renewal Date</th>';
                              
                                              echo '<th>Using AD</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Using Tool</th>';
                                              echo '<th>OEM</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Status</th>';
                                              echo '<th>Plan</th>';
                              
                                              echo '<th>Update</th>';
                                              
                                          echo '</tr>';
                                      echo '</thead>';
                                      echo '<tbody>';

                                      $count=0;
                                          while($frow=mysqli_fetch_assoc($fetdetail))
                                          { 
                                            ?>
                                            <?php
                                              global $count;
                                              $count=$count+1;
                                              $ccid=$frow['id'];
                                              $cpid=$frow['cid'];
                                              $company=$frow['Company'];
                                              $noofserver=$frow['NoOfServer'];
                                              $serveroem=$frow['ServerOEM'];
                                              $serverage=$frow['ServerAge'];
                                              $serverconfig=$frow['ServerConfig'];
                                              $storagetype=$frow['StorageType'];
                                              $storageoem=$frow['StorageOEM'];
                                              $noofstoragebox=$frow['NoOfStorageBox'];
                                              $storagedatasize=$frow['StorageDataSize'];
                                              $osname=$frow['OSName'];
                                              $osdetail=$frow['OSDetail'];
                                              $osoem=$frow['OSOEM'];
                                              $osversion=$frow['OSVersion'];
                                              $virtualizationoem=$frow['VirtualizationOEM'];
                                              $virtualizationversion=$frow['VirtualizationVersion'];
                                              $virtualizationrenewaldate=$frow['VirtualizationRenewalDate'];
                                              $backupdatasize=$frow['BackupDataSize'];
                                              $backupoem=$frow['BackupOEM'];
                                              $backupversion=$frow['BackupVersion'];
                                              $backuprenewaldate=$frow['BackupRenewalDate'];
                                              $cloudoem=$frow['CloudOEM'];
                                              $cloudusecase=$frow['CloudUseCase'];
                                              $cloudmonthlybilling=$frow['CloudMonthlyBilling'];
                                              $cloudmanagedby=$frow['CloudManagedBy'];
                                              $noofendpoints=$frow['NoOfEndpoints'];
                                              $endpointoem=$frow['EndpointOEM'];
                                              $endpointrenewaldate=$frow['EndpointRenewalDate'];
                                              $firewallage=$frow['FirewallAge'];
                                              $firewalloem=$frow['FirewallOEM'];
                                              $dlprenewaldate=$frow['DLPRenewalDate'];
                                              $dlpoem=$frow['DLPOEM'];
                                              $mobilesecurity=$frow['MobileSecurity'];
                                              $mobilesecurityoem=$frow['MobileSecurityOEM'];
                                              $vaptstatus=$frow['VAPTStatus'];
                                              $vaptplan=$frow['VAPTPlan'];
                                              $usingremotedesktop=$frow['UsingRemoteDesktop'];
                                              $remotedesktopoem=$frow['RemoteDesktopOEM'];
                                              $usinghelpdesk=$frow['UsingHelpDesk'];
                                              $helpdeskoem=$frow['HelpDeskOEM'];
                                              $usingerp=$frow['UsingERP'];
                                              $erpoem=$frow['ERPOEM'];
                                              $usingcrm=$frow['UsingCRM'];
                                              $crmoem=$frow['CRMOEM'];
                                              $usingautomation=$frow['UsingAutomation'];
                                              $automationoem=$frow['AutomationOEM'];
                                              $mailnoofusers=$frow['MailNoOfUsers'];
                                              $mailoem=$frow['MailOEM'];
                                              $mailrenewaldate=$frow['MailRenewalDate'];
                                              $usingad=$frow['UsingAD'];
                                              $adoem=$frow['ADOEM'];
                                              $usinganalyticstool=$frow['UsingAnalyticsTool'];
                                              $analyticstooloem=$frow['AnalyticsToolOEM'];
                                              $iso27001status=$frow['ISO27001Status'];
                                              $iso27001plan=$frow['ISO27001Plan'];
                                              $itilstatus=$frow['ITILStatus'];
                                              $itilplan=$frow['ITILPlan'];
                              
                                                
                                              
                                                  echo '<tr>';
                                                  echo "<td>".$count."</td>";
                                                  echo "<td>".$company."</td>";
                                                  echo "<td>".$noofserver."</td>";
                                                  echo "<td>".$serveroem."</td>";
                                                  echo "<td>".$serverage."</td>";
                                                  echo "<td>".$serverconfig."</td>";
                                                  echo "<td>".$storagetype."</td>";
                                                  echo "<td>".$storageoem."</td>";
                                                  echo "<td>".$noofstoragebox."</td>";
                                                  echo "<td>".$storagedatasize."</td>";
                                                  echo "<td>".$osname."</td>";
                                                  echo "<td>".$osdetail."</td>";
                                                  echo "<td>".$osoem."</td>";
                                                  echo "<td>".$osversion."</td>";
                                                  echo "<td>".$virtualizationoem."</td>";
                                                  echo "<td>".$virtualizationversion."</td>";
                                                  echo "<td>".$virtualizationrenewaldate."</td>";
                                                  echo "<td>".$backupdatasize."</td>";
                                                  echo "<td>".$backupoem."</td>";
                                                  echo "<td>".$backupversion."</td>";
                                                  echo "<td>".$backuprenewaldate."</td>";
                                                  echo "<td>".$cloudoem."</td>";
                                                  echo "<td>".$cloudusecase."</td>";
                                                  echo "<td>".$cloudmonthlybilling."</td>";
                                                  echo "<td>".$cloudmanagedby."</td>";
                                                  echo "<td>".$noofendpoints."</td>";
                                                  echo "<td>".$endpointoem."</td>";
                                                  echo "<td>".$endpointrenewaldate."</td>";
                                                  echo "<td>".$firewallage."</td>";
                                                  echo "<td>".$firewalloem."</td>";
                                                  echo "<td>".$dlprenewaldate."</td>";
                                                  echo "<td>".$dlpoem."</td>";
                                                  echo "<td>".$mobilesecurity."</td>";
                                                  echo "<td>".$mobilesecurityoem."</td>";
                                                  echo "<td>".$vaptstatus."</td>";
                                                  echo "<td>".$vaptplan."</td>";
                                                  echo "<td>".$usingremotedesktop."</td>";
                                                  echo "<td>".$remotedesktopoem."</td>";
                                                  echo "<td>".$usinghelpdesk."</td>";
                                                  echo "<td>".$helpdeskoem."</td>";
                                                  echo "<td>{$frow['UsingERP']}</td>";
                                                  echo "<td>{$frow['ERPOEM']}</td>";
                                                  echo "<td>{$frow['UsingCRM']}</td>";
                                                  echo "<td>{$frow['CRMOEM']}</td>";
                                                  echo "<td>{$frow['UsingAutomation']}</td>";
                                                  echo "<td>{$frow['AutomationOEM']}</td>";
                                                  echo "<td>{$frow['MailNoOfUsers']}</td>";
                                                  echo "<td>{$frow['MailOEM']}</td>";
                                                  echo "<td>{$frow['MailRenewalDate']}</td>";
                                                  echo "<td>{$frow['UsingAD']}</td>";
                                                  echo "<td>{$frow['ADOEM']}</td>";
                                                  echo "<td>{$frow['UsingAnalyticsTool']}</td>";
                                                  echo "<td>{$frow['AnalyticsToolOEM']}</td>";
                                                  echo "<td>{$frow['ISO27001Status']}</td>";
                                                  echo "<td>{$frow['ISO27001Plan']}</td>";
                                                  echo "<td>{$frow['ITILStatus']}</td>";
                                                  echo "<td>{$frow['ITILPlan']}</td>";
                              
                              
                                                  
                                                  echo "<td><a  href='editcustprofile.php?u=$ccid&v=$cid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                  /*echo "<td><a  href='../auth/customer/delcust.php?u=$cid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/
                              
                              
                                                 echo "</tr>" ;
                                                 ?>
                                                 <?php
                                                  
                                          }
                                      echo '</tbody>';
                                  echo '</table>';
                              ?>
                        </div>
                     </div>
                     <!--  analytics end -->
                  </div>
               </div>
            </div>
            <!-- input customer details end -->
            <!-- End Panel Basic -->
         </div>
      </div>
      <!-- End Page -->
      <!-- Footer -->
      <?php include "includes/footer.php"; ?>
      <!-- Core  -->
      <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
      <script src="../../assets/global/vendor/jquery/jquery.js"></script>
      <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
      <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
      <script src="../../assets/global/vendor/animsition/animsition.js"></script>
      <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
      <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
      <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
      <script src="../../assets/global/vendor/waves/waves.js"></script>
      <!-- Plugins -->
      <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
      <script src="../../assets/global/vendor/switchery/switchery.js"></script>
      <script src="../../assets/global/vendor/intro-js/intro.js"></script>
      <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
      <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
      <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
      <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
      <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
      <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
      <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
      <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
      <!-- Scripts -->
      <script src="../../assets/global/js/Component.js"></script>
      <script src="../../assets/global/js/Plugin.js"></script>
      <script src="../../assets/global/js/Base.js"></script>
      <script src="../../assets/global/js/Config.js"></script>
      <script src="section/Menubar.js"></script>
      <script src="section/Sidebar.js"></script>
      <script src="section/PageAside.js"></script>
      <script src="section/GridMenu.js"></script>
      <!-- Config -->
      <script src="../../assets/global/js/config/colors.js"></script>
      <script src="../../assets/js/config/tour.js"></script>
      <script>Config.set('assets', '../assets');</script>
      <!-- Page -->
      <script src="../../assets/js/Site.js"></script>
      <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
      <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
      <script src="../../assets/global/js/Plugin/switchery.js"></script>
      <script src="../../assets/global/js/Plugin/matchheight.js"></script>
      <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
      <script src="../../assets/global/js/Plugin/peity.js"></script>
      <script src="../../assets/examples/js/dashboard/v1.js"></script>
      <script src="../../assets/global/js/Plugin/datatables.js"></script>
    
        <script src="../../assets/examples/js/tables/datatable.js"></script>



        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script >
          $(document).ready(function() {
    $('#example').DataTable();
} );
        </script>
        <script >
          $(document).ready(function() {
    $('#example2').DataTable();
} );
        </script>
        <script >
          $(document).ready(function() {
    $('#example3').DataTable();
} );
        </script>
        <script >
          $(document).ready(function() {
    $('#example4').DataTable();
} );
        </script>
        <script >
          $(document).ready(function() {
    $('#example5').DataTable();
} );
        </script>
        <script >
          $(document).ready(function() {
    $('#example6').DataTable();
} );
        </script>
   </body>
</html>