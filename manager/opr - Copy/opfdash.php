<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>OPF Dashboard | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
    
  </head>
  <body class="animsition site-navbar-small dashboard">
    <?php include "navbar-header.php"; ?> 
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <!-- write body content here -->
      <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
          <header class="panel-heading">
            
            <h3 class="panel-title example-title">OPF List</h3>
          </header>
             <div class="panel-body">
           
              <?php
                    $fetdetail=mysqli_query($dbc,"select * from `opf` where `RMail`='$id' order by `ModificationDetail` desc ");



                    /*echo '<table class="table table-striped mb-none" id="datatable-tabletools">';*/
                      echo '<table class="table table-striped table-responsive-md table-bordered example">';
                      echo '<thead>';
                        echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>OPF No</th>';
                                                    echo '<th>QuotRef No</th>';
                                                    echo '<th>Customer Name</th>';
                                                    echo '<th>Vendor Name</th>';
                                                    echo '<th>Customer Price</th>';
                                                    echo '<th>Vendor Price</th>';
                                                    echo '<th>GP</th>';

                                                    /*echo '<th>Credit Card Name</th>';
                                                    echo '<th>Card No Ending</th>';*/
                                                    /*echo '<th>WHT Percentage</th>';
                                                    echo '<th>Conversion Rate to INR</th>';*/
                                                    echo '<th>Generated Date</th>';
                                                    echo '<th>Status</th>';
                                                    echo '<th>Comments</th>';
                                                    echo '<th>View</th>';
                                                    echo '<th>Update</th>';
                                                    /*echo '<th>Remove</th>';*/
                                                    
                        echo '</tr>';
                      echo '</thead>';
                      echo '<tbody>';
                                            $cp=0;
                                            
                        while($frow=mysqli_fetch_assoc($fetdetail))
                        {
                                                    global $cp;
                                                    $cp=$cp+1;
                                                    $opfno=$frow['OPFNo'];
                                                    $quotrefno=$frow['QuotRefNo'];
                                                    $vendorname=$frow['VendorName'];
                                                    $creditcardname=$frow['CreditCardName'];
                                                    $cardendingno=$frow['CardEndingNo'];
                                                    $whtpercentage=$frow['WHTPercentage'];
                                                    $conversionrate=$frow['ConversionRate'];
                                                    $status=$frow['OPFStatus'];
                                                    $comments=$frow['Comments'];
                                                    $modificationdetail=$frow['ModificationDetail'];
                                                    $opfdate=date('Y-m-d',strtotime($modificationdetail));
                                               

                                                        /*$fetfunnelrev=mysqli_query($dbc,"select * from `funnel` where `Company`='$company' and `Products`='$product' and `Services`='$service' ");
                                                       

                                                        while($result=mysqli_fetch_assoc($fetfunnelrev))
                                                        {
                                                            
                                                            $probability=$result['Probability'];

                                                        }*/
                                                        $fetfunnelrev=mysqli_query($dbc,"select * from `quotation` where `QuotNo`='$quotrefno'  ");
                                                       

                                                        while($result=mysqli_fetch_assoc($fetfunnelrev))
                                                        {
                                                            
                                                            $custname=$result['Company'];

                                                        }

                                                        
                            echo '<tr>';
                            echo "<td>".$cp."</td>";
                                                        echo "<td>{$frow['OPFNo']}</td>";
                                                        echo "<td>{$frow['QuotRefNo']}</td>";
                                                        echo '<td>'.$custname.'</td>';
                                                        echo "<td>{$frow['VendorName']}</td>";
                                                        /*echo "<td>{$frow['CreditCardName']}</td>";
                            echo "<td>{$frow['CardEndingNo']}</td>";*/
                            /*echo "<td>{$frow['WHTPercentage']}</td>";
                                                        echo "<td>{$frow['ConversionRate']}</td>";*/
                                                        echo "<td>".$opfdate."</td>";
                                                        echo '<td>';
                                                        if($status == "0")
                                                        {
                                                          echo "Pending";
                                                        }
                                                        elseif ($status == "1") {
                                                          echo "Accepted";
                                                        }
                                                        elseif ($status == "2") {
                                                          echo "Rejected";
                                                        }
                                                        echo '</td>';
                                                        echo "<td>".$comments."</td>";
                            /*echo "<td>".$podate."</td>";
                                                        echo "<td>".$probability."</td>";*/
                                                        /*echo '<td><a class="btn btn-primary" href="quotation.php?n='.$quotno.'">View</a></td>';*/
                                                        /*echo '<td><form action="opf.php" method="get">
                                                        <input type="hidden" name="q" value="'.$opfno.'">
                                                        <input type="submit" name="view" value="View" class="btn btn-primary"></form> </td>';*/

                                                        $quotupdatefetch=mysqli_query($dbc,"select * from `opf_all` where `OPFNo`='$opfno' ");
                                                        $otype=0;
                                                        while($qar=mysqli_fetch_assoc($quotupdatefetch))
                                                        {
                                                          $otype=$otype+1;
                                                        }
                                                         $otype=$otype+1;
                                                        echo '<td><a href="TCPDF/crm/opf.php?q='.$opfno.'" target="_blank" class="btn btn-primary">View</a></td>';

                                                        echo '<td><a href="opfupd.php?o='.$opfno.'&otype='.$otype.'" target="_blank" class="btn btn-primary">Update</a></td>';

                                                        /*echo '<td><form action="opfupd.php" method="post">
                                                        <input type="hidden" name="opfno" value="'.$opfno.'">
                                                        <input type="submit" name="update" value="Update" class="btn btn-warning"></form> </td>';*/

                                                        /*echo '<td><form action="po.php" method="post">
                                                        <input type="hidden" name="pono" value="'.$pono.'">
                                                        <input type="submit" name="delete" value="Delete" class="btn btn-danger"></form> </td>';
*/
                                                        
                                                        
                                                        
                                                        
                                                        /*echo "<td>".$r."</td>";*/
                                                       /* echo "<td><a  href='editprod.php?u=$prodid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        echo "<td><a  href='../auth/product/del.php?u=$prodid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";
                                                        echo '</tr>';*/
  
                            
                        }

                                                        /*echo '<tr>';
                                                        echo "<td>Total</td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td>".$csc."</td>";
                                                        echo "<td>".$ctc."</td>";
                                                        echo "<td>".$cdemo."</td>";
                                                        echo "<td>".$cpoc."</td>";
                                                        echo "<td>".$crevenue."</td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo '</tr>';*/


                      echo '</tbody>';
                    echo '</table>';
                  ?>                           
          
          </div>
        </div>
        <!-- End Panel Basic -->
      </div>
      
</div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>

    
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>



        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script >
          $(document).ready(function() {
    $('.example').DataTable();
} );
        </script>


    
  </body>
</html>
