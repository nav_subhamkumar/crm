<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>HolidayList | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    <!--  date picker  -->
    <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
    
  </head>
  <body class="animsition site-navbar-small dashboard">
    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <!-- write body content here -->
      <div class="page-content">

        <div class="row">
        <div class="col-xl-12 col-md-12">
            <!-- Widget Linearea One-->
        <button class="btn btn-primary" data-target="#ModalResourse" data-toggle="modal" type="button">Resourse Wise</button>
        <button class="btn btn-primary" data-target="#ModalProduct" data-toggle="modal" type="button">Product Wise</button>
        <button class="btn btn-primary" data-target="#ModalCustomer" data-toggle="modal" type="button">Customer Wise</button>
            
            <!-- End Widget Linearea One -->
          </div>
          
                    
                    <!-- Modal -->
                    <div class="modal fade" id="ModalResourse" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <form class="modal-content" action="../auth/target/ins.php" method="post" autocomplete="off" >
                        
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFormModalLabel">Resourse Wise Target</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Employee</label>
                                    <select class="form-control" name="employeemail"  data-plugin="select2" required="required" >
                                        <?php
                                                                        $femp=mysqli_query($dbc,"select * from `team`");
                                                                        echo '<option >Select Employee</option>';
                                                                        while($erow=mysqli_fetch_assoc($femp))
                                                                        {
                                                                            //$pro=$row['ProjectName'];
                                                    
                                                                             echo '<option  value="'.$erow['email'].'">'.$erow['name'].'</option>';

                                                                        }
                                                        

                                                                    ?>
                                    </select>
                                </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Revenue</label>
                            <input type="number" class="form-control" name="revenuepermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Margin</label>
                            <input type="number" class="form-control" name="marginpermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Meetings</label>
                            <input type="number" class="form-control" name="meetingpermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Funnel</label>
                            <input type="number" class="form-control" name="funnelpermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Customers</label>
                            <input type="number" class="form-control" name="customerspermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Sales Certification</label>
                            <input type="number" class="form-control" name="salescertification"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Demo</label>
                            <input type="number" class="form-control" name="demo"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Technical Certification</label>
                            <input type="number" class="form-control" name="technicalcertification"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">POC</label>
                            <input type="number" class="form-control" name="poc"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label"><b style="color:red">*</b>Every Input must be for Month</label>
                            
                              </div>
                              <div class="col-md-12 float-right">
                    <button class="btn btn-primary"  type="submit" name="resourcesubmit">Submit</button>
                       <button class="btn btn-default" data-dismiss="modal" type="reset" >Close</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>


                    <div class="modal fade" id="ModalProduct" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <form class="modal-content" action="../auth/target/ins.php" method="post" autocomplete="off" >
                        
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFormModalLabel">Product Wise Target</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Product Name</label>
                                    <?php $fet=mysqli_query($dbc,"select * from products"); ?>
                                    <select class="form-control" name="product"  data-plugin="select2" required="required" >
                                        <?php
                                                                        while($fetr=mysqli_fetch_assoc($fet))
                                                                        {
                                                                            echo "<option value={$fetr['ProductId']}>{$fetr['ProductName']}</option>";
                                                                        }

                                                                    ?>
                                    </select>
                                </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Revenue</label>
                            <input type="number" class="form-control" name="revenuepermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Margin</label>
                            <input type="number" class="form-control" name="marginpermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Meetings</label>
                            <input type="number" class="form-control" name="meetingpermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Funnel</label>
                            <input type="number" class="form-control" name="funnelpermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Customers</label>
                        <input type="number" class="form-control" name="customerspermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Sales Certification</label>
                            <input type="number" class="form-control" name="salescertification"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Demo</label>
                            <input type="number" class="form-control" name="demo"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Technical Certification</label>
                            <input type="number" class="form-control" name="technicalcertification"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">POC</label>
                            <input type="number" class="form-control" name="poc"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label"><b style="color:red">*</b>Every Input must be for Month</label>
                            
                              </div>
                              <div class="col-md-12 float-right">
                    <button class="btn btn-primary"  type="submit" name="productsubmit">Submit</button>
                       <button class="btn btn-default" data-dismiss="modal" type="reset" >Close</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>

                    <div class="modal fade" id="ModalCustomer" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <form class="modal-content" action="../auth/target/ins.php" method="post" autocomplete="off" >
                        
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFormModalLabel">Customer Wise Target</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Company</label>
                                    <select class="form-control" name="customername"  data-plugin="select2" required="required" >
                                        <?php
                                                                        $femp=mysqli_query($dbc,"select * from `customers`");
                                                                        echo '<option >Select Company</option>';
                                                                        while($erow=mysqli_fetch_assoc($femp))
                                                                        {
                                                                            //$pro=$row['ProjectName'];
                                                    
                                                                             echo '<option  value="'.$erow['Company'].'">'.$erow['Company'].'</option>';

                                                                    
                                                                        }
                                                        

                                                                    ?>
                                    </select>
                                </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Revenue</label>
                            <input type="number" class="form-control" name="revenuepermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Margin</label>
                            <input type="number" class="form-control" name="marginpermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Meetings</label>
                            <input type="number" class="form-control" name="meetingpermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Funnel</label>
                            <input type="number" class="form-control" name="funnelpermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Customers</label>
                            <input type="number" class="form-control" name="customerspermonth"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Sales Certification</label>
                            <input type="number" class="form-control" name="salescertification"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Demo</label>
                            <input type="number" class="form-control" name="demo"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Technical Certification</label>
                            <input type="number" class="form-control" name="technicalcertification"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">POC</label>
                            <input type="number" class="form-control" name="poc"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label"><b style="color:red">*</b>Every Input must be for Month</label>
                            
                              </div>
                              <div class="col-md-12 float-right">
                    <button class="btn btn-primary"  type="submit" name="customersubmit">Submit</button>
                       <button class="btn btn-default" data-dismiss="modal" type="reset" >Close</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>

                    <!-- End Modal -->
                </div>
                <br>
        <!-- Panel Basic -->
        <div class="panel">
          <header class="panel-heading">
            
            <h3 class="panel-title example-title">Resource Wise Target Details For The Month</h3>
          </header>
          <div class="panel-body">
            
              <?php
                                        $fetdetail=mysqli_query($dbc,"select * from `target_manage_resource` where MONTH(ModificationDetail)=MONTH(CURRENT_DATE) and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` ");



                                        /*echo '<table class="table table-striped mb-none" id="datatable-tabletools">';*/
                                        echo '<table class="table table-striped table-responsive-md table-bordered example">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Employee Mail</th>';
                                                    echo '<th>Revenue</th>';
                                                    echo '<th>Margin</th>';
                                                    echo '<th>Meeting</th>';
                                                    echo '<th>Funnel</th>';
                                                    echo '<th>Customers</th>';
                                                    echo '<th>SalesCert</th>';
                                                    echo '<th>TechCert</th>';
                                                    echo '<th>Demo</th>';
                                                    echo '<th>POC</th>';
                                                    echo '<th>Update</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $cp=0;
                                            $cdemo=0;
                                            $cpoc=0;
                                            $crevenue=0;
                                            $csc=0;
                                            $ctc=0;
                                                while($frow=mysqli_fetch_assoc($fetdetail))
                                                {
                                                    global $cp;
                                                    $cp=$cp+1;
                                                    $employeemail=$frow['EmployeeMail'];
                                                    $trevenue=$frow['RevenuePerMonth'];
                                                    $tmargin=$frow['MarginPerMonth'];
                                                    $tmeeting=$frow['MeetingPerMonth'];
                                                    $tfunnel=$frow['FunnelPerMonth'];
                                                    $tcustomer=$frow['CustomersPerMonth'];
                                                    $tsalescert=$frow['SalesCertificationPerMonth'];
                                                    $ttechcert=$frow['TechnicalCertificationPerMonth'];
                                                    $tdemo=$frow['DemoPerMonth'];
                                                    $tpoc=$frow['POCPerMonth'];


                                                        /*$fetfunnelrev=mysqli_query($dbc,"select * from `funnel` where `Products`='$productname' and `Services`='$services' ");
                                                        $r=0;

                                                        while($result=mysqli_fetch_assoc($fetfunnelrev))
                                                        {
                                                            global $r;
                                                            $r=$r+$result['Revenue'];

                                                        }

                                                        global $r;
                                                        global $cdemo;
                                                        global $cpoc;
                                                        global $crevenue;
                                                        global $csc;
                                                        global $ctc;

                                                        if($demo=="Yes")
                                                        {
                                                            $cdemo=$cdemo+1;
                                                        }
                                                        if($poc=="Yes")
                                                        {
                                                            $cpoc=$cpoc+1;
                                                        }
                                                        
                                                        
                                                        $crevenue=$crevenue+$r;
                                                        $ctc=$ctc+$technicalcertification;
                                                        $csc=$csc+$salescertification;*/

    
                                                    
                                                    
                                                        echo '<tr>';
                                                        echo "<td>".$cp."</td>";
                                                        echo "<td>{$frow['EmployeeMail']}</td>";
                                                        echo "<td>{$frow['RevenuePerMonth']}</td>";
                                                        echo "<td>{$frow['MarginPerMonth']}</td>";
                                                        echo "<td>{$frow['MeetingPerMonth']}</td>";
                                                        echo "<td>{$frow['FunnelPerMonth']}</td>";
                                                        echo "<td>{$frow['CustomersPerMonth']}</td>";
                                                        echo "<td>{$frow['SalesCertificationPerMonth']}</td>";
                                                        echo "<td>{$frow['TechnicalCertificationPerMonth']}</td>";
                                                        echo "<td>{$frow['DemoPerMonth']}</td>";
                                                        echo "<td>{$frow['POCPerMonth']}</td>";
                                                        echo '<td><a  href="edittarget.php?u='.$employeemail.'&v=resourcesubmit" class="text-muted text-uppercase btn btn-primary"  ><font color="white">Edit</font></a></td>';
                                                        /*echo "<td>".$r."</td>";
                                                        echo "<td><a  href='editprod.php?u=$prodid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        echo "<td><a  href='../auth/product/del.php?u=$prodid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/
                                                        echo '</tr>';
    
                                                        
                                                }

                                                        /*echo '<tr>';
                                                        echo "<td>Total</td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td>".$csc."</td>";
                                                        echo "<td>".$ctc."</td>";
                                                        echo "<td>".$cdemo."</td>";
                                                        echo "<td>".$cpoc."</td>";
                                                        echo "<td>".$crevenue."</td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo '</tr>';*/


                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>
                                    
          
          </div>
        </div>
        <div class="panel">
          <header class="panel-heading">
            
            <h3 class="panel-title example-title">Product Wise Target Details For The Month</h3>
          </header>
          <div class="panel-body">
            
              <?php
                                        $fetdetail=mysqli_query($dbc,"select * from `target_manage_product` where MONTH(ModificationDetail)=MONTH(CURRENT_DATE) and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` ");



                                        /*echo '<table class="table table-striped mb-none" id="datatable-tabletools">';*/
                                        echo '<table class="table table-striped table-responsive-md table-bordered example">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Product Name</th>';
                                                    echo '<th>Revenue</th>';
                                                    echo '<th>Margin</th>';
                                                    echo '<th>Meeting</th>';
                                                    echo '<th>Funnel</th>';
                                                    echo '<th>Customers</th>';
                                                    echo '<th>SalesCert</th>';
                                                    echo '<th>TechCert</th>';
                                                    echo '<th>Demo</th>';
                                                    echo '<th>POC</th>';
                                                    echo '<th>Update</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $cp=0;
                                            $cdemo=0;
                                            $cpoc=0;
                                            $crevenue=0;
                                            $csc=0;
                                            $ctc=0;
                                                while($frow=mysqli_fetch_assoc($fetdetail))
                                                {
                                                    global $cp;
                                                    $cp=$cp+1;
                                                    $productname=$frow['ProductName'];
                                                    $trevenue=$frow['RevenuePerMonth'];
                                                    $tmargin=$frow['MarginPerMonth'];
                                                    $tmeeting=$frow['MeetingPerMonth'];
                                                    $tfunnel=$frow['FunnelPerMonth'];
                                                    $tcustomer=$frow['CustomersPerMonth'];
                                                    $tsalescert=$frow['SalesCertificationPerMonth'];
                                                    $ttechcert=$frow['TechnicalCertificationPerMonth'];
                                                    $tdemo=$frow['DemoPerMonth'];
                                                    $tpoc=$frow['POCPerMonth'];


                                                        /*$fetfunnelrev=mysqli_query($dbc,"select * from `funnel` where `Products`='$productname' and `Services`='$services' ");
                                                        $r=0;

                                                        while($result=mysqli_fetch_assoc($fetfunnelrev))
                                                        {
                                                            global $r;
                                                            $r=$r+$result['Revenue'];

                                                        }

                                                        global $r;
                                                        global $cdemo;
                                                        global $cpoc;
                                                        global $crevenue;
                                                        global $csc;
                                                        global $ctc;

                                                        if($demo=="Yes")
                                                        {
                                                            $cdemo=$cdemo+1;
                                                        }
                                                        if($poc=="Yes")
                                                        {
                                                            $cpoc=$cpoc+1;
                                                        }
                                                        
                                                        
                                                        $crevenue=$crevenue+$r;
                                                        $ctc=$ctc+$technicalcertification;
                                                        $csc=$csc+$salescertification;*/

    
                                                    
                                                    
                                                        echo '<tr>';
                                                        echo "<td>".$cp."</td>";
                                                        echo "<td>{$frow['ProductName']}</td>";
                                                        echo "<td>{$frow['RevenuePerMonth']}</td>";
                                                        echo "<td>{$frow['MarginPerMonth']}</td>";
                                                        echo "<td>{$frow['MeetingPerMonth']}</td>";
                                                        echo "<td>{$frow['FunnelPerMonth']}</td>";
                                                        echo "<td>{$frow['CustomersPerMonth']}</td>";
                                                        echo "<td>{$frow['SalesCertificationPerMonth']}</td>";
                                                        echo "<td>{$frow['TechnicalCertificationPerMonth']}</td>";
                                                        echo "<td>{$frow['DemoPerMonth']}</td>";
                                                        echo "<td>{$frow['POCPerMonth']}</td>";
                                                        echo '<td><a  href="edittarget.php?u='.$productname.'&v=productsubmit" class="text-muted text-uppercase btn btn-primary"  ><font color="white">Edit</font></a></td>';
                                                        /*echo "<td>".$r."</td>";
                                                        echo "<td><a  href='editprod.php?u=$prodid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        echo "<td><a  href='../auth/product/del.php?u=$prodid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/
                                                        echo '</tr>';
    
                                                        
                                                }

                                                        /*echo '<tr>';
                                                        echo "<td>Total</td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td>".$csc."</td>";
                                                        echo "<td>".$ctc."</td>";
                                                        echo "<td>".$cdemo."</td>";
                                                        echo "<td>".$cpoc."</td>";
                                                        echo "<td>".$crevenue."</td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo '</tr>';*/


                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>
                                    
          
          </div>
        </div>
        <div class="panel">
          <header class="panel-heading">
            
            <h3 class="panel-title example-title">Customer Wise Target Details For The Month</h3>
          </header>
          <div class="panel-body">
            
              <?php
                                        $fetdetail=mysqli_query($dbc,"select * from `target_manage_customer` where MONTH(ModificationDetail)=MONTH(CURRENT_DATE) and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` ");



                                        /*echo '<table class="table table-striped mb-none" id="datatable-tabletools">';*/
                                        echo '<table class="table table-striped table-responsive-md table-bordered example">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Customer Name</th>';
                                                    echo '<th>Revenue</th>';
                                                    echo '<th>Margin</th>';
                                                    echo '<th>Meeting</th>';
                                                    echo '<th>Funnel</th>';
                                                    echo '<th>Customers</th>';
                                                    echo '<th>SalesCert</th>';
                                                    echo '<th>TechCert</th>';
                                                    echo '<th>Demo</th>';
                                                    echo '<th>POC</th>';
                                                    echo '<th>Update</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $cp=0;
                                            $cdemo=0;
                                            $cpoc=0;
                                            $crevenue=0;
                                            $csc=0;
                                            $ctc=0;
                                                while($frow=mysqli_fetch_assoc($fetdetail))
                                                {
                                                    global $cp;
                                                    $cp=$cp+1;
                                                    $customername=$frow['CustomerName'];
                                                    $trevenue=$frow['RevenuePerMonth'];
                                                    $tmargin=$frow['MarginPerMonth'];
                                                    $tmeeting=$frow['MeetingPerMonth'];
                                                    $tfunnel=$frow['FunnelPerMonth'];
                                                    $tcustomer=$frow['CustomersPerMonth'];
                                                    $tsalescert=$frow['SalesCertificationPerMonth'];
                                                    $ttechcert=$frow['TechnicalCertificationPerMonth'];
                                                    $tdemo=$frow['DemoPerMonth'];
                                                    $tpoc=$frow['POCPerMonth'];


                                                        /*$fetfunnelrev=mysqli_query($dbc,"select * from `funnel` where `Products`='$productname' and `Services`='$services' ");
                                                        $r=0;

                                                        while($result=mysqli_fetch_assoc($fetfunnelrev))
                                                        {
                                                            global $r;
                                                            $r=$r+$result['Revenue'];

                                                        }

                                                        global $r;
                                                        global $cdemo;
                                                        global $cpoc;
                                                        global $crevenue;
                                                        global $csc;
                                                        global $ctc;

                                                        if($demo=="Yes")
                                                        {
                                                            $cdemo=$cdemo+1;
                                                        }
                                                        if($poc=="Yes")
                                                        {
                                                            $cpoc=$cpoc+1;
                                                        }
                                                        
                                                        
                                                        $crevenue=$crevenue+$r;
                                                        $ctc=$ctc+$technicalcertification;
                                                        $csc=$csc+$salescertification;*/

    
                                                    
                                                    
                                                        echo '<tr>';
                                                        echo "<td>".$cp."</td>";
                                                        echo "<td>{$frow['CustomerName']}</td>";
                                                        echo "<td>{$frow['RevenuePerMonth']}</td>";
                                                        echo "<td>{$frow['MarginPerMonth']}</td>";
                                                        echo "<td>{$frow['MeetingPerMonth']}</td>";
                                                        echo "<td>{$frow['FunnelPerMonth']}</td>";
                                                        echo "<td>{$frow['CustomersPerMonth']}</td>";
                                                        echo "<td>{$frow['SalesCertificationPerMonth']}</td>";
                                                        echo "<td>{$frow['TechnicalCertificationPerMonth']}</td>";
                                                        echo "<td>{$frow['DemoPerMonth']}</td>";
                                                        echo "<td>{$frow['POCPerMonth']}</td>";
                                                        echo '<td><a  href="edittarget.php?u='.$customername.'&v=customersubmit" class="text-muted text-uppercase btn btn-primary"  ><font color="white">Edit</font></a></td>';
                                                        /*echo "<td>".$r."</td>";
                                                        echo "<td><a  href='editprod.php?u=$prodid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        echo "<td><a  href='../auth/product/del.php?u=$prodid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/
                                                        echo '</tr>';
    
                                                        
                                                }

                                                        /*echo '<tr>';
                                                        echo "<td>Total</td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td>".$csc."</td>";
                                                        echo "<td>".$ctc."</td>";
                                                        echo "<td>".$cdemo."</td>";
                                                        echo "<td>".$cpoc."</td>";
                                                        echo "<td>".$crevenue."</td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo '</tr>';*/


                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>
                                    
          
          </div>
        </div>
        <!-- End Panel Basic -->
      </div>
      
</div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>

    
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>
        <!-- date picker -->
        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="../../assets/global/js/Plugin/bootstrap-datepicker.js"></script>
        <?php include "includes/css/select.php"; ?>
      <?php include "includes/js/select.php"; ?>

        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script >
          $(document).ready(function() {
          $('.example').DataTable();
           } );
        </script>


    
  </body>
</html>
