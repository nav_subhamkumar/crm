<?php
   include "session_handler.php";
   ?>
   
   <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    
        <link rel="stylesheet" href="../../assets/global/vendor/clockpicker/clockpicker.css">
        
        <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
        
        <link rel="stylesheet" href="../../assets/global/vendor/timepicker/jquery-timepicker.css">
        
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

<!-- multipletax include start -->

                                 <div class="linemultivendor" >
   <hr>
<div class="row">
  <?php
  $i=$_SESSION['accesstax'];
  $i=$i+1;
  $_SESSION['accesstax']=$i;

  ?>
  <?php
  $i=$_SESSION['accesstaxvalue'];
  $i=$i+1;
  $_SESSION['accesstaxvalue']=$i;

  ?>

                                 <div class="form-group  col-md-3">
                                    <label class="form-control-label" >Product </label>
                                    <select class="form-control" name="product[]"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(ProductName) from `products` ");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['ProductName'].'">'.$row['ProductName'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Part Description </label>
                                          <input type="text" class="form-control" name="partdesc[]"  placeholder="Part Description" >
                                       </div>

                                       <div class="form-group  col-md-2">
                                          <label class="form-control-label" >Quantity </label>
                                          <input type="number" class="form-control" name="qty[]"  placeholder="Quantity" >
                                       </div>
                                    
                                    
                                       <div class="form-group  col-md-2">
                                          <label class="form-control-label" >Unit Price </label>
                                          <input type="number" class="form-control" name="unitprice[]" placeholder="Unit Price"  >
                                       </div>
                                       <div class="form-group  col-md-2">
                                          <label class="form-control-label" >Vendor Price </label>
                                          <input type="number" class="form-control" name="vendorprice[]"  placeholder="Vendor Price"  > 
                                        </div>

                                       </div>
                                      <div class="row">

                                        <div class="form-group  col-md-3">
                                    <label class="form-control-label" >Vendor/Seller</label>
                                    <select class="form-control" name="sellername"  data-plugin="select2"  required="required" >
                                    <?php
                                                                    for($k=1;$k<=60;$k++)
                                                                    {
                                                                      
                                                                      $prd='product'.$k;
                                                                      
                                                                      //echo '<option  value="'.$sn.'">'.$prd.'</option>';

                                                                    $project=mysqli_query($dbc,"select * from `products` ");
                                                                    
                                                                    echo '<option value="">Select</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                        //$pro=$row['ProjectName'];
                                                                         $sellname=$row['SellerName'];
                                                                         $oemname=$row['OEMName'];
                                                                        $sellname1=$row['SellerName1'];
                                                                                                                                              
                                                                        $sellname2=$row['SellerName2'];
                                                                        $sellname3=$row['SellerName3'];
                                                                        $sellname4=$row['SellerName4'];
                                                                        $sellname5=$row['SellerName5'];
                                                                        $sellname6=$row['SellerName6'];
                                                                        $sellname7=$row['SellerName7'];
                                                                        $sellname8=$row['SellerName8'];
                                                                        $sellname9=$row['SellerName9'];
                                                                        $sellname10=$row['SellerName10'];
                                                                        
                                                                        
                                                                         
                                                                         for($m=1;$m<=60;$m++)
                                                                         {
                                                                            $sn='sellname'.$m;
                                                                            if(!empty($$sn) and !empty($$sn))
                                                                            {
                                                                                echo '<option  value="'.$$sn.'">'.$$sn.'</option>';
                                                                            }
                                                                         }

                                                                         if(!empty($sellname) and !empty($sellername))
                                                                        {
                                                                           echo '<option  value="'.$sellname.'">'.$sellname.'</option>'; 
                                                                        }
                                                                        else
                                                                        {
                                                                            echo '<option  value="'.$oemname.'">'.$oemname.'</option>';
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                ?>
                                    </select>
                                 </div>

                                       



                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >OEM Quotation </label>
                                          <input type="file" class="form-control" name="uplfiles[]"  placeholder="OEM Quotation" >
                                       </div>
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button" onClick="addMoreMultiVend();">

                                          <span class="icon md-plus" ></span>
                                          </button>
                                          <button class="btn btn-danger btn-add-basic" type="button" onClick="deleteRowMultiVend();">

                                          <span class="icon md-minus" ></span>
                                          </button>
                                          <input type="checkbox" name="item_index[]" />
                                          </span>
                                       </div>
                                    </div>

                                 </div>


                                

    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <link rel="stylesheet" href="../../assets/global/vendor/select2/select2.css">
    
    
    <script src="../../assets/global/vendor/select2/select2.full.min.js"></script>

<script src="../../assets/global/js/Plugin/select2.js"></script> 
    
    <!-- Plugins -->     
        
        <script src="../../assets/global/vendor/clockpicker/bootstrap-clockpicker.min.js"></script>
        
        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="../../assets/global/vendor/timepicker/jquery.timepicker.min.js"></script>
        <script src="../../assets/global/vendor/datepair/datepair.min.js"></script>
        <script src="../../assets/global/vendor/datepair/jquery.datepair.min.js"></script>
        
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    
        
        <script src="../../assets/global/js/Plugin/bootstrap-datepicker.js"></script>
        <script src="../../assets/global/js/Plugin/jt-timepicker.js"></script>
        <script src="../../assets/global/js/Plugin/datepair.js"></script>
        
    
        <script src="../../assets/examples/js/forms/advanced.js"></script>