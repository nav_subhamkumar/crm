<div class="site-menubar">
   <ul class="site-menu">
      <li class="site-menu-item ">
         <a class="animsition-link" href="dashboard.php">
         <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
         <span class="site-menu-title">Dashboard</span>
         </a>
      </li>
      <li class="site-menu-item ">
         <a class="animsition-link" href="proscustomers.php">
         <i class="site-menu-icon md-accounts" aria-hidden="true"></i>
         <span class="site-menu-title">Customers</span>
         </a>
      </li>
      <li class="site-menu-item has-sub">
         <a href="javascript:void(0)">
         <i class="site-menu-icon md-accounts" aria-hidden="true"></i>
         <span class="site-menu-title">Prospects</span>
         <span class="site-menu-arrow"></span>
         </a>
         <ul class="site-menu-sub">
            <li class="site-menu-item">
               <a class="animsition-link" href="prospectdashboard.php">
               <span class="site-menu-title">Dashboard</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="addcust.php">
               <span class="site-menu-title">Add Prospect</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="addprospectprofile.php">
               <span class="site-menu-title">Add Prospect Profile</span>
               </a>
            </li>
            
            
         </ul>
      </li>
      <li class="site-menu-item has-sub">
         <a href="javascript:void(0)">
         <i class="site-menu-icon md-input-antenna" aria-hidden="true"></i>
         <span class="site-menu-title">Campaign</span>
         <span class="site-menu-arrow"></span>
         </a>
         <ul class="site-menu-sub">
            <li class="site-menu-item">
               <a class="animsition-link" href="campaigndashboard.php">
               <span class="site-menu-title">Dashboard</span>
               </a>
            </li>
            
            <li class="site-menu-item">
               <a class="animsition-link" href="direct.php">
               <span class="site-menu-title">Direct Campaign</span>
               </a>
            </li>
            <li class="site-menu-item has-sub">
               <a href="javascript:void(0)">
               <span class="site-menu-title">Digital Campaign</span>
               <span class="site-menu-arrow"></span>
               </a>
               <ul class="site-menu-sub">
                  <li class="site-menu-item">
                     <a class="animsition-link" href="digital.php">
                     <span class="site-menu-title">Insert</span>
                     </a>
                  </li>
                  <li class="site-menu-item">
                     <a class="animsition-link" href="digmailchimpdash.php">
                     <span class="site-menu-title">MailChimp</span>
                     </a>
                  </li>
               </ul>
            </li>
            <li class="site-menu-item has-sub">
               <a href="javascript:void(0)">
               <span class="site-menu-title">Telecalling Campaign</span>
               <span class="site-menu-arrow"></span>
               </a>
               <ul class="site-menu-sub">
                  <li class="site-menu-item">
                     <a class="animsition-link" href="telecalling.php">
                     <span class="site-menu-title">Insert</span>
                     </a>
                  </li>
                  <li class="site-menu-item">
                     <a class="animsition-link" href="mailchimp-call.php">
                     <span class="site-menu-title">MailChimps</span>
                     </a>
                  </li>
               </ul>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="reference.php">
               <span class="site-menu-title">Reference Campaign</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="create-campaign.php">
               <span class="site-menu-title">Create Campaign</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="campcust.php">
               <span class="site-menu-title">Update Campaign</span>
               </a>
            </li>
            
         </ul>
      </li>
      <li class="site-menu-item ">
         <a class="animsition-link" href="lead.php">
         <i class="site-menu-icon md-accounts" aria-hidden="true"></i>
         <span class="site-menu-title">Lead</span>
         </a>
      </li>

      <li class="site-menu-item has-sub">
         <a href="javascript:void(0)">
         <i class="site-menu-icon md-accounts-alt" aria-hidden="true"></i>
         <span class="site-menu-title">Meeting</span>
         <span class="site-menu-arrow"></span>
         </a>
         <ul class="site-menu-sub">
            <li class="site-menu-item">
               <a class="animsition-link" href="meetingdash.php">
               <span class="site-menu-title">Dashboard</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="meeting-ins.php">
               <span class="site-menu-title">Add</span>
               </a>
            </li>
            
         </ul>
      </li>
      <li class="site-menu-item has-sub">
         <a href="javascript:void(0)">
         <i class="site-menu-icon md-wifi-alt-2" aria-hidden="true"></i>
         <span class="site-menu-title">Funnel</span>
         <span class="site-menu-arrow"></span>
         </a>
         <ul class="site-menu-sub">
            <li class="site-menu-item">
               <a class="animsition-link" href="funneldash.php">
               <span class="site-menu-title">Dashboard</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="funnel.php">
               <span class="site-menu-title">Update</span>
               </a>
            </li>
            <!-- <li class="site-menu-item">
               <a class="animsition-link" href="funnel-add.php">
               <span class="site-menu-title">Add</span>
               </a>
            </li> -->
            
         </ul>
      </li>
      <li class="site-menu-item has-sub">
         <a href="javascript:void(0)">
         <i class="site-menu-icon md-chart" aria-hidden="true"></i>
         <span class="site-menu-title">Order</span>
         <span class="site-menu-arrow"></span>
         </a>
         <ul class="site-menu-sub">
            <li class="site-menu-item has-sub">
               <a href="javascript:void(0)">
               <span class="site-menu-title">Quotation</span>
               <span class="site-menu-arrow"></span>
               </a>
               <ul class="site-menu-sub">
                  <li class="site-menu-item">
                     <a class="animsition-link" href="quotdash.php">
                     <span class="site-menu-title">Dashboard</span>
                     </a>
                  </li>
                  <li class="site-menu-item">
                     <a class="animsition-link" href="quotgen.php">
                     <span class="site-menu-title">Generate</span>
                     </a>
                  </li>
               </ul>
            </li>
            <li class="site-menu-item has-sub">
               <a href="javascript:void(0)">
               <span class="site-menu-title">OPF</span>
               <span class="site-menu-arrow"></span>
               </a>
               <ul class="site-menu-sub">
                  <li class="site-menu-item">
                     <a class="animsition-link" href="opfdash.php">
                     <span class="site-menu-title">Dashboard</span>
                     </a>
                  </li>
                  <li class="site-menu-item">
                     <a class="animsition-link" href="opfgen.php">
                     <span class="site-menu-title">Generate</span>
                     </a>
                  </li>
               </ul>
            </li>
            <li class="site-menu-item has-sub">
               <a href="javascript:void(0)">
               <span class="site-menu-title">Invoice</span>
               <span class="site-menu-arrow"></span>
               </a>
               <ul class="site-menu-sub">
                  <li class="site-menu-item">
                     <a class="animsition-link" href="#">
                     <span class="site-menu-title">Dashboard</span>
                     </a>
                  </li>
                  <li class="site-menu-item">
                     <a class="animsition-link" href="#">
                     <span class="site-menu-title">Generate</span>
                     </a>
                  </li>
               </ul>
            </li>
         </ul>
      </li>
      <li class="site-menu-item has-sub">
         <a href="javascript:void(0)">
         <i class="site-menu-icon md-star-circle" aria-hidden="true"></i>
         <span class="site-menu-title">Target vs Achievement</span>
         <span class="site-menu-arrow"></span>
         </a>
         <ul class="site-menu-sub">
            <li class="site-menu-item">
               <a class="animsition-link" href="achievement.php">
               <span class="site-menu-title">Achievement</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="targetset.php">
               <span class="site-menu-title">Set Target</span>
               </a>
            </li>
            
         </ul>
      </li>
      <li class="site-menu-item has-sub">
         <a href="javascript:void(0)">
         <i class="site-menu-icon md-mall" aria-hidden="true"></i>
         <span class="site-menu-title">Products</span>
         <span class="site-menu-arrow"></span>
         </a>
         <ul class="site-menu-sub">
            <li class="site-menu-item">
               <a class="animsition-link" href="proddash.php">
               <span class="site-menu-title">Dashboard</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="addprod.php">
               <span class="site-menu-title">Add</span>
               </a>
            </li>
            
         </ul>
      </li>
      <li class="site-menu-item has-sub">
         <a href="javascript:void(0)">
         <i class="site-menu-icon md-accounts-list" aria-hidden="true"></i>
         <span class="site-menu-title">HR</span>
         <span class="site-menu-arrow"></span>
         </a>
         <ul class="site-menu-sub">
            
            <li class="site-menu-item has-sub">
               <a href="javascript:void(0)">
               <span class="site-menu-title">Employee</span>
               <span class="site-menu-arrow"></span>
               </a>
               <ul class="site-menu-sub">
                  <li class="site-menu-item">
                     <a class="animsition-link" href="emplist.php">
                     <span class="site-menu-title">Employee List</span>
                     </a>
                  </li>
                  <li class="site-menu-item">
                     <a class="animsition-link" href="emptat.php">
                     <span class="site-menu-title">Employee TAT</span>
                     </a>
                  </li>
                  <li class="site-menu-item">
                     <a class="animsition-link" href="addemp.php">
                     <span class="site-menu-title">Add</span>
                     </a>
                  </li>
                  <li class="site-menu-item">
                     <a class="animsition-link" href="loginhistory.php">
                     <span class="site-menu-title">Login History</span>
                     </a>
                  </li>
                  <li class="site-menu-item">
                     <a class="animsition-link" href="eattendance-upload.php">
                     <span class="site-menu-title">Upload Attendance</span>
                     </a>
                  </li>
               </ul>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="holidaylist.php">
               <span class="site-menu-title">Holiday</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="leavedash.php">
               <span class="site-menu-title">Leave Applications</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="incentive.php">
               <span class="site-menu-title">Incentive</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="sendnotice.php">
               <span class="site-menu-title">Notice</span>
               </a>
            </li>
         </ul>
      </li>
      <li class="site-menu-item has-sub">
         <a href="javascript:void(0)">
         <i class="site-menu-icon md-settings-square" aria-hidden="true"></i>
         <span class="site-menu-title">Settings</span>
         <span class="site-menu-arrow"></span>
         </a>
         <ul class="site-menu-sub">
            <li class="site-menu-item">
               <a class="animsition-link" href="get-started-upload.php">
               <span class="site-menu-title">Get Started</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="changepros.php">
               <span class="site-menu-title">Prospects</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="changecamp.php">
               <span class="site-menu-title">Campaign</span>
               </a>
            </li>
            <li class="site-menu-item">
               <a class="animsition-link" href="changeorder.php">
               <span class="site-menu-title">Order</span>
               </a>
            </li>
            <li class="site-menu-item has-sub">
               <a href="javascript:void(0)">
               <span class="site-menu-title">HR</span>
               <span class="site-menu-arrow"></span>
               </a>
               <ul class="site-menu-sub">
                  <li class="site-menu-item">
                     <a class="animsition-link" href="changeinc.php">
                     <span class="site-menu-title">Incentive</span>
                     </a>
                  </li>
               </ul>
            </li>
            
            
         </ul>
      </li>
   </ul>
</div>

<!-- <div class="site-gridmenu">
      <div>
        <div>
          <ul>
            <li>
              <a href="apps/mailbox/mailbox.html">
                <i class="icon md-email"></i>
                <span>Mailbox</span>
              </a>
            </li>
            <li>
              <a href="apps/calendar/calendar.html">
                <i class="icon md-calendar"></i>
                <span>Calendar</span>
              </a>
            </li>
            <li>
              <a href="apps/contacts/contacts.html">
                <i class="icon md-account"></i>
                <span>Contacts</span>
              </a>
            </li>
            <li>
              <a href="apps/media/overview.html">
                <i class="icon md-videocam"></i>
                <span>Media</span>
              </a>
            </li>
            <li>
              <a href="apps/documents/categories.html">
                <i class="icon md-receipt"></i>
                <span>Documents</span>
              </a>
            </li>
            <li>
              <a href="apps/projects/projects.html">
                <i class="icon md-image"></i>
                <span>Project</span>
              </a>
            </li>
            <li>
              <a href="apps/forum/forum.html">
                <i class="icon md-comments"></i>
                <span>Forum</span>
              </a>
            </li>
            <li>
              <a href="index.html">
                <i class="icon md-view-dashboard"></i>
                <span>Dashboard</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div> -->
