<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title> Direct Campaign Search | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
      <link rel="stylesheet" href="../../assets/css/site.min.css">
      <!-- Plugins -->
      <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
      <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
      <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
      <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
      <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
      <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
      <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
      <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
      <!-- Fonts -->
      <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
      <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
      <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- table set -->
      <link rel='stylesheet' href='../../assets/css/datatableset.css'>
      <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>

      <link rel='stylesheet' href='../../assets/css/customised-crm.css'>

      <?php include "includes/css/select.php" ?>
      <?php include "includes/css/tables.php" ?>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
    
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <!-- write body content here -->

      <div class="page-content">
      
        <!-- Panel Select 2 -->
        <div class="panel">
               <div class="panel-body container-fluid">
                  <div class="row row-lg">
                     <div class="col-md-12">
                        <div class="example-wrap">
                           <h4 class="example-title">Direct Campaign Search</h4>
                           <div class="example">
                              <form action="" method="post" autocomplete="off">
                                 <div class="row">
                                    <div class="form-group  col-md-6">
                                       <label class="form-control-label" >Project</label>
                                       <select name="project" class="form-control " data-plugin="select2" required="required">
                                          <?php
                                            $sector=implode(',', $_POST['sector']);
                                            $pro=$_POST['project'];
                                            $noofemployees=$_POST['noofemployees'];
                                            $location=$_POST['location'];

                                                $project=mysqli_query($dbc,"select * from `campaign-projects`  ");
                                                echo '<option selected value="'.$pro.'" >'.$pro.'</option>';
                                                while($row=mysqli_fetch_assoc($project))
                                                {
                                                    //$pro=$row['ProjectName'];
                            
                                                    echo '<option  value="'.$row['ProjectName'].'">'.$row['ProjectName'].'</option>';
                                    
                                                }
                                          ?>
                      
                                          </select>
                                    </div>
                                    <div class="form-group  col-md-6">
                                       <label class="form-control-label" >Sector</label>
                                       <select name="sector[]" class="form-control " multiple data-plugin="select2">
                                                                    <?php

                                                                    $comp=$_POST['c'];
                                                                    $project=mysqli_query($dbc,"select distinct(SectorName) from `sector`  ");
                                                                    
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                        //$pro=$row['ProjectName'];


                                                        
                                                                         echo '<option  value="'.$row['SectorName'].'">'.$row['SectorName'].'</option>';
                                                                        
                                                                    }
                                                                   ?>
                      
                                    </select>
                                    </div>
                                    </div>

                                    <div class="row">
                                    <div class="form-group  col-md-6">
                                        <label class="form-control-label" >No. of Employees</label>
                                        <select name="noofemployees" class="form-control maxlength" data-plugin="select2">
                                          <option value="" >Select</option>
                                          <option value="0-50">0-50</option>
                                          <option value="51-250">51-250</option>
                                          <option value="251-500">251-500</option>
                                          <option value="501-1000">501-1000</option>
                                          <option value="Above 1000">Above 1000</option>
                      
                                        </select>
                                    </div>
                                    <div class="form-group  col-md-6">
                                       <label class="form-control-label" >Location</label>
                                       <select name="location" class="form-control " data-plugin="select2">
                                          <?php
                                            $project=mysqli_query($dbc,"select distinct(SubLocation) from `customers` where `RMail`='$id'");
                                            echo '<option value="" >Select</option>';
                                            while($row=mysqli_fetch_assoc($project))
                                            {
                                                //$pro=$row['ProjectName'];
                            
                                                echo '<option  value="'.$row['SubLocation'].'">'.$row['SubLocation'].'</option>';
                                    
                                            }
                                          ?>
                      
                                        </select>
                                    </div>
                                    </div>
                                    <div class="form-group ">
                                       <!-- <input type="submit" name="upload_submit" value="Submit" class="btn btn-success" > -->
                                       <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                    </form>
                                 </div>
                              
                           </div>
                        </div>
                        <!-- End Example Basic Form -->
                     </div>
                  </div>
               </div>


            
            <!-- panel end -->
        </div>
  
<div class="page-content" style="margin-top: -40px">
<!-- Panel Basic -->
        <div class="panel">
          <header class="panel-heading">
            
            <h3 class="panel-title font-size-18">Customer List</h3>
          </header>
          <div class="panel-body">
           
              <?php
                    /*$fetdetail=mysqli_query($dbc,"select * from `$pro` where `Sector`='$item' or SubLocation='$location' or NoOfEmployees='$noofemployees' ");*/

                    echo '<table class="table table-striped table-responsive table-bordered" id="example">';
                      echo '<thead>';
                        echo '<tr>';
                          echo '<th>Sl No.</th>';
                          echo '<th>Company</th>';
                                                    echo '<th>Sector</th>';
                                                    echo '<th>Contact-Person</th>';
                                                    echo '<th>Designation</th>';
                                                    echo '<th>Department</th>';
                                                    echo '<th>Mobile-No</th>';
                                                    echo '<th>Mail-ID</th>';
                                                    echo '<th>Address</th>';
                                                    echo '<th>Location</th>';
                                                    echo '<th>Sub-Location</th>';
                                                    echo '<th>No Of Employees</th>';
                                                    echo '<th>CompanyType</th>';
                                                    echo '<th>Profile</th>';
                                                    echo '<th>Update</th>';
                                                    echo '<th>Remove</th>';
                        echo '</tr>';
                      echo '</thead>';
                      echo '<tbody>';
                      if(isset($_POST['submit']))
                  {
                    
                    $sector=implode(',', $_POST['sector']);
                    $pro=$_POST['project'];
                    $noofemployees=$_POST['noofemployees'];
                    $location=$_POST['location'];
                    $array =  explode(',', $sector);


                    foreach ($array as $item) {

                    if(!empty($item) and !empty($noofemployees) and !empty($location) )
                    {
                      $fetdetail=mysqli_query($dbc,"select distinct(Company),`Sector`,`SubSector`,`FirstName`,`LastName`,`Level`,`Dept`,`Designation`,`Mobile`,`Mail`,`ContactPerson2`,`ContactNumber`,`EmailID`,`Url`,`Address`,`Location`,`SubLocation`,`City`,`Pin`,`State`,`StdCode`,`LandlineNo`,`FaxNo`,`NoOfEmployees`,`CompanyType`,`GSTNo` from `$pro` where  `Sector`='$item' and SubLocation='$location' and NoOfEmployees='$noofemployees' ");

                    }
                    elseif (!empty($item) and !empty($noofemployees) and empty($location)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select distinct(Company),`Sector`,`SubSector`,`FirstName`,`LastName`,`Level`,`Dept`,`Designation`,`Mobile`,`Mail`,`ContactPerson2`,`ContactNumber`,`EmailID`,`Url`,`Address`,`Location`,`SubLocation`,`City`,`Pin`,`State`,`StdCode`,`LandlineNo`,`FaxNo`,`NoOfEmployees`,`CompanyType`,`GSTNo` from `$pro` where  `Sector`='$item' and NoOfEmployees='$noofemployees'  ");
                    }
                    elseif ( !empty($item) and !empty($location) and empty($noofemployees)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select distinct(Company),`Sector`,`SubSector`,`FirstName`,`LastName`,`Level`,`Dept`,`Designation`,`Mobile`,`Mail`,`ContactPerson2`,`ContactNumber`,`EmailID`,`Url`,`Address`,`Location`,`SubLocation`,`City`,`Pin`,`State`,`StdCode`,`LandlineNo`,`FaxNo`,`NoOfEmployees`,`CompanyType`,`GSTNo` from `$pro` where  `Sector`='$item' and SubLocation='$location'  ");
                    }
                    elseif ( empty($item) and !empty($location) and !empty($noofemployees)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select distinct(Company),`Sector`,`SubSector`,`FirstName`,`LastName`,`Level`,`Dept`,`Designation`,`Mobile`,`Mail`,`ContactPerson2`,`ContactNumber`,`EmailID`,`Url`,`Address`,`Location`,`SubLocation`,`City`,`Pin`,`State`,`StdCode`,`LandlineNo`,`FaxNo`,`NoOfEmployees`,`CompanyType`,`GSTNo` from `$pro` where  NoOfEmployees='$noofemployees' and SubLocation='$location'  ");
                    }
                    elseif (!empty($noofemployees)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select distinct(Company),`Sector`,`SubSector`,`FirstName`,`LastName`,`Level`,`Dept`,`Designation`,`Mobile`,`Mail`,`ContactPerson2`,`ContactNumber`,`EmailID`,`Url`,`Address`,`Location`,`SubLocation`,`City`,`Pin`,`State`,`StdCode`,`LandlineNo`,`FaxNo`,`NoOfEmployees`,`CompanyType`,`GSTNo` from `$pro` where  NoOfEmployees='$noofemployees'   ");
                    }
                    elseif (!empty($location)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select distinct(Company),`Sector`,`SubSector`,`FirstName`,`LastName`,`Level`,`Dept`,`Designation`,`Mobile`,`Mail`,`ContactPerson2`,`ContactNumber`,`EmailID`,`Url`,`Address`,`Location`,`SubLocation`,`City`,`Pin`,`State`,`StdCode`,`LandlineNo`,`FaxNo`,`NoOfEmployees`,`CompanyType`,`GSTNo` from `$pro` where  SubLocation='$location'   ");
                    }
                    elseif (!empty($item)) 
                    {
                      $fetdetail=mysqli_query($dbc,"select distinct(Company),`Sector`,`SubSector`,`FirstName`,`LastName`,`Level`,`Dept`,`Designation`,`Mobile`,`Mail`,`ContactPerson2`,`ContactNumber`,`EmailID`,`Url`,`Address`,`Location`,`SubLocation`,`City`,`Pin`,`State`,`StdCode`,`LandlineNo`,`FaxNo`,`NoOfEmployees`,`CompanyType`,`GSTNo` from `$pro` where `RMail`='$id' and Sector='$item' ");
                    }
                    /*elseif (!empty($department1) or !empty($companytype1) or !empty($noofemployees1) or !empty($location1) )
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location'  or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");
                    }
                    elseif (empty($department1) or empty($companytype1) or empty($noofemployees1) or empty($location1) )
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location'  or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");
                    }
                    else
                    {
                      $fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location' or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");

                    }*/
                    
                    //$fetdetail=mysqli_query($dbc,"select * from `customers` where `Sector`='$item'  or SubLocation='$location' or Designation='$designation' or Dept='$department' or CompanyType='$companytype' or NoOfEmployees='$noofemployees' ");


                    
                    $count=0;
                        while($frow=mysqli_fetch_assoc($fetdetail))
                        {
                          global $count;
                          $count=$count+1;
                                                    $cid=$frow['cid'];
                          $company=$frow['Company'];
                                                    $sector=$frow['Sector'];
                                                    $firstname=$frow['FirstName'];
                                                    $lastname=$frow['LastName'];
                                                    $designation=$frow['Designation'];
                                                    $department=$frow['Dept'];
                                                    $mobile=$frow['Mobile'];
                                                    $mailid=$frow['Mail'];
                                                    $address=$frow['Address'];
                                                    $location=$frow['Location'];
                                                    $sublocation=$frow['SubLocation'];
                                                    $noofemployees=$frow['NoOfEmployees'];
                                                    $companytype=$frow['CompanyType'];
                          
                          
                            echo '<tr>';
                            echo "<td>".$count."</td>";
                                                        /*echo "<td>{$frow['Company']}</td>";*/
                                                        echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                      <a href="campins.php?c='.$company.'&f='.$firstnamename.'&l='.$lastname.'&e='.$mailid.'&t='.$pro.'&ctype=Direct" >Campaign</a>
                                                                        <a href="addprospectprofile.php?c='.$company.'">Add Profile</a>
                                                                        <a href="prosprofdash.php?u='.$company.'&v='.$cid.'">View Profile</a>
                                                                        
                                                                    </div>
                                                                </div></td>';
                                                        echo "<td>{$frow['Sector']}</td>";
                                                        echo "<td>{$frow['FirstName']} {$frow['LastName']}</td>";
                                                        echo "<td>{$frow['Designation']}</td>";
                                                        echo "<td>{$frow['Dept']}</td>";
                                                        echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                        echo "<td>{$frow['Mail']}</td>";
                                                        /*echo "<td>{$frow['Address']}</td>";*/
                                                        echo '<td> <span class="more">'.$address.'</span></td>';
                                                        echo "<td>{$frow['Location']}</td>";
                                                        echo "<td>{$frow['SubLocation']}</td>";
                                                        echo "<td>{$frow['NoOfEmployees']}</td>";
                                                        echo "<td>{$frow['CompanyType']}</td>";
                                                        echo "<td><a  href='prosprofdash.php?u=$company&v=$cid' class='text-muted text-uppercase btn btn-info'  ><font color='white'>Profile</font></a></td>";
                                                        echo "<td><a  href='editcust.php?u=$cid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        echo "<td><a  href='../auth/customer/delcust.php?u=$cid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";

                                                          echo "</tr>";
                            
                          }
                        } 
                      }
                      echo '</tbody>';
                    echo '</table>';
                  ?>
          
          </div>
        </div>
        <!-- End Panel Basic -->
      </div>

  </div>
    <!-- End Page -->


     <!-- Footer -->
    <footer class="site-footer">
      <div class="site-footer-legal">© 2018 <a href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a></div>
      <div class="site-footer-right">
        Crafted with <i class="red-600 icon md-favorite"></i> by <a href="https://themeforest.net/user/creation-studio">Creation Studio</a>
      </div>
    </footer>
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
      <script src="../../assets/global/vendor/jquery/jquery.js"></script>
      <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
      <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
      <script src="../../assets/global/vendor/animsition/animsition.js"></script>
      <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
      <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
      <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
      <script src="../../assets/global/vendor/waves/waves.js"></script>
      <!-- Plugins -->
      <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
      <script src="../../assets/global/vendor/switchery/switchery.js"></script>
      <script src="../../assets/global/vendor/intro-js/intro.js"></script>
      <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
      <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
      <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
      <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
      <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
      <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
      <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
      <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
      <!-- Scripts -->
      <script src="../../assets/global/js/Component.js"></script>
      <script src="../../assets/global/js/Plugin.js"></script>
      <script src="../../assets/global/js/Base.js"></script>
      <script src="../../assets/global/js/Config.js"></script>
      <script src="section/Menubar.js"></script>
      <script src="section/Sidebar.js"></script>
      <script src="section/PageAside.js"></script>
      <script src="section/GridMenu.js"></script>
      <!-- Config -->
      <script src="../../assets/global/js/config/colors.js"></script>
      <script src="../../assets/js/config/tour.js"></script>
      <script>Config.set('assets', '../assets');</script>
      <!-- Page -->
      <script src="../../assets/js/Site.js"></script>
      <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
      <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
      <script src="../../assets/global/js/Plugin/switchery.js"></script>
      <script src="../../assets/global/js/Plugin/matchheight.js"></script>
      <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
      <script src="../../assets/global/js/Plugin/peity.js"></script>
      <script src="../../assets/examples/js/dashboard/v1.js"></script>
      <?php include "includes/js/select.php" ?>
      <script src="../../assets/global/js/Plugin/datatables.js"></script>
    
        <script src="../../assets/examples/js/tables/datatable.js"></script>



        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script >
          $(document).ready(function() {
    $('#example').DataTable();
} );
        </script>


    
  <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>
