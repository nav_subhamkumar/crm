<?php
   include "session_handler.php";
?>
<?php
  
   $uid=null;
   $uid=$_GET['u'];
   $v=$_GET['v'];
   $vfd=$_GET['vfd'];
   
   ?>

   
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Dashboard | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    

    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
     <link rel="stylesheet" href="../../assets/global/vendor/multi-select/multi-select.css">
      <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-select/bootstrap-select.css">
           <link rel="stylesheet" href="../../assets/global/vendor/select2/select2.css">
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>

      <link rel="stylesheet" href="../../assets/css/customised-crm.css">

        <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
          <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
         
                  <div class="col-xl-4 col-md-6" data-target="#resourcewisemodal" data-toggle="modal">
            <!-- Widget Linearea One-->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-account grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Resource Wise Review
                  </div>
  
                </div>
               
                
              </div>
            </div>
            <!-- End Widget Linearea One -->
          </div>

          <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="resourcewisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Resource Wise Review</h4>
                          </div>
                          <div class="modal-body">
                            <form action="" method="get" novalidate="novalidate" autocomplete="off" >
                              <div>
                             <label class="form-control-label" >Department:</label>
                                    <select class="form-control" id="Rankd" name="Rank"  data-plugin="select2" required="required" >
                                     <option value="">Select Department</option>
                                              <option value="sales">Sales</option>
                                              <option value="technical">Technical</option>
                                    </select>
                                    </div>
                                      <div class="containersd">
                                             <div class="sales">
                                                <label class="form-control-label" >Employee:</label>
                                                <div>
                                                   <select name="project"   data-plugin="select2" class="form-control" onchange="redirect(this.value);" required >
                                                  <?php
                                                  $project=mysqli_query($dbc,"select * from `team` where `Department`='Sales'");
                                                  echo '<option >Select</option>';
                                                  while($row=mysqli_fetch_assoc($project))
                                                  {
                                                 //$pro=$row['ProjectName'];
                                             
                                                  echo '<option  value="'.$row['email'].'">'.$row['name'].'</option>';
                                                 
                                                 }
                                                 ?>
                                                </select>
                                                </div>
                                             </div>
                                             <div class="technical">
                                                <label class="form-control-label" >Employee:</label>
                                                <div>
                                                   <select class="form-control" data-plugin="select2"  name="project" id="ms_example6" onchange="redirect(this.value);" required >
                                                  <?php
                                                    $project=mysqli_query($dbc,"select * from `team` where `Department`='Technical'");
                                                    echo '<option >Select</option>';
                                                  while($row=mysqli_fetch_assoc($project))
                                                  {
                                                    //$pro=$row['ProjectName'];
                                             
                                                  echo '<option  value="'.$row['email'].'">'.$row['name'].'</option>';
                                                 
                                                  }
                                                  ?>
                                                  </select>
                                                </div>
                                             </div>
                                          </div>
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


          <div class="col-xl-4 col-md-6" data-target="#periodwisemodal" data-toggle="modal">
            <!-- Widget Linearea Two -->
            <div class="card card-shadow" id="widgetLineareaTwo">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-flash grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Period Wise Review
                  </div>
                 
                </div>
              </div>
            </div>
            <!-- End Widget Linearea Two -->
          </div>

          <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="periodwisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Period Wise Review</h4>
                          </div>
                          <div class="modal-body">
                             <form action="" method="get" class="form-horizontal" novalidate="novalidate"  autocomplete="off">
                              <div class="form-group">
                                  <label class="form-control-label" >Review Period:</label>
                                 <div>
                                    <select size="1"  data-plugin="select2" class="form-control Ranka populate" title="" name="Rank">
                                       <option value="Annual">Select</option>
                                       <option value="ryear">Yearly</option>
                                       <option value="rhalfyear">Half Yearly</option>
                                       <option value="rquater">Quarterly</option>
                                       <option value="amonth">Monthly</option>
                                       <!-- <option value="rweek">Weekly</option> -->
                                       <option value="adate">Date Range</option>
                                    </select>
                                 </div>
                                 <div class="containers">
                                    <div class="ryear">
                                       <label class="form-control-label">Year:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project"  id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <?php
                                                $curyear=date('Y');
                                                
                                                ?>
                                             <option value="<?php echo $curyear; ?>"><?php echo $curyear; ?>-<?php echo $curyear+1; ?></option>
                                             <option value="<?php echo $curyear-1; ?>"><?php echo $curyear-1; ?>-<?php echo $curyear; ?></option>
                                             <option value="<?php echo $curyear-2; ?>"><?php echo $curyear-2; ?>-<?php echo $curyear-1; ?></option>
                                             <option value="<?php echo $curyear-3; ?>"><?php echo $curyear-3; ?>-<?php echo $curyear-2; ?></option>
                                             <option value="<?php echo $curyear-4; ?>"><?php echo $curyear-4; ?>-<?php echo $curyear-3; ?></option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rhalfyear">
                                        <label class="form-control-label">Half Year:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <option value="1sty">1st</option>
                                             <option value="2ndy">2nd</option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rquater">
                                      
                                        <label class="form-control-label">Quarter:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <option value="1stq">1st</option>
                                             <option value="2ndq">2nd</option>
                                             <option value="3rdq">3rd</option>
                                             <option value="4thq">4th</option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rweek">
                                        <label class="form-control-label">Week:</label>
                                       <div>
                                          <select name="project"  data-plugin="select2" class="form-control populate" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <?php
                                                $val=1;
                                                while($val<53)
                                                {
                                                   echo '<option value="'.$val.'">'.$val.'</option>'; 
                                                   $val=$val+1;
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="amonth">
                                        <label class="form-control-label">Month:</label>
                                       <div>
                                          <select name="project"  data-plugin="select2" class="form-control populate" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <option value="04">Apr</option>
                                             <option value="05">May</option>
                                             <option value="06">Jun</option>
                                             <option value="07">Jul</option>
                                             <option value="08">Aug</option>
                                             <option value="09">Sep</option>
                                             <option value="10">Oct</option>
                                             <option value="11">Nov</option>
                                             <option value="12">Dec</option>
                                             <option value="01">Jan</option>
                                             <option value="02">Feb</option>
                                             <option value="03">Mar</option>
                                          </select>
                                       </div>
                                    </div>
                                    <!-- </div>
                                       <div class="container"> -->
                                    <div class="adate">
                                       <label class="form-control-label">Date:</label>
                                       <div>
                                          <input type="text" name="fdate" data-plugin="datepicker" class="form-control" onchange="redirectfd(this.value);" placeholder="From Date" required >
                                          <br>
                                          <input type="text" name="tdate" data-plugin="datepicker" class="form-control" onchange="redirectat(this.value);" placeholder="To Date" required >
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </form>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
          <div class="col-xl-4 col-md-6" data-toggle="modal">
            <!-- Widget Linearea Two -->
            <div class="card card-shadow" id="widgetLineareaTwo">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-account grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    <?php 
                                    if($uid == null)
                                    {
                                        echo "<b>Profile:</b> &nbsp;Company";
                                    }
                                    else
                                    {
                                    $fetname=mysqli_query($dbc,"select * from team where email='$uid' ");
                                    while($rwnm=mysqli_fetch_assoc($fetname))
                                    {
                                        $name=$rwnm['name'];
                                    }
                                    echo "<b>Profile:</b> ".$name; 
                                    }?> 
                  </div>
                 
                </div>
              </div>
            </div>
            <!-- End Widget Linearea Two -->
          </div>

          <div class="col-xl-12 col-md-12" data-target="#resourcewisemodal" data-toggle="modal">
            <!-- Widget Linearea One-->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 py-10">
                    <i class="grey-600 font-size-20 vertical-align-bottom mr-5"></i>     <center>           <b>    Target Vs Achievement</b></center>
                  </div>
  
                </div>
               
                
              </div>
            </div>
            <!-- End Widget Linearea One -->
          </div>





           <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-input-antenna grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Revenue Per Year
                  </div>
                   <?php 
                                $county=0;
                                 $fannual=mysqli_query($dbc,"select distinct(EmployeeMail),RevenuePerYear from `target_manage_resource` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                 while($mont=mysqli_fetch_assoc($fannual))
                                 {
                                  /*$val=$mont['ModificationDetail'];
                                  $fetmon=date("y", strtotime($val));

                                  $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                  $rc=mysqli_fetch_assoc($cust);
                                  $res=$rc['count_column'];*/
                                  $val=$mont['RevenuePerYear'];
                                  global $county;
                                  $county=$county+$val;

                                 }
                                
                              ?>
                              <?php 
                                $countya=0;
                                 $fannual=mysqli_query($dbc,"select * from `funnel` where `Stage`='Won' AND   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                 while($mont=mysqli_fetch_assoc($fannual))
                                 {
                                  /*$val=$mont['ModificationDetail'];
                                  $fetmon=date("y", strtotime($val));

                                  $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                  $rc=mysqli_fetch_assoc($cust);
                                  $res=$rc['count_column'];*/
                                  $val=$mont['Revenue'];
                                  global $countya;
                                  $countya=$countya+$val;

                                 }
                                
                              ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

           


            <!-- End Widget Linearea Four -->
          </div>
        
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Revenue Per Month
                  </div>
                  <?php 
                                $county=0;
                                 $fannualm=mysqli_query($dbc,"select distinct(EmployeeMail),RevenuePerMonth from `target_manage_resource` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                 while($mont=mysqli_fetch_assoc($fannualm))
                                 {
                                  /*$val=$mont['ModificationDetail'];
                                  $fetmon=date("y", strtotime($val));

                                  $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                  $rc=mysqli_fetch_assoc($cust);
                                  $res=$rc['count_column'];*/
                                  $val=$mont['RevenuePerMonth'];
                                  global $county;
                                  $county=$county+$val;

                                 }
                                
                              ?>
                              <?php 
                                $countya=0;
                                 $fannual=mysqli_query($dbc,"select * from `funnel` where `Stage`='Won' AND  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                 while($mont=mysqli_fetch_assoc($fannual))
                                 {
                                  /*$val=$mont['ModificationDetail'];
                                  $fetmon=date("y", strtotime($val));

                                  $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                  $rc=mysqli_fetch_assoc($cust);
                                  $res=$rc['count_column'];*/
                                  $val=$mont['Revenue'];
                                  global $countya;
                                  $countya=$countya+$val;

                                 }
                                
                              ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

             





            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-wifi-alt-2 grey-600 font-size-24 vertical-align-bottom mr-5"></i>                     
Margin Per Year
                  </div>
                  <?php 
                                                                $county=0;
                                                                 $fannual=mysqli_query($dbc,"select * from `target_manage_resource` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['MarginPerYear'];
                                                                    /*global $county;*/
                                                                    $county=/*$county+*/$val;

                                                                 }
                                                                
                                                            ?>
                                                            <?php 
                                                                $countya=0;
                                                                 $fannual=mysqli_query($dbc,"select * from `funnel` where `Stage`='Won' AND   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['Margin'];
                                                                    global $countya;
                                                                    $countya=$countya+$val;

                                                                 }
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            <!-- Annual Funnel Modal -->
 <!-- Modal -->
                    
                 


            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-mall grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Margin Per Month
                  </div>
                 <?php 
                                                                $county=0;
                                                                 $fannual=mysqli_query($dbc,"select * from `target_manage_resource` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['MarginPerMonth'];
                                                                    global $county;
                                                                    $county=$county+$val;

                                                                 }
                                                                
                                                            ?>
                                                            <?php 
                                                                $countya=0;
                                                                 $fannual=mysqli_query($dbc,"select * from `funnel` where `Stage`='Won' AND  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['Margin'];
                                                                    global $countya;
                                                                    $countya=$countya+$val;

                                                                 }
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
                
              </div>
            </div>

          </div>

           <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Funnel Per Year
                  </div>
                  <?php 
                                $county=0;
                                 $fannual=mysqli_query($dbc,"select * from `target_manage_resource` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                 while($mont=mysqli_fetch_assoc($fannual))
                                 {
                                  /*$val=$mont['ModificationDetail'];
                                  $fetmon=date("y", strtotime($val));

                                  $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                  $rc=mysqli_fetch_assoc($cust);
                                  $res=$rc['count_column'];*/
                                  $val=$mont['FunnelPerYear'];
                                  /*global $county;*/
                                  $county=/*$county+*/$val;

                                 }
                                
                              ?>
                              <?php 
                                $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `funnel` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                $rc=mysqli_fetch_assoc($cust);
                                $res=$rc['count_column'];
                                $countya=0;
                                 
                              ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>

         <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Funnel Per Month
                  </div>
                  <?php 
                                $county=0;
                                 $fannual=mysqli_query($dbc,"select * from `target_manage_resource` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                 while($mont=mysqli_fetch_assoc($fannual))
                                 {
                                  /*$val=$mont['ModificationDetail'];
                                  $fetmon=date("y", strtotime($val));

                                  $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                  $rc=mysqli_fetch_assoc($cust);
                                  $res=$rc['count_column'];*/
                                  $val=$mont['FunnelPerMonth'];
                                  global $county;
                                  $county=$county+$val;

                                 }
                                
                              ?>
                              <?php 
                                $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `funnel` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                $rc=mysqli_fetch_assoc($cust);
                                $res=$rc['count_column'];
                                $countya=0;
                                 
                              ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                     
Meeting Per Year
                  </div>
                  <?php 
                                $county=0;
                                 $fannual=mysqli_query($dbc,"select * from `target_manage_resource` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                 while($mont=mysqli_fetch_assoc($fannual))
                                 {
                                  /*$val=$mont['ModificationDetail'];
                                  $fetmon=date("y", strtotime($val));

                                  $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                  $rc=mysqli_fetch_assoc($cust);
                                  $res=$rc['count_column'];*/
                                  $val=$mont['MeetingPerYear'];
                                  /*global $county;*/
                                  $county=/*$county+*/$val;

                                 }
                                
                              ?>
                              <?php 
                                $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                $rc=mysqli_fetch_assoc($cust);
                                $res=$rc['count_column'];
                                $countya=0;
                                
                              ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                     
Meeting Per Month
                  </div>
                  <?php 
                                $county=0;
                                 $fannual=mysqli_query($dbc,"select * from `target_manage_resource` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                 while($mont=mysqli_fetch_assoc($fannual))
                                 {
                                  /*$val=$mont['ModificationDetail'];
                                  $fetmon=date("y", strtotime($val));

                                  $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                  $rc=mysqli_fetch_assoc($cust);
                                  $res=$rc['count_column'];*/
                                  $val=$mont['MeetingPerMonth'];
                                  global $county;
                                  $county=$county+$val;

                                 }
                                
                              ?>
                              <?php 
                                $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                $rc=mysqli_fetch_assoc($cust);
                                $res=$rc['count_column'];
                                $countya=0;
                                 
                              ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                     
Customers Per Year
                  </div>
                  <?php 
                                $county=0;
                                 $fannual=mysqli_query($dbc,"select * from `target_manage_resource` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                 while($mont=mysqli_fetch_assoc($fannual))
                                 {
                                  /*$val=$mont['ModificationDetail'];
                                  $fetmon=date("y", strtotime($val));

                                  $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                  $rc=mysqli_fetch_assoc($cust);
                                  $res=$rc['count_column'];*/
                                  $val=$mont['CustomersPerYear'];
                                  /*global $county;*/
                                  $county=/*$county+*/$val;

                                 }
                                
                              ?>
                              <?php 
                                $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign` where  `CampaignType`='Direct' ");
                                $rc=mysqli_fetch_assoc($cust);
                                $res=$rc['count_column'];
                                $countya=0;
                                
                              ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                     
Customers Per Month
                  </div>
                  <?php 
                                $county=0;
                                 $fannual=mysqli_query($dbc,"select * from `target_manage_resource` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                 while($mont=mysqli_fetch_assoc($fannual))
                                 {
                                  /*$val=$mont['ModificationDetail'];
                                  $fetmon=date("y", strtotime($val));

                                  $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                  $rc=mysqli_fetch_assoc($cust);
                                  $res=$rc['count_column'];*/
                                  $val=$mont['CustomersPerMonth'];
                                  global $county;
                                  $county=$county+$val;

                                 }
                                
                              ?>
                              <?php 
                                $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign` where   MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                $rc=mysqli_fetch_assoc($cust);
                                $res=$rc['count_column'];
                                $countya=0;
                                 
                              ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                     
Sales Certification Per Year
                  </div>
                 <?php 
                                                                $county=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(EmployeeMail),SalesCertificationPerYear from `target_manage_resource` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    /*$fe=mysqli_query($dbc,"select distinct(Employee),SalesCertificationPerYear from `target_manage_resource` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");*/

                                                                    $val=$mont['SalesCertificationPerYear'];
                                                                    /*global $county;*/
                                                                    $county=/*$county+*/$val;

                                                                 }
                                                                
                                                            ?>
                                                            <?php 
                                                                $countya=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(RMail),SalesCertificationPerYear from `training` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['SalesCertificationPerYear'];
                                                                    global $countya;
                                                                    $countya=$countya+$val;

                                                                 }
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Sales Certification Per Month
                  </div>
                  <?php 
                                                                $county=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(EmployeeMail),`SalesCertificationPerMonth` from `target_manage_resource` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['SalesCertificationPerMonth'];
                                                                    global $county;
                                                                    $county=$county+$val;

                                                                 }
                                                                
                                                            ?>
                                                            <?php 
                                                                $countya=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(RMail),`SalesCertificationPerMonth` from `training` where MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['SalesCertificationPerMonth'];
                                                                    global $countya;
                                                                    $countya=$countya+$val;

                                                                 }
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Technical Certification Per Year
                  </div>
                  <?php 
                                                                $county=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(EmployeeMail),TechnicalCertificationPerYear from `target_manage_resource` where  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['TechnicalCertificationPerYear'];
                                                                    /*global $county;*/
                                                                    $county=/*$county+*/$val;

                                                                 }
                                                                
                                                            ?>
                                                            <?php 
                                                                $countya=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(RMail),TechnicalCertificationPerYear from `training` where  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['TechnicalCertificationPerYear'];
                                                                    global $countya;
                                                                    $countya=$countya+$val;

                                                                 }
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                     
Technical Certification Per Month
                  </div>
                  <?php 
                                                                $county=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(EmployeeMail),TechnicalCertificationPerMonth from `target_manage_resource` where MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['TechnicalCertificationPerMonth'];
                                                                    global $county;
                                                                    $county=$county+$val;

                                                                 }
                                                                
                                                            ?>
                                                            <?php 
                                                                $countya=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(RMail),TechnicalCertificationPerMonth from `training` where MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['TechnicalCertificationPerMonth'];
                                                                    global $countya;
                                                                    $countya=$countya+$val;

                                                                 }
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                     
Demo Per Year
                  </div>
                 <?php 
                                                                $county=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(EmployeeMail),DemoPerYear from `target_manage_resource` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['DemoPerYear'];
                                                                    /*global $county;*/
                                                                    $county=/*$county+*/$val;

                                                                 }
                                                                
                                                            ?>
                                                            <?php 
                                                                $countya=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(RMail),DemoPerYear from `training` where  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['DemoPerYear'];
                                                                    global $countya;
                                                                    $countya=$countya+$val;

                                                                 }
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                     
Demo Per Month
                  </div>
                  <?php 
                                                                $county=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(EmployeeMail),DemoPerMonth from `target_manage_resource` where MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['DemoPerMonth'];
                                                                    global $county;
                                                                    $county=$county+$val;

                                                                 }
                                                                
                                                            ?>
                                                            <?php 
                                                                $countya=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(RMail),DemoPerMonth from `training` where MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['DemoPerMonth'];
                                                                    global $countya;
                                                                    $countya=$countya+$val;

                                                                 }
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    POC Per Year
                  </div>
                  <?php 
                                                                $county=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(EmployeeMail),POCPerYear from `target_manage_resource` where  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['POCPerYear'];
                                                                    /*global $county;*/
                                                                    $county=/*$county+*/$val;

                                                                 }
                                                                
                                                            ?>
                                                            <?php 
                                                                $countya=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(RMail),POCPerYear from `training` where  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['POCPerYear'];
                                                                    global $countya;
                                                                    $countya=$countya+$val;

                                                                 }
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                     
POC Per Month
                  </div>
                 <?php 
                                                                $county=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(EmployeeMail),POCPerMonth from `target_manage_resource` where MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['POCPerMonth'];
                                                                    global $county;
                                                                    $county=$county+$val;

                                                                 }
                                                                
                                                            ?>
                                                            <?php 
                                                                $countya=0;
                                                                 $fannual=mysqli_query($dbc,"select distinct(RMail),POCPerMonth from `training` where MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                                 while($mont=mysqli_fetch_assoc($fannual))
                                                                 {
                                                                    /*$val=$mont['ModificationDetail'];
                                                                    $fetmon=date("y", strtotime($val));

                                                                    $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];*/
                                                                    $val=$mont['POCPerMonth'];
                                                                    global $countya;
                                                                    $countya=$countya+$val;

                                                                 }
                                                                
                                                            ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $countya; ?> / <?php echo $county; ?></span>
                </div>
               
              </div>
            </div>

            
            <!-- End Widget Linearea Four -->
          </div>

         

        

        </div>
      </div>
    </div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>
    <!-- Core  -->

   
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>

        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
            <script src="../../assets/global/vendor/multi-select/jquery.multi-select.js"></script>
                 <script src="../../assets/global/js/Plugin/multi-select.js"></script>
                  <script src="../../assets/global/js/Plugin/bootstrap-select.js"></script>
                     <script src="../../assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

                  <script >
          $(document).ready(function() {
    $('.example').DataTable();
} );
        </script>
<script>
            $(document).ready(function() {
    $('.example1').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example2').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example3').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example4').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example5').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example6').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example7').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example8').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example9').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example10').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example11').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example12').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example13').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example14').DataTable();
} );
        </script>
        <script src="select-option-aachievement.js"></script>
  <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>
