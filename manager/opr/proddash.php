<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Product Dashboard | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    <!-- <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'> -->

    <?php include "includes/css/tables.php"; ?>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
    
  </head>
  <body class="animsition site-navbar-small dashboard">
    <?php include "navbar-header.php"; ?>    
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <!-- write body content here -->
      <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
          <header class="panel-heading">
            
            <h3 class="panel-title example-title">Product List</h3>
          </header>
             <div class="panel-body">
           
              <?php
                    $fetdetail=mysqli_query($dbc,"select * from `products` order by `ModificationDetail` desc  ");

                    /*echo '<table class="table table-striped mb-none" id="datatable-tabletools">';*/
                   echo '<table  class="table table-striped dataTable table-responsive table-bordered example" data-plugin="dataTable">';
                      echo '<thead>';
                        echo '<tr>';
                          echo '<th>Sl No.</th>';
                          echo '<th>Product Id</th>';
                          echo '<th>Category</th>';
                          echo '<th>Product Name</th>';
                          echo '<th>Unit Price</th>';
                          echo '<th>Service</th>';
                          echo '<th>Partnership Level</th>';
                          echo '<th>Sales Certification</th>';
                          echo '<th>Technical Certification</th>';
                          echo '<th>Demo</th>';
                          echo '<th>POC</th>';
                          echo '<th>PAN Available</th>';
                          echo '<th>Product Margin</th>';
                          echo '<th>OEM Name</th>';
                          echo '<th>OEM Contact Person</th>';
                          echo '<th>OEM Contact No</th>';
                          echo '<th>OEM Contact Mail</th>';
                          echo '<th>Seller Name</th>';
                          echo '<th>Seller Contact Person</th>';
                          echo '<th>Seller Contact No</th>';
                          echo '<th>Seller Contact Mail</th>';
                          echo '<th>Revenue Generated</th>';
                          echo '<th>Update</th>';
                                                    /*echo '<th>Remove</th>';*/
                        echo '</tr>';
                      echo '</thead>';
                      echo '<tbody>';
                                            $count=00;
                                            $cdemo=0;
                                            $cpoc=0;
                                            $crevenue=0;
                                            $csc=0;
                                            $ctc=0;
                        while($frow=mysqli_fetch_assoc($fetdetail))
                        {
                                                    
                                                    $count=$count+1;
                                                    $number = sprintf('%04d',$count);
                                                    $prodid=$frow['ProductId'];
                                                    $category=$frow['Category'];
                                                    $productname=$frow['ProductName'];
                                                    $price=$frow['Price'];
                                                    $services=$frow['Services'];
                                                    $partnershiplevel=$frow['PartnershipLevel'];
                                                    $salescertification=$frow['SalesCertification'];
                                                    $demo=$frow['Demo'];
                                                    $technicalcertification=$frow['TechnicalCertification'];
                                                    $poc=$frow['POC'];
                                                    $service=$frow['Services'];
                                                    $vendormargin=$frow['VendorMargin'];
                                                    $custdiscount=$frow['MaxCustDiscount'];
                                                    $oemcontactperson=$frow['OEMContactPerson'];
                                                    $oemcontactno=$frow['OEMContactNo'];

                                                        $fetfunnelrev=mysqli_query($dbc,"select * from `funnel` where `Products`='$productname' and `Services`='$services' ");
                                                        $r=0;

                                                        while($result=mysqli_fetch_assoc($fetfunnelrev))
                                                        {
                                                            global $r;
                                                            $r=$r+$result['Revenue'];

                                                        }

                                                        global $r;
                                                        global $cdemo;
                                                        global $cpoc;
                                                        global $crevenue;
                                                        global $csc;
                                                        global $ctc;

                                                        if($demo=="Yes")
                                                        {
                                                            $cdemo=$cdemo+1;
                                                        }
                                                        if($poc=="Yes")
                                                        {
                                                            $cpoc=$cpoc+1;
                                                        }
                                                        
                                                        
                                                        $crevenue=$crevenue+$r;
                                                        $ctc=$ctc+$technicalcertification;
                                                        $csc=$csc+$salescertification;

    
                          
                          
                                                      echo '<tr>';
                                                        echo "<td>".$number."</td>";
                                                        echo "<td>{$frow['ProductId']}</td>";
                                                        echo "<td>{$frow['Category']}</td>";
                                                        echo "<td>{$frow['ProductName']}</td>";
                                                        echo "<td>{$frow['Price']}</td>";
                                                        echo "<td>{$frow['Services']}</td>";
                                                        echo "<td>{$frow['PartnershipLevel']}</td>";
                                                        echo "<td>{$frow['SalesCertification']}</td>";
                                                        echo "<td>{$frow['TechnicalCertification']}</td>";
                                                        echo "<td>{$frow['Demo']}</td>";
                                                        echo "<td>{$frow['POC']}</td>";
                                                        echo "<td>{$frow['PANAvailable']}</td>";
                                                        echo "<td>{$frow['ProductMargin']}</td>";
                                                        echo "<td>{$frow['OEMName']}</td>";
                                                        echo "<td>{$frow['OEMContactPerson']}</td>";
                                                        echo "<td>{$frow['OEMContactNo']}</td>";
                                                        echo "<td>{$frow['OEMContactMail']}</td>";
                                                        echo "<td>{$frow['SellerName']}</td>";
                                                        echo "<td>{$frow['SellerContactPerson']}</td>";
                                                        echo "<td>{$frow['SellerContactNo']}</td>";
                                                        echo "<td>{$frow['SelerContactMail']}</td>";
                                                        echo "<td>".$r."</td>";
                                                        echo "<td><a  href='editprod.php?u=$prodid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        /*echo "<td><a  href='../auth/product/del.php?u=$prodid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/
                                                        echo '</tr>';
  
                            
                        }

                                                        echo '<tr>';
                                                        echo "<td>Total</td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td>".$csc."</td>";
                                                        echo "<td>".$ctc."</td>";
                                                        echo "<td>".$cdemo."</td>";
                                                        echo "<td>".$cpoc."</td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        echo "<td></td>";
                                                        
                                                        echo "<td>".$crevenue."</td>";
                                                        echo "<td></td>";
                                                        /*echo "<td></td>";*/
                                                        echo '</tr>';


                                         echo '</tbody>';
                                  echo '</table>';
                                   ?>
                                    
          </div>
        </div>
        <!-- End Panel Basic -->
      </div>
      
</div>
    <!-- End Page -->
    <?php include "includes/footer.php"; ?>  
    <!-- Footer -->
    
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>



        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script >
          $(document).ready(function() {
    $('.example').DataTable();
} );
        </script>


    
  <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>
