<?php
   include "session_handler.php";
   ?>
   
   <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    
        <link rel="stylesheet" href="../../assets/global/vendor/clockpicker/clockpicker.css">
        
        <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
        
        <link rel="stylesheet" href="../../assets/global/vendor/timepicker/jquery-timepicker.css">
        
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

<div class="linemultipletax" >
   <hr>
<div class="row">
  <?php
  $i=$_SESSION['accesstax'];
  $i=$i+1;
  $_SESSION['accesstax']=$i;

  ?>
  <?php
  $i=$_SESSION['accesstaxvalue'];
  $i=$i+1;
  $_SESSION['accesstaxvalue']=$i;

  ?>

                                 <div class="form-group  col-md-3">
                                    <label class="form-control-label" >Product <?php echo $_SESSION['accesstax']+1; ?></label>
                                    <select class="form-control" name="product[]"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(ProductName) from `products` ");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['ProductName'].'">'.$row['ProductName'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Part Description <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <input type="text" class="form-control" name="partdesc[]"  placeholder="Part Description" value="" >
                                       </div>

                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Quantity <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <input type="number" class="form-control" name="qty[]"  placeholder="Quantity" >
                                       </div>
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Unit Price <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <input type="number" class="form-control" name="unitprice[]" placeholder="Unit Price" step="0.01"  >
                                       </div>
                                    </div>
                                    <div class="row">
                                      <div class="form-group  col-md-3">
                                          <label class="form-control-label" >HSN/SAC <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <input type="text" class="form-control" name="hsnsac[]"  placeholder="HSN/SAC" >
                                       </div>

                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Tax <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <select class="form-control" name="tax[]" multiple="multiple" data-plugin="select2" required="required" >
                                          <?php
                                       $project=mysqli_query($dbc,"select distinct(TaxName),TaxPercentage from `setting_tax`");
                                       
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                           //$pro=$row['ProjectName'];
                                       
                                            echo '<option  value="'.$row['TaxName'].'-'.$row['TaxPercentage'].'@'.$_SESSION['accesstaxvalue'].'">'.$row['TaxName'].' '.$row['TaxPercentage'].'%'.'</option>';
                                           
                                       }
                                       ?>
                                          </select>
                                       </div>
                                    
                                       
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >OEM Quotation <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <input type="file" class="form-control" name="uplfiles[]"  placeholder="OEM Quotation" >
                                       </div>
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button" onClick="addMoreTax();">

                                          <span class="icon md-plus" ></span>
                                          </button>
                                          <button class="btn btn-danger btn-add-basic" type="button" onClick="deleteRowTax();">

                                          <span class="icon md-minus" ></span>
                                          </button>
                                          <input type="checkbox" name="item_index[]" />
                                          </span>
                                       </div>
                                    </div>

                                 </div>


    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <link rel="stylesheet" href="../../assets/global/vendor/select2/select2.css">
    
    
    <script src="../../assets/global/vendor/select2/select2.full.min.js"></script>

<script src="../../assets/global/js/Plugin/select2.js"></script> 
    
    <!-- Plugins -->     
        
        <script src="../../assets/global/vendor/clockpicker/bootstrap-clockpicker.min.js"></script>
        
        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="../../assets/global/vendor/timepicker/jquery.timepicker.min.js"></script>
        <script src="../../assets/global/vendor/datepair/datepair.min.js"></script>
        <script src="../../assets/global/vendor/datepair/jquery.datepair.min.js"></script>
        
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    
        
        <script src="../../assets/global/js/Plugin/bootstrap-datepicker.js"></script>
        <script src="../../assets/global/js/Plugin/jt-timepicker.js"></script>
        <script src="../../assets/global/js/Plugin/datepair.js"></script>
        
    
        <script src="../../assets/examples/js/forms/advanced.js"></script>