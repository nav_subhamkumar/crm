<?php
   include "session_handler.php";
   ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="bootstrap material admin template">
      <meta name="author" content="">
      <title>Create Campaign | Bizapp CRM</title>
      <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
      <link rel="shortcut icon" href="../../assets/images/favicon.ico">
      <!-- Stylesheets -->
      <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
      <link rel="stylesheet" href="../../assets/css/site.min.css">
      <!-- Plugins -->
      <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
      <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
      <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
      <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
      <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
      <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
      <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
      <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
      <!-- Fonts -->
      <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
      <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
      <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- table set -->
      <link rel='stylesheet' href='../../assets/css/datatableset.css'>
      <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>
      <?php include "includes/css/select.php" ?>
      <!--[if lt IE 9]>
      <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
      <![endif]-->
      <!--[if lt IE 10]>
      <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
      <script src="../../assets/global/vendor/respond/respond.min.js"></script>
      <![endif]-->
      <!-- Scripts -->
      <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
      <script>
         Breakpoints();
      </script>
   </head>
   <body class="animsition site-navbar-small dashboard">
      <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <?php include "navbar-header.php"; ?>     
      <?php include "side-navigation.php"; ?>   
      <!-- Page -->
      <div class="page">
         <!-- write body content here -->
         <div class="page-content">
            <div class="panel">
               <div class="panel-body container-fluid">
                  <div class="row row-lg">
                     <div class="col-md-12">
                        <div class="example-wrap">
                           <h4 class="example-title">Create Campaign</h4>
                           <div class="example">
                              <form action="../auth/campaign/ccmp.php" method="post"  autocomplete="off"  >
                                 <div class="row">
                                    <div class="form-group  col-md-6">
                                       <label class="form-control-label" >Sector</label>
                                       <select name="sector[]" class="form-control " multiple data-plugin="select2">
                                       <?php
                                          $comp=$_GET['c'];
                                          $project=mysqli_query($dbc,"select distinct(SectorName) from `sector`  ");
                                          
                                          while($row=mysqli_fetch_assoc($project))
                                          {
                                              //$pro=$row['ProjectName'];
                                          
                                          
                                          
                                               echo '<option  value="'.$row['SectorName'].'">'.$row['SectorName'].'</option>';
                                              
                                          }
                                          ?>
                                       </select>
                                    </div>
                                    <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Project Name</label>
                                    <input type="text" name="project" class=" form-control" 
                                          data-placement="bottom-right-inside" >
                                    
                                    </div>
                                    <div class="form-group col-md-12">
                                       <!-- <input type="submit" name="upload_submit" value="Submit" class="btn btn-success" > -->
                                       <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                       <button type="submit" class="btn-default btn-pure btn">Cancel</button>
                                    </div>
                                    
                                 </div>
                              </form>
                           </div>
                        </div>
                        <!-- End Example Basic Form -->
                     </div>
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Page -->
      <!-- Footer -->
      <?php include "includes/footer.php" ?>
      <!-- Core  -->
      <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
      <script src="../../assets/global/vendor/jquery/jquery.js"></script>
      <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
      <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
      <script src="../../assets/global/vendor/animsition/animsition.js"></script>
      <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
      <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
      <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
      <script src="../../assets/global/vendor/waves/waves.js"></script>
      <!-- Plugins -->
      <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
      <script src="../../assets/global/vendor/switchery/switchery.js"></script>
      <script src="../../assets/global/vendor/intro-js/intro.js"></script>
      <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
      <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
      <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
      <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
      <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
      <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
      <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
      <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
      <!-- Scripts -->
      <script src="../../assets/global/js/Component.js"></script>
      <script src="../../assets/global/js/Plugin.js"></script>
      <script src="../../assets/global/js/Base.js"></script>
      <script src="../../assets/global/js/Config.js"></script>
      <script src="section/Menubar.js"></script>
      <script src="section/Sidebar.js"></script>
      <script src="section/PageAside.js"></script>
      <script src="section/GridMenu.js"></script>
      <!-- Config -->
      <script src="../../assets/global/js/config/colors.js"></script>
      <script src="../../assets/js/config/tour.js"></script>
      <script>Config.set('assets', '../assets');</script>
      <!-- Page -->
      <script src="../../assets/js/Site.js"></script>
      <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
      <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
      <script src="../../assets/global/js/Plugin/switchery.js"></script>
      <script src="../../assets/global/js/Plugin/matchheight.js"></script>
      <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
      <script src="../../assets/global/js/Plugin/peity.js"></script>
      <script src="../../assets/examples/js/dashboard/v1.js"></script>
      <?php include "includes/js/select.php" ?>
   <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>