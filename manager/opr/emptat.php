<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Employee TAT | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    

    <link rel='stylesheet' href='../../assets/css/customised-crm.css'>

    <?php include "includes/css/tables.php"; ?>
    
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <div class="page-content" >
<!-- Panel Basic -->

        <div class="panel">
          <header class="panel-heading">
            
            <h3 class="panel-title example-title">Employee TAT</h3>
          </header>
          <div class="example-wrap m-sm-0 col-lg-12">
            
              <?php
                                        $fetdetail=mysqli_query($dbc,"select * from `team` order by `empid` ");

                                        echo '<table class="table table-bordered table-striped mb-none" id="datatable-tabletools">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>SlNo.</th>';
                                                    echo '<th>Employee ID</th>';
                                                    echo '<th>Name</th>';
                                                    echo '<th>Email</th>';
                                                    echo '<th>Lead - Meeting Average</th>';
                                                    echo '<th>Meeting - Quotation Average</th>';
                                                    echo '<th>Quotation - Achievement Average</th>';
                                                    /*echo '<th>Update</th>';
                                                    echo '<th>Remove</th>';*/
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $c=0;
                                                while($frow=mysqli_fetch_assoc($fetdetail))
                                                {
                                                    global $c;
                                                    $c=$c+1;
                                                    $empid=$frow['empid'];
                                                    $fullname=$frow['name'];
                                                    $emailid=$frow['email'];
                                                    $contactno=$frow['contact'];
                                                    $designation=$frow['desig'];
                                                    $salary=$frow['Salary'];
                                                    $department=$frow['Department'];
                                                    $product=$frow['Product'];


                                                    /*Lead- Meeting average start*/

                                                     $fetlm=mysqli_query($dbc,"select * from meeting where RMail='$emailid'  ");
                                                        $calm=0;$avglm=0;$avgflm=0;

                                                        while($rf=mysqli_fetch_assoc($fetlm))
                                                        {
                                                            $company=$rf['Company'];
                                                            $projectname=$rf['ProjectName'];
                                                            $fdate=$rf['ModificationDetail'];
                                                            $fdatef=date("Y-m-d",strtotime($fdate));

                                                            $fetm=mysqli_query($dbc,"select ModificationDetail from leads where RMail='$emailid' and Company='$company' and ProjectName='$projectname' ");
                                                            while($rm=mysqli_fetch_assoc($fetm))
                                                            {
                                                                
                                                                $mdate=$rm['ModificationDetail'];
                                                                

                                                            }
                                                            global $avglm;
                                                                global $difflm;
                                                            $mdateflm=date("Y-m-d",strtotime($mdate));
                                                                $difflm = strtotime($fdate) - strtotime($mdate);

                                                                $avglm=$avglm+$difflm;
                                                            $calm=$calm+1;
                                                            global $avgflm;
                                                            $avgflm=$avglm/$calm;
                                                            $avgflmround=round($avgflm);


                                                        }
                                                        
                                                            $dtF = new DateTime("@0");
                                                            $dtT = new DateTime("@$avgflmround");
                                                            $cavglm=$dtF->diff($dtT)->format('%a D, %h H, %i M, %s S');

                                                            /*Lead- Meeting average end*/

                                                    /*meeting- Quotation average start*/

                                                     $fetfmq=mysqli_query($dbc,"select * from funnel where RMail='$emailid' and Stage='Quotation' ");
                                                        $camq=0;$avgmq=0;$avgfmq=0;

                                                        while($rf=mysqli_fetch_assoc($fetfmq))
                                                        {
                                                            $company=$rf['Company'];
                                                            $fdate=$rf['ModificationDetail'];
                                                            $fdatef=date("Y-m-d",strtotime($fdate));

                                                            $fetm=mysqli_query($dbc,"select ModificationDetail from meeting where RMail='$emailid' and Company='$company' ");
                                                            while($rm=mysqli_fetch_assoc($fetm))
                                                            {
                                                                
                                                                $mdate=$rm['ModificationDetail'];
                                                                

                                                            }
                                                            global $avgmq;
                                                                global $diffmq;
                                                            $mdatef=date("Y-m-d",strtotime($mdate));
                                                                $diffmq = strtotime($fdate) - strtotime($mdate);

                                                                $avgmq=$avgmq+$diffmq;
                                                            $camq=$camq+1;
                                                            global $avgfmq;
                                                            $avgfmq=$avgmq/$camq;
                                                            $avgfmqs=round($avgfmq);

                                                        }
                                                        
                                                            $dtF = new DateTime("@0");
                                                            $dtT = new DateTime("@$avgfmqs");
                                                            $cavgmq=$dtF->diff($dtT)->format('%a D, %h H, %i M, %s S');

                                                            /*meeting- Quotation average end*/

                                                            /*Quotation - Achievement average start*/

                                                     $fetf=mysqli_query($dbc,"select * from funnel where RMail='$emailid' and Stage='Won' ");
                                                        $ca=0;$avg=0;$avgf=0;

                                                        while($rf=mysqli_fetch_assoc($fetf))
                                                        {
                                                            $company=$rf['Company'];
                                                            $fdate=$rf['ModificationDetail'];
                                                            $fdatef=date("Y-m-d",strtotime($fdate));

                                                            $fetm=mysqli_query($dbc,"select ModificationDetail from meeting where RMail='$emailid' and Company='$company' ");
                                                            while($rm=mysqli_fetch_assoc($fetm))
                                                            {
                                                                
                                                                $mdate=$rm['ModificationDetail'];
                                                                

                                                            }
                                                            global $avg;
                                                                global $diff;
                                                            $mdatef=date("Y-m-d",strtotime($mdate));
                                                                $diff = strtotime($fdate) - strtotime($mdate);

                                                                $avg=$avg+$diff;
                                                            $ca=$ca+1;
                                                            
                                                            $avgf=$avg/$ca;
                                                            $avgfround=round($avgf);


                                                        }
                                                        
                                                            $dtF = new DateTime("@0");
                                                            $dtT = new DateTime("@$avgfround");
                                                            $cavg=$dtF->diff($dtT)->format('%a D, %h H, %i M, %s S');

                                                            /*Quotation- Achievement average end*/
                                                           
                                                        
                                                    
                                                    
                                                        echo '<tr>';
                                                        echo "<td>".$c."</td>";
                                                        echo "<td>{$frow['empid']}</td>";
                                                        echo "<td>{$frow['name']}</td>";
                                                        echo "<td>{$frow['email']}</td>";

                                                       
                                                        echo "<td>".$cavglm."</td>";
                                                        echo "<td>".$cavgmq."</td>";
                                                        echo "<td>".$cavg."</td>";
                                                        
                                                        /*echo "<td><a  href='editemp.php?u=$empid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        echo "<td><a  href='../auth/hr/employee/del.php?u=$empid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/


                                                        
                                                        
                                                }
                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>

            
          </div>

        </div>
        <!-- End Panel Basic -->
      </div>
    </div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?> 
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>

        <?php include "includes/js/tables.php"; ?>


    
  <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>
