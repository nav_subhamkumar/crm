<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Customers | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    

    <link rel='stylesheet' href='../../assets/css/customised-crm.css'>


    <script type="text/javascript" src="../../assets/googlechart/js/loader.js"></script>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <div class="page-content" >
        <!-- convertion ratio chart start -->
        <div class="panel">
               <div class="panel-body container-fluid">
                  <div class="row row-lg">
                     <div class="col-md-10">
                        <?php
                                                $fannual=mysqli_query($dbc,"select * from `customers` where  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                $jan=0;
                                                $feb=0;$mar=0;$apr=0;$may=0;$jun=0;$jul=0;$aug=0;$sep=0;$oct=0;$nov=0;$dec=0;
                                                $janm=0;
                                                $febm=0;$marm=0;$aprm=0;$maym=0;$junm=0;$julm=0;$augm=0;$sepm=0;$octm=0;$novm=0;$decm=0;
                                                while($mont=mysqli_fetch_assoc($fannual))
                                                {

                                                    $val=$mont['ModificationDetail'];
                                                    $rev=$mont['Revenue'];
                                                    $fmar=$mont['Margin'];

                                                    $fetmon=date("M", strtotime($val));

                                                    if($fetmon == "Jan")
                                                    {
                                                        /*global $feb;
                                                        $jan=$jan+1;*/
                                                        /*global $rev;
                                                        global $fmar;*/
                                                        global $jan;
                                                        global $janm;
                                                        $jan=$jan + 1;
                                                        $janm=$janm + $fmar;

                                                    } elseif ($fetmon == "Feb")
                                                    {
                                                        /*global $feb;
                                                        $feb=$feb+1;*/
                                                        global $rev;
                                                        global $fmar;
                                                        global $feb;
                                                        global $febm;
                                                        $feb=$feb + 1;
                                                        $febm=$febm + $fmar;

                                                    } elseif ($fetmon == "Mar")
                                                    {
                                                        /*global $mar;
                                                        $mar=$mar+1;*/
                                                        global $rev;
                                                        global $fmar;
                                                        global $mar;
                                                        global $marm;
                                                        $mar=$mar + 1;
                                                        $marm=$marm + $fmar;

                                                    } elseif ($fetmon == "Apr")
                                                    {
                                                        /*global $apr;
                                                        $apr=$apr+1;*/
                                                        global $rev;
                                                        global $fmar;
                                                        global $apr;
                                                        global $aprm;
                                                        $apr=$apr + 1;
                                                        $aprm=$aprm + $fmar;

                                                    } elseif ($fetmon == "May")
                                                    {
                                                        /*global $may;
                                                        $may=$may+1;*/
                                                        global $rev;
                                                        global $fmar;
                                                        global $may;
                                                        global $maym;
                                                        $may=$may + 1;
                                                        $maym=$maym + $fmar;

                                                    } elseif ($fetmon == "Jun")
                                                    {
                                                        /*global $jun;
                                                        $jun=$jun+1;*/
                                                        global $rev;
                                                        global $fmar;
                                                        global $jun;
                                                        global $junm;
                                                        $jun=$jun + 1;
                                                        $junm=$junm + $fmar;

                                                    } elseif ($fetmon == "Jul")
                                                    {
                                                        /*global $jul;
                                                        $jul=$jul+1;*/
                                                        global $rev;
                                                        global $fmar;
                                                        global $jul;
                                                        global $julm;
                                                        $jul=$jul + 1;
                                                        $julm=$julm + $fmar;

                                                    } elseif ($fetmon == "Aug")
                                                    {
                                                        /*global $aug;
                                                        $aug=$aug+1;*/
                                                        global $rev;
                                                        global $fmar;
                                                        global $aug;
                                                        global $augm;
                                                        $aug=$aug + 1;
                                                        $augm=$augm + $fmar;

                                                    } elseif ($fetmon == "Sep")
                                                    {
                                                        /*global $sep;
                                                        $sep=$sep+1;*/
                                                        global $rev;
                                                        global $fmar;
                                                        global $sep;
                                                        global $sepm;
                                                        $sep=$sep + 1;
                                                        $sepm=$sepm + $fmar;

                                                    } elseif ($fetmon == "Oct")
                                                    {
                                                        /*global $oct;
                                                        $oct=$oct+1;*/
                                                        global $rev;
                                                        global $fmar;
                                                        global $oct;
                                                        global $octm;
                                                        $oct=$oct + 1;
                                                        $octm=$octm + $fmar;

                                                    } elseif ($fetmon == "Nov")
                                                    {
                                                        /*global $nov;
                                                        $nov=$nov+1;*/
                                                        global $rev;
                                                        global $fmar;
                                                        global $nov;
                                                        global $novm;
                                                        $nov=$nov + 1;
                                                        $novm=$novm + $fmar;

                                                    } elseif ($fetmon == "Dec")
                                                    {
                                                        global $rev;
                                                        global $fmar;
                                                        global $dec;
                                                        global $decm;
                                                        $dec=$dec + 1;
                                                        $decm=$decm + $fmar;

                                                    }

                                                        
                                                    
                                                }


                                                    
                                                ?>
                                                <?php
                                                $fannualf=mysqli_query($dbc,"select distinct(Company),ModificationDetail from `funnel` where Stage='Won' AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                               
                                                $janm=0;
                                                $febm=0;$marm=0;$aprm=0;$maym=0;$junm=0;$julm=0;$augm=0;$sepm=0;$octm=0;$novm=0;$decm=0;
                                                while($mont=mysqli_fetch_assoc($fannualf))
                                                {

                                                    $val=$mont['ModificationDetail'];
                                                    /*$rev=$mont['Revenue'];*/
                                                    $fmar=$mont['Margin'];

                                                    $fetmon=date("M", strtotime($val));

                                                    if($fetmon == "Jan")
                                                    {
                                                        /*global $feb;
                                                        $jan=$jan+1;*/
                                                        /*global $rev;
                                                        global $fmar;*/
                                                        /*global $jan;*/
                                                        global $janm;
                                                       /* $jan=$jan + 1;*/
                                                        $janm=$janm + 1;

                                                    } elseif ($fetmon == "Feb")
                                                    {
                                                        /*global $feb;
                                                        $feb=$feb+1;*/
                                                        /*global $rev;
                                                        global $fmar;*/
                                                        /*global $feb;*/
                                                        global $febm;
                                                       /* $feb=$feb + 1;*/
                                                        $febm=$febm + 1;

                                                    } elseif ($fetmon == "Mar")
                                                    {
                                                        /*global $mar;
                                                        $mar=$mar+1;*/
                                                        /*global $rev;
                                                        global $fmar;*/
                                                     /*   global $mar;*/
                                                        global $marm;
                                                        /*$mar=$mar + 1;*/
                                                        $marm=$marm + 1;

                                                    } elseif ($fetmon == "Apr")
                                                    {
                                                        /*global $apr;
                                                        $apr=$apr+1;*/
                                                       /* global $rev;
                                                        global $fmar;
                                                        global $apr;*/
                                                        global $aprm;
                                                       /* $apr=$apr + 1;*/
                                                        $aprm=$aprm + 1;

                                                    } elseif ($fetmon == "May")
                                                    {
                                                        /*global $may;
                                                        $may=$may+1;*/
                                                        /*global $rev;
                                                        global $fmar;
                                                        global $may;*/
                                                        global $maym;
                                                        /*$may=$may + 1;*/
                                                        $maym=$maym + 1;

                                                    } elseif ($fetmon == "Jun")
                                                    {
                                                        /*global $jun;
                                                        $jun=$jun+1;*/
                                                        /*global $rev;
                                                        global $fmar;
                                                        global $jun;*/
                                                        global $junm;
                                                        /*$jun=$jun + 1;*/
                                                        $junm=$junm + 1;

                                                    } elseif ($fetmon == "Jul")
                                                    {
                                                        /*global $jul;
                                                        $jul=$jul+1;*/
                                                        /*global $rev;
                                                        global $fmar;
                                                        global $jul;*/
                                                        global $julm;
                                                       /* $jul=$jul + 1;*/
                                                        $julm=$julm + 1;

                                                    } elseif ($fetmon == "Aug")
                                                    {
                                                        /*global $aug;
                                                        $aug=$aug+1;*/
                                                       /* global $rev;
                                                        global $fmar;
                                                        global $aug;*/
                                                        global $augm;
                                                        /*$aug=$aug + 1;*/
                                                        $augm=$augm + 1;

                                                    } elseif ($fetmon == "Sep")
                                                    {
                                                        /*global $sep;
                                                        $sep=$sep+1;*/
                                                        /*global $rev;
                                                        global $fmar;
                                                        global $sep;*/
                                                        global $sepm;
                                                        /*$sep=$sep + 1;*/
                                                        $sepm=$sepm + 1;

                                                    } elseif ($fetmon == "Oct")
                                                    {
                                                        /*global $oct;
                                                        $oct=$oct+1;*/
                                                        /*global $rev;
                                                        global $fmar;
                                                        global $oct;*/
                                                        global $octm;
                                                        /*$oct=$oct + 1;*/
                                                        $octm=$octm + 1;

                                                    } elseif ($fetmon == "Nov")
                                                    {
                                                        /*global $nov;
                                                        $nov=$nov+1;*/
                                                        /*global $rev;
                                                        global $fmar;
                                                        global $nov;*/
                                                        global $novm;
                                                        /*$nov=$nov + 1;*/
                                                        $novm=$novm + 1;

                                                    } elseif ($fetmon == "Dec")
                                                    {
                                                        /*global $rev;
                                                        global $fmar;
                                                        global $dec;*/
                                                        global $decm;
                                                        /*$dec=$dec + 1;*/
                                                        $decm=$decm + 1;

                                                    }

                                                        
                                                    
                                                }


                                                    
                                                ?>

                                                <?php
                                                    $janc=0;$febc=0;$marc=0;$aprc=0;$marc=0;$aprc=0;$mayc=0;$junc=0;$julc=0;$augc=0;$sepc=0;$octc=0;$novc=0;$decc=0;

                                                    if($jan!='0' and $janm!='0' ){
                                                        $janc=$janm/$jan;

                                                    }if( $feb!='0' and $febm!='0' ){
                                                        $febc=$febm/$feb;

                                                    }if( $mar!='0' and $marm!='0' ){
                                                        $marc=$marm/$mar;

                                                    }if( $apr!='0' and $aprm!='0' ){
                                                        $aprc=$aprm/$apr;

                                                    }if( $may!='0' and $maym!='0' ){
                                                         $mayc=$maym/$may;

                                                    }if( $jun!='0' and $junm!='0' ){
                                                        $junc=$junm/$jun;

                                                    }if( $jul!='0' and $julm!='0' ){
                                                        $julc=$julm/$jul;

                                                    }if( $aug!='0' and $augm!='0' ){
                                                        $augc=$augm/$aug;

                                                    }if( $sep!='0' and $sepm!='0' ){
                                                        $sepc=$sepm/$sep;

                                                    }if( $oct!='0' and $octm!='0' ){
                                                        $octc=$octm/$oct;

                                                    }if( $nov!='0' and $novm!='0' ){
                                                        $novc=$novm/$nov;

                                                    }if( $dec!='0' and $decm!='0')
                                                    {
                                                                                                         
                                                        $decc=$decm/$dec;
                                                    }

                                                    
                                                    /**/





                                                    

                                                ?>

                                                <div id="chart_div"  style="width: 100%; height: 300px;"></div>

       <script type="text/javascript">
           
                       google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        /*var data = google.visualization.arrayToDataTable([
         ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
         ['2004/05',  165,      938,         522,             998,           450,      614.6],
         ['2005/06',  135,      1120,        599,             1268,          288,      682],
         ['2006/07',  157,      1167,        587,             807,           397,      623],
         ['2007/08',  139,      1110,        615,             968,           215,      609.4],
         ['2008/09',  136,      691,         629,             1026,          366,      569.6]
      ]);*/

      var data = google.visualization.arrayToDataTable([
         ['Month', 'All', 'Customers', 'Convertion Ratio'],
         ['JAN',  <?php echo $jan; ?>, <?php echo $janm; ?> , <?php echo $janc; ?> ],
         ['FEB',  <?php echo $feb; ?>, <?php echo $febm; ?> , <?php echo $febc; ?>],
         ['MAR',  <?php echo $mar; ?>, <?php echo $marm; ?>, <?php echo $marc; ?> ],
         ['APR',  <?php echo $apr; ?>, <?php echo $aprm; ?>, <?php echo $aprc; ?>],
         ['MAY',  <?php echo $may; ?>, <?php echo $maym; ?>, <?php echo $mayc; ?>],
         ['JUN',  <?php echo $jun; ?>, <?php echo $junm; ?>, <?php echo $junc; ?>],
         ['JUL',  <?php echo $jul; ?>, <?php echo $julm; ?>, <?php echo $julc; ?>],
         ['AUG',  <?php echo $aug; ?>, <?php echo $augm; ?>, <?php echo $augc; ?>],
         ['SEP',  <?php echo $sep; ?>, <?php echo $sepm; ?>, <?php echo $sepc; ?>],
         ['OCT',  <?php echo $oct; ?>, <?php echo $octm; ?>, <?php echo $octc; ?>],
         ['NOV',  <?php echo $nov; ?>, <?php echo $novm; ?>, <?php echo $novc; ?>],
         ['DEC',  <?php echo $dec; ?>, <?php echo $decm; ?>, <?php echo $decc; ?>],
      ]);

    var options = {
      title : 'Prospect Details',
      vAxis: {title: 'Numbers'},
      hAxis: {title: 'Month'},
      seriesType: 'bars',
      series: {2: {type: 'line'}}
    };

    var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }
       </script>
                        
                        
                     </div>
                  </div>
               </div>
            </div>

        <!-- convertion ratio chart end -->
        <!-- cards with link start -->
        <div class="row">
        <div class="col-xl-3 col-md-6">
            <!-- Widget Linearea One-->
            <a href="prospects.php" style="text-decoration: none;">
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>All Prospects
                  </div>
                  <?php
                    $allprospects=mysqli_query($dbc,"select * from `customers`");
                    $noofprospects=0;
                    while ($r=mysqli_fetch_assoc($allprospects)) {
                        $noofprospects=$noofprospects+1;
                    }
                   ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $noofprospects; ?></span>
                </div>
                <div class="mb-20 grey-500">
                    &nbsp;
                  <!-- <i class="icon md-long-arrow-up green-500 font-size-16"></i>                  15% From this yesterday -->
                </div>
                <div class="ct-chart h-50"></div>
              </div>
            </div>
            </a>
            <!-- End Widget Linearea One -->
          </div>
          <div class="col-xl-3 col-md-6">
            <!-- Widget Linearea Two -->
            <a href="proscustomers.php" style="text-decoration: none;">
            <div class="card card-shadow" id="widgetLineareaTwo">
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>Customers
                  </div>
                  <?php
                    $totalcust=0;
                    $funn=mysqli_query($dbc,"select distinct(Company) from `funnel` order by `ModificationDetail` desc");
                    while($fr=mysqli_fetch_assoc($funn))
                    {
                        $totalcust=$totalcust+1;
                    }
                  ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $totalcust; ?></span>
                </div>
                <div class="mb-20 grey-500">
                    &nbsp;
                  <!-- <i class="icon md-long-arrow-up green-500 font-size-16"></i>                  34.2% From this week -->
                </div>
                <div class="ct-chart h-50"></div>
              </div>
            </div>
            </a>
            <!-- End Widget Linearea Two -->
          </div>
          <div class="col-xl-3 col-md-6">
            <!-- Widget Linearea Three -->
            <a href="prosprofdash.php" style="text-decoration: none;">
            <div class="card card-shadow" id="widgetLineareaThree">
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom mr-5"></i>Prospects Profile
                  </div>
                  <?php
                    $totalprofile=0;
                    $funn=mysqli_query($dbc,"select distinct(Company) from `customersprofile` order by `ModificationDetail` desc");
                    while($fr=mysqli_fetch_assoc($funn))
                    {
                        $totalprofile=$totalprofile+1;
                    }
                  ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $totalprofile; ?></span>
                </div>
                <div class="mb-20 grey-500">
                    &nbsp;
                  <!-- <i class="icon md-long-arrow-down red-500 font-size-16"></i>                  15% From this yesterday -->
                </div>
                <div class="ct-chart h-50"></div>
              </div>
            </div>
            </a>
            <!-- End Widget Linearea Three -->
          </div>
          <div class="col-xl-3 col-md-6">
            <!-- Widget Linearea Four -->
            <a href="prosfilter.php" style="text-decoration: none;">
            <div class="card card-shadow" id="widgetLineareaFour">
              <div class="card-block p-20 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-view-list grey-600 font-size-24 vertical-align-bottom mr-5"></i>Prospect Filter
                  </div>
                  <span class="float-right grey-700 font-size-30"><?php echo $noofprospects; ?></span>
                </div>
                <div class="mb-20 grey-500">
                    &nbsp;
                  <!-- <i class="icon md-long-arrow-up green-500 font-size-16"></i>                  18.4% From this yesterday -->
                </div>
                <div class="ct-chart h-50"></div>
              </div>
            </div>
            </a>
            <!-- End Widget Linearea Four -->
          </div>
      </div>
        <!-- cards with link end -->

      </div>
    </div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>

        

        


    
  <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>
