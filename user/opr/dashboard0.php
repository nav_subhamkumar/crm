<?php
   include "session_handler.php";
?>
<?php
  
   $uid=null;
   $uid=$id;
   $v=$_GET['v'];
   $vfd=$_GET['vfd'];
   
   
   ?>
   
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Dashboard | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    

    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
     <link rel="stylesheet" href="../../assets/global/vendor/multi-select/multi-select.css">
      <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-select/bootstrap-select.css">
           <link rel="stylesheet" href="../../assets/global/vendor/select2/select2.css">
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>

      <link rel="stylesheet" href="../../assets/css/customised-crm.css">

        <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
          <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
         
         


          <div class="col-xl-2 col-md-2" data-target="#periodwisemodal" data-toggle="modal">
            <!-- Widget Linearea Two -->
            <div class="card card-shadow" id="widgetLineareaTwo">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-flash grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Period Wise Review
                  </div>
                 
                </div>
              </div>
            </div>
            <!-- End Widget Linearea Two -->
          </div>

          <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="periodwisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Period Wise Review</h4>
                          </div>
                          <div class="modal-body">
                             <form action="" method="get" class="form-horizontal" novalidate="novalidate"  autocomplete="off">
                              <div class="form-group">
                                  <label class="form-control-label" >Review Period:</label>
                                 <div>
                                    <select size="1"  data-plugin="select2" class="form-control Ranka populate" title="" name="Rank">
                                       <option value="Annual">Select</option>
                                       <option value="ryear">Yearly</option>
                                       <option value="rhalfyear">Half Yearly</option>
                                       <option value="rquater">Quarterly</option>
                                       <option value="amonth">Monthly</option>
                                       <!-- <option value="rweek">Weekly</option> -->
                                       <option value="adate">Date Range</option>
                                    </select>
                                 </div>
                                 <div class="containers">
                                    <div class="ryear">
                                       <label class="form-control-label">Year:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project"  id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <?php
                                                $curyear=date('Y');
                                                
                                                ?>
                                             <option value="<?php echo $curyear; ?>"><?php echo $curyear; ?>-<?php echo $curyear+1; ?></option>
                                             <option value="<?php echo $curyear-1; ?>"><?php echo $curyear-1; ?>-<?php echo $curyear; ?></option>
                                             <option value="<?php echo $curyear-2; ?>"><?php echo $curyear-2; ?>-<?php echo $curyear-1; ?></option>
                                             <option value="<?php echo $curyear-3; ?>"><?php echo $curyear-3; ?>-<?php echo $curyear-2; ?></option>
                                             <option value="<?php echo $curyear-4; ?>"><?php echo $curyear-4; ?>-<?php echo $curyear-3; ?></option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rhalfyear">
                                        <label class="form-control-label">Half Year:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <option value="1sty">1st</option>
                                             <option value="2ndy">2nd</option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rquater">
                                      
                                        <label class="form-control-label">Quarter:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <option value="1stq">1st</option>
                                             <option value="2ndq">2nd</option>
                                             <option value="3rdq">3rd</option>
                                             <option value="4thq">4th</option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rweek">
                                        <label class="form-control-label">Week:</label>
                                       <div>
                                          <select name="project"  data-plugin="select2" class="form-control populate" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <?php
                                                $val=1;
                                                while($val<53)
                                                {
                                                   echo '<option value="'.$val.'">'.$val.'</option>'; 
                                                   $val=$val+1;
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="amonth">
                                        <label class="form-control-label">Month:</label>
                                       <div>
                                          <select name="project"  data-plugin="select2" class="form-control populate" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <option value="04">Apr</option>
                                             <option value="05">May</option>
                                             <option value="06">Jun</option>
                                             <option value="07">Jul</option>
                                             <option value="08">Aug</option>
                                             <option value="09">Sep</option>
                                             <option value="10">Oct</option>
                                             <option value="11">Nov</option>
                                             <option value="12">Dec</option>
                                             <option value="01">Jan</option>
                                             <option value="02">Feb</option>
                                             <option value="03">Mar</option>
                                          </select>
                                       </div>
                                    </div>
                                    <!-- </div>
                                       <div class="container"> -->
                                    <div class="adate">
                                       <label class="form-control-label">Date:</label>
                                       <div>
                                          <input type="text" name="fdate" data-plugin="datepicker" class="form-control" onchange="redirectfd(this.value);" placeholder="From Date" required >
                                          <br>
                                          <input type="text" name="tdate" data-plugin="datepicker" class="form-control" onchange="redirectat(this.value);" placeholder="To Date" required >
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </form>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


 <div class="col-xl-2 col-md-2" data-target="#sbuwisemodal" data-toggle="modal">
            <!-- Widget Linearea Two -->
            <div class="card card-shadow" id="widgetLineareaTwo">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-flash grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    SBU Wise Review
                  </div>
                 
                </div>
              </div>
            </div>
            <!-- End Widget Linearea Two -->
          </div>

          <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="sbuwisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">SBU Wise Review</h4>
                          </div>
                          <div class="modal-body">
                             <form action="" method="get" class="form-horizontal" novalidate="novalidate"  autocomplete="off">
                              <div class="form-group">
                                  <label class="form-control-label" >Select SBU:</label>
                                 <div>
                                    <select name="category" class="form-control" data-plugin="select2">
                                        
                                    <option value="Infrastructure">Infrastructure</option>
                                    <option value="Security">Security</option>
                                            
                                    <option value="Software Development & Testing">Software Development & Testing</option>
                                    <option value="Digital Transformation">Digital Transformation</option>
                                    <option value="Analytics">Analytics</option>
                                                   
                                          </select>
                                 </div>
                               
                              </div>
                           </form>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

          <div class="col-xl-2 col-md-2" data-target="#productwisemodal" data-toggle="modal">
            <!-- Widget Linearea Three -->
            <div class="card card-shadow" id="widgetLineareaThree">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-chart grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Product Wise Review
                  </div>
                </div>
                
              </div>
            </div>
            <!-- End Widget Linearea Three -->
          </div>

           <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="productwisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Product Wise Review</h4>
                          </div>
                          <div class="modal-body">
                            <form action="" method="get" class="form-horizontal" novalidate="novalidate" autocomplete="off" >
                                       <div class="form-group">
                                           <label class="form-control-label">Review For:</label>
                                          <div>
                                             <select size="1" id="Rankarf" data-plugin="select2" class="form-control populate" title="" name="Rank">
                                                <option value="">Select</option>
                                                <option value="rproduct">Product</option>
                                                <!-- <option value="ram">Account Manager</option> -->
                                                <!--  <option value="rregion">Region</option>
                                                   <option value="rsbu">SBU</option> -->
                                                <!-- <option value="rsbu">SBU</option> -->
                                                <!-- <option value="week">Weekly</option>
                                                   <option value="adate">Date Range</option> -->
                                             </select>
                                          </div>
                                          <div class="containersrf">
                                             <div class="rproduct">
                                                <label class="form-control-label">Product:</label>
                                                <div>
                                                   <select name="project"  data-plugin="select2" class="form-control populate" id="ms_example6" onchange="redirectmn(this.value);" required >
                                                      <option value="">Select</option>
                                                      <?php
                                                         $resfet=mysqli_query($dbc,"select distinct(ProductName) from products");
                                                         while($valrw=mysqli_fetch_assoc($resfet))
                                                         {
                                                             $pnam=$valrw['ProductName'];
                                                            echo '<option value="'.$pnam.'">'.$pnam.'</option>'; 
                                                            
                                                         }
                                                         ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


          <div class="col-xl-2 col-md-2" data-target="#customerwisemodal" data-toggle="modal">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaFour">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-view-list grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Customer Wise Review
                  </div>
                  
                </div>
                
              </div>
            </div>
            <!-- End Widget Linearea Four -->
          </div>


          <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="customerwisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Customer Wise Review</h4>
                          </div>
                          <div class="modal-body">
                           
                             <form action="" method="get" class="form-horizontal" novalidate="novalidate" autocomplete="off" >
                                       <div class="form-group">
                                          <!-- <label class="col-sm-2">&nbsp;</label>
                                          <label class="col-sm-2 control-label"  for="w4-username"><b>Review For:</b></label>
                                          <div class="col-sm-8" style="margin-left: 30px" >
                                             <select size="1" id="Rankcust" class="form-control" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' title="" name="Rank">
                                                <option value="">Select</option>
                                                <option value="rcustomer">Customers</option>
                                                
                                             </select>
                                          </div> -->
                                          <div class="containercust">
                                             <div class="rcustomer">
                                                <label class="form control-label">Customer Name:</label>
                                                <div>
                                                   <select data-plugin="select2" class="form-control populate" name="project" id="ms_example6" onchange="redirectcusts(this.value);" required >
                                                      <option value="">Select</option>
                                                      <?php
                                                         $resfet=mysqli_query($dbc,"select distinct(Company) from funnel where Stage='Won'");
                                                         while($valrw=mysqli_fetch_assoc($resfet))
                                                         {
                                                             $cname=$valrw['Company'];
                                                            echo '<option value="'.$cname.'">'.$cname.'</option>'; 
                                                            
                                                         }
                                                         ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
                    <div class="col-xl-4 col-md-4">
            <!-- Widget Linearea One-->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-5 pt-5">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                  <i class="icon md-account grey-600 font-size-20 vertical-align mr-2"></i>                 <b><?php 
                                    if($uid == null)
                                    {
                                        echo "Profile: Company";
                                    }
                                    else
                                    {
                                    $fetname=mysqli_query($dbc,"select * from team where email='$uid' ");
                                    while($rwnm=mysqli_fetch_assoc($fetname))
                                    {
                                        $name=$rwnm['name'];
                                    }
                                    echo "Profile: ".$name; 
                                    }?></b>
                  </div>
  
                </div>
               
                
              </div>
            </div>
            <!-- End Widget Linearea One -->
          </div>


           <div class="col-xl-4 col-md-4">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-input-antenna grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Customers
                  </div>
                    <?php 
                                             $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `customers` where RMail='$id' AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                             $rc=mysqli_fetch_assoc($cust);
                                             $res=$rc['count_column'];
                                             
                                             ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                    <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example17-modal-lg" data-toggle="modal" id="totalcustomersmodal">Total</button>
                    <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target="#exampleTabs1" data-toggle="modal">Top</button>
                    <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target="#exampleTabs4" data-toggle="modal">Active/ Inactive</button>
                 
                </div>
              </div>
            </div>


   <!-- Modal -->
                    <div class="modal fade example17-modal-lg" id="totalcustomersmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                    /*$fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail,Date,CampaignType from `campaign` group by Company ");*/
                                    // $fetfunnel=mysqli_query($dbc,"select distinct(t.Company), t.ContactPerson, t.Designation, t.Mobile, t.Mail, t.Date, t.CampaignType from (SELECT * FROM campaign where RMail='$id' AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ORDER BY ModificationDetail DESC) as t group by Company order by   `Date` desc");
                                     
                                     $fetfunnel=mysqli_query($dbc,"select distinct(Company), Address, Sector, SubSector, ContactPerson2, ContactNumber, EmailID from customers where RMail='$id' ORDER BY `ModificationDetail` desc ");
                                    
                                    //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                    /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Sector</th>';
                                                echo '<th>Sub Sector</th>';
                                                echo '<th>Contact Person</th>';
                                                echo '<th>Mail ID</th>';
                                                echo '<th>Contact No.</th>';
                                                
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $cci=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $cci;
                                                $cci=$cci+1;
                                                $Company=$frow['Company'];
                                                $Sector=$frow['Sector'];
                                                $SubSector=$frow['SubSector'];
                                                $ContactPerson2=$frow['ContactPerson2'];
                                                $ContactNumber=$frow['ContactNumber'];
                                                $EmailID=$frow['EmailID'];
                                    
                                                $st=mysqli_query($dbc,"select Stage from funnel where RMail='$id' AND Company='$company' and MeetingDate='$campaigneddate' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                while ($str=mysqli_fetch_assoc($st)) {
                                                    $status=$str['Stage'];
                                                }
                                        
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$cci."</td>";
                                                echo "<td>{$frow['Company']}</td>";
                                                echo "<td>{$frow['Sector']}</td>";
                                                echo "<td>{$frow['SubSector']}</td>";
                                                echo "<td>{$frow['ContactPerson2']}</td>";
                                                echo "<td>{$frow['EmailID']}</td>";
                                                echo "<td>{$frow['ContactNumber']}</td>";
                                                
                                            
                                            
                                                echo '</tr>';
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                     <!-- Modal -->

                <div class="modal fade example22-modal-lg" id="exampleTabs1" aria-hidden="true" aria-labelledby="exampleModalTabs"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalTabs">Top Customers</h4>
                          </div>

                          <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                            <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleLine1"
                                aria-controls="exampleLine1" role="tab">Top Customers</a></li>
                            
                            
                          </ul>

                          <div class="modal-body">
                            <div class="tab-content">
                              <div class="tab-pane active" id="exampleLine1" role="tabpanel">
                               <?php
                                    /*$fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail,Date,CampaignType from `campaign` group by Company ");*/
                                    $fetfunnel=mysqli_query($dbc,"select distinct(Company), CampaignType, Sector, Products, Services, Revenue, Margin, Stage, Probability from funnel where RMail='$id' AND Stage='Won' ORDER BY `Revenue` desc ");
                                    
                                    
                                    //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                    /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Sector</th>';
                                                echo '<th>Products</th>';
                                                echo '<th>Campaign Type</th>';
                                                echo '<th>Stage</th>';
                                                echo '<th>Revenue</th>';
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $cci=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $cci;
                                                $cci=$cci+1;
                                                $company=$frow['Company'];
                                                $contactperson=$frow['ContactPerson'];
                                                $Revenue=$frow['Revenue'];
                                                $campaigneddate=$frow['Date'];
                                                $Sector=$frow['Sector'];
                                                $CampaignType=$frow['CampaignType'];
                                                $Stage=$frow['Stage'];
                                    
                                                //$st=mysqli_query($dbc,"select Stage from funnel where RMail='$id' AND Company='$company' and MeetingDate='$campaigneddate' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                //while ($str=mysqli_fetch_assoc($st)) {
                                                  //  $status=$str['Stage'];
                                                //}
                                        
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$cci."</td>";
                                                echo "<td>{$frow['Company']}</td>";
                                                echo "<td>{$frow['Sector']}</td>";
                                                echo "<td>{$frow['Products']}</td>";
                                                echo "<td>{$frow['CampaignType']}</td>";
                                                echo "<td>{$frow['Stage']}</td>";
                                                echo "<td>{$frow['Revenue']}</td>";
                                                
                                            
                                            
                                                echo '</tr>';
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>
                              </div>

                              

                              

                             
                               
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>     
                               
   <div class="modal fade example21-modal-lg" id="exampleTabs4" aria-hidden="true" aria-labelledby="exampleModalTabs"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalTabs">Top Customers</h4>
                          </div>

                          <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                            <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleLine1"
                                aria-controls="exampleLine1" role="tab">Top Active</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleLine2"
                                aria-controls="exampleLine2" role="tab">Top Inactive</a></li>
                            
                          </ul>

                          <div class="modal-body">
                            <div class="tab-content">
                              <div class="tab-pane active" id="exampleLine1" role="tabpanel">
                               <?php
                                    /*$fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail,Date,CampaignType from `campaign` group by Company ");*/
                                    $fetfunnel=mysqli_query($dbc,"select distinct(Company), QuotNo, GrandTotal, ModificationDetail from quotation where RMail='$id' AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ORDER BY `ModificationDetail` desc ");
                                    
                                    
                                    //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                    /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Quotation No.</th>';
                                                echo '<th>Grand Total</th>';
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $cci=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $cci;
                                                $cci=$cci+1;
                                                $company=$frow['Company'];
                                                $quotno=$frow['quotno'];
                                                $grandtotal=$frow['GrandTotal'];

                                    
                                                //$st=mysqli_query($dbc,"select Stage from funnel where RMail='$id' AND Company='$company' and MeetingDate='$campaigneddate' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                //while ($str=mysqli_fetch_assoc($st)) {
                                                  //  $status=$str['Stage'];
                                                //}
                                        
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$cci."</td>";
                                                echo "<td>{$frow['Company']}</td>";
                                                echo '<td><a href="TCPDF/crm/quotation.php?q='.$quotno.'" target="_blank">'.$quotno.'</a></td>';
                                                echo "<td>{$frow['grandtotal']}</td>";
                                            
                                            
                                                echo '</tr>';
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>
                              </div>

                              <div class="tab-pane" id="exampleLine2" role="tabpanel">
                               <?php
                                    /*$fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail,Date,CampaignType from `campaign` group by Company ");*/
                                    $fetfunnel=mysqli_query($dbc,"select distinct(Company), QuotNo, GrandTotal, ModificationDetail from quotation where RMail='$id' AND DATE(ModificationDetail) NOT BETWEEN '$fydate' AND '$lydate'  ORDER BY `ModificationDetail` desc ");
                                    
                                    
                                    //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                    /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Quotation No.</th>';
                                                echo '<th>Grand Total</th>';
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $cci=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $cci;
                                                $cci=$cci+1;
                                                $company=$frow['Company'];
                                                $quotno=$frow['quotno'];
                                                $grandtotal=$frow['GrandTotal'];

                                    
                                                //$st=mysqli_query($dbc,"select Stage from funnel where RMail='$id' AND Company='$company' and MeetingDate='$campaigneddate' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                //while ($str=mysqli_fetch_assoc($st)) {
                                                  //  $status=$str['Stage'];
                                                //}
                                        
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$cci."</td>";
                                                echo "<td>{$frow['Company']}</td>";
                                                echo '<td><a href="TCPDF/crm/quotation.php?q='.$quotno.'" target="_blank">'.$quotno.'</a></td>';
                                                echo "<td>{$frow['grandtotal']}</td>";
                                            
                                            
                                                echo '</tr>';
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>
                              </div>

                              

                             
                               
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

          


            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-4 col-md-4">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Meetings
                  </div>
                   <?php 
                                             $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` where RMail='$id' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                             $rc=mysqli_fetch_assoc($cust);
                                             $res=$rc['count_column'];
                                             
                                             ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example1-modal-lg" data-toggle="modal" id="totalmeetingsmodal">Total</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example2-modal-lg" data-toggle="modal" id="monthlymeetingsmodal">Monthly</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example3-modal-lg" data-toggle="modal" id="annuallymeetingsmodal">Annually</button>
                </div>
              </div>
            </div>

            <!-- Total Meetings Modal -->
 <!-- Modal -->
                    <div class="modal fade example1-modal-lg" id="totalmeetingsmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Meetings</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                    $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                        select Company, max(Date) as MaxDate
                                        from meeting where RMail='$id' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'
                                        group by Company
                                    ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company order by `Date` desc");
                                    
                                   echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>ContactPerson</th>';
                                                echo '<th>Designation</th>';
                                                /*echo '<th>Mobile</th>';
                                                echo '<th>Mail</th>';*/
                                                echo '<th>Last Meeting</th>';
                                                echo '<th>Status</th>';
                                                echo '<th>Requirement</th>';
                                    
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $tmi=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $tmi;
                                                $tmi=$tmi+1;
                                                $company=$frow['Company'];
                                                $contactperson=$frow['ContactPerson'];
                                                
                                                $mobile=$frow['Mobile'];
                                                $mailid=$frow['Mail'];
                                                $lastmeeting=$frow['Date'];
                                                $pro=$frow['ProjectName'];
                                                $campaigntype=$frow['CampaignType'];
                                                $requirement=$frow['Requirement'];
                                                $sector=$frow['Sector'];
                                                
                                                $companytype=$frow['CompanyType'];
                                                $designation=$frow['Designation'];
                                    
                                    
                                                $st=mysqli_query($dbc,"select Stage from funnel where RMail='$id' AND Company='$company' and MeetingDate='$lastmeeting' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                while ($str=mysqli_fetch_assoc($st)) {
                                                    $status=$str['Stage'];
                                                }
                                    
                                                if($status == null)
                                                {
                                                    echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                                /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>Meeting Stage</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                                echo '</tr>';
                                                $status=null;
                                    
                                                }
                                                else
                                                {
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                               /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>".$status."</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                            
                                            
                                                echo '</tr>';
                                                $status=null;
                                                }
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

<!-- Monthly Meetings Modal -->
 <!-- Modal -->
                    <div class="modal fade example2-modal-lg" id="monthlymeetingsmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Monthly Meetings</h4>
                          </div>
                          <div class="modal-body">
                         <?php
                                    $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                        select Company, max(Date) as MaxDate
                                        from meeting where RMail='$id' AND MONTH(Date) = MONTH(CURRENT_DATE()) AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'
                                        group by Company
                                    ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company order by `Date` desc");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>ContactPerson</th>';
                                                echo '<th>Designation</th>';
                                                /*echo '<th>Mobile</th>';
                                                echo '<th>Mail</th>';*/
                                                echo '<th>Last Meeting</th>';
                                                echo '<th>Status</th>';
                                                echo '<th>Requirement</th>';
                                    
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $tmi=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $tmi;
                                                $tmi=$tmi+1;
                                                $company=$frow['Company'];
                                                $contactperson=$frow['ContactPerson'];
                                                
                                                $mobile=$frow['Mobile'];
                                                $mailid=$frow['Mail'];
                                                $lastmeeting=$frow['Date'];
                                                $pro=$frow['ProjectName'];
                                                $campaigntype=$frow['CampaignType'];
                                                $requirement=$frow['Requirement'];
                                                $sector=$frow['Sector'];
                                                
                                                $companytype=$frow['CompanyType'];
                                                $designation=$frow['Designation'];
                                    
                                    
                                                $st=mysqli_query($dbc,"select Stage from funnel where RMail='$id' AND Company='$company' and MeetingDate='$lastmeeting' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                while ($str=mysqli_fetch_assoc($st)) {
                                                    $status=$str['Stage'];
                                                }
                                    
                                                if($status == null)
                                                {
                                                    echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                                /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>Meeting Stage</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                                echo '</tr>';
                                                $status=null;
                                    
                                                }
                                                else
                                                {
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                               /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>".$status."</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                            
                                            
                                                echo '</tr>';
                                                $status=null;
                                                }
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                    <!-- Annually Meetings Modal -->
 <!-- Modal -->
                    <div class="modal fade example3-modal-lg" id="annuallymeetingsmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Annual Meetings</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                    $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                        select Company, max(Date) as MaxDate
                                        from meeting where RMail='$id' AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'
                                        group by Company
                                    ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company order by `Date` desc");
                                    
                                    echo '<table class="table table-striped table-responsive table-bordered example"  >';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>ContactPerson</th>';
                                                echo '<th>Designation</th>';
                                                /*echo '<th>Mobile</th>';
                                                echo '<th>Mail</th>';*/
                                                echo '<th>Last Meeting</th>';
                                                echo '<th>Status</th>';
                                                echo '<th>Requirement</th>';
                                    
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $tmi=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $tmi;
                                                $tmi=$tmi+1;
                                                $company=$frow['Company'];
                                                $contactperson=$frow['ContactPerson'];
                                                
                                                $mobile=$frow['Mobile'];
                                                $mailid=$frow['Mail'];
                                                $lastmeeting=$frow['Date'];
                                                $pro=$frow['ProjectName'];
                                                $campaigntype=$frow['CampaignType'];
                                                $requirement=$frow['Requirement'];
                                                $sector=$frow['Sector'];
                                                
                                                $companytype=$frow['CompanyType'];
                                                $designation=$frow['Designation'];
                                    
                                    
                                                $st=mysqli_query($dbc,"select Stage from funnel where RMail='$id' AND Company='$company' and MeetingDate='$lastmeeting' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                while ($str=mysqli_fetch_assoc($st)) {
                                                    $status=$str['Stage'];
                                                }
                                    
                                                if($status == null)
                                                {
                                                    echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                                /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>Meeting Stage</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                                echo '</tr>';
                                                $status=null;
                                    
                                                }
                                                else
                                                {
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                               /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>".$status."</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                            
                                            
                                                echo '</tr>';
                                                $status=null;
                                                }
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-4 col-md-4">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Orders
                  </div>
                  <?php 
                                             $cust=mysqli_query($dbc,"select count(Company) as count_column from `funnel` where RMail='$id' AND `Stage`='Won' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'");
                                             $rc=mysqli_fetch_assoc($cust);
                                             $res=$rc['count_column'];
                                             
                                             ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example4-modal-lg" data-toggle="modal" id="annualordersmodal">Annually</button>
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example5-modal-lg" data-toggle="modal" id="monthlyordersmodal">Monthly</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example8-modal-lg" data-toggle="modal" id="totalordersmodal">Total</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target="#exampleTabs2" data-toggle="modal">Top</button>
                </div>
              </div>
            </div>


   <div class="modal fade example19-modal-lg" id="exampleTabs2" aria-hidden="true" aria-labelledby="exampleModalTabs"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalTabs">Top</h4>
                          </div>

                          <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                            <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleLine5"
                                aria-controls="exampleLine5" role="tab">Top Orders by Revenue</a></li>
                                 <li class="nav-item" role="presentation"><a class="nav-link " data-toggle="tab" href="#exampleLine6"
                                aria-controls="exampleLine6" role="tab">Top Orders by Margin</a></li>
                            
                          </ul>

                          <div class="modal-body">
                            <div class="tab-content">
                              <div class="tab-pane active" id="exampleLine5" role="tabpanel">
                               <?php
                                    /*$fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail,Date,CampaignType from `campaign` group by Company ");*/
                                    $fetfunnel=mysqli_query($dbc,"select distinct(Company), CampaignType, Sector, Products, Services, Revenue, Margin, Stage, Probability from funnel where RMail='$id' AND Stage='Won' ORDER BY `Revenue` desc ");
                                    
                                    
                                    //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                    /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Sector</th>';
                                                echo '<th>Products</th>';
                                                echo '<th>Campaign Type</th>';
                                                echo '<th>Stage</th>';
                                                echo '<th>Revenue</th>';
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $cci=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $cci;
                                                $cci=$cci+1;
                                                $company=$frow['Company'];
                                                $contactperson=$frow['ContactPerson'];
                                                $Revenue=$frow['Revenue'];
                                                $campaigneddate=$frow['Date'];
                                                $Sector=$frow['Sector'];
                                                $CampaignType=$frow['CampaignType'];
                                                $Stage=$frow['Stage'];
                                    
                                                //$st=mysqli_query($dbc,"select Stage from funnel where RMail='$id' AND Company='$company' and MeetingDate='$campaigneddate' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                //while ($str=mysqli_fetch_assoc($st)) {
                                                  //  $status=$str['Stage'];
                                                //}
                                        
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$cci."</td>";
                                                echo "<td>{$frow['Company']}</td>";
                                                echo "<td>{$frow['Sector']}</td>";
                                                echo "<td>{$frow['Products']}</td>";
                                                echo "<td>{$frow['CampaignType']}</td>";
                                                echo "<td>{$frow['Stage']}</td>";
                                                echo "<td>{$frow['Revenue']}</td>";
                                                
                                            
                                            
                                                echo '</tr>';
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>
                              </div>

                              <div class="tab-pane" id="exampleLine6" role="tabpanel">
                               <?php
                                    /*$fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail,Date,CampaignType from `campaign` group by Company ");*/
                                    $fetfunnel=mysqli_query($dbc,"select distinct(Company), CampaignType, Sector, Products, Services, Revenue, Margin, Stage, Probability from funnel where RMail='$id' AND Stage='Won' AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ORDER BY `Margin` desc ");
                                    
                                    
                                    //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                    /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Sector</th>';
                                                echo '<th>Products</th>';
                                                echo '<th>Campaign Type</th>';
                                                echo '<th>Stage</th>';
                                                echo '<th>Margin</th>';
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $cci=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $cci;
                                                $cci=$cci+1;
                                                $company=$frow['Company'];
                                                $contactperson=$frow['ContactPerson'];
                                                $Revenue=$frow['Revenue'];
                                                $campaigneddate=$frow['Date'];
                                                $Sector=$frow['Sector'];
                                                $CampaignType=$frow['CampaignType'];
                                                $Stage=$frow['Stage'];
                                                $Margin=$frow['Margin'];
                                    
                                                //$st=mysqli_query($dbc,"select Stage from funnel where RMail='$id' AND Company='$company' and MeetingDate='$campaigneddate' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                //while ($str=mysqli_fetch_assoc($st)) {
                                                  //  $status=$str['Stage'];
                                                //}
                                        
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$cci."</td>";
                                                echo "<td>{$frow['Company']}</td>";
                                                echo "<td>{$frow['Sector']}</td>";
                                                echo "<td>{$frow['Products']}</td>";
                                                echo "<td>{$frow['CampaignType']}</td>";
                                                echo "<td>{$frow['Stage']}</td>";
                                                echo "<td>{$frow['Margin']}</td>";
                                                
                                            
                                            
                                                echo '</tr>';
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>
                              </div>

                              

                             
                               
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
             <!-- Annually Orders Modal -->
 <!-- Modal -->
                    <div class="modal fade example4-modal-lg" id="annualordersmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Annual Orders</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                    $fetfromopf=mysqli_query($dbc,"select * from `opf` where RMail='$id' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc");

                                      echo '<table class="table table-striped table-responsive table-bordered example"  >';
                                        echo '<thead>';
                                            echo '<tr>';
                                            echo '<th>Sl No.</th>';
                                            echo '<th>Company</th>';
                                            echo '<th>Product</th>';
                                            echo '<th>Service</th>';
                                            echo '<th>Detail</th>';
                                            echo '<th>Revenue</th>';
                                            echo '<th>Margin</th>';
                                            echo '<th>Margin %</th>';
                                            echo '<th>Closure Date</th>';
                                            
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                    $toir=0;
                                    $ttotal=0;
                                    $ttaxval=0;
                                    $trevenue=0;
                                    while($fopf=mysqli_fetch_assoc($fetfromopf))
                                    {
                                      $opfno=$fopf['OPFNo'];
                                      $quotrefno=$fopf['QuotRefNo'];
                                      $vendorname=$fopf['VendorName'];

                                      $closuredate=$fopf['ModificationDetail'];

                                      $vendorprice=0;$quantity=0;$qt=0;$reve=0;$marginvalue=0;$up=0;
                                      $productsordered=null;$partdesc=null;
                                      for($k=1;$k<=10;$k++)
                                      {
                                        $prod=$fopf['Product'.$k];
                                        if($prod != " " && $prod != null)
                                        {
                                          $productsordered=$productsordered." ".$prod.",";

                                        }

                                        $pdesc=$fopf['PartDescription'.$k];
                                        if($pdesc != " " && $pdesc != null)
                                        {
                                          $partdesc=$partdesc." ".$pdesc.",";

                                        }

                                      
                                        $vp=$fopf['VendorPrice'.$k];
                                        $qt=$fopf['Quantity'.$k];
                                        if($vp != " " && $vp != 0)
                                        {
                                          $vendorprice=$vendorprice+$vp;
                                          $reve=$reve+($vp*$qt);

                                        }

                                        $up=$fopf['UnitPrice'.$k];
                                        $qt=$fopf['Quantity'.$k];
                                        if($up != " " && $up != 0)
                                        {
                                          $unitprice=$unitprice+$up;
                                          $marginvalue=$marginvalue+($up*$qt);

                                        }
                                        
                                        $qt=$fopf['Quantity'.$k];
                                        if($qt != " " && $qt != 0)
                                        {
                                          $quantity=$quantity+$qt;

                                        }

                                        $fetfromquot=mysqli_query($dbc,"select * from `quotation` where RMail='$id' AND `QuotNo`='$quotrefno' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc");
                                        while ($fquot=mysqli_fetch_assoc($fetfromquot)) 
                                        {
                                          $company=$fquot['Company'];
                                          $tax=$fquot['Tax'];
                                          $currency=$fquot['Currency'];
                                          $servicename=$fquot['ServiceName'];
                                        }
                                        
                                      }
                                   


                                        
                                              global $toir;
                                                $toir=$toir+1;

                                                $closuredate_final=date("d/m/Y", strtotime($closuredate));

                                                $array =  explode(',', $tax);
                                                $count=0;
                                                $taxval=0;
                                                foreach ($array as $item) {
                                                global $count;
                                                  $count=$count+1;
                                                }

                                                foreach ($array as $item) {

                                                    $taxperarr =  explode('-', $item);

                                                    foreach ($taxperarr as $per) {
                                                      $taxpercentage=$per;

                                                    }

                                                    $taxval=$taxval+(($taxpercentage/100)*$marginvalue);

        
                                                }
                                                
                                                $revenue=sprintf("%.2f",$taxval+$marginvalue);
                                                $margin=sprintf("%.2f",$marginvalue-$reve);
                                                $percentage=sprintf("%.2f",($margin/$revenue)*100);
                                                echo '<tr>';
                                                echo "<td>".$toir."</td>";
                                                echo "<td>".$company."</td>";
                                                echo "<td>".$productsordered."</td>";
                                                echo "<td>".$servicename."</td>";
                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                echo '<td><span class="more">'.$partdesc.'</span></td>';
                                                echo "<td>".$revenue."</td>";
                                                echo '<td>'.$margin.'</td>';
                                                echo "<td>".$percentage."</td>";
                                                
                                                
                                                
                                                echo "<td>".$closuredate_final."</td>";
                                            
                                                echo '</tr>';

                                                $ttotal=$ttotal+$margin;
                                                $ttaxval=$ttaxval+$percentage;
                                                $trevenue=$trevenue+$revenue;
                                        
                                            

                                    }



                                    
                                    
                                              
                                        echo '</tbody>';
                                    
                                    echo '</table>';
                                    echo '<table class="table table-striped table-responsive-md table-bordered example"  >';
                                      echo '<tr>';
                                                
                                                echo '<th>Total</th>';
                                                echo '<th>Revenue: '.$trevenue.'</th>';
                                                echo '<th>Margin: '.$ttotal.'</th>';
                                                echo '<th>Margin %: '.(($total/$trevenue) * 100).'</th>';
                                              
                                    
                                                echo '</tr>';
                                    echo '</table>';
                                    ?>


                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                     <!-- Monthly Orders Modal -->
 <!-- Modal -->
                    <div class="modal fade example5-modal-lg" id="monthlyordersmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Monthly Orders</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                    
                                    
                                    $fetfromopf=mysqli_query($dbc,"select * from `opf` where RMail='$id' and MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc");

                                     echo '<table class="table table-striped table-responsive-md table-bordered example"  >';
                                        echo '<thead>';
                                            echo '<tr>';
                                            echo '<th>Sl No.</th>';
                                            echo '<th>Company</th>';
                                            echo '<th>Product</th>';
                                            echo '<th>Service</th>';
                                            echo '<th>Detail</th>';
                                            echo '<th>Revenue</th>';
                                            echo '<th>Margin</th>';
                                            echo '<th>Margin %</th>';
                                            echo '<th>Closure Date</th>';
                                            
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                    $toir=0;
                                    $ttotal=0;
                                    $ttaxval=0;
                                    $trevenue=0;
                                    while($fopf=mysqli_fetch_assoc($fetfromopf))
                                    {
                                      $opfno=$fopf['OPFNo'];
                                      $quotrefno=$fopf['QuotRefNo'];
                                      $vendorname=$fopf['VendorName'];

                                      $closuredate=$fopf['ModificationDetail'];

                                      $vendorprice=0;$quantity=0;$qt=0;$reve=0;$marginvalue=0;$up=0;
                                      $productsordered=null;$partdesc=null;
                                      for($k=1;$k<=10;$k++)
                                      {
                                        $prod=$fopf['Product'.$k];
                                        if($prod != " " && $prod != null)
                                        {
                                          $productsordered=$productsordered." ".$prod.",";

                                        }

                                        $pdesc=$fopf['PartDescription'.$k];
                                        if($pdesc != " " && $pdesc != null)
                                        {
                                          $partdesc=$partdesc." ".$pdesc.",";

                                        }

                                      
                                        $vp=$fopf['VendorPrice'.$k];
                                        $qt=$fopf['Quantity'.$k];
                                        if($vp != " " && $vp != 0)
                                        {
                                          $vendorprice=$vendorprice+$vp;
                                          $reve=$reve+($vp*$qt);

                                        }

                                        $up=$fopf['UnitPrice'.$k];
                                        $qt=$fopf['Quantity'.$k];
                                        if($up != " " && $up != 0)
                                        {
                                          $unitprice=$unitprice+$up;
                                          $marginvalue=$marginvalue+($up*$qt);

                                        }
                                        
                                        $qt=$fopf['Quantity'.$k];
                                        if($qt != " " && $qt != 0)
                                        {
                                          $quantity=$quantity+$qt;

                                        }

                                        $fetfromquot=mysqli_query($dbc,"select * from `quotation` where RMail='$id' AND `QuotNo`='$quotrefno' and MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc");
                                        while ($fquot=mysqli_fetch_assoc($fetfromquot)) 
                                        {
                                          $company=$fquot['Company'];
                                          $tax=$fquot['Tax'];
                                          $currency=$fquot['Currency'];
                                          $servicename=$fquot['ServiceName'];
                                        }
                                        
                                      }
                                   


                                        
                                              global $toir;
                                                $toir=$toir+1;

                                                $closuredate_final=date("d/m/Y", strtotime($closuredate));

                                                $array =  explode(',', $tax);
                                                $count=0;
                                                $taxval=0;
                                                foreach ($array as $item) {
                                                global $count;
                                                  $count=$count+1;
                                                }

                                                foreach ($array as $item) {

                                                    $taxperarr =  explode('-', $item);

                                                    foreach ($taxperarr as $per) {
                                                      $taxpercentage=$per;

                                                    }

                                                    $taxval=$taxval+(($taxpercentage/100)*$marginvalue);

        
                                                }
                                                
                                                $revenue=sprintf("%.2f",$taxval+$marginvalue);
                                                $margin=sprintf("%.2f",$marginvalue-$reve);
                                                $percentage=sprintf("%.2f",($margin/$revenue)*100);
                                                echo '<tr>';
                                                echo "<td>".$toir."</td>";
                                                echo "<td>".$company."</td>";
                                                echo "<td>".$productsordered."</td>";
                                                echo "<td>".$servicename."</td>";
                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                echo '<td><span class="more">'.$partdesc.'</span></td>';
                                                echo "<td>".$revenue."</td>";
                                                echo '<td>'.$margin.'</td>';
                                                echo "<td>".$percentage."</td>";
                                                
                                                
                                                
                                                echo "<td>".$closuredate_final."</td>";
                                            
                                                echo '</tr>';

                                                $ttotal=$ttotal+$margin;
                                                $ttaxval=$ttaxval+$percentage;
                                                $trevenue=$trevenue+$revenue;
                                        
                                            

                                    }



                                    
                                    
                                            
                                         
                                    
                                                echo '</tr>';
                                        echo '</tbody>';
                                    echo '</table>';
                                     echo '<table class="table table-striped table-responsive-md table-bordered example"  >';

                                                echo '<th>Total</th>';
                                                echo '<th>Revenue: '.$trevenue.'</th>';
                                                echo '<th>Margin: '.$ttotal.'</th>';
                                                echo '<th>Margin %: '.(($total/$trevenue) * 100).'</th>';
                                     echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
 <!-- Weekly Orders Modal -->
 <!-- Modal -->
   


<!-- Today Orders Modal -->
 <!-- Modal -->
                    <div class="modal fade example8-modal-lg" id="totalordersmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Orders</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                    /*$fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$id' AND `Stage`='Won' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc");*/

                                    $fetfromopf=mysqli_query($dbc,"select * from `opf` where RMail='$id'  AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  order by `ModificationDetail` desc");

                                   echo '<table class="table table-striped table-responsive table-bordered example" >';
                                        echo '<thead>';
                                            echo '<tr>';
                                            echo '<th>Sl No.</th>';
                                            echo '<th>Company</th>';
                                            echo '<th>Product</th>';
                                            echo '<th>Service</th>';
                                            echo '<th>Detail</th>';
                                            echo '<th>Revenue</th>';
                                            echo '<th>Margin</th>';
                                            echo '<th>Margin %</th>';
                                            echo '<th>Closure Date</th>';
                                            
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                    $toir=0;
                                    $ttotal=0;
                                    $ttaxval=0;
                                    $trevenue=0;
                                    while($fopf=mysqli_fetch_assoc($fetfromopf))
                                    {
                                      $opfno=$fopf['OPFNo'];
                                      $quotrefno=$fopf['QuotRefNo'];
                                      $vendorname=$fopf['VendorName'];

                                      $closuredate=$fopf['ModificationDetail'];

                                      $vendorprice=0;$quantity=0;$qt=0;$reve=0;$marginvalue=0;$up=0;
                                      $productsordered=null;$partdesc=null;
                                      for($k=1;$k<=10;$k++)
                                      {
                                        $prod=$fopf['Product'.$k];
                                        if($prod != " " && $prod != null)
                                        {
                                          $productsordered=$productsordered." ".$prod.",";

                                        }

                                        $pdesc=$fopf['PartDescription'.$k];
                                        if($pdesc != " " && $pdesc != null)
                                        {
                                          $partdesc=$partdesc." ".$pdesc.",";

                                        }

                                      
                                        $vp=$fopf['VendorPrice'.$k];
                                        $qt=$fopf['Quantity'.$k];
                                        if($vp != " " && $vp != 0)
                                        {
                                          $vendorprice=$vendorprice+$vp;
                                          $reve=$reve+($vp*$qt);

                                        }

                                        $up=$fopf['UnitPrice'.$k];
                                        $qt=$fopf['Quantity'.$k];
                                        if($up != " " && $up != 0)
                                        {
                                          $unitprice=$unitprice+$up;
                                          $marginvalue=$marginvalue+($up*$qt);

                                        }
                                        
                                        $qt=$fopf['Quantity'.$k];
                                        if($qt != " " && $qt != 0)
                                        {
                                          $quantity=$quantity+$qt;

                                        }

                                        $fetfromquot=mysqli_query($dbc,"select * from `quotation` where RMail='$id' AND `QuotNo`='$quotrefno' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc");
                                        while ($fquot=mysqli_fetch_assoc($fetfromquot)) 
                                        {
                                          $company=$fquot['Company'];
                                          $tax=$fquot['Tax'];
                                          $currency=$fquot['Currency'];
                                          $servicename=$fquot['ServiceName'];
                                        }
                                        
                                      }
                                   


                                        
                                              global $toir;
                                                $toir=$toir+1;

                                                $closuredate_final=date("d/m/Y", strtotime($closuredate));

                                                $array =  explode(',', $tax);
                                                $count=0;
                                                $taxval=0;
                                                foreach ($array as $item) {
                                                global $count;
                                                  $count=$count+1;
                                                }

                                                foreach ($array as $item) {

                                                    $taxperarr =  explode('-', $item);

                                                    foreach ($taxperarr as $per) {
                                                      $taxpercentage=$per;

                                                    }

                                                    $taxval=$taxval+(($taxpercentage/100)*$marginvalue);

        
                                                }
                                                /*$company=$frow['Company'];
                                                $product=$frow['Products'];
                                                $service=$frow['Services'];
                                                $detail=$frow['Detail'];
                                                $revenue=$frow['Revenue'];
                                                $margin=$frow['Margin'];
                                                $closuredate=$frow['ModificationDetail'];
                                        
                                                $closuredate_final=date("d/m/Y", strtotime($closuredate));
                                    
                                                $marginpercent=($margin/$revenue)*100;
                                                $marginpercent_final=number_format((float)$marginpercent, 2, '.', '');*/
                                                $revenue=sprintf("%.2f",$taxval+$marginvalue);
                                                $margin=sprintf("%.2f",$marginvalue-$reve);
                                                $percentage=sprintf("%.2f",($margin/$revenue)*100);
                                                echo '<tr>';
                                                echo "<td>".$toir."</td>";
                                                echo "<td>".$company."</td>";
                                                echo "<td>".$productsordered."</td>";
                                                echo "<td>".$servicename."</td>";
                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                echo '<td><span class="more">'.$partdesc.'</span></td>';
                                                echo "<td>".$revenue."</td>";
                                                echo '<td>'.$margin.'</td>';
                                                echo "<td>".$percentage."</td>";
                                                
                                                
                                                
                                                echo "<td>".$closuredate_final."</td>";
                                            
                                                echo '</tr>';

                                                $ttotal=$ttotal+$margin;
                                                $ttaxval=$ttaxval+$percentage;
                                                $trevenue=$trevenue+$revenue;
                                        
                                            

                                    }



                                    /*$fetfunnel=mysqli_query($dbc,"select * from `quotation` where RMail='$id' AND `Stage`='Won' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc");*/
                                    
                                    /*echo '<table class="table table-striped mb-none">';*/
                                   
                                                /*$totalr=0;
                                                $custr=mysqli_query($dbc,"select `Revenue` from `funnel` where RMail='$id' AND `Stage`='Won' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                while($rcr=mysqli_fetch_assoc($custr))
                                                {
                                                    $rev=$rcr['Revenue'];
                                                    global $totalr;
                                                    $totalr=$totalr+$rev;
                                                }
                                                $trevenue=$totalr;
                                    
                                                $totalm=0;
                                                $custm=mysqli_query($dbc,"select `Margin` from `funnel` where RMail='$id' AND `Stage`='Won' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                while($rcm=mysqli_fetch_assoc($custm))
                                                {
                                                    $mar=$rcm['Margin'];
                                                    global $totalm;
                                                    $totalm=$totalm+$mar;
                                                }
                                                $tmargin=$totalm;*/
                                    
                                                echo '<tr>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th>Total</th>';
                                                echo '<th>'.$trevenue.'</th>';
                                                echo '<th>'.$ttotal.'</th>';
                                                echo '<th>'.(($total/$trevenue) * 100).'</th>';
                                                echo '<th></th>';
                                    
                                                echo '</tr>';
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>
                         
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-4 col-md-4">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-wifi-alt-2 grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Funnels
                  </div>
                   <?php 
                                             /*$total=0;
                                             $cust=mysqli_query($dbc,"select `Revenue` from `funnel` where  `Stage`='Won' ");
                                             while($rc=mysqli_fetch_assoc($cust))
                                             {
                                                 $rev=$rc['Revenue'];
                                                 global $total;
                                                 $total=$total+$rev;
                                             }
                                             $res=$total;*/
                                             $cust=mysqli_query($dbc,"select count(id) as count_column from `funnel`  where RMail='$id' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'   ");
                                                     $rc=mysqli_fetch_assoc($cust);
                                                     $res=$rc['count_column'];
                                             
                                             ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right"  data-target="#exampleTabs" data-toggle="modal">Probability</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example15-modal-lg" data-toggle="modal" id="todayfunnelmodal">Today</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example16-modal-lg" data-toggle="modal" id="weeklyfunnelmodal">Weekly</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example10-modal-lg" data-toggle="modal" id="monthlyfunnelmodal">Monthly</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example9-modal-lg" data-toggle="modal" id="annualfunnelmodal">Annually</button>
                </div>
              </div>
            </div>

            <!-- Annual Funnel Modal -->
 <!-- Modal -->
<div class="modal fade example16-modal-lg" id="annualfunnelmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Week's Funnel</h4>
                          </div>
                          <div class="modal-body">
                           

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

 <div class="modal fade example15-modal-lg" id="todayfunnelmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Today's Funnel</h4>
                          </div>
                          <div class="modal-body">
                           

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


                    <div class="modal fade example9-modal-lg" id="annualfunnelmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Annual Funnel</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                    $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$id' AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc ");
                                    
                                   echo '<table class="table table-striped table-responsive table-bordered example"  >';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Product</th>';
                                                echo '<th>Detail</th>';
                                                echo '<th>Revenue</th>';
                                                echo '<th>Margin</th>';
                                                echo '<th>Stage</th>';
                                                echo '<th>Probability</th>';
                                                echo '<th>Expected Date Of Closure</th>';
                                                echo '<th>Expected Closure Month</th>';
                                                echo '<th>Date of Closure</th>';
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $totalrevenue=0;
                                        $totalmargin=0;
                                        $fai=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $totalrevenue;
                                                global $totalmargin;
                                    
                                                global $fai;
                                                $fai=$fai+1;
                                    
                                                $company=$frow['Company'];
                                                $product=$frow['Products'];
                                                $detail=$frow['Detail'];
                                                $revenue=$frow['Revenue'];
                                                $margin=$frow['Margin'];
                                                $stage=$frow['Stage'];
                                                $probability=$frow['Probability'];
                                                $dateofclosure=$frow['DateOfClosure'];
                                                $closuremonth=$frow['ExpectedClosure'];
                                                $closuredate=$frow['ModificationDetail'];
                                                
                                                $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                    
                                                if($stage == "Won")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }elseif($probability == "0")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "25")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "50")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "75")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "100")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }
                                                
                                    
                                            }
                                            echo "<tr>";
                                            echo "<th>Total</th>";
                                            echo "<th></th>";
                                            echo "<th></th>";
                                            echo "<th></th>";
                                            echo "<th>".$totalrevenue."</th>";
                                            echo "<th>".$totalmargin."</th>";
                                            echo "<th></th>";
                                            echo "<th></th>";
                                            echo "<th></th>";
                                            echo "<th></th>";
                                            
                                            echo "<th></th>";
                                            echo "</tr>";
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                     <!-- Monthly Funnel Modal -->
 <!-- Modal -->
                    <div class="modal fade example10-modal-lg" id="monthlyfunnelmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Monthly Funnel</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                    $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$id' AND MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc ");
                                    
                                   echo '<table class="table table-striped table-responsive table-bordered example"  >';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Product</th>';
                                                echo '<th>Detail</th>';
                                                echo '<th>Revenue</th>';
                                                echo '<th>Margin</th>';
                                                echo '<th>Stage</th>';
                                                echo '<th>Probability</th>';
                                                echo '<th>Expected Date Of Closure</th>';
                                                echo '<th>Expected Closure Month</th>';
                                                echo '<th>Date of Closure</th>';
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $totalrevenue=0;
                                        $totalmargin=0;
                                        $fmi=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $totalrevenue;
                                                global $totalmargin;
                                    
                                                global $fmi;
                                    
                                                $fmi=$fmi+1;
                                    
                                                $company=$frow['Company'];
                                                $product=$frow['Products'];
                                                $detail=$frow['Detail'];
                                                $revenue=$frow['Revenue'];
                                                $margin=$frow['Margin'];
                                                $stage=$frow['Stage'];
                                                $probability=$frow['Probability'];
                                                $dateofclosure=$frow['DateOfClosure'];
                                                $closuremonth=$frow['ExpectedClosure'];
                                                $closuredate=$frow['ModificationDetail'];
                                                
                                                $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                    
                                                if($stage == "Won")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }elseif($probability == "0")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "25")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "50")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "75")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "100")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }
                                                
                                    
                                            }
                                            echo "<tr>";
                                            echo "<th>Total</th>";
                                            echo "<th></th>";
                                            echo "<th></th>";
                                            echo "<th></th>";
                                            echo "<th>".$totalrevenue."</th>";
                                            echo "<th>".$totalmargin."</th>";
                                            echo "<th></th>";
                                            echo "<th></th>";
                                            echo "<th></th>";
                                            echo "<th></th>";
                                            
                                            echo "<th></th>";
                                            echo "</tr>";
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                     <!-- weekly Funnel Modal -->
 

               
   <div class="modal fade example12-modal-lg" id="exampleTabs" aria-hidden="true" aria-labelledby="exampleModalTabs"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalTabs">Funnel Probability</h4>
                          </div>

                          <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                            <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleLine1"
                                aria-controls="exampleLine1" role="tab">25%</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleLine2"
                                aria-controls="exampleLine2" role="tab">50%</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleLine3"
                                aria-controls="exampleLine3" role="tab">75%</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleLine4"
                                aria-controls="exampleLine4" role="tab">100%</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleLine5"
                                aria-controls="exampleLine5" role="tab">0-100%</a></li>
                          </ul>

                          <div class="modal-body">
                            <div class="tab-content">
                              <div class="tab-pane active" id="exampleLine1" role="tabpanel">
                                <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$id' AND `Probability`='25' AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc ");
                                                
                                               echo '<table class="table table-striped table-responsive table-bordered example"  >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>Product</th>';
                                                            echo '<th>Detail</th>';
                                                            echo '<th>Revenue</th>';
                                                            echo '<th>Margin</th>';
                                                            echo '<th>Stage</th>';
                                                            echo '<th>Probability</th>';
                                                            echo '<th>Expected Date Of Closure</th>';
                                                            echo '<th>Expected Closure Month</th>';
                                                            echo '<th>Date of Closure</th>';
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                       $totalrevenue=0;
                                                    $totalmargin=0;
                                                    $f25i=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $totalrevenue;
                                                            global $totalmargin;
                                                
                                                            global $f25i;
                                                            $f25i=$f25+1;
                                                
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $stage=$frow['Stage'];
                                                            $probability=$frow['Probability'];
                                                            $dateofclosure=$frow['DateOfClosure'];
                                                            $closuremonth=$frow['ExpectedClosure'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                            
                                                            $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                
                                                            
                                                                echo '<tr>';
                                                                echo "<td>".$f25i."</td>";
                                                                echo "<td>{$frow['Company']}</td>";
                                                                echo "<td>{$frow['Products']}</td>";
                                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                                echo '<td><span class="more">'.$detail.'</span></td>';
                                                                echo "<td>{$frow['Revenue']}</td>";
                                                                echo "<td>{$frow['Margin']}</td>";
                                                                echo "<td>{$frow['Stage']}</td>";
                                                                echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                                    echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 25%;'>";
                                                                        echo "{$frow['Probability']}";
                                                                    echo '</div>';
                                                                echo '</div></td>';
                                                                echo "<td>{$frow['DateOfClosure']}</td>";
                                                                echo "<td>{$frow['ExpectedClosure']}</td>";
                                                                echo "<td>{$closuredate_final}</td>";
                                                                echo '</tr>';
                                                            
                                                            $totalrevenue=$totalrevenue+$revenue;
                                                                $totalmargin=$totalmargin+$margin;
                                                            
                                                            
                                                
                                                        }
                                                        echo '<tr>';
                                                        echo '<th>Total</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th>';echo $totalrevenue;echo '</th>';
                                                        echo '<th>';echo $totalmargin;echo '</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                                ?>
                              </div>

                              <div class="tab-pane" id="exampleLine2" role="tabpanel">
                               <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$id' AND `Probability`='50' AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc ");
                                                
                                                 echo '<table class="table table-striped table-responsive table-bordered example"  >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>Product</th>';
                                                            echo '<th>Detail</th>';
                                                            echo '<th>Revenue</th>';
                                                            echo '<th>Margin</th>';
                                                            echo '<th>Stage</th>';
                                                            echo '<th>Probability</th>';
                                                            echo '<th>Expected Date Of Closure</th>';
                                                            echo '<th>Expected Closure Month</th>';
                                                            echo '<th>Date of Closure</th>';
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                        $totalrevenue=0;
                                                    $totalmargin=0;
                                                    $f50i=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $totalrevenue;
                                                            global $totalmargin;
                                                
                                                            global $f50i;
                                                            $f50i=$f50i+1;
                                                
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $stage=$frow['Stage'];
                                                            $probability=$frow['Probability'];
                                                            $dateofclosure=$frow['DateOfClosure'];
                                                            $closuremonth=$frow['ExpectedClosure'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                            
                                                            $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                
                                                            
                                                                echo '<tr>';
                                                                echo "<td>".$f50i."</td>";
                                                                echo "<td>{$frow['Company']}</td>";
                                                                echo "<td>{$frow['Products']}</td>";
                                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                                echo '<td><span class="more">'.$detail.'</span></td>';
                                                                echo "<td>{$frow['Revenue']}</td>";
                                                                echo "<td>{$frow['Margin']}</td>";
                                                                echo "<td>{$frow['Stage']}</td>";
                                                                echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                                    echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 50%;'>";
                                                                        echo "{$frow['Probability']}";
                                                                    echo '</div>';
                                                                echo '</div></td>';
                                                                echo "<td>{$frow['DateOfClosure']}</td>";
                                                                echo "<td>{$frow['ExpectedClosure']}</td>";
                                                                echo "<td>{$closuredate_final}</td>";
                                                                echo '</tr>';
                                                            
                                                          $totalrevenue=$totalrevenue+$revenue;
                                                                $totalmargin=$totalmargin+$margin;
                                                            
                                                            
                                                
                                                        }
                                                        echo '<tr>';
                                                        echo '<th>Total</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th>';echo $totalrevenue;echo '</th>';
                                                        echo '<th>';echo $totalmargin;echo '</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                                ?>
                              </div>

                              <div class="tab-pane" id="exampleLine3" role="tabpanel">
                               <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$id' AND `Probability`='75' AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc ");
                                                
                                                echo '<table class="table table-striped table-responsive table-bordered example"  >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>Product</th>';
                                                            echo '<th>Detail</th>';
                                                            echo '<th>Revenue</th>';
                                                            echo '<th>Margin</th>';
                                                            echo '<th>Stage</th>';
                                                            echo '<th>Probability</th>';
                                                            echo '<th>Expected Date Of Closure</th>';
                                                            echo '<th>Expected Closure Month</th>';
                                                            echo '<th>Date of Closure</th>';
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $totalrevenue=0;
                                                    $totalmargin=0;
                                                    $f75i=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $totalrevenue;
                                                            global $totalmargin;
                                                
                                                            global $f75i;
                                                            $f75i=$f75i+1;
                                                
                                                
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $stage=$frow['Stage'];
                                                            $probability=$frow['Probability'];
                                                            $dateofclosure=$frow['DateOfClosure'];
                                                            $closuremonth=$frow['ExpectedClosure'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                            
                                                            $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                
                                                            
                                                                echo '<tr>';
                                                                echo "<td>".$f75i."</td>";
                                                                echo "<td>{$frow['Company']}</td>";
                                                                echo "<td>{$frow['Products']}</td>";
                                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                                echo '<td><span class="more">'.$detail.'</span></td>';
                                                                echo "<td>{$frow['Revenue']}</td>";
                                                                echo "<td>{$frow['Margin']}</td>";
                                                                echo "<td>{$frow['Stage']}</td>";
                                                                echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                                    echo "<div class='progress-bar progress-bar' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 75%;background-color: #3CB371'>";
                                                                        echo "{$frow['Probability']}";
                                                                    echo '</div>';
                                                                echo '</div></td>';
                                                                echo "<td>{$frow['DateOfClosure']}</td>";
                                                                echo "<td>{$frow['ExpectedClosure']}</td>";
                                                                echo "<td>{$closuredate_final}</td>";
                                                                echo '</tr>';
                                                            
                                                                $totalrevenue=$totalrevenue+$revenue;
                                                                $totalmargin=$totalmargin+$margin;
                                                            
                                                            
                                                
                                                        }
                                                        echo '<tr>';
                                                        echo '<th>Total</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th>';echo $totalrevenue;echo '</th>';
                                                        echo '<th>';echo $totalmargin;echo '</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                                ?>
                              </div>

                              <div class="tab-pane" id="exampleLine4" role="tabpanel">
                              <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  RMail='$id' AND `Probability`='100' AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc");
                                                
                                                 echo '<table class="table table-striped table-responsive table-bordered example"  >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>Product</th>';
                                                            echo '<th>Detail</th>';
                                                            echo '<th>Revenue</th>';
                                                            echo '<th>Margin</th>';
                                                            echo '<th>Stage</th>';
                                                            echo '<th>Probability</th>';
                                                            echo '<th>Expected Date Of Closure</th>';
                                                            echo '<th>Expected Closure Month</th>';
                                                            echo '<th>Date of Closure</th>';
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $totalrevenue=0;
                                                    $totalmargin=0;
                                                    $f100i=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $f100i;
                                                            $f100i=$f100i+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $stage=$frow['Stage'];
                                                            $probability=$frow['Probability'];
                                                            $dateofclosure=$frow['DateOfClosure'];
                                                            $closuremonth=$frow['ExpectedClosure'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                            
                                                            $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                
                                                            global $totalmargin;
                                                            global $totalrevenue;
                                                
                                                            
                                                                echo '<tr>';
                                                                echo "<td>".$f100i."</td>";
                                                                echo "<td>{$frow['Company']}</td>";
                                                                echo "<td>{$frow['Products']}</td>";
                                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                                echo '<td><span class="more">'.$detail.'</span></td>';
                                                                echo "<td>{$frow['Revenue']}</td>";
                                                                echo "<td>{$frow['Margin']}</td>";
                                                                echo "<td>{$frow['Stage']}</td>";
                                                                echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                                    echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                        echo "{$frow['Probability']}";
                                                                    echo '</div>';
                                                                echo '</div></td>';
                                                                echo "<td>{$frow['DateOfClosure']}</td>";
                                                                echo "<td>{$frow['ExpectedClosure']}</td>";
                                                                echo "<td>{$closuredate_final}</td>";
                                                                echo '</tr>';
                                                                $totalrevenue=$totalrevenue+$revenue;
                                                                $totalmargin=$totalmargin+$margin;
                                                            
                                                            
                                                
                                                        }
                                                        echo '<tr>';
                                                        echo '<th>Total</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th>';echo $totalrevenue;echo '</th>';
                                                        echo '<th>';echo $totalmargin;echo '</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        
                                                        echo '<th></th>';
                                                        echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                                ?>
                              </div>
                               <div class="tab-pane" id="exampleLine5" role="tabpanel">
                                <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$id' AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' order by `ModificationDetail` desc ");
                                                
                                                 echo '<table class="table table-striped table-responsive table-bordered example"  >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>Product</th>';
                                                            echo '<th>Detail</th>';
                                                            echo '<th>Revenue</th>';
                                                            echo '<th>Margin</th>';
                                                            echo '<th>Stage</th>';
                                                            echo '<th>Probability</th>';
                                                            echo '<th>Expected Date Of Closure</th>';
                                                            echo '<th>Expected Closure Month</th>';
                                                            echo '<th>Date of Closure</th>';
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                        $totalrevenue=0;
                                                    $totalmargin=0;
                                                    $falli=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $totalrevenue;
                                                            global $totalmargin;
                                                
                                                            global $falli;
                                                            $falli=$falli+1;
                                                
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $stage=$frow['Stage'];
                                                            $probability=$frow['Probability'];
                                                            $dateofclosure=$frow['DateOfClosure'];
                                                            $closuremonth=$frow['ExpectedClosure'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                            
                                                            $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                
                                                            
                                                                echo '<tr>';
                                                                echo "<td>".$falli."</td>";
                                                                echo "<td>{$frow['Company']}</td>";
                                                                echo "<td>{$frow['Products']}</td>";
                                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                                echo '<td><span class="more">'.$detail.'</span></td>';
                                                                echo "<td>{$frow['Revenue']}</td>";
                                                                echo "<td>{$frow['Margin']}</td>";
                                                                echo "<td>{$frow['Stage']}</td>";
                                                                echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                                    echo "<div class='progress-bar ' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;background-color:#20B2AA '>";
                                                                        echo "{$frow['Probability']}";
                                                                    echo '</div>';
                                                                echo '</div></td>';
                                                                echo "<td>{$frow['DateOfClosure']}</td>";
                                                                echo "<td>{$frow['ExpectedClosure']}</td>";
                                                                echo "<td>{$closuredate_final}</td>";
                                                                echo '</tr>';
                                                            
                                                            $totalrevenue=$totalrevenue+$revenue;
                                                                $totalmargin=$totalmargin+$margin;
                                                            
                                                            
                                                
                                                        }
                                                        echo '<tr>';
                                                        echo '<th>Total</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th>';echo $totalrevenue;echo '</th>';
                                                        echo '<th>';echo $totalmargin;echo '</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                                ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>


            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-4 col-md-4">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-mall grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Products
                  </div>
                  <?php 
                                             $total=0;
                                             $fetchall=mysqli_query($dbc,"select * from `products`");
                                             $totalproducts=mysqli_num_rows($fetchall);
                                             /*while($rc=mysqli_fetch_assoc($cust))
                                             {
                                                 $rev=$rc['Margin'];
                                                 global $total;
                                                 $total=$total+$rev;
                                             }
                                             $res=$total;
                                             */
                                             ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $totalproducts; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example13-modal-lg" data-toggle="modal" id="productsmodal">View</button>
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target="#exampleTabs3" data-toggle="modal">Top</button>
                </div>
              </div>
            </div>



   <div class="modal fade example20-modal-lg" id="exampleTabs3" aria-hidden="true" aria-labelledby="exampleModalTabs"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalTabs">Top</h4>
                          </div>

                          <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                            <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleLine7"
                                aria-controls="exampleLine7" role="tab">Top Orders by Revenue</a></li>
                                 <li class="nav-item" role="presentation"><a class="nav-link " data-toggle="tab" href="#exampleLine8"
                                aria-controls="exampleLine8" role="tab">Top Orders by Margin</a></li>
                            
                          </ul>

                          <div class="modal-body">
                            <div class="tab-content">
                              <div class="tab-pane active" id="exampleLine7" role="tabpanel">
                                <?php
                                    /*$fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail,Date,CampaignType from `campaign` group by Company ");*/
                                    $fetfunnel=mysqli_query($dbc,"select distinct(Products), Revenue, Margin, Stage from funnel where RMail='$id' ORDER BY `Revenue` desc ");
                                    
                                    
                                    //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                    /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Products</th>';
                                                echo '<th>Revenue</th>';
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $cci=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $cci;
                                                $cci=$cci+1;
                                                $Products=$frow['Products'];
                                                $Revenue=$frow['Revenue'];
                                              
                                    
                                                //$st=mysqli_query($dbc,"select Stage from funnel where RMail='$id' AND Company='$company' and MeetingDate='$campaigneddate' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                //while ($str=mysqli_fetch_assoc($st)) {
                                                  //  $status=$str['Stage'];
                                                //}
                                        
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$cci."</td>";
                                                echo "<td>{$frow['Products']}</td>";
                                                echo "<td>{$frow['Revenue']}</td>";
                                                
                                            
                                            
                                                echo '</tr>';
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>
                              </div>

                              <div class="tab-pane" id="exampleLine8" role="tabpanel">
                               <?php
                                    /*$fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail,Date,CampaignType from `campaign` group by Company ");*/
                                    $fetfunnel=mysqli_query($dbc,"select distinct(Products), Revenue, Margin, Stage from funnel where RMail='$id' AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ORDER BY `Margin`  desc ");
                                    
                                    
                                    //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                    /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Products</th>';
                                                echo '<th>Margin</th>';
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $cci=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $cci;
                                                $cci=$cci+1;
                                                $Products=$frow['Products'];
                                                $Revenue=$frow['Revenue'];
                                                $Stage=$frow['Stage'];
                                                $Margin=$frow['Margin'];
                                    
                                                //$st=mysqli_query($dbc,"select Stage from funnel where RMail='$id' AND Company='$company' and MeetingDate='$campaigneddate' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                //while ($str=mysqli_fetch_assoc($st)) {
                                                  //  $status=$str['Stage'];
                                                //}
                                        
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$cci."</td>";
                                                echo "<td>{$frow['Products']}</td>";
                                                echo "<td>{$frow['Margin']}</td>";
                                                
                                            
                                            
                                                echo '</tr>';
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>
                              </div>

                              

                             
                               
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
             <!-- Products Modal -->
 <!-- Modal -->
                    <div class="modal fade example13-modal-lg" id="productsmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Products</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                    $fetfunnel=mysqli_query($dbc,"select * from `products` ");
                                      $pi=0;
                                   echo '<table class="table table-striped table-responsive-md table-bordered example"  >';
                                        echo '<thead>';
                                            echo '<tr>';
                                            echo '<th>Slno</th>';
                                            echo '<th>Category</th>';
                                            echo '<th>Product Name</th>';
                                            //echo '<th><b>Revenue</b></th>';
                                            /*echo '<th><b>Margin</b></th>';
                                            echo '<th>Closure Date</th>';*/
                                            
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $pi;
                                                $pi=$pi+1;
                                                $category=$frow['Category'];
                                                $productname=$frow['ProductName'];
                                                /*$detail=$frow['Detail'];*/
                                                //$revenue=$frow['Revenue'];
                                                /*$margin=$frow['Margin'];
                                                $closuredate=$frow['ModificationDetail'];
                                        
                                                $closuredate_final=date("d/m/Y", strtotime($closuredate));*/
                                        
                                                echo '<tr>';
                                                echo "<td>".$pi."</td>";
                                                echo "<td>{$frow['Category']}</td>";
                                                echo "<td>{$frow['ProductName']}</td>";
                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                //echo "<td><b>{$frow['Revenue']}</b></td>";
                                                /*echo "<td><b>{$frow['Margin']}</b></td>";
                                                echo "<td>{$closuredate_final}</td>";*/
                                            
                                            
                                                echo '</tr>';
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    <!-- End Modal -->
            <!-- End Widget Linearea Four -->


            <div class="col-xl-4 col-md-4">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-mall grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Tickets
                  </div>
                
                  <span class="float-right grey-700 font-size-30"></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example14-modal-lg" data-toggle="modal" id="ticketmodal">Critical</button>
                </div>
              </div>
            </div>


             <!-- Products Modal -->
 <!-- Modal -->
                    <div class="modal fade example14-modal-lg" id="ticketmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Critical Tickets</h4>
                          </div>
                          <div class="modal-body">
                           

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>

          </div>



          <div class="col-xxl-8 col-lg-8">
            <!-- Widget Jvmap -->
            <div class="card card-shadow">
              <div class="card-block p-0">
              <?php
                                 $fannual=mysqli_query($dbc,"select * from `funnel` where Stage='Won' and RMail='$id' AND   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                 $jan=0;
                                 $feb=0;$mar=0;$apr=0;$may=0;$jun=0;$jul=0;$aug=0;$sep=0;$oct=0;$nov=0;$dec=0;
                                 $janm=0;
                                 $febm=0;$marm=0;$aprm=0;$maym=0;$junm=0;$julm=0;$augm=0;$sepm=0;$octm=0;$novm=0;$decm=0;
                                 while($mont=mysqli_fetch_assoc($fannual))
                                 {
                                 
                                     $val=$mont['ModificationDetail'];
                                     $rev=$mont['Revenue'];
                                     $fmar=$mont['Margin'];
                                 
                                     $fetmon=date("M", strtotime($val));
                                 
                                     if($fetmon == "Jan")
                                     {
                                         /*global $feb;
                                         $jan=$jan+1;*/
                                         global $rev;
                                         global $fmar;
                                         global $jan;
                                         global $janm;
                                         $jan=$jan + $rev;
                                         $janm=$janm + $fmar;
                                 
                                     } elseif ($fetmon == "Feb")
                                     {
                                         /*global $feb;
                                         $feb=$feb+1;*/
                                         global $rev;
                                         global $fmar;
                                         global $feb;
                                         global $febm;
                                         $feb=$feb + $rev;
                                         $febm=$febm + $fmar;
                                 
                                     } elseif ($fetmon == "Mar")
                                     {
                                         /*global $mar;
                                         $mar=$mar+1;*/
                                         global $rev;
                                         global $fmar;
                                         global $mar;
                                         global $marm;
                                         $mar=$mar + $rev;
                                         $marm=$marm + $fmar;
                                 
                                     } elseif ($fetmon == "Apr")
                                     {
                                         /*global $apr;
                                         $apr=$apr+1;*/
                                         global $rev;
                                         global $fmar;
                                         global $apr;
                                         global $aprm;
                                         $apr=$apr + $rev;
                                         $aprm=$aprm + $fmar;
                                 
                                     } elseif ($fetmon == "May")
                                     {
                                         /*global $may;
                                         $may=$may+1;*/
                                         global $rev;
                                         global $fmar;
                                         global $may;
                                         global $maym;
                                         $may=$may + $rev;
                                         $maym=$maym + $fmar;
                                 
                                     } elseif ($fetmon == "Jun")
                                     {
                                         /*global $jun;
                                         $jun=$jun+1;*/
                                         global $rev;
                                         global $fmar;
                                         global $jun;
                                         global $junm;
                                         $jun=$jun + $rev;
                                         $junm=$junm + $fmar;
                                 
                                     } elseif ($fetmon == "Jul")
                                     {
                                         /*global $jul;
                                         $jul=$jul+1;*/
                                         global $rev;
                                         global $fmar;
                                         global $jul;
                                         global $julm;
                                         $jul=$jul + $rev;
                                         $julm=$julm + $fmar;
                                 
                                     } elseif ($fetmon == "Aug")
                                     {
                                         /*global $aug;
                                         $aug=$aug+1;*/
                                         global $rev;
                                         global $fmar;
                                         global $aug;
                                         global $augm;
                                         $aug=$aug + $rev;
                                         $augm=$augm + $fmar;
                                 
                                     } elseif ($fetmon == "Sep")
                                     {
                                         /*global $sep;
                                         $sep=$sep+1;*/
                                         global $rev;
                                         global $fmar;
                                         global $sep;
                                         global $sepm;
                                         $sep=$sep + $rev;
                                         $sepm=$sepm + $fmar;
                                 
                                     } elseif ($fetmon == "Oct")
                                     {
                                         /*global $oct;
                                         $oct=$oct+1;*/
                                         global $rev;
                                         global $fmar;
                                         global $oct;
                                         global $octm;
                                         $oct=$oct + $rev;
                                         $octm=$octm + $fmar;
                                 
                                     } elseif ($fetmon == "Nov")
                                     {
                                         /*global $nov;
                                         $nov=$nov+1;*/
                                         global $rev;
                                         global $fmar;
                                         global $nov;
                                         global $novm;
                                         $nov=$nov + $rev;
                                         $novm=$novm + $fmar;
                                 
                                     } elseif ($fetmon == "Dec")
                                     {
                                         global $rev;
                                         global $fmar;
                                         global $dec;
                                         global $decm;
                                         $dec=$dec + $rev;
                                         $decm=$decm + $fmar;
                                 
                                     }
                                 
                                         
                                     
                                 }
                                 
                                 
                                 
                                 
                                 
                                 
                                     
                                 ?>
                              <!-- target fetch for year -->
                              <?php 
                                 $county=0;
                                  $fannual=mysqli_query($dbc,"select * from `target_admin` where  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                  while($mont=mysqli_fetch_assoc($fannual))
                                  {
                                     /*$val=$mont['ModificationDetail'];
                                     $fetmon=date("y", strtotime($val));
                                 
                                     $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                                     $rc=mysqli_fetch_assoc($cust);
                                     $res=$rc['count_column'];*/
                                     $val=$mont['RevenuePerYear'];
                                     global $county;
                                     $county=$county+$val;
                                 
                                  }
                                 
                                 ?>
                              <script type="text/javascript">
                                 google.charts.load('current', {'packages':['corechart']});
                                 google.charts.setOnLoadCallback(drawChart);
                                 
                                 function drawChart() {
                                   var data = google.visualization.arrayToDataTable([
                                     ['Month', 'Revenue',  'Revenue Target','Expenses', 'Margin','Margin Target'],
                                     
                                     ['Apr',  <?php echo $apr=$apr; ?>,  <?php echo $county; ?>, 0, <?php echo $aprm; ?>, <?php echo $janm; ?>],
                                     ['May',  <?php echo $may=$may+$apr; ?>,   <?php echo $county; ?>, 0, <?php echo $maym; ?>, <?php echo $janm; ?>],
                                     ['Jun',  <?php echo $jun=$jun+$may; ?>,   <?php echo $county; ?>,0, <?php echo $junm; ?>, <?php echo $janm; ?>],
                                     ['Jul',  <?php echo $jul=$jul+$jun; ?>,  <?php echo $county; ?>,0,  <?php echo $julm; ?>, <?php echo $janm; ?>],
                                     ['Aug',  <?php echo $aug=$aug+$jul; ?>,  <?php echo $county; ?>,0, <?php echo $augm; ?>, <?php echo $janm; ?>],
                                     ['Sep',  <?php echo $sep=$sep+$aug; ?>,  <?php echo $county; ?>,0, <?php echo $sepm; ?>, <?php echo $janm; ?>],
                                     ['Oct',  <?php echo $oct=$oct+$sep; ?>,  <?php echo $county; ?>,0, <?php echo $octm; ?>, <?php echo $janm; ?>],
                                     ['Nov',  <?php echo $nov=$nov+$oct; ?>,  <?php echo $county; ?>,0,  <?php echo $novm; ?>, <?php echo $janm; ?>],
                                     ['Dec',  <?php echo $dec=$dec+$nov; ?>,   <?php echo $county; ?>,0, <?php echo $decm; ?>, <?php echo $janm; ?>],
                                     ['Jan',  <?php echo $jan=$jan+$dec; ?>,             <?php echo $county; ?>, 0, <?php echo $janm; ?>, <?php echo $janm; ?>],
                                     ['Feb',  <?php echo $feb=$feb+$jan; ?>,  <?php echo $county; ?>, 0, <?php echo $febm; ?>, <?php echo $janm; ?>],
                                     ['Mar',  <?php echo $mar=$mar+$feb; ?>,  <?php echo $county; ?>, 0, <?php echo $marm; ?>, <?php echo $janm; ?>],
                                   ]);
                                 
                                   var options = {
                                     title: 'Your Performance This Year',
                                     hAxis: {title: 'Month',  titleTextStyle: {color: '#333'}},
                                     vAxis: {minValue: 0}
                                   };
                                 
                                   var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                                   chart.draw(data, options);
                                 }
                              </script>
                              <div id="chart_div" style="width: 100%; height: 300px;"></div>
              </div>
            </div>
            <!-- End Widget Jvmap -->
          </div>


          <div class="col-xl-4 col-lg-4">
            <!-- Widget User list -->
           <div class="card card-shadow">
              <div class="card-block p-0">
                
               <?php
                                 $inf=0;$sec=0;$sd=0;$an=0;$dt=0;
                                 $fetprod=mysqli_query($dbc,"select Products from funnel where RMail='$id' AND  Stage='Won'  AND   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                 
                                 while($rwprods=mysqli_fetch_assoc($fetprod))
                                 {
                                 
                                     $prodname=$rwprods['Products'];
                                     
                                     $fetsbu=mysqli_query($dbc,"select * from products where  ProductName='$prodname' ");
                                     while($rwprod=mysqli_fetch_assoc($fetsbu))
                                     {
                                        $category=$rwprod['Category'];
                                      }
                                     
                                 
                                     if($category == "Infrastructure") {
                                         $inf=$inf+1;
                                 
                                     } 
                                 
                                     
                                 
                                     if($category == "Security") {
                                         $sec=$sec+1;
                                 
                                     } 
                                     
                                 
                                     if($category == "Software Development & Testing") {
                                         $sd=$sd+1;
                                 
                                     } 
                                     
                                 
                                     if($category == "Analytics") {
                                         $an=$an+1;
                                 
                                     } 
                                     
                                 
                                     if($category == "Digital Transformation") {
                                         $dt=$dt+1;
                                 
                                     }   
                                 
                                 
                                 }
                                 
                                 
                                 ?>
                              <script type="text/javascript">
                                 google.charts.load("current", {packages:["corechart"]});
                                 google.charts.setOnLoadCallback(drawChart);
                                 function drawChart() {
                                   var data = google.visualization.arrayToDataTable([
                                     ['SBU', 'Products'],
                                     ['Infrastructure',     <?php echo $inf; ?>],
                                     ['Security',      <?php echo $sec; ?>],
                                     ['Software Development & Testing',  <?php echo $sd; ?>],
                                     ['Analytics', <?php echo $an; ?>],
                                     ['Digital Transformation',    <?php echo $dt; ?>]
                                   ]);
                                 
                                   var options = {
                                     title: 'SBU Achievement',
                                     is3D: true,
                                   };
                                 
                                   var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
                                   chart.draw(data, options);
                                 }
                              </script>
                              <!-- pie chart end -->
                              <!-- alt pie chart bar graph start -->

                              
                        <script type="text/javascript">
                           google.charts.load('current', {'packages':['corechart']});
                           google.charts.setOnLoadCallback(drawVisualization);
                           
                           function drawVisualization() {
                           
                           var data = google.visualization.arrayToDataTable([
                           ['Month','Value'],
                           ['Infrastructure', 0],
                           ['Security', 0  ],
                           ['Soft&Dev', 0 ],
                           ['Analytics', 0 ],
                           ['DT',  0   ]
                           
                           
                           ]);
                           
                           var options = {
                           title : 'SBU Achievement',
                           vAxis: {title: 'Value'},
                           hAxis: {title: ''},
                           seriesType: 'bars',
                           series: {2: {type: 'line'}}
                           };
                           
                           var chart = new google.visualization.ComboChart(document.getElementById('chart_pie_alt'));
                           chart.draw(data, options);
                           }
                        </script>

                              <?php
                              if($inf == 0 and $sec==0 and $sd == 0 and $an ==0 and $dt == 0)
                              {
                                echo '<div id="chart_pie_alt"  style="width: 100%; height: 300px;"></div>';
                              }
                              else
                              {
                                
                                echo '<div id="piechart_3d" style="width: 100%; height: 300px;"></div>';

                              }

                              ?>
              </div>
            </div>
            <!-- End Widget User list -->
          </div>

           <div class="col-xl-12 col-lg-12">
            <!-- Widget User list -->
           <div class="card card-shadow">
              <div class="card-block p-0">
                <?php 
                           $countymeet=0;
                            $fannualm=mysqli_query($dbc,"select * from `target` where Employee='$id' and   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($montm=mysqli_fetch_assoc($fannualm))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $valm=$montm['MeetingPerYear'];
                               global $countymeet;
                               $countymeet=$countymeet+$valm;
                           
                            }
                           
                           ?>
                        <?php 
                           $resmeet=0;
                           $custm=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                           $rcm=mysqli_fetch_assoc($custm);
                           $resmeet=$rcm['count_column'];
                           
                           
                           ?>
                        <?php 
                           $countymeetm=0;
                            $fannual=mysqli_query($dbc,"select * from `target` where Employee='$id' and MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['MeetingPerMonth'];
                               global $countymeetm;
                               $countymeetm=$countymeetm+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                           $rc=mysqli_fetch_assoc($cust);
                           $resmeetm=$rc['count_column'];
                           $countya=0;
                            
                           ?>
                        <!-- customers -->
                        <?php 
                           $countycust=0;
                            $fannual=mysqli_query($dbc,"select * from `target` where Employee='$id' and  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['CustomersPerYear'];
                               global $county;
                               $countycust=$countycust+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign` ");
                           $rc=mysqli_fetch_assoc($cust);
                           $rescusty=$rc['count_column'];
                           $countya=0;
                           
                           ?>
                        <?php 
                           $countmcust=0;
                            $fannual=mysqli_query($dbc,"select * from `target` where Employee='$id' and MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['CustomersPerMonth'];
                               global $countmcust;
                               $countmcust=$countmcust+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign` where   MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                           $rc=mysqli_fetch_assoc($cust);
                           $rescustm=$rc['count_column'];
                           $countya=0;
                            
                           ?>
                        <!-- sales certification -->
                        <?php 
                           $countysc=0;
                            $fannual=mysqli_query($dbc,"select distinct(Employee),SalesCertificationPerYear from `target` where  Employee='$id' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               /*$fe=mysqli_query($dbc,"select distinct(Employee),SalesCertificationPerYear from `target` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");*/
                           
                               $val=$mont['SalesCertificationPerYear'];
                               global $countysc;
                               $countysc=$countysc+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countyasc=0;
                            $fannual=mysqli_query($dbc,"select distinct(RMail),SalesCertificationPerYear from `training` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['SalesCertificationPerYear'];
                               global $countyasc;
                               $countyasc=$countyasc+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countmsc=0;
                            $fannual=mysqli_query($dbc,"select distinct(Employee),`SalesCertificationPerMonth` from `target` where Employee='$id' and MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['SalesCertificationPerMonth'];
                               global $countmsc;
                               $countmsc=$countmsc+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countmasc=0;
                            $fannual=mysqli_query($dbc,"select distinct(RMail),`SalesCertificationPerMonth` from `training` where MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['SalesCertificationPerMonth'];
                               global $countmasc;
                               $countmasc=$countmasc+$val;
                           
                            }
                           
                           ?>
                        <!-- technical certification -->
                        <?php 
                           $countytc=0;
                            $fannual=mysqli_query($dbc,"select distinct(Employee),TechnicalCertificationPerYear from `target` where Employee='$id' and  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['TechnicalCertificationPerYear'];
                               global $countytc;
                               $countytc=$countytc+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countyatc=0;
                            $fannual=mysqli_query($dbc,"select distinct(RMail),TechnicalCertificationPerYear from `training` where  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['TechnicalCertificationPerYear'];
                               global $countyatc;
                               $countyatc=$countyatc+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countmtc=0;
                            $fannual=mysqli_query($dbc,"select distinct(Employee),TechnicalCertificationPerMonth from `target` where Employee='$id' and MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['TechnicalCertificationPerMonth'];
                               global $countmtc;
                               $countmtc=$countmtc+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countmatc=0;
                            $fannual=mysqli_query($dbc,"select distinct(RMail),TechnicalCertificationPerMonth from `training` where MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['TechnicalCertificationPerMonth'];
                               global $countmatc;
                               $countmatc=$countmatc+$val;
                           
                            }
                           
                           ?>
                        <!-- demo -->
                        <?php 
                           $countyd=0;
                            $fannual=mysqli_query($dbc,"select distinct(Employee),DemoPerYear from `target` where Employee='$id' and  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['DemoPerYear'];
                               global $countyd;
                               $countyd=$countyd+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countyad=0;
                            $fannual=mysqli_query($dbc,"select distinct(RMail),DemoPerYear from `training` where  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['DemoPerYear'];
                               global $countyad;
                               $countyad=$countyad+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countmd=0;
                            $fannual=mysqli_query($dbc,"select distinct(Employee),DemoPerMonth from `target` where Employee='$id' and MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['DemoPerMonth'];
                               global $countmd;
                               $countmd=$countmd+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countmad=0;
                            $fannual=mysqli_query($dbc,"select distinct(RMail),DemoPerMonth from `training` where MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['DemoPerMonth'];
                               global $countmad;
                               $countmad=$countmad+$val;
                           
                            }
                           
                           ?>
                        <!-- poc -->
                        <?php 
                           $countyp=0;
                            $fannual=mysqli_query($dbc,"select distinct(Employee),POCPerYear from `target` where Employee='$id' and DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['POCPerYear'];
                               global $countyp;
                               $countyp=$countyp+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countyap=0;
                            $fannual=mysqli_query($dbc,"select distinct(RMail),POCPerYear from `training` where  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['POCPerYear'];
                               global $countyap;
                               $countyap=$countyap+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countmp=0;
                            $fannual=mysqli_query($dbc,"select distinct(Employee),POCPerMonth from `target` where Employee='$id' and MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['POCPerMonth'];
                               global $countmp;
                               $countmp=$countmp+$val;
                           
                            }
                           
                           ?>
                        <?php 
                           $countmap=0;
                            $fannual=mysqli_query($dbc,"select distinct(RMail),POCPerMonth from `training` where MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                            while($mont=mysqli_fetch_assoc($fannual))
                            {
                               /*$val=$mont['ModificationDetail'];
                               $fetmon=date("y", strtotime($val));
                           
                               $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign`   ");
                               $rc=mysqli_fetch_assoc($cust);
                               $res=$rc['count_column'];*/
                               $val=$mont['POCPerMonth'];
                               global $countmap;
                               $countmap=$countmap+$val;
                           
                            }
                           
                           ?>
                        <?php
                           $avgmeet=0;$avgcust=0;$avgsc=0;$avgtc=0;$avgd=0;$avgp=0;
                           
                               $avgmeet=intval($resmeet/$countymeet);
                           
                               $avgcust=intval($rescusty/$countycust);
                               $avgsc=intval($countyasc/$countysc);
                               $avgtc=intval($countyatc/$countytc);
                               $avgd=intval($countyad/$countyd);
                               $avgp=intval($countyap/$countyp);
                               
                           
                           ?>
                        <!-- <script type="text/javascript" src="../../../log/assets/googlechart/js/loader.js"></script> -->
                        <div id="chart_div_tgtacv"  style="width: 100%; height: 300px;"></div>
                        <script type="text/javascript">
                           google.charts.load('current', {'packages':['corechart']});
                           google.charts.setOnLoadCallback(drawVisualization);
                           
                           function drawVisualization() {
                           // Some raw data (not necessarily accurate)
                           /*var data = google.visualization.arrayToDataTable([
                           ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
                           ['2004/05',  165,      938,         522,             998,           450,      614.6],
                           ['2005/06',  135,      1120,        599,             1268,          288,      682],
                           ['2006/07',  157,      1167,        587,             807,           397,      623],
                           ['2007/08',  139,      1110,        615,             968,           215,      609.4],
                           ['2008/09',  136,      691,         629,             1026,          366,      569.6]
                           ]);*/
                           
                           var data = google.visualization.arrayToDataTable([
                           ['Month', 'PerYear', 'TargetPerYear' ,'PerMonth','TargetPerMonth',  'Achievement Ratio'],
                           ['Meeting',  <?php echo $resmeet; ?> ,   <?php echo $countymeet; ?> , <?php echo $resmeetm; ?>, <?php echo $countymeetm; ?>, <?php echo $avgmeet; ?>   ],
                           ['Prospects', <?php echo $rescusty; ?> , <?php echo $countycust; ?> , <?php echo $rescustm; ?>, <?php echo $countmcust; ?>, <?php echo $avgcust; ?>   ],
                           ['SalCert',  <?php echo $countyasc; ?> ,    <?php echo $countysc; ?> , <?php echo $countmasc; ?>, <?php echo $countmsc; ?>, <?php echo $avgsc; ?>   ],
                           ['TechCert', <?php echo $countyatc; ?> ,   <?php echo $countytc; ?> , <?php echo $countmatc; ?>, <?php echo $countmtc; ?>, <?php echo $avgtc; ?>   ],
                           ['Demo',  <?php echo $countyad; ?> ,   <?php echo $countyd; ?> , <?php echo $countmad; ?>, <?php echo $countmd; ?>, <?php echo $avgd; ?>   ],
                           ['POC',  <?php echo $countyap; ?> ,    <?php echo $countyp; ?> , <?php echo $countmap; ?>, <?php echo $countmp; ?>, <?php echo $avgp; ?>   ]
                           
                           ]);
                           
                           var options = {
                           title : 'Target vs Achievement',
                           vAxis: {title: 'Value'},
                           hAxis: {title: ''},
                           seriesType: 'bars',
                           series: {4: {type: 'line'}}
                           };
                           
                           var chart = new google.visualization.ComboChart(document.getElementById('chart_div_tgtacv'));
                           chart.draw(data, options);
                           }
                        </script>
               
              </div>
            </div>
            <!-- End Widget User list -->
          </div>
         

         

        

        </div>
      </div>
    </div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>
    <!-- Core  -->

    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>

        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
            <script src="../../assets/global/vendor/multi-select/jquery.multi-select.js"></script>
                 <script src="../../assets/global/js/Plugin/multi-select.js"></script>
                  <script src="../../assets/global/js/Plugin/bootstrap-select.js"></script>
                     <script src="../../assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

                  <script >
          $(document).ready(function() {
    $('.example').DataTable();
} );
        </script>
<script>
            $(document).ready(function() {
    $('.example1').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example2').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example3').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example4').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example5').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example6').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example7').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example8').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example9').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example10').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example11').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example12').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example13').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example14').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example15').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example16').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example17').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example18').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example19').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example20').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example21').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example22').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example23').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example24').DataTable();
} );
        </script>
   <script src="select-option-js.js"></script>
  <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>
