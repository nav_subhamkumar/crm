<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Update | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    <!--  date picker  -->
    <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../../assets/global/vendor/clockpicker/clockpicker.css">
    <link rel="stylesheet" href="../../assets/global/fonts/web-icons/web-icons.min.css">
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <?php include "includes/css/select.php"; ?>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>     
    <?php include "side-navigation.php"; ?>   
    <?php
                      //$q=$_GET['v'];

                      $pro=$_GET['t'];
                      $company=$_GET['c'];
                      $fname=$_GET['f'];
                      $lname=$_GET['l'];
                      $e=$_GET['e'];
                                            
                    

                      $c=$_GET['c'];
                      $ctype=$_GET['ctype'];
                      //$cp=$_GET['cp'];
                      $cp=$fname.' '.$lname;
                      $e=$_GET['e'];
                                            $query=mysqli_query($dbc,"select * from `leads` where  `Company`='$c'  order by id ");
                                            while($row=mysqli_fetch_array($query))
                                            {
                                              $projectname=$row['ProjectName'];
                                                $company=$row['Company'];
                                                $sector=$row['Sector'];
                        $campaigntype=$row['CampaignType'];
                        $product=$row['Product'];
                        $date=$row['Date'];
                        $contactperson=$row['ContactPerson'];
                                                $designation=$row['Designation'];
                                                $email=$row['Mail'];
                                                $mobile=$row['Mobile'];
                        $response=$row['Response'];
                        $meetingstatus=$row['MeetingStatus'];
                        $followup=$row['FollowUp'];
                        $requirements=$row['Requirement'];
                        $remarks=$row['Remarks'];
                        $infra=$row['Infrastructure'];
                                               
                                            }    

                                        ?>



    <!-- Page -->
    <div class="page">
      <!-- write body content here -->

      <div class="page-content">

        <div class="panel">
               <div class="panel-body container-fluid">
                  <div class="row row-lg">
                     <div class="col-md-12">
                        <div class="example-wrap">
                           <h4 class="example-title">Update Meeting Data</h4>
                        <form action="../auth/campaign/ins.php" method="post" novalidate="novalidate"  >

                           <input type="hidden" name="campaigntype" value="<?php echo $campaigntype;?>" />
                           <input type="hidden" name="projectname" value="<?php echo $projectname; ?>" />
                           <input type="hidden" name="meetingdate" value="<?php echo $meetingdate; ?>" />

                             <div class="row">
                                <div class="form-group  col-md-12">
                                <label class="form-control-label" >Campaign Type</label>
                  <select name="campaigntype" class="form-control maxlength" data-plugin="select2">
                             
                             <?php
                                  $camp=mysqli_query($dbc,"select * from `campaignlist`");
                                  echo '<option  value="'.$ctype.'">'.$ctype.'</option>';
                                  while($row=mysqli_fetch_assoc($camp))
                                  {
                                    //$pro=$row['ProjectName'];
                            
                                     echo '<option  value="'.$row['CampaignName'].'">'.$row['CampaignName'].'</option>';
                                    
                                  }
                                ?>
                    </select>
                           </div>

                              <div class="form-group  col-md-6">
                                <label class="form-control-label" >Project Name</label>
                  
                             <select name="project" class="form-control maxlength" data-plugin="select2">
                                <?php
                                  $proj=mysqli_query($dbc,"select * from `campaign-projects`");
                                  echo '<option  value="'.$pro.'">'.$pro.'</option>';
                                  while($row=mysqli_fetch_assoc($proj))
                                  {
                                    //$pro=$row['ProjectName'];
                            
                                     echo '<option  value="'.$row['ProjectName'].'">'.$row['ProjectName'].'</option>';
                                    
                                  }
                                ?>
                    </select>
                                      
                         </div>
                
                <!-- End Example Basic -->
              <div class="form-group  col-md-6">
                  <label class="form-control-label" >Meeting Date & Time</label>
                    <div class="input-daterange">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="icon wb-calendar" aria-hidden="true"></i>
                      </span>
                      <input type="text" name="date" class="form-control datepair-date datepair-end date"  name="end" data-plugin="datepicker">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="icon wb-time" aria-hidden="true"></i>
                      </span>
                      <input type="text" name="time" class="timepicker form-control" data-plugin="clockpicker" data-autoclose="true">
                    </div>
                  </div>
                  </div>
                  <div class="form-group  col-md-6">
                                <label class="form-control-label" >Company</label>
                  
                        <input type="text" name="company" class=" form-control" data-plugin="maxlength"
                               data-placement="bottom-right-inside" value="<?php echo $company; ?>">
                    
                  </div>
                  <div class="form-group  col-md-6">
                                <label class="form-control-label" >Sector</label>
                  
                        <input type="text" name="sector" class=" form-control" data-plugin="maxlength"
                               data-placement="bottom-right-inside" value="<?php echo $sector; ?>">
                    
                  </div>
                  <div class="form-group  col-md-6">
                                <label class="form-control-label" >Contact Person</label>
                  
                  <input type="text" name="contactperson" class=" form-control" data-plugin="maxlength"
        data-placement="bottom-right-inside" value="<?php echo $contactperson; ?>" onKeyUp="fname()">
                    
                  </div>
                  <div class="form-group  col-md-6">
                                <label class="form-control-label" >Designation</label>
                  
                  <input type="text" name="designation" class=" form-control" data-plugin="maxlength"
                     data-placement="bottom-right-inside" value="<?php echo $designation; ?>" >
                    
                  </div>
                  <div class="form-group  col-md-6">
                                <label class="form-control-label" >Mobile</label>
                  
                  <input type="number" name="mobile" class=" form-control" data-plugin="maxlength"
    data-placement="bottom-right-inside" value="<?php echo $mobile; ?>" onKeyUp="mob()" maxlength="10" >
                    
                  </div>
                  <div class="form-group  col-md-6">
                                <label class="form-control-label" >Email</label>
                  
                  <input type="email" name="email" class=" form-control" data-plugin="maxlength"
                     data-placement="bottom-right-inside" value="<?php echo $email; ?>" >
                    
                  </div>

                    <div class="form-group  col-md-6">
                                <label class="form-control-label" >Meeting</label>
                      
                             <select name="meetingstatus" class="form-control maxlength" data-plugin="select2">
                        <option value="<?php echo $followup; ?>"><?php echo $meetingstatus; ?></option>
                                                                <option value="Yes">Yes</option>
                                                                <option value="No">No</option>
                            </select>
                                      
                    </div>

                    <div class="form-group  col-md-6">
                                <label class="form-control-label" >Follow Up</label>
                      
                          <select name="followup" class="form-control maxlength" data-plugin="select2">
                          <option value="<?php echo $followup; ?>"><?php echo $followup; ?></option>
                                                                <option value="Yes">Yes</option>
                                                                <option value="No">No</option>
                            </select>
                                      
                    </div>
                    <div class="form-group  col-md-6">
                  <label class="form-control-label" >FollowUp Date & Time</label>
                    <div class="input-daterange">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="icon wb-calendar" aria-hidden="true"></i>
                      </span>
                      <input type="text" name="followupdate" class="form-control datepair-date datepair-end date"  name="end" data-plugin="datepicker">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="icon wb-time" aria-hidden="true"></i>
                      </span>
                      <input type="text" name="followuptime" class="timepicker form-control" data-plugin="clockpicker" data-autoclose="true">
                    </div>
                  </div>
                  </div>

                      <div class="form-group  col-md-6">
                                <label class="form-control-label" >Requirements</label>
                   
                             <textarea name="requirements" value="<?php echo $requirements; ?>" rows="5" title="Input Requirements." class="form-control" placeholder="Enter customer requirements" ><?php echo $requirements; ?></textarea>
                          
                      </div>
                           <div class="form-group  col-md-6">
                                <label class="form-control-label" >Remarks</label>
                  
                             
                      <input type="number" name="remarks" class=" form-control" data-plugin="maxlength"
                               data-placement="bottom-right-inside" value="<?php echo $remarks; ?>">
                    
                           </div>
                  </div>
              <button type="submit" name="funnel" class="btn-primary btn">Send to Funnel</button>
              <button type="submit" name="submit" class="btn-primary btn">Submit</button>
              <button type="submit" class="btn-default btn-pure btn">Cancel</button>
        </form>
        </div> 
      </div>
    </div></div></div></div></div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>

    
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>

<?php include "includes/css/select.php"; ?>
      <?php include "includes/js/select.php"; ?>

        <!-- date picker -->
        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="../../assets/global/js/Plugin/bootstrap-datepicker.js"></script>
        <script src="../../assets/global/vendor/clockpicker/bootstrap-clockpicker.min.js"></script>

    
  <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>
