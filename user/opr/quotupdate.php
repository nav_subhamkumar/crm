<?php
   include "session_handler.php";
   ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="bootstrap material admin template">
      <meta name="author" content="">
      <title>Quotation Update | Bizapp CRM</title>
      <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
      <link rel="shortcut icon" href="../../assets/images/favicon.ico">
      <!-- Stylesheets -->
      <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
      <link rel="stylesheet" href="../../assets/css/site.min.css">
      <!-- Plugins -->
      <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
      <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
      <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
      <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
      <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
      <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
      <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
      <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
      <!-- Fonts -->
      <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
      <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
      <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- table set -->
      <link rel='stylesheet' href='../../assets/css/customised-crm.css'>
      <script src="../../assets/js/customised-crm.js"></script>

      <link rel="stylesheet" href="../../assets/global/vendor/select2/select2.css">
    <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="../../assets/global/vendor/multi-select/multi-select.css">


    <link rel="stylesheet" href="../../assets/examples/css/uikit/modals.css">

    <?php include "includes/css/datepicker.php"; ?>
      <!--[if lt IE 9]>
      <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
      <![endif]-->
      <!--[if lt IE 10]>
      <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
      <script src="../../assets/global/vendor/respond/respond.min.js"></script>
      <![endif]-->
      <!-- Scripts -->
      <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
      <script>
         Breakpoints();
      </script>
   </head>
   <body class="animsition site-navbar-small dashboard">
      <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <?php include "navbar-header.php"; ?>
      <?php include "side-navigation.php"; ?>  
      <?php
    $_SESSION['accessgeneral']=0;
    $_SESSION['accesslicence']=0;
    $_SESSION['accesstax']=0;
    $_SESSION['accesstaxvalue']=0;
    $_SESSION['accessupload']=0;

    ?> 
      <!-- Page -->
      <div class="page">
         <div class="page-content" >
            <!-- Panel Basic -->
            
            <!-- input customer details  start -->
            <div class="panel">
               <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                  <h4 class="example-title">Update Quotation</h4>
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                    <?php
                                       $quotno=$_GET['q'];
                                        $qtype=$_GET['qtype'];
                                        $fetchall=mysqli_query($dbc,"select * from `quotation` where `QuotNo`='$quotno'");

while($row=mysqli_fetch_assoc($fetchall))
{
    $company=$row['Company'];
    /*$product=$row['Product'];*/
    $tax=$row['Tax'];
    $currency=$row['Currency'];
    /*$hsnsac=$row['HSNSAC'];*/
    $service=$row['Service'];

    $product1=$row['Product1'];
    $pdescription1=$row['PartDescription1'];
    $quantity1=$row['Quantity1'];
    $unitprice1=$row['UnitPrice1'];
    $hsnsac1=$row['HSNSAC1'];
    $licence1=$row['LicenceFor1'];

    $product2=$row['Product2'];
    $pdescription2=$row['PartDescription2'];
    $quantity2=$row['Quantity2'];
    $unitprice2=$row['UnitPrice2'];
    $hsnsac2=$row['HSNSAC2'];
    $licence2=$row['LicenceFor2'];

    $product3=$row['Product3'];
    $pdescription3=$row['PartDescription3'];
    $quantity3=$row['Quantity3'];
    $unitprice3=$row['UnitPrice3'];
    $hsnsac3=$row['HSNSAC3'];
    $licence3=$row['LicenceFor3'];

    $product4=$row['Product4'];
    $pdescription4=$row['PartDescription4'];
    $quantity4=$row['Quantity4'];
    $unitprice4=$row['UnitPrice4'];
    $hsnsac4=$row['HSNSAC4'];
    $licence4=$row['LicenceFor4'];

    $product5=$row['Product5'];
    $pdescription5=$row['PartDescription5'];
    $quantity5=$row['Quantity5'];
    $unitprice5=$row['UnitPrice5'];
    $hsnsac5=$row['HSNSAC5'];
    $licence5=$row['LicenceFor5'];

    $product6=$row['Product6'];
    $pdescription6=$row['PartDescription6'];
    $quantity6=$row['Quantity6'];
    $unitprice6=$row['UnitPrice6'];
    $hsnsac6=$row['HSNSAC6'];
    $licence6=$row['LicenceFor6'];

    $product7=$row['Product7'];
    $pdescription7=$row['PartDescription7'];
    $quantity7=$row['Quantity7'];
    $unitprice7=$row['UnitPrice7'];
    $hsnsac7=$row['HSNSAC7'];
    $licence7=$row['LicenceFor7'];

    $product8=$row['Product8'];
    $pdescription8=$row['PartDescription8'];
    $quantity8=$row['Quantity8'];
    $unitprice8=$row['UnitPrice8'];
    $hsnsac8=$row['HSNSAC8'];
    $licence8=$row['LicenceFor8'];

    $product9=$row['Product9'];
    $pdescription9=$row['PartDescription9'];
    $quantity9=$row['Quantity9'];
    $unitprice9=$row['UnitPrice9'];
    $hsnsac9=$row['HSNSAC9'];
    $licence9=$row['LicenceFor9'];

    $product10=$row['Product10'];
    $pdescription10=$row['PartDescription10'];
    $quantity10=$row['Quantity10'];
    $unitprice10=$row['UnitPrice10'];
    $hsnsac10=$row['HSNSAC10'];
    $licence10=$row['LicenceFor10'];

    $product11=$row['Product11'];
    $pdescription11=$row['PartDescription11'];
    $quantity11=$row['Quantity11'];
    $unitprice11=$row['UnitPrice11'];
    $hsnsac11=$row['HSNSAC11'];
    $licence11=$row['LicenceFor11'];

    $product12=$row['Product12'];
    $pdescription12=$row['PartDescription12'];
    $quantity12=$row['Quantity12'];
    $unitprice12=$row['UnitPrice12'];
    $hsnsac12=$row['HSNSAC12'];
    $licence12=$row['LicenceFor12'];

    $product13=$row['Product13'];
    $pdescription13=$row['PartDescription13'];
    $quantity13=$row['Quantity13'];
    $unitprice13=$row['UnitPrice13'];
    $hsnsac13=$row['HSNSAC13'];
    $licence13=$row['LicenceFor13'];

    $product14=$row['Product14'];
    $pdescription14=$row['PartDescription14'];
    $quantity14=$row['Quantity14'];
    $unitprice14=$row['UnitPrice14'];
    $hsnsac14=$row['HSNSAC14'];
    $licence14=$row['LicenceFor14'];

    $product15=$row['Product15'];
    $pdescription15=$row['PartDescription15'];
    $quantity15=$row['Quantity15'];
    $unitprice15=$row['UnitPrice15'];
    $hsnsac15=$row['HSNSAC15'];
    $licence15=$row['LicenceFor15'];

    $product16=$row['Product16'];
    $pdescription16=$row['PartDescription16'];
    $quantity16=$row['Quantity16'];
    $unitprice16=$row['UnitPrice16'];
    $hsnsac16=$row['HSNSAC16'];
    $licence16=$row['LicenceFor16'];

    $product17=$row['Product17'];
    $pdescription17=$row['PartDescription17'];
    $quantity17=$row['Quantity17'];
    $unitprice17=$row['UnitPrice17'];
    $hsnsac17=$row['HSNSAC17'];
    $licence17=$row['LicenceFor17'];

    $product18=$row['Product18'];
    $pdescription18=$row['PartDescription18'];
    $quantity18=$row['Quantity18'];
    $unitprice18=$row['UnitPrice18'];
    $hsnsac18=$row['HSNSAC18'];
    $licence18=$row['LicenceFor18'];

    $product19=$row['Product19'];
    $pdescription19=$row['PartDescription19'];
    $quantity19=$row['Quantity19'];
    $unitprice19=$row['UnitPrice19'];
    $hsnsac19=$row['HSNSAC19'];
    $licence19=$row['LicenceFor19'];

    $product20=$row['Product20'];
    $pdescription20=$row['PartDescription20'];
    $quantity20=$row['Quantity20'];
    $unitprice20=$row['UnitPrice20'];
    $hsnsac20=$row['HSNSAC20'];
    $licence20=$row['LicenceFor20'];

    $product21=$row['Product21'];
    $pdescription21=$row['PartDescription21'];
    $quantity21=$row['Quantity21'];
    $unitprice21=$row['UnitPrice21'];
    $hsnsac21=$row['HSNSAC21'];
    $licence21=$row['LicenceFor21'];

    $product22=$row['Product22'];
    $pdescription22=$row['PartDescription22'];
    $quantity22=$row['Quantity22'];
    $unitprice22=$row['UnitPrice22'];
    $hsnsac22=$row['HSNSAC22'];
    $licence22=$row['LicenceFor22'];

    $product23=$row['Product23'];
    $pdescription23=$row['PartDescription23'];
    $quantity23=$row['Quantity23'];
    $unitprice23=$row['UnitPrice23'];
    $hsnsac23=$row['HSNSAC23'];
    $licence23=$row['LicenceFor23'];

    $product24=$row['Product24'];
    $pdescription24=$row['PartDescription24'];
    $quantity24=$row['Quantity24'];
    $unitprice24=$row['UnitPrice24'];
    $hsnsac24=$row['HSNSAC24'];
    $licence24=$row['LicenceFor24'];

    $product25=$row['Product25'];
    $pdescription25=$row['PartDescription25'];
    $quantity25=$row['Quantity25'];
    $unitprice25=$row['UnitPrice25'];
    $hsnsac25=$row['HSNSAC25'];
    $licence25=$row['LicenceFor25'];

    $product26=$row['Product26'];
    $pdescription26=$row['PartDescription26'];
    $quantity26=$row['Quantity26'];
    $unitprice26=$row['UnitPrice26'];
    $hsnsac26=$row['HSNSAC26'];
    $licence26=$row['LicenceFor26'];

    $product27=$row['Product27'];
    $pdescription27=$row['PartDescription27'];
    $quantity27=$row['Quantity27'];
    $unitprice27=$row['UnitPrice27'];
    $hsnsac27=$row['HSNSAC27'];
    $licence27=$row['LicenceFor27'];

    $product28=$row['Product28'];
    $pdescription28=$row['PartDescription28'];
    $quantity28=$row['Quantity28'];
    $unitprice28=$row['UnitPrice28'];
    $hsnsac28=$row['HSNSAC28'];
    $licence28=$row['LicenceFor28'];

    $product29=$row['Product29'];
    $pdescription29=$row['PartDescription29'];
    $quantity29=$row['Quantity29'];
    $unitprice29=$row['UnitPrice29'];
    $hsnsac29=$row['HSNSAC29'];
    $licence29=$row['LicenceFor29'];

    $product30=$row['Product30'];
    $pdescription30=$row['PartDescription30'];
    $quantity30=$row['Quantity30'];
    $unitprice30=$row['UnitPrice30'];
    $hsnsac30=$row['HSNSAC30'];
    $licence30=$row['LicenceFor30'];

    $product31=$row['Product31'];
    $pdescription31=$row['PartDescription31'];
    $quantity31=$row['Quantity31'];
    $unitprice31=$row['UnitPrice31'];
    $hsnsac31=$row['HSNSAC31'];
    $licence31=$row['LicenceFor31'];

    $product32=$row['Product32'];
    $pdescription32=$row['PartDescription32'];
    $quantity32=$row['Quantity32'];
    $unitprice32=$row['UnitPrice32'];
    $hsnsac32=$row['HSNSAC32'];
    $licence32=$row['LicenceFor32'];

    $product33=$row['Product33'];
    $pdescription33=$row['PartDescription33'];
    $quantity33=$row['Quantity33'];
    $unitprice33=$row['UnitPrice33'];
    $hsnsac33=$row['HSNSAC33'];
    $licence33=$row['LicenceFor33'];

    $product34=$row['Product34'];
    $pdescription34=$row['PartDescription34'];
    $quantity34=$row['Quantity34'];
    $unitprice34=$row['UnitPrice34'];
    $hsnsac34=$row['HSNSAC34'];
    $licence34=$row['LicenceFor34'];

    $product35=$row['Product35'];
    $pdescription35=$row['PartDescription35'];
    $quantity35=$row['Quantity35'];
    $unitprice35=$row['UnitPrice35'];
    $hsnsac35=$row['HSNSAC35'];
    $licence35=$row['LicenceFor35'];

    $product36=$row['Product36'];
    $pdescription36=$row['PartDescription36'];
    $quantity36=$row['Quantity36'];
    $unitprice36=$row['UnitPrice36'];
    $hsnsac36=$row['HSNSAC36'];
    $licence36=$row['LicenceFor36'];

    $product37=$row['Product37'];
    $pdescription37=$row['PartDescription37'];
    $quantity37=$row['Quantity37'];
    $unitprice37=$row['UnitPrice37'];
    $hsnsac37=$row['HSNSAC37'];
    $licence37=$row['LicenceFor37'];

    $product38=$row['Product38'];
    $pdescription38=$row['PartDescription38'];
    $quantity38=$row['Quantity38'];
    $unitprice38=$row['UnitPrice38'];
    $hsnsac38=$row['HSNSAC38'];
    $licence38=$row['LicenceFor38'];

    $product39=$row['Product39'];
    $pdescription39=$row['PartDescription39'];
    $quantity39=$row['Quantity39'];
    $unitprice39=$row['UnitPrice39'];
    $hsnsac39=$row['HSNSAC39'];
    $licence39=$row['LicenceFor39'];

    $product40=$row['Product40'];
    $pdescription40=$row['PartDescription40'];
    $quantity40=$row['Quantity40'];
    $unitprice40=$row['UnitPrice40'];
    $hsnsac40=$row['HSNSAC40'];
    $licence40=$row['LicenceFor40'];

    $product41=$row['Product41'];
    $pdescription41=$row['PartDescription41'];
    $quantity41=$row['Quantity41'];
    $unitprice41=$row['UnitPrice41'];
    $hsnsac41=$row['HSNSAC41'];
    $licence41=$row['LicenceFor41'];

    $product42=$row['Product42'];
    $pdescription42=$row['PartDescription42'];
    $quantity42=$row['Quantity42'];
    $unitprice42=$row['UnitPrice42'];
    $hsnsac42=$row['HSNSAC42'];
    $licence42=$row['LicenceFor42'];

    $product43=$row['Product43'];
    $pdescription43=$row['PartDescription43'];
    $quantity43=$row['Quantity43'];
    $unitprice43=$row['UnitPrice43'];
    $hsnsac43=$row['HSNSAC43'];
    $licence43=$row['LicenceFor43'];

    $product44=$row['Product44'];
    $pdescription44=$row['PartDescription44'];
    $quantity44=$row['Quantity44'];
    $unitprice44=$row['UnitPrice44'];
    $hsnsac44=$row['HSNSAC44'];
    $licence44=$row['LicenceFor44'];

    $product45=$row['Product45'];
    $pdescription45=$row['PartDescription45'];
    $quantity45=$row['Quantity45'];
    $unitprice45=$row['UnitPrice45'];
    $hsnsac45=$row['HSNSAC45'];
    $licence45=$row['LicenceFor45'];

    $product46=$row['Product46'];
    $pdescription46=$row['PartDescription46'];
    $quantity46=$row['Quantity46'];
    $unitprice46=$row['UnitPrice46'];
    $hsnsac46=$row['HSNSAC46'];
    $licence46=$row['LicenceFor46'];

    $product47=$row['Product47'];
    $pdescription47=$row['PartDescription47'];
    $quantity47=$row['Quantity47'];
    $unitprice47=$row['UnitPrice47'];
    $hsnsac47=$row['HSNSAC47'];
    $licence47=$row['LicenceFor47'];

    $product48=$row['Product48'];
    $pdescription48=$row['PartDescription48'];
    $quantity48=$row['Quantity48'];
    $unitprice48=$row['UnitPrice48'];
    $hsnsac48=$row['HSNSAC48'];
    $licence48=$row['LicenceFor48'];

    $product49=$row['Product49'];
    $pdescription49=$row['PartDescription49'];
    $quantity49=$row['Quantity49'];
    $unitprice49=$row['UnitPrice49'];
    $hsnsac49=$row['HSNSAC49'];
    $licence49=$row['LicenceFor49'];

    $product50=$row['Product50'];
    $pdescription50=$row['PartDescription50'];
    $quantity50=$row['Quantity50'];
    $unitprice50=$row['UnitPrice50'];
    $hsnsac50=$row['HSNSAC50'];
    $licence50=$row['LicenceFor50'];

    $product51=$row['Product51'];
    $pdescription51=$row['PartDescription51'];
    $quantity51=$row['Quantity51'];
    $unitprice51=$row['UnitPrice51'];
    $hsnsac51=$row['HSNSAC51'];
    $licence51=$row['LicenceFor51'];

    $product52=$row['Product52'];
    $pdescription52=$row['PartDescription52'];
    $quantity52=$row['Quantity52'];
    $unitprice52=$row['UnitPrice52'];
    $hsnsac52=$row['HSNSAC52'];
    $licence52=$row['LicenceFor52'];

    $product53=$row['Product53'];
    $pdescription53=$row['PartDescription53'];
    $quantity53=$row['Quantity53'];
    $unitprice53=$row['UnitPrice53'];
    $hsnsac53=$row['HSNSAC53'];
    $licence53=$row['LicenceFor53'];

    $product54=$row['Product54'];
    $pdescription54=$row['PartDescription54'];
    $quantity54=$row['Quantity54'];
    $unitprice54=$row['UnitPrice54'];
    $hsnsac54=$row['HSNSAC54'];
    $licence54=$row['LicenceFor54'];

    $product55=$row['Product55'];
    $pdescription55=$row['PartDescription55'];
    $quantity55=$row['Quantity55'];
    $unitprice55=$row['UnitPrice55'];
    $hsnsac55=$row['HSNSAC55'];
    $licence55=$row['LicenceFor55'];

    $product56=$row['Product56'];
    $pdescription56=$row['PartDescription56'];
    $quantity56=$row['Quantity56'];
    $unitprice56=$row['UnitPrice56'];
    $hsnsac56=$row['HSNSAC56'];
    $licence56=$row['LicenceFor56'];

    $product57=$row['Product57'];
    $pdescription57=$row['PartDescription57'];
    $quantity57=$row['Quantity57'];
    $unitprice57=$row['UnitPrice57'];
    $hsnsac57=$row['HSNSAC57'];
    $licence57=$row['LicenceFor57'];

    $product58=$row['Product58'];
    $pdescription58=$row['PartDescription58'];
    $quantity58=$row['Quantity58'];
    $unitprice58=$row['UnitPrice58'];
    $hsnsac58=$row['HSNSAC58'];
    $licence58=$row['LicenceFor58'];

    $product59=$row['Product59'];
    $pdescription59=$row['PartDescription59'];
    $quantity59=$row['Quantity59'];
    $unitprice59=$row['UnitPrice59'];
    $hsnsac59=$row['HSNSAC59'];
    $licence59=$row['LicenceFor59'];

    $product60=$row['Product60'];
    $pdescription60=$row['PartDescription60'];
    $quantity60=$row['Quantity60'];
    $unitprice60=$row['UnitPrice60'];
    $hsnsac60=$row['HSNSAC60'];
    $licence60=$row['LicenceFor60'];

    $delivery=$row['Delivery'];
    $validity=$row['Validity'];
    $payment=$row['Payment'];

    $servicename=$row['ServiceName'];
    $servicecost=$row['ServiceCost'];
    $servicetax=$row['ServiceTax'];
    $freightname=$row['FreightName'];
    $freightcost=$row['FreightCost'];
    $freighttax=$row['FreightTax'];

    $quotpath1=$row['QuotationPath1'];
    $quotpath2=$row['QuotationPath2'];
    $quotpath3=$row['QuotationPath3'];
    $quotpath4=$row['QuotationPath4'];
    $quotpath5=$row['QuotationPath5'];
    $quotpath6=$row['QuotationPath6'];
    $quotpath7=$row['QuotationPath7'];
    $quotpath8=$row['QuotationPath8'];
    $quotpath9=$row['QuotationPath9'];
    $quotpath10=$row['QuotationPath10'];
    $quotpath11=$row['QuotationPath11'];
    $quotpath12=$row['QuotationPath12'];
    $quotpath13=$row['QuotationPath13'];
    $quotpath14=$row['QuotationPath14'];
    $quotpath15=$row['QuotationPath15'];
    $quotpath16=$row['QuotationPath16'];
    $quotpath17=$row['QuotationPath17'];
    $quotpath18=$row['QuotationPath18'];
    $quotpath19=$row['QuotationPath19'];
    $quotpath20=$row['QuotationPath20'];
    $quotpath21=$row['QuotationPath21'];
    $quotpath22=$row['QuotationPath22'];
    $quotpath23=$row['QuotationPath23'];
    $quotpath24=$row['QuotationPath24'];
    $quotpath25=$row['QuotationPath25'];
    $quotpath26=$row['QuotationPath26'];
    $quotpath27=$row['QuotationPath27'];
    $quotpath28=$row['QuotationPath28'];
    $quotpath29=$row['QuotationPath29'];
    $quotpath30=$row['QuotationPath30'];
    $quotpath31=$row['QuotationPath31'];
    $quotpath32=$row['QuotationPath32'];
    $quotpath33=$row['QuotationPath33'];
    $quotpath34=$row['QuotationPath34'];
    $quotpath35=$row['QuotationPath35'];
    $quotpath36=$row['QuotationPath36'];
    $quotpath37=$row['QuotationPath37'];
    $quotpath38=$row['QuotationPath38'];
    $quotpath39=$row['QuotationPath39'];
    $quotpath40=$row['QuotationPath40'];
    $quotpath41=$row['QuotationPath41'];
    $quotpath42=$row['QuotationPath42'];
    $quotpath43=$row['QuotationPath43'];
    $quotpath44=$row['QuotationPath44'];
    $quotpath45=$row['QuotationPath45'];
    $quotpath46=$row['QuotationPath46'];
    $quotpath47=$row['QuotationPath47'];
    $quotpath48=$row['QuotationPath48'];
    $quotpath49=$row['QuotationPath49'];
    $quotpath50=$row['QuotationPath50'];
    $quotpath51=$row['QuotationPath51'];
    $quotpath52=$row['QuotationPath52'];
    $quotpath53=$row['QuotationPath53'];
    $quotpath54=$row['QuotationPath54'];
    $quotpath55=$row['QuotationPath55'];
    $quotpath56=$row['QuotationPath56'];
    $quotpath57=$row['QuotationPath57'];
    $quotpath58=$row['QuotationPath58'];
    $quotpath59=$row['QuotationPath59'];
    $quotpath60=$row['QuotationPath60'];

    $otherfile=$row['Attachment1'];

    $remarks=$row['Remarks'];

    $quotformname=$row['QuotFormName'];

    $addressreq=$row['AddressReq'];

    $uniqueid=$row['UniqueId'];   

    $expectedclosure=$row['ExpectedClosure'];

}


                                        ?>
                    <?php
                    if($quotformname=="general" or $quotformname=="" )
                    {

                    ?>
                    <li class="nav-item" role="presentation">
                        <a class="active nav-link" data-toggle="tab" href="#general" aria-controls="general" role="tab">General</a>
                     </li>
                     <!-- <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#licence" aria-controls="or" role="tab">Licence</a>
                     </li> -->
                     <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#multipletax" aria-controls="multipletax" role="tab">Multiple Tax</a>
                     </li>
                     <!-- <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#multipleupload" aria-controls="multipleupload" role="tab">Multiple Upload</a>
                     </li> -->
                     
                     <?php
                    }
                    elseif ($quotformname=="multipletax") 
                    {
                      
                     ?>
                     <li class="nav-item" role="presentation">
                        <a class=" nav-link" data-toggle="tab" href="#general" aria-controls="general" role="tab">General</a>
                     </li>
                     <!-- <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#licence" aria-controls="or" role="tab">Licence</a>
                     </li> -->
                     <li class="nav-item" role="presentation">
                        <a class="active nav-link" data-toggle="tab" href="#multipletax" aria-controls="multipletax" role="tab">Multiple Tax</a>
                     </li>
                     <!-- <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#multipleupload" aria-controls="multipleupload" role="tab">Multiple Upload</a>
                     </li> -->
                      <?php
                    }
                     ?>
                  </ul>
                  <!--  general start -->
                  <div class="tab-content">
                     <?php
                    if($quotformname=="general" or $quotformname=="" )
                    {

                    ?>
                     <div class="tab-pane active animation-slide-left" id="general" role="tabpanel">
                      <?php
                    }
                      else
                      {
                        ?>
                        <div class="tab-pane  animation-slide-left" id="general" role="tabpanel">

                        <?php
                      }
                      ?>
                        <div class="example">
                           <form action="../auth/order/quotation/updquot.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              <input type="hidden" name="quotformname" value="general">
                              
                              <input type="hidden" name="quotno" value="<?php echo $quotno; ?>" >
                              <input type="hidden" name="qtype" value="<?php echo $qtype; ?>" >
                              <input type="hidden" name="uniqueid" value="<?php echo $uniqueid; ?>" >
                              
                              <div class="row">
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="company"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(Company) from `customers` where RMail='$id' order by id desc");
                                 echo '<option value="'.$company.'" >'.$company.'</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['Company'].'" >'.$row['Company'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddCustomer" data-toggle="modal" type="button"><i class="icon md-account "></i>Add New Customer</button>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddProduct" data-toggle="modal" type="button"><i class="icon md-mall  "></i>Add New Product</button>
                                 </div>
                                 </div>
                                 <hr>




                                 <?php  

                                                    for($j=1;$j<=60;$j++)
                                                    {
                                                        $pr="product".$j;
                                                        $pd="pdescription".$j;
                                                        $qt="quantity".$j;
                                                        $up="unitprice".$j;
                                                        $qp="quotpath".$j;
                                                        $hsn="hsnsac".$j;

                                                        if(!empty($$pr))
                                                        {


                                                    
                                                    echo '<div class="linegeneral" ><div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Product </label>
                                    ';?>
                                    <select class="form-control" name="product[]"  data-plugin="select2" required="required" >
                                        <?php                         

                                            $project=mysqli_query($dbc,"select distinct(ProductName) from `products` ");
                           
                          echo '<option value="'.$$pr.'" selected>'.$$pr.'</option>';
                           while($row=mysqli_fetch_assoc($project))
                           {
                               
                           
                                echo '<option  value="'.$row['ProductName'].'">'.$row['ProductName'].'</option>';
                               
                           }
                           ?>
                           <?php
                                   echo '</select>
                            
                                                        </div>

                                                        <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Part Description </label>
                                            <input type="text" class="form-control" name="partdesc[]"  value="'.$$pd.'" >
                                                        </div>
                                                    
                                                        
                                                        <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Quantity </label>
                                          <input type="number" class="form-control" name="qty[]" value="'.$$qt.'"  placeholder="Quantity" >
                                                           
                                                            
                                                        </div>
                                                 </div>       
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Unit Price </label>
                                          <input type="number" class="form-control" name="unitprice[]" value="'.$$up.'" placeholder="Unit Price" step="0.01" > 
                                                        </div>

                                      
                                                        <div class="form-group  col-md-4">
                                          <label class="form-control-label" >OEM Quotation </label>
                                          <input type="file" class="form-control" name="uplfiles[]" value="'.$$qp.'" placeholder="OEM Quotation" >

                           </div>

                           <div class="form-group  col-md-4">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button" onClick="addMore();">

                                          <span class="icon md-plus" ></span>
                                          </button>
                                          <button class="btn btn-danger btn-add-basic" type="button" onClick="deleteRow();">

                                          <span class="icon md-minus" ></span>
                                          </button>
                                          <input type="checkbox" name="item_index[]" />
                                          </span>
                                       </div>
                                    </div>
                                    </div>
                                                    ';
                                                    /*if(!empty($j))
                                                    {
                                                      $_SESSION['accesstax']=$_SESSION['accesstax']+1;
                                                    }*/
                                                    

                                                    }
                                                    else
                                                    {

                                                      break;
                                                    }
                                                    
                                                    

                                                }

                                                /*$_SESSION['accesstax']=$j-2;*/
                                                ?>







                                    <SCRIPT>
                              function addMore() {
                                  $("<div>").load("quotgen-general.php", function() {
                                          $("#linegeneral").append($(this).html());
                                  }); 
                              }
                              function deleteRow() {
                                  $('div.linegeneral').each(function(index, item){
                                      jQuery(':checkbox', this).each(function () {
                                          if ($(this).is(':checked')) {
                                              $(item).remove();
                                          }
                                      });
                                  });
                              }
                           </SCRIPT>

                          
                                 <div id="linegeneral">
                                    
                                 </div>
                              
                                    <hr>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Currency</label>
                                    <select class="form-control" name="currency"  data-plugin="select2" required="required" >
                                      <option value="<?php echo $currency; ?>"><?php echo $currency; ?></option>
                                      <option value="rupee">Rupee &#8377;</option>
                                       <option value="dollar">Dollar &#036;</option>
                                       <option value="euro">Euro &euro;</option>
                                       <option value="pound">Pound &pound;</option>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >HSN/SAC</label>
                                          <input type="text" class="form-control" name="hsnsac[]" value="<?php echo $hsnsac1; ?>"  placeholder="HSN/SAC" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Tax</label>
                                          <select class="form-control" name="tax[]" multiple="multiple" data-plugin="select2" required="required" >
                                          <?php
                                                                    $project=mysqli_query($dbc,"select distinct(TaxName),TaxPercentage from `setting_tax`");

                                                                    $array =  explode(',', $tax);
                                                                    $count=0;
                                                                    foreach ($array as $item) {
                                                                        global $count;
                                                                        $count=$count+1;
                                                                        }

                                                                        foreach ($array as $item) {


                                                                            $taxperarr =  explode('-', $item);

                                                                            foreach ($taxperarr as $per) {
                                                                                $taxpercentage=$per;

                                                                                }

                                                                           

                                                                            if(!empty($taxpercentage))
                                                                                {
                                                                                echo '<option selected  value="'.$taxperarr[0].'-'.$taxpercentage.'">'.$taxperarr[0].' '.$taxpercentage.'%'.'</option>';
                                                                            }

        
                                                                            

                                                                        }

                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                        //$pro=$row['ProjectName'];
                                                        
                                                                         echo '<option  value="'.$row['TaxName'].'-'.$row['TaxPercentage'].'">'.$row['TaxName'].' '.$row['TaxPercentage'].'%'.'</option>';
                                                                        
                                                                    }
                                                                
                                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Validity (In Days)</label>
                                    <input type="number" class="form-control" name="validity" value="<?php echo $validity; ?>" placeholder="Validity (In Days)" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Delivery (In Days)</label>
                                          <input type="number" class="form-control" name="delivery" value="<?php echo $delivery; ?>" placeholder="Delivery (In Days)" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Payment</label>
                                          <select class="form-control" name="payment" data-plugin="select2" required="required" >
                                            <option selected value="<?php if($payment=="100% Advance")
                                            {echo $payment;}
                                            else{
                                              echo $payment;
                                            } ?>"><?php if($payment=="100% Advance")
                                            {echo $payment;}
                                            else{
                                              echo $payment;
                                            } ?></option>
                                          <option  value="100% Advance">100% Advance</option>
                                       <option value="30 Days">30 Days</option>
                                       <option value="45 Days">45 Days</option>
                                       <option value="60 Days">60 Days</option>
                                       <option value="90 Days">90 Days</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Service Name</label>
                                    <input type="text" class="form-control" name="servicename" value="<?php echo $servicename; ?>" placeholder="Service Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Cost</label>
                                          <input type="number" class="form-control" name="servicecost" value="<?php echo $servicecost; ?>"  placeholder="Service Cost" step="0.01" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Tax (%)</label>
                                          <input type="number" class="form-control" name="servicetax" value="<?php echo $servicetax; ?>" placeholder="Service Tax (%)" >
                                       </div>
                                    </div>

                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Freight Name</label>
                                    <input type="text" class="form-control" name="freightname" value="<?php echo $freightname; ?>" placeholder="Freight Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Cost</label>
                                          <input type="number" class="form-control" name="freightcost" value="<?php echo $freightcost; ?>"  placeholder="Freight Cost" step="0.01" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Tax (%)</label>
                                          <input type="number" class="form-control" name="freighttax" value="<?php echo $freighttax; ?>" placeholder="Freight Tax (%)" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Expected Closure</label>
                                    <input type="text" class="form-control" name="expectedclosure" value="<?php echo $expectedclosure; ?>" placeholder="Expected Closure" data-plugin="datepicker" >
                                 </div>

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Note</label>
                                    <input type="text" class="form-control" name="remarks" value="<?php echo $remarks; ?>"  placeholder="Note" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Other Attachment</label>
                                          <input type="file" class="form-control" name="attachment[]" value="<?php echo $otherfile; ?>"  placeholder="Freight Cost" >
                                       </div>

                                       
                                    </div>

                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                          <input type="checkbox"  name="addreq" value="<?php if($addreq=="Yes")
                                          {
                                            echo "Yes";
                                          } ?>">Address Required in Quotation
                                       </div>
                                    </div>
                                    
                                 
                              <div class="form-group ">
                                 <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                 <button type="reset"  class="btn btn-default">Reset</button>
                              </div>
                           </form>
                        </div>
                     </div>
                     <!--  general end -->
                     <!--  Licence start -->
                     <?php
                    if($quotformname=="licence")
                    {

                    ?>
                     <div class="tab-pane active animation-slide-left" id="licence" role="tabpanel">
                      <?php
                    }
                      else
                      {
                        ?>
                        <div class="tab-pane animation-slide-left" id="licence" role="tabpanel">

                        <?php
                      }
                      ?>
                     
                        <div class="example">
                           <form action="../auth/order/quotation/updquot.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              <input type="hidden" name="quotformname" value="licence">
                              
                              <div class="row">
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="company"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(Company) from `customers` where RMail='$id'  order by id desc");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['Company'].'" >'.$row['Company'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddCustomer" data-toggle="modal" type="button"><i class="icon md-account "></i>Add New Customer</button>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddProduct" data-toggle="modal" type="button"><i class="icon md-mall  "></i>Add New Product</button>
                                 </div>
                                 </div>

                                 <hr>

                                 
                                 <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Product <?php echo $_SESSION['accesslicence']+1; ?></label>
                                    <select class="form-control" name="product[]"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(ProductName) from `products` ");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['ProductName'].'">'.$row['ProductName'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Part Description <?php echo $_SESSION['accesslicence']+1; ?></label>
                                          <input type="text" class="form-control" name="partdesc[]"  placeholder="Part Description" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Quantity <?php echo $_SESSION['accesslicence']+1; ?></label>
                                          <input type="number" class="form-control" name="qty[]"  placeholder="Quantity" >
                                       </div>

                                    </div>
                                       <div class="row">
                                    
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Unit Price</label>
                                          <input type="number" class="form-control" name="unitprice[]" placeholder="Unit Price" step="0.01" >
                                       </div>
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >OEM Quotation <?php echo $_SESSION['accesslicence']+1; ?></label>
                                          <input type="file" class="form-control" name="uplfiles[]"  placeholder="OEM Quotation" >
                                       </div>
                                       <div class="form-group  col-md-2">
                                          <label class="form-control-label" >Licence For (In Years) <?php echo $_SESSION['accesslicence']+1; ?></label>
                                          <select class="form-control" name="licencefor[]"  data-plugin="select2" required="required" >
                                          <?php
                                       
                                          echo '<option value="" >Select</option>';
                                       for($l=1;$l<=10;$l++)
                                       {
                                           
                                       
                                            echo '<option  value="'.$l.'">'.$l.'</option>';
                                           
                                       }
                                       ?>
                                          </select>
                                       </div>
                                       <div class="form-group  col-md-2">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button" onClick="addMoreLicence();">

                                          <span class="icon md-plus" ></span>
                                          </button>
                                          <!-- <button class="btn btn-danger btn-add-basic" type="button" onClick="deleteRow();">

                                          <span class="icon md-minus" ></span>
                                          </button> -->
                                          </span>
                                       </div>
                                    </div>

                              <SCRIPT>
                              function addMoreLicence() {
                                  $("<div>").load("quotgen-licence.php", function() {
                                          $("#linelicence").append($(this).html());
                                  }); 
                              }
                              function deleteRowLicence() {
                                  $('div.linelicence').each(function(index, item){
                                      jQuery(':checkbox', this).each(function () {
                                          if ($(this).is(':checked')) {
                                              $(item).remove();
                                          }
                                      });
                                  });
                              }
                           </SCRIPT>

                          
                                 <div id="linelicence">
                                    
                                 </div>
                              
                                    <hr>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Currency</label>
                                    <select class="form-control" name="currency"  data-plugin="select2" required="required" >
                                    <option value="rupee">Rupee &#8377;</option>
                                       <option value="dollar">Dollar &#036;</option>
                                       <option value="euro">Euro &euro;</option>
                                       <option value="pound">Pound &pound;</option>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >HSN/SAC</label>
                                          <input type="text" class="form-control" name="hsnsac[]"  placeholder="HSN/SAC" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Tax</label>
                                          <select class="form-control" name="tax[]" multiple="multiple" data-plugin="select2" required="required" >
                                          <?php
                                       $project=mysqli_query($dbc,"select distinct(TaxName),TaxPercentage from `setting_tax`");
                                       
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                           //$pro=$row['ProjectName'];
                                       
                                            echo '<option  value="'.$row['TaxName'].'-'.$row['TaxPercentage'].'">'.$row['TaxName'].' '.$row['TaxPercentage'].'%'.'</option>';
                                           
                                       }
                                       ?>
                                          </select>
                                       </div>
                                       
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Validity (In Days)</label>
                                    <input type="number" class="form-control" name="validity"  placeholder="Validity (In Days)" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Delivery (In Days)</label>
                                          <input type="number" class="form-control" name="delivery"  placeholder="Delivery (In Days)" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Payment</label>
                                          <select class="form-control" name="payment" data-plugin="select2" required="required" >
                                          <option selected value="100% Advance">100% Advance</option>
                                       <option value="30 Days">30 Days</option>
                                       <option value="45 Days">45 Days</option>
                                       <option value="60 Days">60 Days</option>
                                       <option value="90 Days">90 Days</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Service Name</label>
                                    <input type="text" class="form-control" name="servicename"  placeholder="Service Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Cost</label>
                                          <input type="number" class="form-control" name="servicecost"  placeholder="Service Cost" step="0.01" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Tax (%)</label>
                                          <input type="number" class="form-control" name="servicetax"  placeholder="Service Tax (%)" >
                                       </div>
                                    </div>

                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Freight Name</label>
                                    <input type="text" class="form-control" name="freightname"  placeholder="Freight Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Cost</label>
                                          <input type="number" class="form-control" name="freightcost"  placeholder="Freight Cost" step="0.01" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Tax (%)</label>
                                          <input type="number" class="form-control" name="freighttax"  placeholder="Freight Tax (%)" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Expected Closure</label>
                                    <input type="text" class="form-control" name="expectedclosure"  placeholder="Expected Closure" data-plugin="datepicker" >
                                 </div>

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Note</label>
                                    <input type="text" class="form-control" name="remarks"  placeholder="Note" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Other Attachment</label>
                                          <input type="file" class="form-control" name="attachment[]"  placeholder="Freight Cost" >
                                       </div>

                                       
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                          <input type="checkbox"  name="addreq" value="Yes">Address Required in Quotation
                                       </div>
                                    </div>
                                    
                                 
                              <div class="form-group ">
                                 <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                 <button type="reset"  class="btn btn-default">Reset</button>
                              </div>
                           </form>
                        </div>
                           </div>
                           <!--  Licence end -->
                           <!--  Multiple Tax start -->
                           <?php
                    if($quotformname=="multipletax")
                    {

                    ?>
                     <div class="tab-pane active animation-slide-left" id="multipletax" role="tabpanel">
                      <?php
                    }
                      else
                      {
                        ?>
                        <div class="tab-pane animation-slide-left" id="multipletax" role="tabpanel">

                        <?php
                      }
                      ?>

                     
                        <div class="example">
                           <form action="../auth/order/quotation/updquot.php" method="post"  enctype="multipart/form-data" autocomplete="off">

                              <input type="hidden" name="quotformname" value="multipletax">

                              <input type="hidden" name="quotno" value="<?php echo $quotno; ?>" >
                              <input type="hidden" name="qtype" value="<?php echo $qtype; ?>" >
                              <input type="hidden" name="uniqueid" value="<?php echo $uniqueid; ?>" >
                              
                              <div class="row">
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="company"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(Company) from `customers` where RMail='$id' order by id desc");
                                 echo '<option value="'.$company.'" >'.$company.'</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['Company'].'" >'.$row['Company'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddCustomer" data-toggle="modal" type="button"><i class="icon md-account "></i>Add New Customer</button>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddProduct" data-toggle="modal" type="button"><i class="icon md-mall  "></i>Add New Product</button>
                                 </div>
                                 </div>

                                 <hr>


                                  <?php  

                                                    for($j=1;$j<=60;$j++)
                                                    {
                                                        $pr="product".$j;
                                                        $pd="pdescription".$j;
                                                        $qt="quantity".$j;
                                                        $up="unitprice".$j;
                                                        $qp="quotpath".$j;
                                                        $hsn="hsnsac".$j;

                                                        if(!empty($$pr))
                                                        {


                                                    
                                                    echo '<div class="linemultipletax" ><div class="row">

                                 <div class="form-group  col-md-3">
                                    <label class="form-control-label" >Product </label>
                                    ';?>
                                    <select class="form-control" name="product[]"  data-plugin="select2" required="required" >
                                        <?php                         

                                            $project=mysqli_query($dbc,"select distinct(ProductName) from `products` ");
                           
                          echo '<option value="'.$$pr.'" selected>'.$$pr.'</option>';
                           while($row=mysqli_fetch_assoc($project))
                           {
                               
                           
                                echo '<option  value="'.$row['ProductName'].'">'.$row['ProductName'].'</option>';
                               
                           }
                           ?>
                           <?php
                                   echo '</select>
                            
                                                        </div>

                                                        <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Part Description </label>
                                            <input type="text" class="form-control" name="partdesc[]"  value="'.$$pd.'" >
                                                        </div>
                                                    
                                                        
                                                        <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Quantity </label>
                                          <input type="number" class="form-control" name="qty[]" value="'.$$qt.'"  placeholder="Quantity" >
                                                           
                                                            
                                                        </div>
                                                        
                                    
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Unit Price </label>
                                          <input type="number" class="form-control" name="unitprice[]" value="'.$$up.'" placeholder="Unit Price" step="0.01" > 
                                                        </div>

                                                        </div>

                                                        <div class="row">

                                          <div class="form-group  col-md-3">
                                          <label class="form-control-label" >HSN/SAC </label>
                                          <input type="text" class="form-control" name="hsnsac[]" value="'.$$hsn.'"  placeholder="HSN/SAC" >
                                       </div>

                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Tax </label>
                                          ';?>
                                          <select class="form-control" name="tax[]" multiple="multiple" data-plugin="select2" required="required" >
                                          <?php
                                       $project=mysqli_query($dbc,"select distinct(TaxName),TaxPercentage from `setting_tax`");

                                       $array =  explode(",", $tax);
                                       $count=0;
                                       $grandtotal1=0;
                                       $subtotal1=0;
                                       $cgstshow=0;

                                       

                                       foreach ($array as $item) {
                                       global $count;
                                       $count=$count+1;
                                       }
                                       
                                       
                                       
                                       foreach ($array as $item) {
                                       
                                       
                                       $taxperarr =  explode("-", $item);


                                       
                                       foreach ($taxperarr as $perr) {
                                        $persep=explode("@", $perr);
                                        
                                          $taxpercentage=$persep[0];
                                        
                                          }

                                          

                                          foreach ($taxperarr as $perr) {
                                            
                                        $persep=explode("@", $perr);


                                          
                                        
                                          $showtax=$persep[1];

                                          $m=$j+1;
                                          if($m==$showtax)
                                          {

                                       if($count>1 and $taxperarr[0]=="SGST" or $taxperarr[0]=="CGST")
                                       {
                                       
                                       
                                        echo '<option selected  value="'.$taxperarr[0].'-'.$taxpercentage.'@'.$persep[1].'">'.$taxperarr[0].' '.$taxpercentage.'%</option>';
                                        while($row=mysqli_fetch_assoc($project))
                                       {
                                           
                                       
                                            echo '<option  value="'.$row['TaxName'].'-'.$row['TaxPercentage'].'@'.$persep[1].'">'.$row['TaxName'].' '.$row['TaxPercentage'].'%</option>';
                                       }

                                        
                                          if($globtax<=$persep[1])
                                          {
                                            global $globtax;
                                            $globtax=$persep[1];
                                          }
                                        

                                       
                                       }
                                       else
                                       {

                                          echo '<option selected  value="'.$taxperarr[0].'-'.$taxpercentage.'@'.$persep[1].'">'.$taxperarr[0].' '.$taxpercentage.'%</option>';

                                          while($row=mysqli_fetch_assoc($project))
                                       {
                                           
                                       
                                            echo '<option  value="'.$row['TaxName'].'-'.$row['TaxPercentage'].'@'.$persep[1].'">'.$row['TaxName'].' '.$row['TaxPercentage'].'%</option>';
                                       }

                                          
                                          if($globtax<=$persep[1])
                                          {
                                            global $globtax;
                                            $globtax=$persep[1];
                                          }
                                                                              
                                       }
                                       
                                    
                                   }

                                       }
                                      
                                       
                                       
                                     }

                                     /*$_SESSION['accesstaxvalue']=$showtax+1;*/
                                     
                                     
                                        $taxpv=$globtax+1;
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                           
                                       
                                            echo '<option  value="'.$row['TaxName'].'-'.$row['TaxPercentage'].'@'.$taxpv.'">'.$row['TaxName'].' '.$row['TaxPercentage'].'%</option>';
                                       }

                                       ?>

                                       <?php
                                          echo '</select>
                                       </div>

                                                        <div class="form-group  col-md-3">
                                          <label class="form-control-label" >OEM Quotation </label>
                                          <input type="file" class="form-control" name="uplfiles[]" value="'.$$qp.'" placeholder="OEM Quotation" >

                           </div>

                           <div class="form-group  col-md-3">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button" onClick="addMoreTax();">

                                          <span class="icon md-plus" ></span>
                                          </button>
                                          <button class="btn btn-danger btn-add-basic" type="button" onClick="deleteRowTax();">

                                          <span class="icon md-minus" ></span>
                                          </button>
                                          <input type="checkbox" name="item_index[]" />
                                          </span>
                                       </div>
                                    </div>
                                    </div>
                                                    ';
                                                    /*if(!empty($j))
                                                    {
                                                      $_SESSION['accesstax']=$_SESSION['accesstax']+1;
                                                    }*/
                                                    

                                                    }
                                                    else
                                                    {

                                                      break;
                                                    }
                                                    
                                                    

                                                }

                                                $_SESSION['accesstax']=$j-2;
                                                ?>


                                 
                                

                              <SCRIPT>
                                
                              function addMoreTax() {
                                 
                                  $("<div>").load("quotgen-multipletax.php", function() {
                                          $("#linemultipletax").append($(this).html());
                                  }); 
                              }
                              function deleteRowTax() {
                                  $('div.linemultipletax').each(function(index, item){
                                      jQuery(':checkbox', this).each(function () {
                                          if ($(this).is(':checked')) {
                                              $(item).remove();
                                          }
                                      });
                                  });
                              }
                           </SCRIPT>


                                 <div id="linemultipletax">
                                  <?php
                                    $_SESSION['accesstaxvalue']=$taxpv-1;

                                    ?>

                                 </div>
                              
                                    <hr>
                                    <div class="row">

                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Currency</label>
                                    <select class="form-control" name="currency"  data-plugin="select2" required="required" >
                                      <option value="<?php echo $currency; ?>"><?php echo $currency; ?></option>
                                      <option value="rupee">Rupee &#8377;</option>
                                       <option value="dollar">Dollar &#036;</option>
                                       <option value="euro">Euro &euro;</option>
                                       <option value="pound">Pound &pound;</option>
                                    </select>
                                 </div>

                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Validity (In Days)</label>
                                    <input type="number" class="form-control" name="validity" value="<?php echo $validity; ?>" placeholder="Validity (In Days)" >
                                 </div>
                              
                                       
                                       
                                    </div>
                                    <div class="row">

                                 
                              
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Delivery (In Days)</label>
                                          <input type="number" class="form-control" name="delivery" value="<?php echo $delivery; ?>" placeholder="Delivery (In Days)" >
                                       </div>

                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Payment</label>
                                          <select class="form-control" name="payment" data-plugin="select2" required="required" >
                                            <option value="<?php if($payment=="100% Advance")
                                            {echo $payment;}
                                            else{
                                              echo $payment;
                                            } ?>"><?php if($payment=="100% Advance")
                                            {echo $payment;}
                                            else{
                                              echo $payment;
                                            } ?></option>
                                          <option  value="100% Advance">100% Advance</option>
                                       <option value="30 Days">30 Days</option>
                                       <option value="45 Days">45 Days</option>
                                       <option value="60 Days">60 Days</option>
                                       <option value="90 Days">90 Days</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Service Name</label>
                                    <input type="text" class="form-control" name="servicename" value="<?php echo $servicename; ?>" placeholder="Service Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Cost</label>
                                          <input type="number" class="form-control" name="servicecost" value="<?php echo $servicecost; ?>"  placeholder="Service Cost" step="0.01" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Tax (%)</label>
                                          <input type="number" class="form-control" name="servicetax" value="<?php echo $servicetax; ?>"  placeholder="Service Tax (%)" >
                                       </div>
                                    </div>

                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Freight Name</label>
                                    <input type="text" class="form-control" name="freightname" value="<?php echo $freightname; ?>"  placeholder="Freight Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Cost</label>
                                          <input type="number" class="form-control" name="freightcost" value="<?php echo $freightcost; ?>"  placeholder="Freight Cost" step="0.01" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Tax (%)</label>
                                          <input type="number" class="form-control" name="freighttax" value="<?php echo $freighttax; ?>"  placeholder="Freight Tax (%)" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Expected Closure</label>
                                    <input type="text" class="form-control" name="expectedclosure" value="<?php echo $expectedclosure; ?>"  placeholder="Expected Closure" data-plugin="datepicker"  >
                                 </div>

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Note</label>
                                    <input type="text" class="form-control" name="remarks" value="<?php echo $remarks; ?>"  placeholder="Note" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Other Attachment</label>
                                          <input type="file" class="form-control" name="attachment[]" value="<?php echo $attachment; ?>"  placeholder="Freight Cost" >
                                       </div>

                                       
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                          <input type="checkbox"  name="addreq" value="<?php echo $addreq; ?>" >Address Required in Quotation
                                       </div>
                                    </div>
                                    
                                 
                              <div class="form-group ">
                                 <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                 <button type="reset"  class="btn btn-default">Reset</button>
                              </div>
                           </form>
                        </div>
                           </div>
                           <!--  Multiple Tax end -->
                           <!--  Multiple Upload start -->
                           <?php
                    if($quotformname=="multipleupload")
                    {

                    ?>
                     <div class="tab-pane active animation-slide-left" id="multipleupload" role="tabpanel">
                      <?php
                    }
                      else
                      {
                        ?>
                        <div class="tab-pane animation-slide-left" id="multipleupload" role="tabpanel">

                        <?php
                      }
                      ?>

                     
                        <div class="example">
                           <form action="../auth/order/quotation/generate.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              <input type="hidden" name="quotformname" value="multipleupload">
                              
                              <div class="row">
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="company"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(Company) from `customers` where RMail='$id'  order by id desc");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['Company'].'" >'.$row['Company'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddCustomer" data-toggle="modal" type="button"><i class="icon md-account "></i>Add New Customer</button>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddProduct" data-toggle="modal" type="button"><i class="icon md-mall  "></i>Add New Product</button>
                                 </div>
                                 </div>

                                 <hr>

                                 
                                 <div class="row">

                                 <div class="form-group  col-md-3">
                                    <label class="form-control-label" >Product</label>
                                    <select class="form-control" name="product[]"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(ProductName) from `products` ");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['ProductName'].'">'.$row['ProductName'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Part Description</label>
                                          <input type="text" class="form-control" name="partdesc[]"  placeholder="Part Description" >
                                       </div>

                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Quantity</label>
                                          <input type="number" class="form-control" name="qty[]"  placeholder="Quantity" >
                                       </div>
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Unit Price</label>
                                          <input type="number" class="form-control" name="unitprice[]" placeholder="Unit Price" step="0.01" >
                                       </div>

                                    </div>
                                       <div class="row">

                                          <div class="form-group  col-md-3">
                                          <label class="form-control-label" >HSN/SAC</label>
                                          <input type="text" class="form-control" name="hsnsac[]"  placeholder="HSN/SAC" >
                                       </div>

                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Tax</label>
                                          <select class="form-control" name="tax[]" multiple="multiple" data-plugin="select2" required="required" >
                                          <?php
                                       $project=mysqli_query($dbc,"select distinct(TaxName),TaxPercentage from `setting_tax`");
                                       
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                           //$pro=$row['ProjectName'];
                                       
                                            echo '<option  value="'.$row['TaxName'].'-'.$row['TaxPercentage'].'">'.$row['TaxName'].' '.$row['TaxPercentage'].'%'.'</option>';
                                           
                                       }
                                       ?>
                                          </select>
                                       </div>
                                    
                                       
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >OEM Quotation</label>
                                          <input type="file" class="form-control" name="uplfiles[]"  placeholder="OEM Quotation" >
                                       </div>
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button" onClick="addMoreUpload();">

                                          <span class="icon md-plus" ></span>
                                          </button>
                                          <!-- <button class="btn btn-danger btn-add-basic" type="button" onClick="deleteRow();">

                                          <span class="icon md-minus" ></span>
                                          </button> -->
                                          </span>
                                       </div>
                                    </div>

                              <SCRIPT>
                              function addMoreUpload() {
                                  $("<div>").load("quotgen-multipleupload.php", function() {
                                          $("#linemultipleupload").append($(this).html());
                                  }); 
                              }
                              function deleteRowUpload() {
                                  $('div.linemultipleupload').each(function(index, item){
                                      jQuery(':checkbox', this).each(function () {
                                          if ($(this).is(':checked')) {
                                              $(item).remove();
                                          }
                                      });
                                  });
                              }
                           </SCRIPT>

                          
                                 <div id="linemultipleupload">
                                    
                                 </div>
                              
                                    <hr>
                                    <div class="row">

                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Currency</label>
                                    <select class="form-control" name="currency"  data-plugin="select2" required="required" >
                                    <option value="rupee">Rupee &#8377;</option>
                                       <option value="dollar">Dollar &#036;</option>
                                       <option value="euro">Euro &euro;</option>
                                       <option value="pound">Pound &pound;</option>
                                    </select>
                                 </div>
                              
                                       
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Licence For (In Years)</label>
                                          <select class="form-control" name="licencefor[]" multiple="multiple" data-plugin="select2" required="required" >
                                          <?php
                                       
                                       
                                       for($l=1;$l<=10;$l++)
                                       {
                                           //$pro=$row['ProjectName'];
                                       
                                            echo '<option  value="'.$l.'">'.$l.'</option>';
                                           
                                       }
                                       ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Validity (In Days)</label>
                                    <input type="number" class="form-control" name="validity"  placeholder="Validity (In Days)" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Delivery (In Days)</label>
                                          <input type="number" class="form-control" name="delivery"  placeholder="Delivery (In Days)" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Payment</label>
                                          <select class="form-control" name="payment" data-plugin="select2" required="required" >
                                          <option selected value="100% Advance">100% Advance</option>
                                       <option value="30 Days">30 Days</option>
                                       <option value="45 Days">45 Days</option>
                                       <option value="60 Days">60 Days</option>
                                       <option value="90 Days">90 Days</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Service Name</label>
                                    <input type="text" class="form-control" name="servicename"  placeholder="Service Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Cost</label>
                                          <input type="number" class="form-control" name="servicecost"  placeholder="Service Cost" step="0.01" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Tax (%)</label>
                                          <input type="number" class="form-control" name="servicetax"  placeholder="Service Tax (%)" >
                                       </div>
                                    </div>

                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Freight Name</label>
                                    <input type="text" class="form-control" name="freightname"  placeholder="Freight Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Cost</label>
                                          <input type="number" class="form-control" name="freightcost"  placeholder="Freight Cost" step="0.01" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Tax (%)</label>
                                          <input type="number" class="form-control" name="freighttax"  placeholder="Freight Tax (%)" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Expected Closure</label>
                                    <input type="text" class="form-control" name="expectedclosure"  placeholder="Expected Closure" data-plugin="datepicker" >
                                 </div>

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Note</label>
                                    <input type="text" class="form-control" name="remarks"  placeholder="Note" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Other Attachment</label>
                                          <input type="file" class="form-control" name="attachment[]"  placeholder="Freight Cost" >
                                       </div>

                                       
                                    </div>

                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                          <input type="checkbox"  name="addreq" value="Yes">Address Required in Quotation
                                       </div>
                                    </div>
                                    
                                 
                              <div class="form-group ">
                                 <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                 <button type="reset"  class="btn btn-default">Reset</button>
                              </div>
                           </form>
                        </div>
                           </div>
                           <!--  Multiple Upload end -->
                        </div>
                     </div>
                  </div>
                  <!-- input customer details end -->
                <!-- End Panel Basic -->
                <!-- Modal Add Customer -->
                    <div class="modal fade" id="modalAddCustomer" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-lg">
                        <form class="modal-content" action="../auth/customer/icmp.php" method="post" autocomplete="off">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFormModalLabel">Input Company Details</h4>
                          </div>
                          <div class="modal-body">
                           <h5 class="mb-lg">Company Information</h5>
                            <div class="row">
                                <input type="hidden" name="from" value="<?php echo $id; ?>">

                                <input type="hidden" name="datafrom" value="quotgen">

                              <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Company Name</label>
                                    <input type="text" class="form-control" name="company" placeholder="Company Name" onkeypress="return blockSpecialChar(event)" required="required" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Sector</label>
                                    <select class="form-control" name="sector"  data-plugin="select2" required="required" >
                                    <?php
                                       $comp=$_GET['c'];
                                       $project=mysqli_query($dbc,"select distinct(SectorName) from `sector`  ");
                                       echo '<option  value="">Select</option>';
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                         echo '<option  value="'.$row['SectorName'].'">'.$row['SectorName'].'</option>';
                                                                                 
                                       }
                                       ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >City</label>
                                    <input type="text" class="form-control" name="city" placeholder="City"  required="required" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >State</label>
                                    <input type="text" class="form-control" name="state" placeholder="State"  required="required" >
                                 </div>
                              </div>
                              <h5 class="mb-lg">Contact Information</h5>
                              <div class="controls-basic">
                                 <div role="form" class="entry">
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Name</label>
                                          <input type="text" class="form-control" name="name[]" id="w4-f" onKeyUp="fname()" placeholder="Name" required="required">
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Designation</label>
                                          <input type="text" class="form-control" name="designation[]" placeholder="Designation" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Level</label>
                                          <input type="text" class="form-control" name="level[]" placeholder="Level" >
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Department</label>
                                          <input type="text" class="form-control" name="department[]" placeholder="Department" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Email</label>
                                          <input type="email" class="form-control" name="email[]" placeholder="Email"  >
                                       </div>
                                       <div class="form-group  col-md-5">
                                          <label class="form-control-label" >Mobile</label>
                                          <input type="text" class="form-control" name="mobile[]" id="w4-mob" onKeyUp="mob()" placeholder="Mobile" maxlength="12" required="required">
                                       </div>
                                       <div class="form-group  col-md-1">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button">
                                          <span class="icon md-plus" ></span>
                                          </button>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                              </div>


                              <div class="row">
                              
                              <div class="col-md-12 float-right">
                                <button class="btn btn-primary"  type="submit" name="submit">Submit</button>
                                <button class="btn btn-default"  type="reset" >Reset</button>
                                <button class="btn btn-default" data-dismiss="modal" type="reset" >Close</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <!-- End Modal add Customer -->
                    <!-- Modal add Product -->
                    <div class="modal fade" id="modalAddProduct" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-lg">
                        <form class="modal-content" action="../auth/product/ins.php" method="post" autocomplete="off">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFormModalLabel">Input Product Details</h4>
                          </div>
                          <div class="modal-body">
                            <h5 class="mb-lg">OEM Information</h5>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Name</label>
                                    <input type="text" class="form-control" name="oemname" placeholder="Name" onkeypress="return blockSpecialChar(event)" required="required" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Contact Peson</label>
                                    <input type="text" class="form-control" name="oemcontactperson" placeholder="Contact Person" onkeypress="return blockSpecialChar(event)" required="required" >
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Contact Mail</label>
                                    <input type="email" class="form-control" name="oemcontactmail" placeholder="City"  required="required" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Contact No</label>
                                    <input type="number" class="form-control" name="oemcontactno" placeholder="Contact No" maxlength="10" required="required" >
                                 </div>
                              </div>
                              <h5 class="mb-lg">Product Information</h5>
                              
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Category</label>
                                 <select name="category" class="form-control" data-plugin="select2">
                                        
                                    <option value="Infrastructure">Infrastructure</option>
                                    <option value="Security">Security</option>
                                            
                                    <option value="Software Development & Testing">Software Development & Testing</option>
                                    <option value="Digital Transformation">Digital Transformation</option>
                                    <option value="Analytics">Analytics</option>
                                                   
                                          </select>
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Product Name</label>
                                          <input type="text" class="form-control" name="productname" id="w4-l"  placeholder="Product Name" required="required" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >PAN Available</label>
                                       <select name="panavailable" class="form-control" data-plugin="select2">
                                          <option value="Yes">Yes</option>
                                          <option value="No">No</option>
                                       </select>
                                       </div>
                                       
                                    </div>


                                    <h5 class="mb-lg">Vendor/Seller Information</h5>
                                    <div class="controls">
                                 <div role="form" class="entry">
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Name</label>
                                    <input type="text" class="form-control" name="sellername[]" placeholder="Name" onkeypress="return blockSpecialChar(event)" required="required" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Contact Peson</label>
                                    <input type="text" class="form-control" name="sellercontactperson[]" placeholder="Contact Person" onkeypress="return blockSpecialChar(event)" required="required" >
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Contact Mail</label>
                                    <input type="email" class="form-control" name="sellercontactmail[]" placeholder="Contact Mail"  required="required" >
                                 </div>
                                 <div class="form-group  col-md-5">
                                    <label class="form-control-label" >Contact No</label>
                                    <input type="number" class="form-control" name="sellercontactno[]" placeholder="Contact No"  required="required" >
                                 </div>
                                    <div class="form-group  col-md-1">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add" type="button">
                                          <span class="icon md-plus" ></span>
                                          </button>
                                          </span>
                                       </div>
                                     </div>
                                   </div>
                                 </div>
                              <div class="row">
                              <div class="col-md-12 float-right">
                                <button class="btn btn-primary"  type="submit" name="submit">Submit</button>
                                <button class="btn btn-default"  type="reset" >Reset</button>
                                <button class="btn btn-default" data-dismiss="modal" type="reset" >Close</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <!-- End Modal add product -->
         </div>
      </div>
      <!-- End Page -->
      <!-- Footer -->
      <?php include "includes/footer.php"; ?>
      <!-- Core  -->
      <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
      <script src="../../assets/global/vendor/jquery/jquery.js"></script>
      <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
      <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
      <script src="../../assets/global/vendor/animsition/animsition.js"></script>
      <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
      <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
      <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
      <script src="../../assets/global/vendor/waves/waves.js"></script>
      <!-- Plugins -->
      <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
      <script src="../../assets/global/vendor/switchery/switchery.js"></script>
      <script src="../../assets/global/vendor/intro-js/intro.js"></script>
      <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
      <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
      <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
      <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
      <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
      <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
      <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
      <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
      <!-- Scripts -->
      <script src="../../assets/global/js/Component.js"></script>
      <script src="../../assets/global/js/Plugin.js"></script>
      <script src="../../assets/global/js/Base.js"></script>
      <script src="../../assets/global/js/Config.js"></script>
      <script src="section/Menubar.js"></script>
      <script src="section/Sidebar.js"></script>
      <script src="section/PageAside.js"></script>
      <script src="section/GridMenu.js"></script>
      <!-- Config -->
      <script src="../../assets/global/js/config/colors.js"></script>
      <script src="../../assets/js/config/tour.js"></script>
      <script>Config.set('assets', '../assets');</script>
      <!-- Page -->
      <script src="../../assets/js/Site.js"></script>
      <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
      <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
      <script src="../../assets/global/js/Plugin/switchery.js"></script>
      <script src="../../assets/global/js/Plugin/matchheight.js"></script>
      <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
      <script src="../../assets/global/js/Plugin/peity.js"></script>
      <script src="../../assets/examples/js/dashboard/v1.js"></script>
      
      <script src="../../assets/global/vendor/select2/select2.full.min.js"></script>
<script src="../../assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
<script src="../../assets/global/vendor/multi-select/jquery.multi-select.js"></script>
<script src="../../assets/global/js/Plugin/select2.js"></script>
<script src="../../assets/global/js/Plugin/bootstrap-select.js"></script>
<script src="../../assets/global/js/Plugin/multi-select.js"></script>



      <?php include "includes/js/datepicker.php"; ?>
      <?php include "includes/js/dynamicaddition.php"; ?>
      <script src="../../assets/examples/js/forms/advanced.js"></script>
   <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>