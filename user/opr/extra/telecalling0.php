<?php
session_start();
error_reporting(E_PARSE | E_ERROR);
$id=$_SESSION['id'];

if(!$_SESSION['id'])
{
	echo '<script>location.replace("../../index.php");</script>';
} 

require_once "../conn/conn.php";
$chk=mysqli_query($dbc,"select * from team where email='$id'");
while($fchk=mysqli_fetch_assoc($chk))
{
	$type=$fchk['EmployeeType'];
	if($type != "Admin")
	{
		echo '<script>location.replace("../../index.php");</script>';
	}
}

?>

<style>
.a{
    margin-top:-47px;
    margin-left:80px;
    margin-bottom:-20px;
}

</style>
<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Dashboard | Navikra CRM</title>
		<meta name="keywords" content="Navikra CRM" />
		<meta name="description" content="Navikra CRM">
		<meta name="author" content="navikra.com">

		<link rel="apple-touch-icon" sizes="180x180" href="../../../log/assets/images/apple-touch-icon.png">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-32x32.png" sizes="32x32">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-16x16.png" sizes="16x16">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="../../../log/assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="../../../log/assets/vendor/modernizr/modernizr.js"></script>

		<!-- search -->
		<!-- <script type="text/javascript" src="assets/ajax/search.js" ></script> -->
		
		<script type="text/javascript" src="../../../log/assets/ajax/jquery-1.8.0.min.js"></script>
		<script type="text/javascript">
$(function(){
$(".search").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
if(searchid!='')
{
    $.ajax({
    type: "POST",
    url: "../auth/campaign/tlclsearch.php",
    data: dataString,
    cache: false,
    success: function(html)
    {
    $("#result").html(html).show();
    }
    });
}return false;    
});

jQuery("#result").live("click",function(e){ 
    var $clicked = $(e.target);
    var $name = $clicked.find('.name').html();
    var decoded = $("<div/>").html($name).text();
    $('#searchid').val(decoded);
});
jQuery(document).live("click", function(e) { 
    var $clicked = $(e.target);
    if (! $clicked.hasClass("search")){
    jQuery("#result").fadeOut(); 
    }
});
$('#searchid').click(function(){
    jQuery("#result").fadeIn();
});
});
</script>



		

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="../" class="logo">
						<img src="../../../log/assets/images/logo.png" height="35" alt="Navikra CRM" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
			
				
                    
                    <?php
                        $ad=mysqli_query($dbc,"select * from team where email='$id'");
                        while($r=mysqli_fetch_assoc($ad))
                        {
                            $name=$r['name'];
                            $role=$r['desig'];
                        }
                    ?>
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="../../../log/assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="<?php echo $id; ?>">
								<span class="name"><?php echo $name; ?></span>
								<span class="role"><?php echo $role; ?></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="#"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<!-- <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a> -->
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="../../index.php"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li >
										<a href="dashboard.php">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<!-- <li>
										<a href="mailbox-folder.html">
											<span class="pull-right label label-primary">182</span>
											<i class="fa fa-envelope" aria-hidden="true"></i>
											<span>Mailbox</span>
										</a>
                                    </li> -->
                                    <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-users" aria-hidden="true"></i>
                                       <span>Customers</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="custdash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addcust.php">
                                                 Add Customer
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="campcust.php">
                                                 Campaign Customer
                                                </a>
                                        
                                           </li>
                                    
                                          <!--  <li >
                                              <a href="targetset.php">
                                                 Set Target
                                              </a>
                                        
                                           </li> -->
                                    

                                    </ul>
                                 </li>
                                    <li class="nav-parent nav-active nav-expanded">
										<a >
											<!-- <span class="pull-right label label-primary">182</span> -->
											<i class="fa fa-plus" aria-hidden="true"></i>
											<span>Campaign</span>
										</a>

										<ul class="nav nav-children">

											<li >
												<a href="create-campaign.php">
													Create 
												</a>
											</li>
											<li class="nav-parent " >
												<a >
													Direct 
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="dirdash.php">
													 		Show
														</a>
													</li>
													<li >
														<a href="direct.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>
											
											<li class="nav-parent ">
												<a >
													 Digital
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="digdash.php">
													 		Show
														</a>
													</li>
													<li>
														<a href="digital.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>

											<li class="nav-parent nav-active nav-expanded">
												<a >
													 Telecalling
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="teldash.php">
													 		Show
														</a>
													</li>
													<li class="nav-active">
														<a href="telecalling.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>
											<li class="nav-parent">
												<a >
													 Reference
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="refdash.php">
													 		Show
														</a>
													</li>
													<li>
														<a href="reference.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>

											
										</ul>

                                    </li>


									<li class=" nav-parent ">
										<a>
											<!-- <span class="pull-right label label-primary">182</span> -->
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Meeting</span>
										</a>

										<ul class="nav nav-children">


											<li >
												<a href="meetingdash.php">
													Show 
												</a>
												
											</li>
											
											<li  >
												<a href="meeting.php">
													 Update
												</a>
												
											</li>

										</ul>

                                    </li>

									<li class="nav-parent">
										<a >
										
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Funnel</span>
										</a>
										<ul class="nav nav-children">


											<li >
												<a href="funneldash.php">
													Show 
												</a>
											
											</li>
										
											<li >
												<a href="funnel.php">
													 Update
												</a>
											
											</li>

										</ul>
									</li>

									
                                    <li class="nav-parent">
									<a >
										
										<i class="fa fa-check" aria-hidden="true"></i>
										<span>Target Vs Achievement</span>
									</a>
									<ul class="nav nav-children">
	
	
										<!-- <li >
											<a href="funneldash.php">
												View 
											</a>
											
										</li> -->
										<li >
											<a href="achievement.php">
												 Achievement
											</a>
											
										</li>
										
										<li >
											<a href="targetset.php">
												Set Target
											</a>
											
										</li>
										
	
									</ul>
								</li>
								<li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-users" aria-hidden="true"></i>
                                       <span>Employee</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="emplist.php">
                                                 Employee List
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addemp.php">
                                                 Add
                                                </a>
                                        
                                           </li>
                                    
                                          <!--  <li >
                                              <a href="targetset.php">
                                                 Set Target
                                              </a>
                                        
                                           </li> -->
                                    

                                    </ul>
                                 </li>
                                 
									<li>
										<a href="#">
											
											<i class="fa fa-book" aria-hidden="true"></i>
											<span>Reports</span>
										</a>
                                    </li>

									<!-- <li>
										<a href="upd.php">
											
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Update</span>
										</a>
                                    </li>
									<li>
										<a href="csv.php">
											
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Update CSV</span>
										</a>
                                    </li> -->


									<!-- <li class="nav-parent">
										<a>
											<i class="fa fa-copy" aria-hidden="true"></i>
											<span>Pages</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="pages-signup.html">
													 Sign Up
												</a>
											</li>
											<li>
												<a href="pages-signin.html">
													 Sign In
												</a>
											</li>
											<li>
												<a href="pages-recover-password.html">
													 Recover Password
												</a>
											</li>
											<li>
												<a href="pages-lock-screen.html">
													 Locked Screen
												</a>
											</li>
											<li>
												<a href="pages-user-profile.html">
													 User Profile
												</a>
											</li>
											<li>
												<a href="pages-session-timeout.html">
													 Session Timeout
												</a>
											</li>
											<li>
												<a href="pages-calendar.html">
													 Calendar
												</a>
											</li>
											<li>
												<a href="pages-timeline.html">
													 Timeline
												</a>
											</li>
											<li>
												<a href="pages-media-gallery.html">
													 Media Gallery
												</a>
											</li>
											<li>
												<a href="pages-invoice.html">
													 Invoice
												</a>
											</li>
											<li>
												<a href="pages-blank.html">
													 Blank Page
												</a>
											</li>
											<li>
												<a href="pages-404.html">
													 404
												</a>
											</li>
											<li>
												<a href="pages-500.html">
													 500
												</a>
											</li>
											<li>
												<a href="pages-log-viewer.html">
													 Log Viewer
												</a>
											</li>
											<li >
												<a href="pages-search-results.html">
													 Search Results
												</a>
											</li>
										</ul>
									</li> -->
									
									
								</ul>
							</nav>
				
							<!-- <hr class="separator" />
				
							<div class="sidebar-widget widget-tasks">
								<div class="widget-header">
									<h6>Projects</h6>
									<div class="widget-toggle">+</div>
								</div>
								<div class="widget-content">
									<ul class="list-unstyled m-none">
										<li><a href="#">Porto HTML5 Template</a></li>
										<li><a href="#">Tucson Template</a></li>
										<li><a href="#">Porto Admin</a></li>
									</ul>
								</div>
							</div>
 -->				
                           
							<hr class="separator" />
				
							<div class="sidebar-widget widget-stats">
								<div class="widget-header">
									<h6>Company Stats</h6>
									<div class="widget-toggle">+</div>
								</div>
								<div class="widget-content">
									<ul>
										<li>
											<span class="stats-title">Stat 1</span>
											<span class="stats-complete">85%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">
													<span class="sr-only">85% Complete</span>
												</div>
											</div>
										</li>
										<li>
											<span class="stats-title">Stat 2</span>
											<span class="stats-complete">70%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
													<span class="sr-only">70% Complete</span>
												</div>
											</div>
										</li>
										<li>
											<span class="stats-title">Stat 3</span>
											<span class="stats-complete">2%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
													<span class="sr-only">2% Complete</span>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Telecalling Campaign Search</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Pages</span></li>
								<li><span>Search</span></li>
							</ol>
					
							<!-- <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a> -->
							<a class="sidebar-right-toggle" ><i class="fa fa-chevron-down"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<div class="search-content">
						<div class="search-control-wrapper">
							<form >
								<div class="form-group">
									<div class="input-group col-sm-12">
									<label class="col-sm-1 control-label" for="w4-username">Projects</label>
														<div class="col-sm-5" >
										
														<select class=" form-control" name="project"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6" onchange="selected(this)" required >
															<?php
																$project=mysqli_query($dbc,"select * from `campaign-projects`");
																echo '<option >Select Project</option>';
																while($row=mysqli_fetch_assoc($project))
																{
																	//$pro=$row['ProjectName'];
															
																	 echo '<option  value="'.$row['ProjectName'].'">'.$row['ProjectName'].'</option>';
																	 
															
																}
															?>
														</select>
														<script>
															function selected(obj){
																document.cookie = "proj = " + obj.value ;
															}
														</script>
														<?php
															unset($_SESSION['proj']);
															$_SESSION['proj']= $_COOKIE['proj'];
														?>
														</div>
														<div class="col-sm-6" >
														
										<input type="text"  class="search form-control col-sm-2" id="searchid" placeholder="Search..." onKeyUp="emptyVal(this)" >
										
										</div>
										<!-- <span class="input-group-btn">
											<button class="btn btn-primary" type="button">Search</button>
										</span> -->
									</div>
								</div>
							</form>
						</div>
						<div class="search-toolbar">
							<ul class="list-unstyled nav nav-pills">
								<li class="active">
									<a href="#everything" data-toggle="tab">Search Result</a>
								</li>
								<!-- <li>
									<a href="#medias" data-toggle="tab">Medias</a>
								</li> -->
							</ul>
						</div>
						<div class="tab-content">
							<div id="everything" class="tab-pane active">
								<!-- <p class="total-results text-muted">Showing 1 to 10 of 47 results</p> -->

								<ul class="list-unstyled search-results-list">
									<li>
										<!-- <p class="result-type">
											<span class="label label-primary">user</span>
										</p> -->
										<a  class="has-thumb">
											<!-- <div class="result-thumb">
												<img src="assets/images/!logged-user.jpg" alt="John Doe" />
											</div> -->
											<br>
											<div class="result-data">
												<!-- <p class="h3 title text-primary">John Doe</p>
												<p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante nisl, sagittis nec lacus et, convallis efficitur justo. Curabitur elementum feugiat quam. Etiam ac orci iaculis, luctus nisl et, aliquet metus. Praesent congue tortor venenatis, ornare eros eu, semper orci.</p> -->
												<div id="result"></div>
											</div>
										</a>
									</li>
									<!-- <li>
										<p class="result-type">
											<span class="label label-primary">page</span>
										</p>
										<a href="ui-elements-charts.html">
											<div class="result-data">
												<p class="h3 title text-primary">Charts</p>
												<p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante nisl, sagittis nec lacus et, convallis efficitur justo. Curabitur <strong>something</strong> elementum feugiat quam. Etiam ac orci iaculis, luctus nisl et, aliquet metus. Praesent congue tortor venenatis, ornare eros eu, semper orci.</p>
											</div>
										</a>
									</li>
									<li>
										<p class="result-type">
											<span class="label label-primary">page</span>
										</p>
										<a href="pages-invoice.html" class="has-thumb">
											<div class="result-thumb">
												<img src="assets/images/projects/project-6.jpg" alt="Invoice" />
											</div>
											<div class="result-data">
												<p class="h3 title text-primary">Invoice</p>
												<p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante nisl, sagittis nec lacus et, convallis efficitur justo. Curabitur elementum feugiat quam. Etiam ac orci iaculis, luctus nisl et, aliquet metus. Praesent congue tortor venenatis, ornare eros eu, semper orci.</p>
											</div>
										</a>
									</li>
									<li>
										<p class="result-type">
											<span class="label label-primary">email</span>
										</p>
										<a href="mailbox-email.html" class="has-thumb">
											<div class="result-thumb">
												<i class="fa fa-envelope"></i>
											</div>
											<div class="result-data">
												<p class="h3 title text-primary">John Doe</p>
												<p class="description">
													<small>05/09/2014 11:34PM</small>
													<br/>
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante nisl, sagittis nec lacus et, convallis efficitur justo. Curabitur elementum feugiat quam. Etiam ac orci iaculis, luctus nisl et, aliquet metus. Praesent congue tortor venenatis, ornare eros eu, semper orci.
												</p>
											</div>
										</a>
									</li>
									<li>
										<p class="result-type">
											<span class="label label-primary">page</span>
										</p>
										<a href="pages-media-gallery.html">
											<div class="result-data">
												<p class="h3 title text-primary">Media Gallery</p>
												<p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante nisl, sagittis nec lacus et, convallis efficitur justo. Curabitur elementum feugiat quam. Etiam ac orci iaculis, luctus nisl et, aliquet metus. Praesent congue tortor venenatis, ornare eros eu, semper orci.</p>
											</div>
										</a>
									</li>
									<li>
										<p class="result-type">
											<span class="label label-primary">page</span>
										</p>
										<a href="pages-invoice.html">
											<div class="result-data">
												<p class="h3 title text-primary">Invoice</p>
												<p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante nisl, sagittis nec lacus et, convallis efficitur justo. Curabitur elementum feugiat quam. Etiam ac orci iaculis, luctus nisl et, aliquet metus. Praesent congue tortor venenatis, ornare eros eu, semper orci.</p>
											</div>
										</a>
									</li>
									<li>
										<p class="result-type">
											<span class="label label-primary">page</span>
										</p>
										<a href="pages-calendar.html">
											<div class="result-data">
												<p class="h3 title text-primary">Calendar</p>
												<p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante nisl, sagittis nec lacus et, convallis efficitur justo. Curabitur elementum feugiat quam. Etiam ac orci iaculis, luctus nisl et, aliquet metus. Praesent congue tortor venenatis, ornare eros eu, semper orci.</p>
											</div>
										</a>
									</li> -->
								</ul>

								<hr class="solid mb-none" />

								<!-- <ul class="pagination">
									<li class="prev disabled">
										<a href="#">
											<i class="fa fa-chevron-left"></i>
										</a>
									</li>
									<li class="active">
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">2</a>
									</li>
									<li>
										<a href="#">3</a>
									</li>
									<li>
										<a href="#">4</a>
									</li>
									<li>
										<a href="#">5</a>
									</li>
									<li class="next">
										<a href="#">
											<i class="fa fa-chevron-right"></i>
										</a>
									</li>
								</ul> -->
							</div>
							<div id="medias" class="tab-pane">
								<div class="row">
									<div class="col-sm-6 col-md-4 col-lg-3">
										<div class="thumbnail">
											<div class="thumb-preview">
												<a class="thumb-image" href="#">
													<img src="../../../log/assets/images/projects/project-2.jpg" class="img-responsive" alt="Project">
												</a>
											</div>
											<h5 class="mg-title text-weight-semibold">Blog<small>.png</small></h5>
										</div>
									</div>
									<div class="col-sm-6 col-md-4 col-lg-3">
										<div class="thumbnail">
											<div class="thumb-preview">
												<a class="thumb-image" href="#">
													<img src="../../../log/assets/images/projects/project-5.jpg" class="img-responsive" alt="Project">
												</a>
											</div>
											<h5 class="mg-title text-weight-semibold">Friends<small>.png</small></h5>
										</div>
									</div>
									<div class="col-sm-6 col-md-4 col-lg-3">
										<div class="thumbnail">
											<div class="thumb-preview">
												<a class="thumb-image" href="#">
													<img src="../../../log/assets/images/projects/project-4.jpg" class="img-responsive" alt="Project">
												</a>
											</div>
											<h5 class="mg-title text-weight-semibold">Life<small>.png</small></h5>
										</div>
									</div>
									<div class="col-sm-6 col-md-4 col-lg-3">
										<div class="thumbnail">
											<div class="thumb-preview">
												<a class="thumb-image" href="#">
													<img src="../../../log/assets/images/projects/project-5.jpg" class="img-responsive" alt="Project">
												</a>
											</div>
											<h5 class="mg-title text-weight-semibold">Poetry<small>.png</small></h5>
										</div>
									</div>
								</div>
							</div>
							<div id="emails" class="tab-pane">
								<p>Recent</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>
							</div>
						</div>
					</div>
					<!-- end: page -->
				</section>
			</div>

			
		</section>

		<!-- Vendor -->
		<script src="../../../log/assets/vendor/jquery/jquery.js"></script>
		<script src="../../../log/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../../../log/assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../../../log/assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../../../log/assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../../../log/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../../../log/assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../../../log/assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../../../log/assets/javascripts/theme.init.js"></script>



		<script src="../../../log/assets/javascripts/forms/examples.wizard.js"></script>
		<script src="../../../log/assets/javascripts/forms/examples.advanced.form.js"></script>

		<script>
										function emptyVal(mytext)
												{
													if(mytext.value == 0)
  												 {
													window.location.reload();
  												 }
												}

										</script>

	</body>
</html>