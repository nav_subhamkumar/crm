<?php
include "session_handler.php";
?>

<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Dashboard | Navikra CRM</title>
		<meta name="keywords" content="Navikra CRM" />
		<meta name="description" content="Navikra CRM">
		<meta name="author" content="navikra.com">

		<link rel="apple-touch-icon" sizes="180x180" href="../../../log/assets/images/apple-touch-icon.png">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-32x32.png" sizes="32x32">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-16x16.png" sizes="16x16">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="../../../log/assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="../../../log/assets/vendor/modernizr/modernizr.js"></script>

    <!-- fa-product icon -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet" />

        <!-- Hide and Show -->
        <script src="../../../log/assets/jquery/jquery.min.js"></script>

         <!-- Read more and less css and JS -->
<style type="text/css">
    .morecontent span {
    display: none;
    text-decoration: none;
    background-color: white;
}
.morelink {
    display: block;
    text-decoration: none;
}
</style>
<script type="text/javascript">
    $(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 20;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Read More...";
    var lesstext = "Read Less...";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>
<!-- Read more and less css and JS end -->

<!--on hover select link start -->
<style type="text/css">
    .dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {color: #660064}

.dropdown:hover .dropdown-content {
    display: block;
}
.dropdown:hover .dropbtn {
    color: #660064;
}

</style>
<!--on hover select link end -->


	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
            <div class="logo-container">
                <a href="dashboard.php" class="logo">
                    <img src="../../../log/assets/images/logo.png" height="48" alt="Navikra CRM" />
                </a>
                <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>
        
            <!-- start: search & user box -->
            <div class="header-right">
        
            <?php include "notification.php"; ?>
                
                <?php
                    $ad=mysqli_query($dbc,"select * from team where email='$id'");
                    while($r=mysqli_fetch_assoc($ad))
                    {
                        $name=$r['name'];
                        $role=$r['desig'];

                    }
                    $uid=$_GET['u'];
                ?>
        
                <span class="separator"></span>
        
                <div id="userbox" class="userbox">
                    <a href="#" data-toggle="dropdown">
                        <figure class="profile-picture">
                            <img src="../../../log/assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
                        </figure>
                        <div class="profile-info" data-lock-name="John Doe" data-lock-email="<?php echo $id; ?>">
                            <span class="name"><?php echo $name; ?></span>
                            <span class="role"><?php echo $role; ?></span>
                        </div>
        
                        <i class="fa custom-caret"></i>
                    </a>
        
                    <div class="dropdown-menu">
                        <ul class="list-unstyled">
                            <li class="divider"></li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="profile.php"><i class="fa fa-user"></i> My Profile</a>
                            </li>
                            <li>
                                <!-- <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a> -->
                            </li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="../../index.php"><i class="fa fa-power-off"></i> Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end: search & user box -->
        </header>
        <!-- end: header -->

        

        <div class="inner-wrapper">
            <!-- start: sidebar -->
            <aside id="sidebar-left" class="sidebar-left">
            
                <div class="sidebar-header">
                    <div class="sidebar-title">
                        Navigation
                    </div>
                    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
            
                <div class="nano">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li >
                                    <a href="dashboard.php">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>
                                <!-- <li>
                                    <a href="mailbox-folder.html">
                                        <span class="pull-right label label-primary">182</span>
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span>Mailbox</span>
                                    </a>
                                </li> -->
                                <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-users" aria-hidden="true"></i>
                                       <span>Prospects</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="custdash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addcust.php">
                                                 Add Prospect
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="customerprofile.php">
                                                 Add Prospect Profile
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="campcust.php">
                                                 Campaign Prospects
                                                </a>
                                        
                                           </li>
                                    
                                          <!--  <li >
                                              <a href="targetset.php">
                                                 Set Target
                                              </a>
                                        
                                           </li> -->
                                    

                                    </ul>
                                 </li>
                                <li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span>Campaign</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        <li >
                                            
                                            <a href="create-campaign.php">
                                                Create 
                                            </a>
                                            
                                        </li>


                                        <li class="nav-parent" >
                                            <a >
                                                Direct 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="dirdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="direct.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Digital
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="digdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="digital.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Telecalling
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="teldash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="telecalling.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-parent">
                                            <a >
                                                 Reference
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="refdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="reference.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        
                                    </ul>

                                </li>

                                <li >
                           <a href="lead.php">
                              <!-- <span class="pull-right label label-primary">182</span> -->
                              <i class="fa fa-user" aria-hidden="true"></i>
                              <span>Lead</span>
                           </a>
                           
                        </li>

                                <li class="nav-parent">
                                    <a>
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <span>Meeting</span>
                                    </a>

                                    <ul class="nav nav-children">


                                        <li >
                                            <a href="meetingdash.php">
                                                Dashboard 
                                            </a>
                                            
                                        </li>
                                        
                                        <!-- <li >
                                            <a href="meeting.php">
                                                 Update
                                            </a>
                                            
                                        </li> -->

                                    </ul>

                                </li>

                                <li class="nav-parent nav-active nav-expanded">
									<a >
								
									<i class="fa fa-refresh" aria-hidden="true"></i>
									<span>Funnel</span>
									</a>
									<ul class="nav nav-children">


									<li class="nav-active">
										<a href="funneldash.php">
											Show 
										</a>
									
									</li>
								
									<li >
										<a href="funnel.php">
											 Update
										</a>
									
									</li>

									</ul>
								</li>

                                <li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <span>Order</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Quotation 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="quotdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotation-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 PO
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="podash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="po-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="pogen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Invoice
                                            </a>
                                            <ul class="nav nav-children">
                                               <li>
                                                    <a href="invdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invoice-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>
                                 

                                
                                <li class="nav-parent">
								<a >
									
									<i class="fa fa-check" aria-hidden="true"></i>
									<span>Target Vs Achievement</span>
								</a>
								<ul class="nav nav-children">


									<!-- <li >
										<a href="funneldash.php">
											View 
										</a>
										
									</li> -->
									<li >
										<a href="achievement.php">
											 Achievement
										</a>
										
									</li>
									
									<li >
										<a href="targetset.php">
											Set Target
										</a>
										
									</li>
									

								</ul>
							</li>
							<li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                        <span>HR</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Employee 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="emplist.php">
                                                         Employee List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addemp.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Holiday
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="holidaylist.php">
                                                         Holiday List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addholiday.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Leave
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="leavedash.php">
                                                         Applications
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>

                                 <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                       <span>Products</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="proddash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addprod.php">
                                                 Add
                                                </a>
                                        
                                           </li>
                                    
                                          

                                    </ul>
                                 </li>
                                 <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-cogs" aria-hidden="true"></i>
                                       <span>Change</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="changepros.php">
                                                 Prospects
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="changecamp.php">
                                                 Campaign
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Meeting
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Funnel
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Order
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Target
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 HR
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Products
                                                </a>
                                        
                                           </li>

                                    
                                          

                                    </ul>
                                 </li>

                                 
                                <li>
                                    <a href="#">
                                        
                                        <i class="fa fa-book" aria-hidden="true"></i>
                                        <span>Reports</span>
                                    </a>
                                </li>
                                <li >
                            <a href="support.php">
                                        
                              <i class="fa fa-question-circle" aria-hidden="true"></i>
                              <span>Support</span>
                            </a>
                        </li>

                                


                                
                                
                                
                            </ul>
                        </nav>
            
                        <!-- <hr class="separator" />
            
                        <div class="sidebar-widget widget-tasks">
                            <div class="widget-header">
                                <h6>Projects</h6>
                                <div class="widget-toggle">+</div>
                            </div>
                            <div class="widget-content">
                                <ul class="list-unstyled m-none">
                                    <li><a href="#">Porto HTML5 Template</a></li>
                                    <li><a href="#">Tucson Template</a></li>
                                    <li><a href="#">Porto Admin</a></li>
                                </ul>
                            </div>
                        </div>
-->				
                       
                         
                    </div>
            
                </div>
            
            </aside>
            <!-- end: sidebar -->


				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Funnel Dashboard</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Dashboard</span></li>
								<li><span>Funnel</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-6 col-lg-12 col-xl-12">
							<section class="panel">
								<div class="panel-body">
                                	<div class="row">
										<form action="" method="get" class="form-horizontal" novalidate="novalidate"  >
                                            
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label" style="align:left" for="w4-username"><b>Department:</b></label>
                                                        <div class="col-sm-3" style="align:left">


                                                        <select size="1" id="Rank" class="form-control" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' title="" name="Rank">
                                                            <option value="">Select Department</option>
                                                            <option value="sales">Sales</option>
                                                            <option value="technical">Technical</option>
                                                        </select>


                                                    </div>

                                                    <?php  

                                                        $pname=mysqli_query($dbc,"select `name` from `team` where email='$uid' ");
                                                        while ($pfet=mysqli_fetch_assoc($pname)) {
                                                            $pfname=$pfet['name'];
                                                        }

                                                     ?>

                                                    <label class="col-sm-6 control-label" style="align:left" for="w4-username"><b> Profile: <?php  echo $pfname; ?></b></label>




                                            <div class="container">
                                                <div class="sales">

                                                       <label class="col-sm-3 control-label" style="align:left" for="w4-username"><b>Employees:</b></label>
                                                        <div class="col-sm-3" style="align:left">
                                                            
                                                            <select class="form-control" name="project"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6" onchange="redirect(this.value);" required >
                                                                <?php
                                                                    $project=mysqli_query($dbc,"select * from `team` where `Department`='Sales'");
                                                                    echo '<option >Select</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                        //$pro=$row['ProjectName'];
                                                        
                                                                         echo '<option  value="'.$row['email'].'">'.$row['name'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                <!-- </div>
                                                <div class="container"> -->
                                                <div class="technical">

                                                       <label class="col-sm-2 control-label" style="align:left" for="w4-username"><b>Employees:</b></label>
                                                        <div class="col-sm-3" style="align:left">
                                                            
                                                            <select class="form-control" name="project"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6" onchange="redirect(this.value);" required >
                                                                <?php
                                                                    $project=mysqli_query($dbc,"select * from `team` where `Department`='Technical'");
                                                                    echo '<option >Select</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                        //$pro=$row['ProjectName'];
                                                        
                                                                         echo '<option  value="'.$row['email'].'">'.$row['name'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </form>
                                                <script type="text/javascript">
                                                    function redirect(x)
                                                    {
                                                        var opted=x;
                                                        location.replace("afunneldash.php?u="+opted);

                                                    }

                                                </script>




                                                <script type="text/javascript">
    
                                                 $(document).ready(function() {
                                                $('#Rank').bind('change', function() {
                                                var elements = $('div.container').children().hide(); // hide all the elements
                                                /*var elements = $('div.container').children().hide();*/
                                                var value = $(this).val();

                                                if (value.length) { // if somethings' selected
                                                    elements.filter('.' + value).show(); // show the ones we want
                                                }
                                                }).trigger('change');
                                                });
                                                </script>
                                    
                                	</div>
                                
                                
                                </div>
								
								
							</section>
						</div>
						
						<div class="col-md-6 col-lg-12 col-xl-12">
                        <div class="row">
                            <div class="col-md-12 col-lg-6 col-xl-6">
                                <section class="panel panel-featured-left panel-featured-primary">
                                    <div class="panel-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-primary">
                                                    <i class="fa fa-users"></i>
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Customers</h4>
                                                    <div class="info">
                                                        <?php 
                                                            $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign` where RMail='$uid' ");
                                                            $rc=mysqli_fetch_assoc($cust);
                                                            $res=$rc['count_column'];
                                                            
                                                        ?>
                                                        <strong class="amount"><?php echo $res; ?></strong>
                                                        <!-- <span class="text-primary">(14 unread)</span> -->
                                                    </div>
                                                </div>
                                                <div class="summary-footer">
                                                    <a href="custdash.php" class="text-muted text-uppercase btn btn-primary"  ><font color="white">Total</font></a>
                                                    <a id="showcustomer" class="text-muted text-uppercase btn btn-primary"  ><font color="white">Campaigned</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showcustomer").click(function(){
                                                                $("#customer").show();
                                                                });
                                                            $("#hidecustomer").click(function(){
                                                                $("#customer").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div id="customer" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Campaigned Customers</h2>
                                    </header>
                            
                                    <div class="panel-body paging">
                            
                                        <div class="table-responsive">
                                            <a id="hidecustomer" class=" text-muted text-uppercase btn btn-primary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail, CampaignDate   from `campaign` where RMail='$uid' ");
                                                //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                        
                                                echo '<table class="table table-bordered  table-striped mb-none" >';
                                                /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>CampaignDate</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $cci=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $cci;
                                                            $cci=$cci+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $campaigneddate=$frow['CampaignDate'];
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$cci."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['CampaignDate']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>
                

                            <div class="col-md-12 col-lg-6 col-xl-6">
                                <section class="panel panel-featured-left panel-featured-secondary">
                                    <div class="panel-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-secondary">
                                                    <i class="fa fa-comment-o" ></i>
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Meetings</h4>
                                                    <div class="info">
                                                        <?php 
                                                            $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` where RMail='$uid' ");
                                                            $rc=mysqli_fetch_assoc($cust);
                                                            $res=$rc['count_column'];
                                                            
                                                        ?>
                                                        <strong class="amount"><?php echo $res; ?></strong>
                                                    </div>
                                                </div>
                                                <div class="summary-footer">
                                                    <a id="showmeetings" class="text-muted text-uppercase btn bg-secondary"  ><font color="white">Total</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showmeetings").click(function(){
                                                                $("#meeting").show();
                                                                });
                                                            $("#hidemeetings").click(function(){
                                                                $("#meeting").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                    <a id="showmeetingsmonthly" class="text-muted text-uppercase btn bg-secondary"  ><font color="white">Monthly</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showmeetingsmonthly").click(function(){
                                                                $("#meetingmonthly").show();
                                                                });
                                                            $("#hidemeetingsmonthly").click(function(){
                                                                $("#meetingmonthly").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                    <a id="showmeetingsannually" class="text-muted text-uppercase btn bg-secondary"  ><font color="white">Annually</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showmeetingsannually").click(function(){
                                                                $("#meetingannually").show();
                                                                });
                                                            $("#hidemeetingsannually").click(function(){
                                                                $("#meetingannually").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="meeting" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Total Meetings</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidemeetings" class=" text-muted text-uppercase btn bg-secondary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.Company as Company, m.ContactPerson as ContactPerson, m.Designation as Designation, m.Mobile as Mobile, m.Mail as Mail, m.Date as Date   from `meeting`  m inner join (
                                                    select Company, max(Date) as MaxDate
                                                    from meeting where RMail='$uid' 
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate  ");
                                        
                                                echo '<table class="table  table-bordered table-striped mb-none">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>Last Meeting</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tmi=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tmi;
                                                            $tmi=$tmi+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $mailid=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];

                                                            

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tmi."</td>";
                                                            echo "<td><a href='meeting-ins.php?c=".$company."&e=".$mailid."&t=$pro' >{$frow['Company']}</a></td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['Date']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="meetingmonthly" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Meetings Monthly</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidemeetingsmonthly" class=" text-muted text-uppercase btn bg-secondary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.Company, m.ContactPerson, m.Designation, m.Mobile, m.Mail, m.Date   from `meeting` m inner join (
                                                    select Company, max(date) as MaxDate
                                                    from meeting where RMail='$uid' and  MONTH(Date) = MONTH(CURRENT_DATE()) AND YEAR(Date) = YEAR(CURRENT_DATE())
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate  ");

                                                /*as per insertion*/

                                                /*$fetfunnel=mysqli_query($dbc,"select m.Company, m.ContactPerson, m.Designation, m.Mobile, m.Mail, m.Date   from `meeting` m inner join (
                                                    select Company, max(date) as MaxDate
                                                    from meeting where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' 
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate  ");*/
                                        
                                                echo '<table class="table  table-bordered table-striped mb-none">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>Last Meeting</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $mmi=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $mmi;
                                                            $mmi=$mmi+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$mmi."</td>";
                                                            echo "<td><a href='meeting-ins.php?c=".$company."&e=".$mailid."&t=$pro' >{$frow['Company']}</a></td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['Date']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="meetingannually" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Meetings Annually</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidemeetingsannually" class=" text-muted text-uppercase btn bg-secondary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.Company, m.ContactPerson, m.Designation, m.Mobile, m.Mail, m.Date   from `meeting` m inner join (
                                                    select Company, max(date) as MaxDate
                                                    from meeting where RMail='$uid' and  YEAR(Date) = YEAR(CURRENT_DATE())
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate  ");

                                                /*as per insertion*/

                                                /*$fetfunnel=mysqli_query($dbc,"select m.Company, m.ContactPerson, m.Designation, m.Mobile, m.Mail, m.Date   from `meeting` m inner join (
                                                    select Company, max(date) as MaxDate
                                                    from meeting where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' 
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate  ");*/
                                        
                                                echo '<table class="table  table-bordered  table-striped mb-none">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>Last Meeting</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $mai=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $mai;
                                                            $mai=$mai+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$mai."</td>";
                                                            echo "<td><a href='meeting-ins.php?c=".$company."&e=".$mailid."&t=$pro' >{$frow['Company']}</a></td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['Date']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div class="col-md-12 col-lg-6 col-xl-6">
                                <section class="panel panel-featured-left panel-featured-tertiary">
                                    <div class="panel-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-tertiary">
                                                    <i class="fa fa-shopping-cart"></i>
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Orders</h4>
                                                    <div class="info">
                                                        <?php 
                                                            $cust=mysqli_query($dbc,"select count(Company) as count_column from `funnel` where RMail='$uid' and  `Stage`='Won'");
                                                            $rc=mysqli_fetch_assoc($cust);
                                                            $res=$rc['count_column'];
                                                            
                                                        ?>
                                                        <strong class="amount"><?php echo $res; ?></strong>
                                                    </div>
                                                </div>
                                                <div class="summary-footer">
                                                    <a id="showorders" class="text-muted text-uppercase btn bg-tertiary"  ><font color="white">Total</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showorders").click(function(){
                                                                $("#order").show();
                                                                });
                                                            $("#hideorders").click(function(){
                                                                $("#order").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>
                                                    <a id="showordersmonthly" class="text-muted text-uppercase btn bg-tertiary"  ><font color="white">Monthly</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showordersmonthly").click(function(){
                                                                $("#ordermonth").show();
                                                                });
                                                            $("#hideordersmonthly").click(function(){
                                                                $("#ordermonth").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>
                                                    <a id="showordersannualy" class="text-muted text-uppercase btn bg-tertiary"  ><font color="white">Annually</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showordersannualy").click(function(){
                                                                $("#orderannually").show();
                                                                });
                                                            $("#hideordersannually").click(function(){
                                                                $("#orderannually").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="order" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Total Orders</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hideorders" class=" text-muted text-uppercase btn bg-tertiary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  `Stage`='Won' order by id desc");
                                        
                                                /*echo '<table class="table table-striped mb-none">';*/
                                                echo '<table class="table table-bordered table-striped mb-none"  >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>Product</th>';
                                                        echo '<th>Service</th>';
                                                        echo '<th>Detail</th>';
                                                        echo '<th>Revenue</th>';
                                                        echo '<th>Margin</th>';
                                                        echo '<th>Margin %</th>';
                                                        echo '<th>Closure Date</th>';
                                                        
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $toir=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $toir;
                                                            $toir=$toir+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $service=$frow['Services'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                    
                                                            $closuredate_final=date("d/m/Y", strtotime($closuredate));

                                                            $marginpercent=($margin/$revenue)*100;
                                                            $marginpercent_final=number_format((float)$marginpercent, 2, '.', '');
                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$toir."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['Products']}</td>";
                                                            echo "<td>{$frow['Services']}</td>";
                                                            /*echo "<td>{$frow['Detail']}</td>";*/
                                                            echo '<td><span class="more">'.$detail.'</span></td>';
                                                            echo "<td>{$frow['Revenue']}</td>";
                                                            echo "<td>{$frow['Margin']}</td>";
                                                            echo "<td>".$marginpercent_final."</td>";
                                                            echo "<td>{$closuredate_final}</td>";
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                            $totalr=0;
                                                            $custr=mysqli_query($dbc,"select `Revenue` from `funnel` where RMail='$uid' and  `Stage`='Won' ");
                                                            while($rcr=mysqli_fetch_assoc($custr))
                                                            {
                                                                $rev=$rcr['Revenue'];
                                                                global $totalr;
                                                                $totalr=$totalr+$rev;
                                                            }
                                                            $trevenue=$totalr;

                                                            $totalm=0;
                                                            $custm=mysqli_query($dbc,"select `Margin` from `funnel` where RMail='$uid' and  `Stage`='Won' ");
                                                            while($rcm=mysqli_fetch_assoc($custm))
                                                            {
                                                                $mar=$rcm['Margin'];
                                                                global $totalm;
                                                                $totalm=$totalm+$mar;
                                                            }
                                                            $tmargin=$totalm;

                                                            echo '<tr>';
                                                            echo '<th>Total</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th>'.$trevenue.'</th>';
                                                            echo '<th>'.$tmargin.'</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';

                                                            echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="ordermonth" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Total Orders Monthly</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hideordersmonthly" class=" text-muted text-uppercase btn bg-tertiary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  `Stage`='Won' AND  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                        
                                                echo '<table class="table table-bordered table-striped mb-none">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>Product</th>';
                                                        echo '<th>Service</th>';
                                                        echo '<th>Detail</th>';
                                                        echo '<th>Revenue</th>';
                                                        echo '<th>Margin</th>';
                                                        echo '<th>Margin %</th>';
                                                        echo '<th>Closure Date</th>';
                                                        
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tomi=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tomi;
                                                            $tomi=$tomi+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $service=$frow['Services'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                    
                                                            $closuredate_final=date("d/m/Y", strtotime($closuredate));

                                                            $marginpercent=($margin/$revenue)*100;
                                                            $marginpercent_final=number_format((float)$marginpercent, 2, '.', '');
                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tomi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['Products']}</td>";
                                                            echo "<td>{$frow['Services']}</td>";
                                                            /*echo "<td>{$frow['Detail']}</td>";*/
                                                            echo '<td><span class="more">'.$detail.'</span></td>';
                                                            echo "<td>{$frow['Revenue']}</td>";
                                                            echo "<td>{$frow['Margin']}</td>";
                                                            echo "<td>".$marginpercent_final."</td>";
                                                            echo "<td>{$closuredate_final}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                            $totalr=0;
                                                            $custr=mysqli_query($dbc,"select `Revenue` from `funnel` where RMail='$uid' and  `Stage`='Won' AND  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                                            while($rcr=mysqli_fetch_assoc($custr))
                                                            {
                                                                $rev=$rcr['Revenue'];
                                                                global $totalr;
                                                                $totalr=$totalr+$rev;
                                                            }
                                                            $trevenue=$totalr;

                                                            $totalm=0;
                                                            $custm=mysqli_query($dbc,"select `Margin` from `funnel` where RMail='$uid' and  `Stage`='Won' AND  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                            while($rcm=mysqli_fetch_assoc($custm))
                                                            {
                                                                $mar=$rcm['Margin'];
                                                                global $totalm;
                                                                $totalm=$totalm+$mar;
                                                            }
                                                            $tmargin=$totalm;

                                                            echo '<tr>';
                                                            echo '<th>Total</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th>'.$trevenue.'</th>';
                                                            echo '<th>'.$tmargin.'</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>


                            <div id="orderannually" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Total Orders Annually</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hideordersannually" class=" text-muted text-uppercase btn bg-tertiary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  `Stage`='Won' AND   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                        
                                                echo '<table class="table table-bordered table-striped mb-none">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>Product</th>';
                                                        echo '<th>Service</th>';
                                                        echo '<th>Detail</th>';
                                                        echo '<th>Revenue</th>';
                                                        echo '<th>Margin</th>';
                                                        echo '<th>Margin %</th>';
                                                        echo '<th>Closure Date</th>';
                                                        
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $toai=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $toai;
                                                            $toai=$toai+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $service=$frow['Services'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                    
                                                            $closuredate_final=date("d/m/Y", strtotime($closuredate));

                                                            $marginpercent=($margin/$revenue)*100;
                                                            $marginpercent_final=number_format((float)$marginpercent, 2, '.', '');
                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$toai."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['Products']}</td>";
                                                            echo "<td>{$frow['Services']}</td>";
                                                            /*echo "<td>{$frow['Detail']}</td>";*/
                                                            echo '<td><span class="more">'.$detail.'</span></td>';
                                                            echo "<td>{$frow['Revenue']}</td>";
                                                            echo "<td>{$frow['Margin']}</td>";
                                                            echo "<td>".$marginpercent_final."</td>";
                                                            echo "<td>{$closuredate_final}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                            $totalr=0;
                                                            $custr=mysqli_query($dbc,"select `Revenue` from `funnel` where RMail='$uid' and  `Stage`='Won' AND   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                                            while($rcr=mysqli_fetch_assoc($custr))
                                                            {
                                                                $rev=$rcr['Revenue'];
                                                                global $totalr;
                                                                $totalr=$totalr+$rev;
                                                            }
                                                            $trevenue=$totalr;

                                                            $totalm=0;
                                                            $custm=mysqli_query($dbc,"select `Margin` from `funnel` where RMail='$uid' and  `Stage`='Won' AND   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                                            while($rcm=mysqli_fetch_assoc($custm))
                                                            {
                                                                $mar=$rcm['Margin'];
                                                                global $totalm;
                                                                $totalm=$totalm+$mar;
                                                            }
                                                            $tmargin=$totalm;

                                                            echo '<tr>';
                                                            echo '<th>Total</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th>'.$trevenue.'</th>';
                                                            echo '<th>'.$tmargin.'</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div class="col-md-12 col-lg-6 col-xl-6">
                                <section class="panel panel-featured-left panel-featured-quartenary">
                                    <div class="panel-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-quartenary">
                                                    <i class="fa fa-reply"></i>
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Funnel</h4>
                                                    <div class="info">
                                                        <?php 
                                                            /*$total=0;
                                                            $cust=mysqli_query($dbc,"select `Revenue` from `funnel` where  `Stage`='Won' ");
                                                            while($rc=mysqli_fetch_assoc($cust))
                                                            {
                                                                $rev=$rc['Revenue'];
                                                                global $total;
                                                                $total=$total+$rev;
                                                            }
                                                            $res=$total;*/
                                                            $cust=mysqli_query($dbc,"select count(id) as count_column from `funnel` where RMail='$uid'    ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];
                                                            
                                                        ?>
                                                        <strong class="amount"><?php echo $res; ?></strong>
                                                    </div>
                                                </div>
                                                <div class="summary-footer">
                                                    <a id="showfunnelprobability" class="text-muted text-uppercase btn bg-quartenary"  ><font color="white">Probability</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfunnelprobability").click(function(){
                                                                $("#funnelprobability").show();
                                                                });
                                                            $("#hidefunnelprobability").click(function(){
                                                                $("#funnelprobability").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                    <a id="showfunnelmonthly" class="text-muted text-uppercase btn bg-quartenary"  ><font color="white">Monthly</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfunnelmonthly").click(function(){
                                                                $("#funnelmonthly").show();
                                                                });
                                                            $("#hidefunnelmonthly").click(function(){
                                                                $("#funnelmonthly").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                    <a id="showfunnelannually" class="text-muted text-uppercase btn bg-quartenary"  ><font color="white">Annually</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfunnelannually").click(function(){
                                                                $("#funnelannually").show();
                                                                });
                                                            $("#hidefunnelannually").click(function(){
                                                                $("#funnelannually").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="funnelprobability" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Funnel</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">

                                            <a id="showfunnel100" class="text-muted text-uppercase btn bg-primary"  ><font color="white">100 %</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfunnel100").click(function(){
                                                                $("#funnelprobability100").show();
                                                                });
                                                            $("#hidefunnel100").click(function(){
                                                                $("#funnelprobability100").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>
                                            <a id="showfunnel75" class="text-muted text-uppercase btn " style="background-color: #3CB371 "  ><font color="white">75 %</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfunnel75").click(function(){
                                                                $("#funnelprobability75").show();
                                                                });
                                                            $("#hidefunnel75").click(function(){
                                                                $("#funnelprobability75").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>
                                            <a id="showfunnel50" class="text-muted text-uppercase btn bg-warning"  ><font color="white">50 %</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfunnel50").click(function(){
                                                                $("#funnelprobability50").show();
                                                                });
                                                            $("#hidefunnel50").click(function(){
                                                                $("#funnelprobability50").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>
                                            <a id="showfunnel25" class="text-muted text-uppercase btn bg-danger"  ><font color="white">25 %</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfunnel25").click(function(){
                                                                $("#funnelprobability25").show();
                                                                });
                                                            $("#hidefunnel25").click(function(){
                                                                $("#funnelprobability25").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>
                                            <a id="showfunnel0" class="text-muted text-uppercase btn bg-tertiary"  ><font color="white">0 - 100 %</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfunnel0").click(function(){
                                                                $("#funnelprobability0").show();
                                                                });
                                                            $("#hidefunnel0").click(function(){
                                                                $("#funnelprobability0").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>


                                                    <a id="hidefunnelprobability" class=" text-muted text-uppercase btn bg-quartenary pull-right"  ><font color="white">hide</font></a>

                            <div id="funnelprobability100" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        
                                        <h2 class="panel-title">Funnel 100 %</h2>
                                    </header>
                                    <a id="hidefunnel100" class=" text-muted text-uppercase btn bg-primary pull-right"  ><font color="white">hide</font></a>
                            
                                    <div class="panel-body">
                                        
                            
                                        <div class="table-responsive">
 
                                            <?php
                                        $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  `Probability`='100' ");

                                        echo '<table class="table table-bordered table-striped mb-none">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Company</th>';
                                                    echo '<th>Product</th>';
                                                    echo '<th>Detail</th>';
                                                    echo '<th>Revenue</th>';
                                                    echo '<th>Margin</th>';
                                                    echo '<th>Stage</th>';
                                                    echo '<th>Probability</th>';
                                                    echo '<th>Expected Date Of Closure</th>';
                                                    echo '<th>Expected Closure Month</th>';
                                                    echo '<th>Date of Closure</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $totalrevenue=0;
                                            $totalmargin=0;
                                            $f100i=0;
                                                while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $f100i;
                                                    $f100i=$f100i+1;
                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));

                                                    global $totalmargin;
                                                    global $totalrevenue;

                                                    
                                                        echo '<tr>';
                                                        echo "<td>".$f100i."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td>{$closuredate_final}</td>";
                                                        echo '</tr>';
                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;
                                                    
                                                    

                                                }
                                                echo '<tr>';
                                                echo '<th>Total</th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th>';echo $totalrevenue;echo '</th>';
                                                echo '<th>';echo $totalmargin;echo '</th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                
                                                echo '<th></th>';
                                                echo '</tr>';
                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="funnelprobability75" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        
                                        <h2 class="panel-title">Funnel 75 %</h2>
                                    </header>
                                    <a id="hidefunnel75" class=" text-muted text-uppercase btn  pull-right" style="background-color: #3CB371 " ><font color="white">hide</font></a>
                            
                                    <div class="panel-body">
                                        
                            
                                        <div class="table-responsive">
 
                                            <?php
                                        $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  `Probability`='75' ");

                                        echo '<table class="table table-bordered table-striped mb-none">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Company</th>';
                                                    echo '<th>Product</th>';
                                                    echo '<th>Detail</th>';
                                                    echo '<th>Revenue</th>';
                                                    echo '<th>Margin</th>';
                                                    echo '<th>Stage</th>';
                                                    echo '<th>Probability</th>';
                                                    echo '<th>Expected Date Of Closure</th>';
                                                    echo '<th>Expected Closure Month</th>';
                                                    echo '<th>Date of Closure</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $totalrevenue=0;
                                            $totalmargin=0;
                                            $f75i=0;
                                                while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    global $f75i;
                                                    $f75i=$f75i+1;


                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));

                                                    
                                                        echo '<tr>';
                                                        echo "<td>".$f75i."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 75%;background-color: #3CB371'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td>{$closuredate_final}</td>";
                                                        echo '</tr>';
                                                    
                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;
                                                    
                                                    

                                                }
                                                echo '<tr>';
                                                echo '<th>Total</th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th>';echo $totalrevenue;echo '</th>';
                                                echo '<th>';echo $totalmargin;echo '</th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '</tr>';
                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="funnelprobability50" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        
                                        <h2 class="panel-title">Funnel 50 %</h2>
                                    </header>
                                    <a id="hidefunnel50" class=" text-muted text-uppercase btn bg-warning pull-right"  ><font color="white">hide</font></a>
                            
                                    <div class="panel-body">
                                        
                            
                                        <div class="table-responsive">
 
                                            <?php
                                        $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  `Probability`='50' ");

                                        echo '<table class="table table-bordered table-striped mb-none">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Company</th>';
                                                    echo '<th>Product</th>';
                                                    echo '<th>Detail</th>';
                                                    echo '<th>Revenue</th>';
                                                    echo '<th>Margin</th>';
                                                    echo '<th>Stage</th>';
                                                    echo '<th>Probability</th>';
                                                    echo '<th>Expected Date Of Closure</th>';
                                                    echo '<th>Expected Closure Month</th>';
                                                    echo '<th>Date of Closure</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                                $totalrevenue=0;
                                            $totalmargin=0;
                                            $f50i=0;
                                                while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    global $f50i;
                                                    $f50i=$f50i+1;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));

                                                    
                                                        echo '<tr>';
                                                        echo "<td>".$f50i."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 50%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td>{$closuredate_final}</td>";
                                                        echo '</tr>';
                                                    
                                                  $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;
                                                    
                                                    

                                                }
                                                echo '<tr>';
                                                echo '<th>Total</th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th>';echo $totalrevenue;echo '</th>';
                                                echo '<th>';echo $totalmargin;echo '</th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '</tr>';
                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="funnelprobability25" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        
                                        <h2 class="panel-title">Funnel 25 %</h2>
                                    </header>
                                    <a id="hidefunnel25" class=" text-muted text-uppercase btn bg-danger pull-right"  ><font color="white">hide</font></a>
                            
                                    <div class="panel-body">
                                        
                            
                                        <div class="table-responsive">
 
                                            <?php
                                        $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  `Probability`='25' ");

                                        echo '<table class="table table-bordered table-striped mb-none">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Company</th>';
                                                    echo '<th>Product</th>';
                                                    echo '<th>Detail</th>';
                                                    echo '<th>Revenue</th>';
                                                    echo '<th>Margin</th>';
                                                    echo '<th>Stage</th>';
                                                    echo '<th>Probability</th>';
                                                    echo '<th>Expected Date Of Closure</th>';
                                                    echo '<th>Expected Closure Month</th>';
                                                    echo '<th>Date of Closure</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                               $totalrevenue=0;
                                            $totalmargin=0;
                                            $f25i=0;
                                                while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    global $f25i;
                                                    $f25i=$f25+1;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));

                                                    
                                                        echo '<tr>';
                                                        echo "<td>".$f25i."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 25%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td>{$closuredate_final}</td>";
                                                        echo '</tr>';
                                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;
                                                    
                                                    

                                                }
                                                echo '<tr>';
                                                echo '<th>Total</th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th>';echo $totalrevenue;echo '</th>';
                                                echo '<th>';echo $totalmargin;echo '</th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '</tr>';
                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="funnelprobability0" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        
                                        <h2 class="panel-title">Funnel 0 - 100 %</h2>
                                    </header>
                                    <a id="hidefunnel0" class=" text-muted text-uppercase btn bg-tertiary pull-right"  ><font color="white">hide</font></a>
                            
                                    <div class="panel-body">
                                        
                            
                                        <div class="table-responsive">
 
                                            <?php
                                        $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid'  ");

                                        echo '<table class="table table-bordered table-striped mb-none">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Company</th>';
                                                    echo '<th>Product</th>';
                                                    echo '<th>Detail</th>';
                                                    echo '<th>Revenue</th>';
                                                    echo '<th>Margin</th>';
                                                    echo '<th>Stage</th>';
                                                    echo '<th>Probability</th>';
                                                    echo '<th>Expected Date Of Closure</th>';
                                                    echo '<th>Expected Closure Month</th>';
                                                    echo '<th>Date of Closure</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                                $totalrevenue=0;
                                            $totalmargin=0;
                                            $falli=0;
                                                while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    global $falli;
                                                    $falli=$falli+1;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));

                                                    
                                                        echo '<tr>';
                                                        echo "<td>".$falli."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar ' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;background-color:#20B2AA '>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td>{$closuredate_final}</td>";
                                                        echo '</tr>';
                                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;
                                                    
                                                    

                                                }
                                                echo '<tr>';
                                                echo '<th>Total</th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th>';echo $totalrevenue;echo '</th>';
                                                echo '<th>';echo $totalmargin;echo '</th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '<th></th>';
                                                echo '</tr>';
                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>


                                        </div>
                                    </div>
                                </section>
                            </div>



                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="funnelmonthly" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Funnel Monthly</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidefunnelmonthly" class=" text-muted text-uppercase btn bg-quartenary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                        $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");

                                        echo '<table class="table table-bordered table-striped mb-none">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Company</th>';
                                                    echo '<th>Product</th>';
                                                    echo '<th>Detail</th>';
                                                    echo '<th>Revenue</th>';
                                                    echo '<th>Margin</th>';
                                                    echo '<th>Stage</th>';
                                                    echo '<th>Probability</th>';
                                                    echo '<th>Expected Date Of Closure</th>';
                                                    echo '<th>Expected Closure Month</th>';
                                                    echo '<th>Date of Closure</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $totalrevenue=0;
                                            $totalmargin=0;
                                            $fmi=0;
                                                while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    global $fmi;

                                                    $fmi=$fmi+1;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));

                                                    if($stage == "Won")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fmi."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td>{$closuredate_final}</td>";
                                                        echo '</tr>';

                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    }elseif($probability == "0")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fmi."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td></td>";
                                                        
                                                        echo '</tr>';

                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    } elseif($probability == "25")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fmi."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td></td>";
                                                        echo '</tr>';

                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    } elseif($probability == "50")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fmi."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td></td>";
                                                        echo '</tr>';

                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    } elseif($probability == "75")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fmi."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td></td>";
                                                        echo '</tr>';
                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    } elseif($probability == "100")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fmi."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td>{$closuredate_final}</td>";
                                                        echo "<td></td>";
                                                        echo '</tr>';

                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    }
                                                    

                                                }
                                                echo "<tr>";
                                                echo "<th>Total</th>";
                                                echo "<th></th>";
                                                echo "<th></th>";
                                                echo "<th></th>";
                                                echo "<th>".$totalrevenue."</th>";
                                                echo "<th>".$totalmargin."</th>";
                                                echo "<th></th>";
                                                echo "<th></th>";
                                                echo "<th></th>";
                                                echo "<th></th>";
                                                
                                                echo "<th></th>";
                                                echo "</tr>";
                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="funnelannually" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Funnel Annually</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidefunnelannually" class=" text-muted text-uppercase btn bg-quartenary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                        $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");

                                        echo '<table class="table table-bordered table-striped mb-none">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>Sl No.</th>';
                                                    echo '<th>Company</th>';
                                                    echo '<th>Product</th>';
                                                    echo '<th>Detail</th>';
                                                    echo '<th>Revenue</th>';
                                                    echo '<th>Margin</th>';
                                                    echo '<th>Stage</th>';
                                                    echo '<th>Probability</th>';
                                                    echo '<th>Expected Date Of Closure</th>';
                                                    echo '<th>Expected Closure Month</th>';
                                                    echo '<th>Date of Closure</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $totalrevenue=0;
                                            $totalmargin=0;
                                            $fai=0;
                                                while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    global $fai;
                                                    $fai=$fai+1;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));

                                                    if($stage == "Won")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fai."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td>{$closuredate_final}</td>";
                                                        echo '</tr>';

                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    }elseif($probability == "0")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fai."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td></td>";
                                                        
                                                        echo '</tr>';

                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    } elseif($probability == "25")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fai."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td></td>";
                                                        echo '</tr>';

                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    } elseif($probability == "50")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fai."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td></td>";
                                                        echo '</tr>';

                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    } elseif($probability == "75")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fai."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td></td>";
                                                        echo '</tr>';
                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    } elseif($probability == "100")
                                                    {
                                                        echo '<tr>';
                                                        echo "<td>".$fai."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        /*echo "<td>{$frow['Detail']}</td>";*/
                                                        echo '<td><span class="more">'.$detail.'</span></td>';
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Margin']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['Probability']}";
                                                            echo '</div>';
                                                        echo '</div></td>';
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        echo "<td>{$frow['ExpectedClosure']}</td>";
                                                        echo "<td>{$closuredate_final}</td>";
                                                        echo '</tr>';

                                                        $totalrevenue=$totalrevenue+$revenue;
                                                        $totalmargin=$totalmargin+$margin;

                                                    }
                                                    

                                                }
                                                echo "<tr>";
                                                echo "<th>Total</th>";
                                                echo "<th></th>";
                                                echo "<th></th>";
                                                echo "<th></th>";
                                                echo "<th>".$totalrevenue."</th>";
                                                echo "<th>".$totalmargin."</th>";
                                                echo "<th></th>";
                                                echo "<th></th>";
                                                echo "<th></th>";
                                                echo "<th></th>";
                                                
                                                echo "<th></th>";
                                                echo "</tr>";
                                            echo '</tbody>';
                                        echo '</table>';
                                    ?>


                                        </div>
                                    </div>
                                </section>
                            </div>


                            
                    <!-- <div class="row">
						<div class="col-md-12">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
										<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
									</div>

									<h2 class="panel-title">Best Seller</h2>
									<p class="panel-subtitle">Customize the graphs as much as you want, there are so many options and features to display information using Porto Admin Template.</p>
								</header>
								<div class="panel-body">

									Flot: Basic
									<div class="chart chart-md" id="flotDashBasic"></div>
									<script>

										var flotDashBasicData = [{
											data: [
												[0, 170],
												[1, 169],
												[2, 173],
												[3, 188],
												[4, 147],
												[5, 113],
												[6, 128],
												[7, 169],
												[8, 173],
												[9, 128],
												[10, 128]
											],
											label: "Series 1",
											color: "#0088cc"
										}, {
											data: [
												[0, 115],
												[1, 124],
												[2, 114],
												[3, 121],
												[4, 115],
												[5, 83],
												[6, 102],
												[7, 148],
												[8, 147],
												[9, 103],
												[10, 113]
											],
											label: "Series 2",
											color: "#2baab1"
										}, {
											data: [
												[0, 70],
												[1, 69],
												[2, 73],
												[3, 88],
												[4, 47],
												[5, 13],
												[6, 28],
												[7, 69],
												[8, 73],
												[9, 28],
												[10, 28]
											],
											label: "Series 3",
											color: "#734ba9"
										}];

										

									</script>

								</div>
							</section>
						</div>
						
					</div> -->
                    
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
                <div class="nano">
                    <div class="nano-content">
                        <a href="#" class="mobile-close visible-xs">
                            Collapse <i class="fa fa-chevron-right"></i>
                        </a>
            
                        <div class="sidebar-right-wrapper">
            
                            <div class="sidebar-widget widget-calendar">
                                <h6>Upcoming Tasks</h6>
                                <div data-plugin-datepicker data-plugin-skin="dark" ></div>
            
                                <ul>
                                    <li>
                                        <!-- <time datetime="2014-04-19T00:00+00:00">04/19/2014</time> -->
                                        <span>Company</span>
                                    </li>
                                </ul>
                            </div>
            
                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Meeting</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where RMail='$uid' and  `FollowUp`='Yes' order by FollowUpDate desc");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($followupdate == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Contact Person: {$frow['ContactPerson']}</span>
                                            <span class=title>Mobile: <a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></span>
                                            <span class=title>Meeting Time: $cdate</span>
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                    
                                </ul>
                            </div>




                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Funnel Follow Up</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($dateofclosure == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Product: {$frow['Products']}</span>
                                            
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                   
                                </ul>
                            </div>

            
                        </div>
                    </div>
                </div>
            </aside>
		</section>

		<!-- Vendor -->
		<script src="../../../log/assets/vendor/jquery/jquery.js"></script>
		<script src="../../../log/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../../../log/assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../../../log/assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../../../log/assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../../../log/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="../../../log/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="../../../log/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="../../../log/assets/vendor/jquery-appear/jquery.appear.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="../../../log/assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.js"></script>
		<script src="../../../log/assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.pie.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.categories.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.resize.js"></script>
		<script src="../../../log/assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
		<script src="../../../log/assets/vendor/raphael/raphael.js"></script>
		<script src="../../../log/assets/vendor/morris/morris.js"></script>
		<script src="../../../log/assets/vendor/gauge/gauge.js"></script>
		<script src="../../../log/assets/vendor/snap-svg/snap.svg.js"></script>
		<script src="../../../log/assets/vendor/liquid-meter/liquid.meter.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/jquery.vmap.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../../../log/assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../../../log/assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../../../log/assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="../../../log/assets/javascripts/dashboard/examples.dashboard.js"></script>

        <!-- modal form -->
      <script src="../assets/javascripts/ui-elements/examples.modals.js"></script>
	</body>
</html>