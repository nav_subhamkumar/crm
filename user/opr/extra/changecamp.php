<?php
include "session_handler.php";
?>

<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Change Campaign | Navikra CRM</title>
		<meta name="keywords" content="Navikra CRM" />
		<meta name="description" content="Navikra CRM">
		<meta name="author" content="navikra.com">

		<link rel="apple-touch-icon" sizes="180x180" href="../../../log/assets/images/apple-touch-icon.png">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-32x32.png" sizes="32x32">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-16x16.png" sizes="16x16">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="../../../log/assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="../../../log/assets/vendor/modernizr/modernizr.js"></script>

    <!-- fa-product icon -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet" />

		<!-- Hide and Show -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Delete Confirmation -->
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script language="JavaScript" type="text/javascript">
            $(document).ready(function(){
             $("a.delete").click(function(e){
             if(!confirm('Are you sure?')){
                  e.preventDefault();
                 return false;
                 }
                return true;
             });
            });
        </script>

        <!--on hover select link start -->
<style type="text/css">
    .dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {color: #660064}

.dropdown:hover .dropdown-content {
    display: block;
}
.dropdown:hover .dropbtn {
    color: #660064;
}

</style>
<!--on hover select link end -->
<!-- Specific Page Vendor CSS Modal -->
        <!-- <link rel="stylesheet" href="../../../log/assets/vendor/pnotify/pnotify.custom.css" /> -->


	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
            <div class="logo-container">
                <a href="dashboard.php" class="logo">
                    <img src="../../../log/assets/images/logo.png" height="48" alt="Navikra CRM" />
                </a>
                <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>
        
            <!-- start: search & user box -->
            <div class="header-right">
        
            <?php include "notification.php"; ?>
                
                <?php
                    $ad=mysqli_query($dbc,"select * from team where email='$id'");
                    while($r=mysqli_fetch_assoc($ad))
                    {
                        $name=$r['name'];
                        $role=$r['desig'];
                    }
                ?>
        
                <span class="separator"></span>
        
                <div id="userbox" class="userbox">
                    <a href="#" data-toggle="dropdown">
                        <figure class="profile-picture">
                            <img src="../../../log/assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
                        </figure>
                        <div class="profile-info" data-lock-name="John Doe" data-lock-email="<?php echo $id; ?>">
                            <span class="name"><?php echo $name; ?></span>
                            <span class="role"><?php echo $role; ?></span>
                        </div>
        
                        <i class="fa custom-caret"></i>
                    </a>
        
                    <div class="dropdown-menu">
                        <ul class="list-unstyled">
                            <li class="divider"></li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="profile.php"><i class="fa fa-user"></i> My Profile</a>
                            </li>
                            <li>
                                <!-- <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a> -->
                            </li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="../../index.php"><i class="fa fa-power-off"></i> Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end: search & user box -->
        </header>
        <!-- end: header -->

        

        <div class="inner-wrapper">
            <!-- start: sidebar -->
            <aside id="sidebar-left" class="sidebar-left">
            
                <div class="sidebar-header">
                    <div class="sidebar-title">
                        Navigation
                    </div>
                    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
            
                <div class="nano">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li >
                                    <a href="dashboard.php">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>
                                <!-- <li>
                                    <a href="mailbox-folder.html">
                                        <span class="pull-right label label-primary">182</span>
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span>Mailbox</span>
                                    </a>
                                </li> -->
                                <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-users" aria-hidden="true"></i>
                                       <span>Prospects</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="custdash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addcust.php">
                                                 Add Prospect
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="customerprofile.php">
                                                 Add Prospect Profile
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="campcust.php">
                                                 Campaign Prospects
                                                </a>
                                        
                                           </li>
                                    
                                          <!--  <li >
                                              <a href="targetset.php">
                                                 Set Target
                                              </a>
                                        
                                           </li> -->
                                    

                                    </ul>
                                 </li>
                                <li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span>Campaign</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        <li >
                                            
                                            <a href="create-campaign.php">
                                                Create 
                                            </a>
                                            
                                        </li>


                                        <li class="nav-parent" >
                                            <a >
                                                Direct 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="dirdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="direct.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Digital
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="digdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="digital.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Telecalling
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="teldash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="telecalling.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-parent">
                                            <a >
                                                 Reference
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="refdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="reference.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        
                                    </ul>

                                </li>

                                <li >
                           <a href="lead.php">
                              <!-- <span class="pull-right label label-primary">182</span> -->
                              <i class="fa fa-user" aria-hidden="true"></i>
                              <span>Lead</span>
                           </a>
                           
                        </li>


                                <li class="nav-parent">
                                    <a>
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <span>Meeting</span>
                                    </a>

                                    <ul class="nav nav-children">


                                        <li >
                                            <a href="meetingdash.php">
                                                Dashboard 
                                            </a>
                                            
                                        </li>
                                        
                                        <!-- <li >
                                            <a href="meeting.php">
                                                 Update
                                            </a>
                                            
                                        </li> -->

                                    </ul>

                                </li>

                                <li class="nav-parent">
                                    <a >
                                        
                                        <i class="fa fa-refresh" aria-hidden="true"></i>
                                        <span>Funnel</span>
                                    </a>
                                    <ul class="nav nav-children">


                                        <li >
                                            <a href="funneldash.php">
                                                Show 
                                            </a>
                                            
                                        </li>
                                        
                                        <li >
                                            <a href="funnel.php">
                                                 Update
                                            </a>
                                            
                                        </li>

                                    </ul>
                                </li>

                                <li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <span>Order</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Quotation 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="quotdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotation-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 PO
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="podash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="po-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="pogen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Invoice
                                            </a>
                                            <ul class="nav nav-children">
                                               <li>
                                                    <a href="invdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invoice-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>
                                 

                                
                                <li class="nav-parent">
								    <a >
									
									   <i class="fa fa-check" aria-hidden="true"></i>
									   <span>Target Vs Achievement</span>
								    </a>
								    <ul class="nav nav-children">


									   <!-- <li >
										  <a href="funneldash.php">
										  	View 
										  </a>
										
								    	</li> -->
									       <li >
										  <a href="achievement.php">
											 Achievement
										  </a>
										
									       </li>
									
									       <li >
										      <a href="targetset.php">
											     Set Target
										      </a>
										
									       </li>
									

								    </ul>
							     </li>

                                 <li class="nav-parent">
                           <a >
                           <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                           <span>Products</span>
                           </a>
                           <ul class="nav nav-children">
                              <li >
                                 <a href="proddash.php">
                                 Dashboard
                                 </a>
                              </li>
                              <li >
                                 <a href="addprod.php">
                                 Add
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="nav-parent">
                           <a >
                              <!-- <span class="pull-right label label-primary">182</span> -->
                              <i class="fa fa-users" aria-hidden="true"></i>
                              <span>HR</span>
                           </a>
                           <ul class="nav nav-children">
                              <li class="nav-parent" >
                                 <a >
                                 Employee 
                                 </a>
                                 <ul class="nav nav-children">
                                    <li>
                                       <a href="emplist.php">
                                       Employee List
                                       </a>
                                    </li>
                                    <li>
                                       <a href="addemp.php">
                                       Add
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li class="nav-parent">
                                 <a >
                                 Holiday
                                 </a>
                                 <ul class="nav nav-children">
                                    <li>
                                       <a href="holidaylist.php">
                                       Holiday List
                                       </a>
                                    </li>
                                    <li>
                                       <a href="addholiday.php">
                                       Add
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li class="nav-parent">
                                 <a >
                                 Leave
                                 </a>
                                 <ul class="nav nav-children">
                                    <li>
                                       <a href="leavedash.php">
                                       Applications
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li >
                                 <a href="incentive.php">
                                 Incentive
                                 </a>
                                 <!-- <ul class="nav nav-children">
                                    <li>
                                        <a href="leavedash.php">
                                             Applications
                                        </a>
                                    </li>
                                    
                                    </ul> -->
                              </li>
                              <li >
                                 <a href="#">
                                 Pay Roll
                                 </a>
                                 <!-- <ul class="nav nav-children">
                                    <li>
                                        <a href="leavedash.php">
                                             Applications
                                        </a>
                                    </li>
                                    
                                    </ul> -->
                              </li>
                           </ul>
                        </li>
                        <li class="nav-parent">
                           <a >
                           <i class="fa fa-inr" aria-hidden="true"></i>
                           <span>Finance</span>
                           </a>
                           <ul class="nav nav-children">
                              <li >
                                 <a href="#">
                                 Dashboard
                                 </a>
                              </li>
                              <li >
                                 <a href="#">
                                 Add
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="nav-parent nav-active nav-expanded">
                           <a >
                           <i class="fa fa-cogs" aria-hidden="true"></i>
                           <span>Change</span>
                           </a>
                           <ul class="nav nav-children">
                              <li >
                                 <a href="changepros.php">
                                 Prospects
                                 </a>
                              </li>
                              <li class="nav-active" >
                                 <a href="changecamp.php">
                                 Campaign
                                 </a>
                              </li>
                              <li >
                                 <a href="#">
                                 Meeting
                                 </a>
                              </li>
                              <li >
                                 <a href="#">
                                 Funnel
                                 </a>
                              </li>
                              <li >
                                 <a href="#">
                                 Order
                                 </a>
                              </li>
                              <li >
                                 <a href="#">
                                 Target
                                 </a>
                              </li>
                              <li class="nav-parent" >
                                 <a >
                                 HR 
                                 </a>
                                 <ul class="nav nav-children">
                                    <li>
                                       <a href="#">
                                       Employee
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                       Holiday
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                       Leave
                                       </a>
                                    </li>
                                    <li>
                                       <a href="changeinc.php">
                                       Incentive
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                       Payroll
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li >
                                 <a href="#">
                                 Products
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <!-- <li>
                           <a href="#">
                               
                               <i class="fa fa-search" aria-hidden="true"></i>
                               <span>Track</span>
                           </a>
                           </li> -->
                        <li class="nav-parent">
                           <a >
                           <i class="fa fa-home" aria-hidden="true"></i>
                           <span>Company</span>
                           </a>
                           <ul class="nav nav-children">
                              <li >
                                 <a href="compdash.php">
                                 Dashboard
                                 </a>
                              </li>
                              <li >
                                 <a href="addcomp.php">
                                 Add
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="#">
                           <i class="fa fa-book" aria-hidden="true"></i>
                           <span>Logistics</span>
                           </a>
                        </li>
                        <li>
                           <a href="#">
                           <i class="fa fa-book" aria-hidden="true"></i>
                           <span>Service</span>
                           </a>
                        </li>
                        <li>
                           <a href="#">
                           <i class="fa fa-book" aria-hidden="true"></i>
                           <span>Reports</span>
                           </a>
                        </li>
                     </ul>
                  </nav>
                 
               </div>
            </div>
         </aside>
            <!-- end: sidebar -->


				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Change Campaign Layout</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Campaign</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					
				

				
				<div class="row">
					
					<div class="col-lg-12 col-md-12">
						<section class="panel">
							
                            <div class="panel-body" >
                                <div class="table-responsive">
                                    <div class="dropdown">
                                        <a class="dropbtn btn btn-primary pull-right"  >Add</a>
                                    
                                            <div class="dropdown-content">
                                                <a class="modal-with-form" href="#modalForm">Add Campaign</a>
                                                

                                            </div>
                                    </div><br><br><br><br>

                                <div id="modalForm" class="modal-block modal-block-primary mfp-hide">
                                        <section class="panel">
                                            <header class="panel-heading">
                                                <h2 class="panel-title">Add Campaign</h2>
                                            </header>
                                            <div class="panel-body">
                                                <form action="../auth/campaign/icampchange.php" method="post" id="demo-form" class="form-horizontal mb-lg" novalidate="novalidate">
                                                    <div class="form-group mt-lg">
                                                        <label class="col-sm-3 control-label">Campaign Name</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="campaignname" class="form-control" placeholder="Type Campaign Name..." required/>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <label class="col-sm-3 control-label">Email</label>
                                                        <div class="col-sm-9">
                                                            <input type="email" name="email" class="form-control" placeholder="Type your email..." required/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">URL</label>
                                                        <div class="col-sm-9">
                                                            <input type="url" name="url" class="form-control" placeholder="Type an URL..." />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Comment</label>
                                                        <div class="col-sm-9">
                                                            <textarea rows="5" class="form-control" placeholder="Type your comment..." required></textarea>
                                                        </div>
                                                    </div> -->
                                                
                                            </div>
                                            <footer class="panel-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-right">
                                                        <!-- <button class="btn btn-primary modal-confirm" type="submit" name="submit">Submit</button> -->
                                                        <input type="submit" name="campaign" value="Submit" class="btn btn-primary ">
                                                        <button class="btn btn-default modal-dismiss">Cancel</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </footer>
                                        </section>
                                    </div>

                                </div>
                            </div><br>
                            <header class="panel-heading panel-heading-transparent">
                                <!-- <div class="panel-actions">
                                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                </div> -->

                                <h2 class="panel-title">Campaign List</h2>
                            </header>
							<div class="panel-body">
								<div class="table-responsive">
									<?php
										$fetdetail=mysqli_query($dbc,"select * from `campaignlist` ");



										echo '<table class="table table-striped mb-none">';
											echo '<thead>';
												echo '<tr>';
													echo '<th>Sl No.</th>';
                                                    echo '<th>Campaign Name</th>';
                                                    
                                                    echo '<th>Delete</th>';
													
													
												echo '</tr>';
											echo '</thead>';
											echo '<tbody>';
                                            $cp=0;
												while($frow=mysqli_fetch_assoc($fetdetail))
												{
                                                    global $cp;
                                                    $cp=$cp+1;
                                                    $campaignno=$frow['slno'];
													$sectorname=$frow['CampaignName'];
                                                    $sectorid=$frow['CampaignId'];
                                                   

                                                       
														echo '<tr>';
														echo "<td>".$cp."</td>";
                                                        echo "<td>{$frow['CampaignName']}</td>";
														
                                                        
                                                        echo "<td><a  href='../auth/campaign/del.php?u=$campaignno' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";


														
														
												}
											echo '</tbody>';
										echo '</table>';
									?>
                                    <!-- <a id="showcustomer" class="text-muted text-uppercase btn btn-primary"  ><font color="white">view</font></a> -->
                                                   
									
								</div>
							</div>
						</section>
					</div>
                    
				</div>
                    <!-- <div class="row">
						<div class="col-md-12">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
										<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
									</div>

									<h2 class="panel-title">Best Seller</h2>
									<p class="panel-subtitle">Customize the graphs as much as you want, there are so many options and features to display information using Porto Admin Template.</p>
								</header>
								<div class="panel-body">

									Flot: Basic
									<div class="chart chart-md" id="flotDashBasic"></div>
									<script>

										var flotDashBasicData = [{
											data: [
												[0, 170],
												[1, 169],
												[2, 173],
												[3, 188],
												[4, 147],
												[5, 113],
												[6, 128],
												[7, 169],
												[8, 173],
												[9, 128],
												[10, 128]
											],
											label: "Series 1",
											color: "#0088cc"
										}, {
											data: [
												[0, 115],
												[1, 124],
												[2, 114],
												[3, 121],
												[4, 115],
												[5, 83],
												[6, 102],
												[7, 148],
												[8, 147],
												[9, 103],
												[10, 113]
											],
											label: "Series 2",
											color: "#2baab1"
										}, {
											data: [
												[0, 70],
												[1, 69],
												[2, 73],
												[3, 88],
												[4, 47],
												[5, 13],
												[6, 28],
												[7, 69],
												[8, 73],
												[9, 28],
												[10, 28]
											],
											label: "Series 3",
											color: "#734ba9"
										}];

										

									</script>

								</div>
							</section>
						</div>
						
					</div> -->
                    
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
                <div class="nano">
                    <div class="nano-content">
                        <a href="#" class="mobile-close visible-xs">
                            Collapse <i class="fa fa-chevron-right"></i>
                        </a>
            
                        <div class="sidebar-right-wrapper">
            
                            <div class="sidebar-widget widget-calendar">
                                <h6>Upcoming Tasks</h6>
                                <div data-plugin-datepicker data-plugin-skin="dark" ></div>
            
                                <ul>
                                    <li>
                                        <!-- <time datetime="2014-04-19T00:00+00:00">04/19/2014</time> -->
                                        <span>Company</span>
                                    </li>
                                </ul>
                            </div>
            
                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Meeting</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($followupdate == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Contact Person: {$frow['ContactPerson']}</span>
                                            <span class=title>Mobile: <a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></span>
                                            <span class=title>Meeting Time: $cdate</span>
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                    
                                </ul>
                            </div>




                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Funnel Follow Up</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($dateofclosure == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Product: {$frow['Products']}</span>
                                            
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                   
                                </ul>
                            </div>

            
                        </div>
                    </div>
                </div>
            </aside>
		</section>

		<!-- Vendor -->
		<script src="../../../log/assets/vendor/jquery/jquery.js"></script>
		<script src="../../../log/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../../../log/assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../../../log/assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../../../log/assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../../../log/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="../../../log/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="../../../log/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="../../../log/assets/vendor/jquery-appear/jquery.appear.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="../../../log/assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.js"></script>
		<script src="../../../log/assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.pie.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.categories.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.resize.js"></script>
		<script src="../../../log/assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
		<script src="../../../log/assets/vendor/raphael/raphael.js"></script>
		<script src="../../../log/assets/vendor/morris/morris.js"></script>
		<script src="../../../log/assets/vendor/gauge/gauge.js"></script>
		<script src="../../../log/assets/vendor/snap-svg/snap.svg.js"></script>
		<script src="../../../log/assets/vendor/liquid-meter/liquid.meter.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/jquery.vmap.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../../../log/assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../../../log/assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../../../log/assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="../../../log/assets/javascripts/dashboard/examples.dashboard.js"></script>

        <!-- modal form -->

        <script src="../../../log/assets/javascripts/ui-elements/examples.modals.js"></script>
	</body>
</html>