<?php
include "session_handler.php";
?>


<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Insert | Navikra CRM</title>
		<meta name="keywords" content="Navikra CRM" />
		<meta name="description" content="Navikra CRM">
		<meta name="author" content="navikra.com">

		<link rel="apple-touch-icon" sizes="180x180" href="../assets/images/apple-touch-icon.png">
    	<link rel="icon" type="image/png" href="../assets/images/favicon-32x32.png" sizes="32x32">
    	<link rel="icon" type="image/png" href="../assets/images/favicon-16x16.png" sizes="16x16">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
		
				<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
				<link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
				<link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />
		
				<!-- Specific Page Vendor CSS -->
				<link rel="stylesheet" href="../assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
				<link rel="stylesheet" href="../assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
				<link rel="stylesheet" href="../assets/vendor/morris/morris.css" />

				
		
				<!-- Theme CSS -->
				<link rel="stylesheet" href="../assets/stylesheets/theme.css" />
		
				<!-- Skin CSS -->
				<link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />
		
				<!-- Theme Custom CSS -->
				<link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">
		
				<!-- Head Libs -->
				<script src="../assets/vendor/modernizr/modernizr.js"></script>

    <!-- fa-product icon -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet" />

		<!-- search -->
		<!-- <script type="text/javascript" src="assets/ajax/search.js" ></script> -->
		
		<script type="text/javascript" src="../assets/ajax/jquery-1.8.0.min.js"></script>
		<script type="text/javascript">
$(function(){
$(".search").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
if(searchid!='')
{
    $.ajax({
    type: "POST",
    url: "../auth/search.php",
    data: dataString,
    cache: false,
    success: function(html)
    {
    $("#result").html(html).show();
    }
    });
}return false;    
});

jQuery("#result").live("click",function(e){ 
    var $clicked = $(e.target);
    var $name = $clicked.find('.name').html();
    var decoded = $("<div/>").html($name).text();
    $('#searchid').val(decoded);
});
jQuery(document).live("click", function(e) { 
    var $clicked = $(e.target);
    if (! $clicked.hasClass("search")){
    jQuery("#result").fadeOut(); 
    }
});
$('#searchid').click(function(){
    jQuery("#result").fadeIn();
});
});
</script>
		

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="dashboard.php" class="logo">
						<img src="../assets/images/logo.png" height="48" alt="Navikra CRM" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
			
				<?php include "notification.php"; ?>
                    
                    <?php
                        $ad=mysqli_query($dbc,"select * from team where email='$id'");
                        while($r=mysqli_fetch_assoc($ad))
                        {
                            $name=$r['name'];
                            $role=$r['desig'];
                        }
                    ?>
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="../assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="<?php echo $id; ?>">
								<span class="name"><?php echo $name; $_SESSION['n']=$name; ?></span>
								<span class="role"><?php echo $role; ?></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="profile.php"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<!-- <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a> -->
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="../../index.php"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li >
										<a href="dashboard.php">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<!-- <li>
										<a href="mailbox-folder.html">
											<span class="pull-right label label-primary">182</span>
											<i class="fa fa-envelope" aria-hidden="true"></i>
											<span>Mailbox</span>
										</a>
                                    </li> -->
                                    <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-users" aria-hidden="true"></i>
                                       <span>Prospects</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="custdash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addcust.php">
                                                 Add Prospect
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="customerprofile.php">
                                                 Add Prospect Profile
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="campcust.php">
                                                 Campaign Prospects
                                                </a>
                                        
                                           </li>
                                    
                                          <!--  <li >
                                              <a href="targetset.php">
                                                 Set Target
                                              </a>
                                        
                                           </li> -->
                                    

                                    </ul>
                                 </li>
                                <li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span>Campaign</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        <li >
                                            
                                            <a href="create-campaign.php">
                                                Create 
                                            </a>
                                            
                                        </li>


                                        <li class="nav-parent" >
                                            <a >
                                                Direct 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="dirdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="direct.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Digital
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="digdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="digital.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Telecalling
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="teldash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="telecalling.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-parent">
                                            <a >
                                                 Reference
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="refdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="reference.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        
                                    </ul>

                                </li>

                                <li >
                           <a href="lead.php">
                              <!-- <span class="pull-right label label-primary">182</span> -->
                              <i class="fa fa-user" aria-hidden="true"></i>
                              <span>Lead</span>
                           </a>
                           
                        </li>


                                <li class="nav-parent">
                                    <a>
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <span>Meeting</span>
                                    </a>

                                    <ul class="nav nav-children">


                                        <li >
                                            <a href="meetingdash.php">
                                                Dashboard 
                                            </a>
                                            
                                        </li>
                                        
                                        <!-- <li >
                                            <a href="meeting.php">
                                                 Update
                                            </a>
                                            
                                        </li> -->

                                    </ul>

                                </li>

									<li class="nav-parent">
										<a >
										
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Funnel</span>
										</a>
										<ul class="nav nav-children">


											<li >
												<a href="funneldash.php">
													Show 
												</a>
											
											</li>
										
											<li >
												<a href="funnel.php">
													 Update
												</a>
											
											</li>

										</ul>
									</li>

									<li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <span>Order</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Quotation 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="quotdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotation-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 PO
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="podash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="po-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="pogen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Invoice
                                            </a>
                                            <ul class="nav nav-children">
                                               <li>
                                                    <a href="invdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invoice-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>
                                 

									
                                    <li class="nav-parent">
									<a >
										
										<i class="fa fa-check" aria-hidden="true"></i>
										<span>Target Vs Achievement</span>
									</a>
									<ul class="nav nav-children">
	
	
										<!-- <li >
											<a href="funneldash.php">
												View 
											</a>
											
										</li> -->
										<li >
											<a href="achievement.php">
												 Achievement
											</a>
											
										</li>
										
										<li >
											<a href="targetset.php">
												Set Target
											</a>
											
										</li>
										
	
									</ul>
								</li>
								<li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                        <span>HR</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Employee 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="emplist.php">
                                                         Employee List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addemp.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Holiday
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="holidaylist.php">
                                                         Holiday List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addholiday.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Leave
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="leavedash.php">
                                                         Applications
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>
                                 <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                       <span>Products</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="proddash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addprod.php">
                                                 Add
                                                </a>
                                        
                                           </li>
                                    
                                          

                                    </ul>
                                 </li>

                                 <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-cogs" aria-hidden="true"></i>
                                       <span>Change</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="changepros.php">
                                                 Prospects
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="changecamp.php">
                                                 Campaign
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Meeting
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Funnel
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Order
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Target
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 HR
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Products
                                                </a>
                                        
                                           </li>

                                    
                                          

                                    </ul>
                                 </li>

                                 
									<li>
										<a href="#">
											
											<i class="fa fa-book" aria-hidden="true"></i>
											<span>Reports</span>
										</a>
                                    </li>

									
									
								</ul>
							</nav>
				
							
                           
							
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Update Customer Details</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<!-- <li><span>Forms</span></li> -->
								<li><span>Update</span></li>
							</ol>
					<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
							<!-- <a class="sidebar-right-toggle" ><i class="fa fa-chevron-down"></i></a> -->
						</div>
					</header>

					<!-- start: page -->

					<!-- start: page -->
											
					<div class="row">
						<div class="col-xs-12">
							<section class="panel form-wizard" id="w4">
								<!-- <header class="panel-heading">
										<h2 class="panel-title">Upload Customers Detail</h2>
								</header>
								<div class="panel-body">
									<form action="../auth/customer/ucust.php" method="post" class="form-horizontal" novalidate="novalidate" enctype="multipart/form-data"  >


													<div class="form-group">
										
													</div>
									
													<div class="form-group">
														<label class="col-sm-2 control-label" for="w4-username">Upload</label>
														<div class="col-sm-9">
															<input type="file" class="form-control" name="file" id="w4-username" >
														</div>
													</div>
													
													<div class="form-group">
														<div class="col-sm-2"></div>
														<div class="col-sm-9">
															<div class="checkbox-custom">
																<input type="submit" name="upload_submit" value="Submit" class="btn btn-success pull-right" />
																
															</div>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-9"></div>
														<div class="col-sm-9">
															<div class="checkbox-custom">
															<h5><font color="red">*</font> Upload Maximum 100 Row</h5>
																
															</div>
														</div>
													</div>
												
											</form>
											</div>
								 -->
								
						

						
					<!-- end: page -->
				


					<!-- j -->
											
					
								<header class="panel-heading">
										<h2 class="panel-title">Update Customer Details</h2>
									</header>
								<div class="panel-body">
									<div class="wizard-progress wizard-progress-lg">
										<div class="steps-progress">
											<div class="progress-indicator"></div>
										</div>
										<ul class="wizard-steps">
											<li class="active">
												<a href="#w4-account" data-toggle="tab"><span>1</span>Company Info</a>
											</li>
											<!-- <li>
												<a href="#w4-profile" data-toggle="tab"><span>2</span>Contact Info</a>
											</li>
											<li>
												<a href="#w4-billing" data-toggle="tab"><span>3</span>Billing Info</a>
											</li> -->
											<li>
												<a href="#w4-confirm" data-toggle="tab"><span>2</span>Contact Info</a>
											</li>
										</ul>
									</div>

									<?php
										$cid=$_GET['u'];
										$fet=mysqli_query($dbc,"select * from `customers` where `id`='$cid'");
										while($rw=mysqli_fetch_assoc($fet))
										{
											$company=$rw['Company'];
											$sector=$rw['Sector'];
											$subsector=$rw['SubSector'];
											$firstname=$rw['FirstName'];
											$lastname=$rw['LastName'];
											$level=$rw['Level'];
											$department=$rw['Dept'];
											$designation=$rw['Designation'];
											$mobile=$rw['Mobile'];
											$mail=$rw['Mail'];
											$contactpersont=$rw['ContactPerson2'];
											$contactnumber=$rw['ContactNumber'];
											$emailid=$rw['EmailID'];
											$url=$rw['Url'];
											$address=$rw['Address'];
											$location=$rw['Location'];
											$sublocation=$rw['SubLocation'];
											$city=$rw['City'];
											$pin=$rw['Pin'];
											$state=$rw['State'];
											$stdcode=$rw['StdCode'];
											$landlineno=$rw['LandlineNo'];
											$faxno=$rw['FaxNo'];
											$noofemployees=$rw['NoOfEmployees'];
											$companytype=$rw['CompanyType'];
											$gstno=$rw['GSTNo'];
	
										}

									?>
					
									<form action="../auth/customer/editcust.php" method="post" class="form-horizontal" novalidate="novalidate" enctype="multipart/form-data" >
										<div class="tab-content">
											<div id="w4-account" class="tab-pane active">

											

												
											<input type="hidden" name="cid" value="<?php echo $cid; ?>">
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Company</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" value="<?php echo $company; ?>" name="company" id="w4-username" required>
													</div>
												<!-- </div>

												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-password">Sector</label>
													<div class="col-sm-4">
														
														<select class="form-control" name="sector"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6" required >
                                                                <?php

                                                                $comp=$_GET['c'];
                                                                    $project=mysqli_query($dbc,"select distinct(SectorName) from `sector`  ");
                                                                    echo '<option  value="">Select</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                        //$pro=$row['ProjectName'];


                                                        
                                                                         echo '<option  value="'.$row['SectorName'].'">'.$row['SectorName'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                                            </select>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-password">Url</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="url" value="<?php echo $url; ?>" id="w4-password" >
													</div>
												<!-- </div>

												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">Sub Sector</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="subsector" value="<?php echo $subsector; ?>" id="w4-username" >
													</div>
												</div>
												

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Address</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="address" value="<?php echo $address; ?>" id="w4-username" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-password">Location</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="location" value="<?php echo $location; ?>" id="w4-password" >
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-password">City</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="city" value="<?php echo $city; ?>" id="w4-password" >
													</div>
												<!-- </div>

												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">Sub Location</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="sublocation" value="<?php echo $sublocation; ?>" id="w4-username" >
													</div>
												</div>
												

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Pin</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="pin" value="<?php echo $pin; ?>" id="w4-pin" maxlength="6" minlength="6" onKeyUp="pins()" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-password">State</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="state" value="<?php echo $state; ?>" id="w4-password" >
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Std Code</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="stdcode" value="<?php echo $stdcode; ?>" id="w4-std" onKeyUp="stdc()" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-password">Landline No</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="landlineno" value="<?php echo $landlineno; ?>" id="w4-land" onKeyUp="landl()" >
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Fax No</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="faxno" value="<?php echo $faxno; ?>" id="w4-username" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">No of Employees</label>
													<div class="col-sm-4">
														<select class="form-control" name="noofemployees" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															<option selected value="<?php echo $noofemployees; ?>"><?php echo $noofemployees; ?></option>
															<option value="0-50">0-50</option>
															<option value="51-250">51-250</option>
															<option value="251-500">251-500</option>
															<option value="501-1000">501-1000</option>
															<option value="Above 1000">Above 1000</option>
														
													</select>
													</div>
												</div>

												<div class="form-group"> 
													<label class="col-sm-2 control-label" for="w4-username">Company Type</label>
													<div class="col-sm-4">
														<select class="form-control" name="companytype" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option selected value="<?php echo $companytype; ?>"><?php echo $companytype; ?></option>
															<option value="SMB">SMB</option>
															<option value="Enterprise">Enterprise</option>
															<option value="Large">Large</option>
															
														
													</select>
													</div>

													<label class="col-sm-1 control-label" for="w4-username">GST No</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="gstno" value="<?php echo $gstno; ?>" id="w4-password" >
													</div>
												</div>
												



											</div>
											
											<div id="w4-confirm" class="tab-pane">
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">First Name</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="firstname" value="<?php echo $firstname; ?>" id="w4-f" onKeyUp="fname()" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-email">Last Name</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="lastname" value="<?php echo $lastname; ?>" id="w4-l" onKeyUp="lname()" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Level</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="level" value="<?php echo $level; ?>" id="w4-email" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-email">Department</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="department" value="<?php echo $department; ?>" id="w4-email" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Designation</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="designation" value="<?php echo $designation; ?>"id="w4-email" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-email">Mobile</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="mobile" value="<?php echo $mobile; ?>" id="w4-mob" onKeyUp="mob()" maxlength="12">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Email</label>
													<div class="col-sm-4">
														<input type="email" class="form-control" name="email" value="<?php echo $mail; ?>" id="w4-email" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-email">Contact Person 2</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="contactpersont" value="<?php echo $contactpersont; ?>" id="w4-cp" onKeyUp="cpt()" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Contact Number</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="contactnumber" value="<?php echo $contactnumber; ?>" id="w4-cn" onKeyUp="cn()" maxlength="10" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-email">Email ID</label>
													<div class="col-sm-4">
														<input type="email" class="form-control" name="emailid" value="<?php echo $emailid; ?>" id="w4-email" >
													</div>
												</div>
												<!-- <div class="form-group">
													<div class="col-sm-2"></div>
													<div class="col-sm-9">
														<div class="checkbox-custom">
															<input type="checkbox" name="terms" id="w4-terms" required>
															<label for="w4-terms">I agree to the terms of service</label>
															
														</div>
													</div>
												</div> -->
												<div class="form-group">
													<div class="col-sm-2"></div>
													<div class="col-sm-9">
														<div class="checkbox-custom">
														<input type="submit" name="submit" value="Submit" class="btn btn-success pull-right" />
															
														</div>
													</div>
												</div>
											</div>
										</div>
										
									
								</div>
								<div class="panel-footer">
									<ul class="pager">
										<li class="previous disabled">
											<a><i class="fa fa-angle-left"></i> Previous</a>
										</li>
										<li class="finish hidden pull-right">
											<!-- <a>Finish</a> -->
											<!-- <input type="submit" name="submit" value="Submit" class="btn btn-success" /> -->
											</form>
										</li>
										<li class="next">
											<a>Next <i class="fa fa-angle-right"></i></a>
										</li>
									</ul>
									
								</div>
							</section>
						</div>
					</div>

						
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
                <div class="nano">
                    <div class="nano-content">
                        <a href="#" class="mobile-close visible-xs">
                            Collapse <i class="fa fa-chevron-right"></i>
                        </a>
            
                        <div class="sidebar-right-wrapper">
            
                            <div class="sidebar-widget widget-calendar">
                                <h6>Upcoming Tasks</h6>
                                <div data-plugin-datepicker data-plugin-skin="dark" ></div>
            
                                <ul>
                                    <li>
                                        <!-- <time datetime="2014-04-19T00:00+00:00">04/19/2014</time> -->
                                        <span>Company</span>
                                    </li>
                                </ul>
                            </div>
            
                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Meeting</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($followupdate == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Contact Person: {$frow['ContactPerson']}</span>
                                            <span class=title>Mobile: <a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></span>
                                            <span class=title>Meeting Time: $cdate</span>
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                    
                                </ul>
                            </div>




                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Funnel Follow Up</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND YEAR(ModificationDetail) = YEAR(CURRENT_DATE()) ");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($dateofclosure == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Product: {$frow['Products']}</span>
                                            
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                   
                                </ul>
                            </div>

            
                        </div>
                    </div>
                </div>
            </aside>
		</section>

		<!-- Vendor -->
		<script src="../assets/vendor/jquery/jquery.js"></script>
		<script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="../assets/vendor/jquery-validation/jquery.validate.js"></script>
		<script src="../assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
		<script src="../assets/vendor/pnotify/pnotify.custom.js"></script>

		<script src="../assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="../assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="../assets/vendor/select2/select2.js"></script>
		<script src="../assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		
		
		
	
		
		
		
		
		<script src="../assets/vendor/ios7-switch/ios7-switch.js"></script>
		
		
		<!-- Theme Base, Components and Settings -->
		<script src="../assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="../assets/javascripts/forms/examples.wizard.js"></script>
		<script src="../assets/javascripts/forms/examples.advanced.form.js"></script>
        <!-- modal form -->
      <script src="../assets/javascripts/ui-elements/examples.modals.js"></script>
		<script>
		function pins()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-pin")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-pin")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function stdc()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-std")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-std")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function mob()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-mob")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-mob")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function cn()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-cn")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-cn")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function landl()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-land")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-land")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function fname()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-f")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-f")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}

		function lname()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-l")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-l")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}

		function cpt()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-cp")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-cp")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}
		</script>
	</body>
</html>