<?php
include "session_handler.php";
?>

<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Dashboard | Navikra CRM</title>
		<meta name="keywords" content="Navikra CRM" />
		<meta name="description" content="Navikra CRM">
		<meta name="author" content="navikra.com">

		<link rel="apple-touch-icon" sizes="180x180" href="../../../log/assets/images/apple-touch-icon.png">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-32x32.png" sizes="32x32">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-16x16.png" sizes="16x16">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="../../../log/assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="../../../log/assets/vendor/modernizr/modernizr.js"></script>

    <!-- fa-product icon -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet" />

    <!-- Hide and Show -->
        <script src="../../../log/assets/jquery/jquery.min.js"></script>


        <!-- Read more and less css and JS -->
<style type="text/css">
    .morecontent span {
    display: none;
    text-decoration: none;
    background-color: white;
}
.morelink {
    display: block;
    text-decoration: none;
}
</style>
<script type="text/javascript">
    $(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 20;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Read More...";
    var lesstext = "Read Less...";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>
<!-- Read more and less css and JS end -->

<!--on hover select link start -->
<style type="text/css">
    .dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {color: #660064}

.dropdown:hover .dropdown-content {
    display: block;
}
.dropdown:hover .dropbtn {
    color: #660064;
}

</style>
<!--on hover select link end -->

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
            <div class="logo-container">
                <a href="dashboard.php" class="logo">
                    <img src="../../../log/assets/images/logo.png" height="48" alt="Navikra CRM" />
                </a>
                <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>
        
            <!-- start: search & user box -->
            <div class="header-right">
        
            <?php include "notification.php"; ?>
                
                <?php
                    $ad=mysqli_query($dbc,"select * from team where email='$id'");
                    while($r=mysqli_fetch_assoc($ad))
                    {
                        $name=$r['name'];
                        $role=$r['desig'];
                    }
                ?>
        
                <span class="separator"></span>
        
                <div id="userbox" class="userbox">
                    <a href="#" data-toggle="dropdown">
                        <figure class="profile-picture">
                            <img src="../../../log/assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
                        </figure>
                        <div class="profile-info" data-lock-name="John Doe" data-lock-email="<?php echo $id; ?>">
                            <span class="name"><?php echo $name; ?></span>
                            <span class="role"><?php echo $role; ?></span>
                        </div>
        
                        <i class="fa custom-caret"></i>
                    </a>
        
                    <div class="dropdown-menu">
                        <ul class="list-unstyled">
                            <li class="divider"></li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="profile.php"><i class="fa fa-user"></i> My Profile</a>
                            </li>
                            <li>
                                <!-- <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a> -->
                            </li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="../../index.php"><i class="fa fa-power-off"></i> Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end: search & user box -->
        </header>
        <!-- end: header -->

        

        <div class="inner-wrapper">
            <!-- start: sidebar -->
            <aside id="sidebar-left" class="sidebar-left">
            
                <div class="sidebar-header">
                    <div class="sidebar-title">
                        Navigation
                    </div>
                    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
            
                <div class="nano">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li >
                                    <a href="dashboard.php">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>
                                <!-- <li>
                                    <a href="mailbox-folder.html">
                                        <span class="pull-right label label-primary">182</span>
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span>Mailbox</span>
                                    </a>
                                </li> -->
                                <li class="nav-parent">
                           <a >
                           <i class="fa fa-users" aria-hidden="true"></i>
                           <span>Prospects</span>
                           </a>
                           <ul class="nav nav-children">
                              <li >
                                 <a href="custdash.php">
                                 Dashboard
                                 </a>
                              </li>
                              <li >
                                 <a href="addcust.php">
                                 Add Prospect
                                 </a>
                              </li>
                              <li >
                                 <a href="customerprofile.php">
                                 Add Prospect Profile
                                 </a>
                              </li>
                              <li >
                                 <a href="campcust.php">
                                 Campaign Prospects
                                 </a>
                              </li>
                              <!--  <li >
                                 <a href="targetset.php">
                                    Set Target
                                 </a>
                                 
                                 </li> -->
                           </ul>
                        </li>
                        <li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span>Campaign</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        <li >
                                            
                                            <a href="create-campaign.php">
                                                Create 
                                            </a>
                                            
                                        </li>


                                        <li class="nav-parent" >
                                            <a >
                                                Direct 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="dirdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="direct.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Digital
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="digdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="digital.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Telecalling
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="teldash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="telecalling.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-parent">
                                            <a >
                                                 Reference
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="refdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="reference.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        
                                    </ul>

                                </li>

                                <li >
                           <a href="lead.php">
                              <!-- <span class="pull-right label label-primary">182</span> -->
                              <i class="fa fa-user" aria-hidden="true"></i>
                              <span>Lead</span>
                           </a>
                           
                        </li>
                        <li class="nav-parent">
                           <a>
                              <!-- <span class="pull-right label label-primary">182</span> -->
                              <i class="fa fa-user" aria-hidden="true"></i>
                              <span>Meeting</span>
                           </a>
                           <ul class="nav nav-children">
                              <li >
                                 <a href="meetingdash.php">
                                 Dashboard 
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="nav-parent">
                           <a >
                           <i class="fa fa-refresh" aria-hidden="true"></i>
                           <span>Funnel</span>
                           </a>
                           <ul class="nav nav-children">
                              <li >
                                 <a href="funneldash.php">
                                 Show 
                                 </a>
                              </li>
                              <li >
                                 <a href="funnel.php">
                                 Update
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="nav-parent">
                           <a >
                              <!-- <span class="pull-right label label-primary">182</span> -->
                              <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                              <span>Order</span>
                           </a>
                           <ul class="nav nav-children">
                              <li class="nav-parent" >
                                 <a >
                                 Quotation 
                                 </a>
                                 <ul class="nav nav-children">
                                    <li>
                                       <a href="quotdash.php">
                                       Dashboard
                                       </a>
                                    </li>
                                    <li>
                                       <a href="quotation-upload.php">
                                       Upload
                                       </a>
                                    </li>
                                    <li>
                                       <a href="quotgen.php">
                                       Generate
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li class="nav-parent">
                                 <a >
                                 PO
                                 </a>
                                 <ul class="nav nav-children">
                                    <li>
                                       <a href="podash.php">
                                       Dashboard
                                       </a>
                                    </li>
                                    <li>
                                       <a href="po-upload.php">
                                       Upload
                                       </a>
                                    </li>
                                    <li>
                                       <a href="pogen.php">
                                       Generate
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li class="nav-parent">
                                 <a >
                                 Invoice
                                 </a>
                                 <ul class="nav nav-children">
                                    <li>
                                       <a href="invdash.php">
                                       Dashboard
                                       </a>
                                    </li>
                                    <li>
                                       <a href="invoice-upload.php">
                                       Upload
                                       </a>
                                    </li>
                                    <li>
                                       <a href="invgen.php">
                                       Generate
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                           </ul>
                        </li>
                        <li class="nav-parent">
                           <a >
                           <i class="fa fa-check" aria-hidden="true"></i>
                           <span>Target Vs Achievement</span>
                           </a>
                           <ul class="nav nav-children">
                              <!-- <li >
                                 <a href="funneldash.php">
                                   View 
                                 </a>
                                 
                                 </li> -->
                              <li >
                                 <a href="achievement.php">
                                 Achievement
                                 </a>
                              </li>
                              <li >
                                 <a href="targetset.php">
                                 Set Target
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="nav-parent">
                           <a >
                           <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                           <span>Products</span>
                           </a>
                           <ul class="nav nav-children">
                              <li >
                                 <a href="proddash.php">
                                 Dashboard
                                 </a>
                              </li>
                              <li >
                                 <a href="addprod.php">
                                 Add
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="nav-parent">
                           <a >
                              <!-- <span class="pull-right label label-primary">182</span> -->
                              <i class="fa fa-users" aria-hidden="true"></i>
                              <span>HR</span>
                           </a>
                           <ul class="nav nav-children">
                              <li class="nav-parent" >
                                 <a >
                                 Employee 
                                 </a>
                                 <ul class="nav nav-children">
                                    <li>
                                       <a href="emplist.php">
                                       Employee List
                                       </a>
                                    </li>
                                    <li>
                                       <a href="addemp.php">
                                       Add
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li class="nav-parent">
                                 <a >
                                 Holiday
                                 </a>
                                 <ul class="nav nav-children">
                                    <li>
                                       <a href="holidaylist.php">
                                       Holiday List
                                       </a>
                                    </li>
                                    <li>
                                       <a href="addholiday.php">
                                       Add
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li class="nav-parent">
                                 <a >
                                 Leave
                                 </a>
                                 <ul class="nav nav-children">
                                    <li>
                                       <a href="leavedash.php">
                                       Applications
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li >
                                 <a href="incentive.php">
                                 Incentive
                                 </a>
                                 <!-- <ul class="nav nav-children">
                                    <li>
                                        <a href="leavedash.php">
                                             Applications
                                        </a>
                                    </li>
                                    
                                    </ul> -->
                              </li>
                              <li >
                                 <a href="#">
                                 Pay Roll
                                 </a>
                                 <!-- <ul class="nav nav-children">
                                    <li>
                                        <a href="leavedash.php">
                                             Applications
                                        </a>
                                    </li>
                                    
                                    </ul> -->
                              </li>
                           </ul>
                        </li>
                        <li class="nav-parent">
                           <a >
                           <i class="fa fa-inr" aria-hidden="true"></i>
                           <span>Finance</span>
                           </a>
                           <ul class="nav nav-children">
                              <li >
                                 <a href="#">
                                 Dashboard
                                 </a>
                              </li>
                              <li >
                                 <a href="#">
                                 Add
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="nav-parent">
                           <a >
                           <i class="fa fa-cogs" aria-hidden="true"></i>
                           <span>Change</span>
                           </a>
                           <ul class="nav nav-children">
                              <li >
                                 <a href="changepros.php">
                                 Prospects
                                 </a>
                              </li>
                              <li >
                                 <a href="changecamp.php">
                                 Campaign
                                 </a>
                              </li>
                              <li >
                                 <a href="#">
                                 Meeting
                                 </a>
                              </li>
                              <li >
                                 <a href="#">
                                 Funnel
                                 </a>
                              </li>
                              <li >
                                 <a href="#">
                                 Order
                                 </a>
                              </li>
                              <li >
                                 <a href="#">
                                 Target
                                 </a>
                              </li>
                              <li class="nav-parent" >
                                 <a >
                                 HR 
                                 </a>
                                 <ul class="nav nav-children">
                                    <li>
                                       <a href="#">
                                       Employee
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                       Holiday
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                       Leave
                                       </a>
                                    </li>
                                    <li>
                                       <a href="changeinc.php">
                                       Incentive
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                       Payroll
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li >
                                 <a href="#">
                                 Products
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <!-- <li>
                           <a href="#">
                               
                               <i class="fa fa-search" aria-hidden="true"></i>
                               <span>Track</span>
                           </a>
                           </li> -->
                        <li class="nav-parent">
                           <a >
                           <i class="fa fa-home" aria-hidden="true"></i>
                           <span>Company</span>
                           </a>
                           <ul class="nav nav-children">
                              <li >
                                 <a href="compdash.php">
                                 Dashboard
                                 </a>
                              </li>
                              <li >
                                 <a href="addcomp.php">
                                 Add
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="#">
                           <i class="fa fa-book" aria-hidden="true"></i>
                           <span>Logistics</span>
                           </a>
                        </li>
                        <li>
                           <a href="#">
                           <i class="fa fa-book" aria-hidden="true"></i>
                           <span>Service</span>
                           </a>
                        </li>
                        <li>
                           <a href="#">
                           <i class="fa fa-book" aria-hidden="true"></i>
                           <span>Reports</span>
                           </a>
                        </li>
                     </ul>
                  </nav>
                 
               </div>
            </div>
         </aside>
            <!-- end: sidebar -->


				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Campaign Dashboard</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Dashboard</span></li>
								<li><span>Campaign</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-6 col-lg-12 col-xl-12">
							<section class="panel">
								<div class="panel-body">
                                	<div class="row">
										<form action="" method="get" class="form-horizontal" novalidate="novalidate"  >
                                            
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label" style="align:left" for="w4-username"><b>Department:</b></label>
                                                        <div class="col-sm-3" style="align:left">


                                                        <select size="1" id="Rank" class="form-control" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' title="" name="Rank">
                                                            <option value="">Select Department</option>
                                                            <option value="sales">Sales</option>
                                                            <option value="technical">Technical</option>
                                                        </select>


                                                    </div>
                                                    <?php  

                                                        $pname=mysqli_query($dbc,"select `name` from `team` where email='$id' ");
                                                        while ($pfet=mysqli_fetch_assoc($pname)) {
                                                            $pfname=$pfet['name'];
                                                        }

                                                     ?>

                                                    <label class="col-sm-6 control-label" style="align:left" for="w4-username"><b> Profile: <?php  echo $pfname; ?></b></label>




                                            <div class="container">
                                                <div class="sales">

                                                       <label class="col-sm-3 control-label" style="align:left" for="w4-username"><b>Employees:</b></label>
                                                        <div class="col-sm-3" style="align:left">
                                                            
                                                            <select class="form-control" name="project"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6" onchange="redirect(this.value);" required >
                                                                <?php
                                                                    $project=mysqli_query($dbc,"select * from `team` where `Department`='Sales'");
                                                                    echo '<option >Select</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                        //$pro=$row['ProjectName'];
                                                        
                                                                         echo '<option  value="'.$row['email'].'">'.$row['name'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                <!-- </div>
                                                <div class="container"> -->
                                                <div class="technical">

                                                       <label class="col-sm-2 control-label" style="align:left" for="w4-username"><b>Employees:</b></label>
                                                        <div class="col-sm-3" style="align:left">
                                                            
                                                            <select class="form-control" name="project"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6" onchange="redirect(this.value);" required >
                                                                <?php
                                                                    $project=mysqli_query($dbc,"select * from `team` where `Department`='Technical'");
                                                                    echo '<option >Select</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                        //$pro=$row['ProjectName'];
                                                        
                                                                         echo '<option  value="'.$row['email'].'">'.$row['name'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </form>
                                                <script type="text/javascript">
                                                    function redirect(x)
                                                    {
                                                        var opted=x;
                                                        location.replace("acampdash.php?u="+opted);

                                                    }

                                                </script>




                                                <script type="text/javascript">
    
                                                 $(document).ready(function() {
                                                $('#Rank').bind('change', function() {
                                                var elements = $('div.container').children().hide(); // hide all the elements
                                                /*var elements = $('div.container').children().hide();*/
                                                var value = $(this).val();

                                                if (value.length) { // if somethings' selected
                                                    elements.filter('.' + value).show(); // show the ones we want
                                                }
                                                }).trigger('change');
                                                });
                                                </script>
                                    
                                	</div>
                                
                                
                                </div>
								
								
							</section>
						</div>
						
						<div class="col-md-6 col-lg-12 col-xl-12">
                        <div class="row">
                            <div class="col-md-12 col-lg-6 col-xl-6">
                                <section class="panel panel-featured-left panel-featured-primary">
                                    <div class="panel-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-primary">
                                                    <i class="fa fa-users"></i>
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Prospects</h4>
                                                    <div class="info">
                                                        <?php 
                                                            $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign` ");
                                                            $rc=mysqli_fetch_assoc($cust);
                                                            $res=$rc['count_column'];
                                                            
                                                        ?>
                                                        <strong class="amount"><?php echo $res; ?></strong>
                                                        <!-- <span class="text-primary">(14 unread)</span> -->
                                                    </div>
                                                </div>
                                                <div class="summary-footer">
                                                    <a href="custdash.php" class="text-muted text-uppercase btn btn-primary"  ><font color="white">Total</font></a>
                                                    <a id="showcustomer" class="text-muted text-uppercase btn btn-primary"  ><font color="white">Campaigned</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showcustomer").click(function(){
                                                                $("#customer").show();
                                                                });
                                                            $("#hidecustomer").click(function(){
                                                                $("#customer").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div id="customer" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Campaigned Prospects</h2>
                                    </header>
                            
                                    <div class="panel-body paging">
                                        <a id="hidecustomer" class=" text-muted text-uppercase btn btn-primary pull-right"  ><font color="white">hide</font></a>
                            
                                        <div class="table-responsive">
                                            
                                            <?php
                                                /*$fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail,Date,CampaignType from `campaign` group by Company ");*/
                                                $fetfunnel=mysqli_query($dbc,"select distinct(t.Company), t.ContactPerson, t.Designation, t.Mobile, t.Mail, t.Date, t.CampaignType from (SELECT * FROM campaign ORDER BY ModificationDetail DESC) as t group by Company ");
                                                
                                                
                                                //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                        
                                                echo '<table class="table table-striped mb-none" id="datatable-tabletools" >';
                                                /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>Campaign Type</th>';
                                                            echo '<th>Status</th>';
                                                            echo '<th>CampaignDate</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $cci=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $cci;
                                                            $cci=$cci+1;
                                                            $company=$frow['Company'];
                                                            $contactperson=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $campaigneddate=$frow['Date'];

                                                            $st=mysqli_query($dbc,"select Stage from funnel where Company='$company' and MeetingDate='$campaigneddate' ");
                                                            while ($str=mysqli_fetch_assoc($st)) {
                                                                $status=$str['Stage'];
                                                            }
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$cci."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['CampaignType']}</td>";
                                                            echo "<td>".$status."</td>";
                                                            echo "<td>{$frow['Date']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>
                

                            <div class="col-md-12 col-lg-6 col-xl-6">
                                <section class="panel panel-featured-left panel-featured-secondary">
                                    <div class="panel-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-secondary">
                                                    <i class="fa fa-comment-o" ></i>
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Meetings</h4>
                                                    <div class="info">
                                                        <?php 
                                                            $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` ");
                                                            $rc=mysqli_fetch_assoc($cust);
                                                            $res=$rc['count_column'];
                                                            
                                                        ?>
                                                        <strong class="amount"><?php echo $res; ?></strong>
                                                    </div>
                                                </div>
                                                <div class="summary-footer">
                                                    <a id="showmeetings" class="text-muted text-uppercase btn bg-secondary"  ><font color="white">Total</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showmeetings").click(function(){
                                                                $("#meeting").show();
                                                                });
                                                            $("#hidemeetings").click(function(){
                                                                $("#meeting").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                    <a id="showmeetingsmonthly" class="text-muted text-uppercase btn bg-secondary"  ><font color="white">Monthly</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showmeetingsmonthly").click(function(){
                                                                $("#meetingmonthly").show();
                                                                });
                                                            $("#hidemeetingsmonthly").click(function(){
                                                                $("#meetingmonthly").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                    <a id="showmeetingsannually" class="text-muted text-uppercase btn bg-secondary"  ><font color="white">Annually</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showmeetingsannually").click(function(){
                                                                $("#meetingannually").show();
                                                                });
                                                            $("#hidemeetingsannually").click(function(){
                                                                $("#meetingannually").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="meeting" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Total Meetings</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidemeetings" class=" text-muted text-uppercase btn bg-secondary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                                    select Company, max(Date) as MaxDate
                                                    from meeting 
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company");
                                        
                                                echo '<table class="table table-striped table-bordered mb-none" >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            /*echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';*/
                                                            echo '<th>Last Meeting</th>';
                                                            echo '<th>Status</th>';
                                                            echo '<th>Requirement</th>';

                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tmi=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tmi;
                                                            $tmi=$tmi+1;
                                                            $company=$frow['Company'];
                                                            $contactperson=$frow['ContactPerson'];
                                                            
                                                            $mobile=$frow['Mobile'];
                                                            $mailid=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                            $pro=$frow['ProjectName'];
                                                            $campaigntype=$frow['CampaignType'];
                                                            $requirement=$frow['Requirement'];
                                                            $sector=$frow['Sector'];
                                                            
                                                            $companytype=$frow['CompanyType'];
                                                            $designation=$frow['Designation'];


                                                            $st=mysqli_query($dbc,"select Stage from funnel where Company='$company' and MeetingDate='$lastmeeting' ");
                                                            while ($str=mysqli_fetch_assoc($st)) {
                                                                $status=$str['Stage'];
                                                            }

                                                            if($status == null)
                                                            {
                                                                echo '<tr>';
                                                            echo "<td>".$tmi."</td>";
                                                            /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                            echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                                        <a href="proscat.php?c='.$company.'">View Profile</a>
                                                                    </div>
                                                                </div></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";*/
                                                            echo "<td>{$frow['Date']}</td>";
                                                            echo "<td>Meeting Stage</td>";
                                                            echo '<td><span class="more">'.$requirement.'</span></td>';
                                                            
                                                            echo '</tr>';
                                                            $status=null;

                                                            }
                                                            else
                                                            {

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tmi."</td>";
                                                            /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                            echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                                        <a href="proscat.php?c='.$company.'">View Profile</a>
                                                                    </div>
                                                                </div></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                           /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";*/
                                                            echo "<td>{$frow['Date']}</td>";
                                                            echo "<td>".$status."</td>";
                                                            echo '<td><span class="more">'.$requirement.'</span></td>';
                                                            
                                                        
                                                        
                                                            echo '</tr>';
                                                            $status=null;
                                                            }
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="meetingmonthly" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Meetings Monthly</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidemeetingsmonthly" class=" text-muted text-uppercase btn bg-secondary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                                    select Company, max(Date) as MaxDate
                                                    from meeting where MONTH(Date) = MONTH(CURRENT_DATE()) AND YEAR(Date) = YEAR(CURRENT_DATE())
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company");
                                        
                                                echo '<table class="table table-striped table-bordered mb-none" >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            /*echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';*/
                                                            echo '<th>Last Meeting</th>';
                                                            echo '<th>Status</th>';
                                                            echo '<th>Requirement</th>';

                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tmi=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tmi;
                                                            $tmi=$tmi+1;
                                                            $company=$frow['Company'];
                                                            $contactperson=$frow['ContactPerson'];
                                                            
                                                            $mobile=$frow['Mobile'];
                                                            $mailid=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                            $pro=$frow['ProjectName'];
                                                            $campaigntype=$frow['CampaignType'];
                                                            $requirement=$frow['Requirement'];
                                                            $sector=$frow['Sector'];
                                                            
                                                            $companytype=$frow['CompanyType'];
                                                            $designation=$frow['Designation'];


                                                            $st=mysqli_query($dbc,"select Stage from funnel where Company='$company' and MeetingDate='$lastmeeting' ");
                                                            while ($str=mysqli_fetch_assoc($st)) {
                                                                $status=$str['Stage'];
                                                            }

                                                            if($status == null)
                                                            {
                                                                echo '<tr>';
                                                            echo "<td>".$tmi."</td>";
                                                            /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                            echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                                        <a href="proscat.php?c='.$company.'">View Profile</a>
                                                                    </div>
                                                                </div></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";*/
                                                            echo "<td>{$frow['Date']}</td>";
                                                            echo "<td>Meeting Stage</td>";
                                                            echo '<td><span class="more">'.$requirement.'</span></td>';
                                                            
                                                            echo '</tr>';
                                                            $status=null;

                                                            }
                                                            else
                                                            {

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tmi."</td>";
                                                            /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                            echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                                        <a href="proscat.php?c='.$company.'">View Profile</a>
                                                                    </div>
                                                                </div></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                           /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";*/
                                                            echo "<td>{$frow['Date']}</td>";
                                                            echo "<td>".$status."</td>";
                                                            echo '<td><span class="more">'.$requirement.'</span></td>';
                                                            
                                                        
                                                        
                                                            echo '</tr>';
                                                            $status=null;
                                                            }
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>

                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="meetingannually" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Meetings Annually</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidemeetingsannually" class=" text-muted text-uppercase btn bg-secondary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                                    select Company, max(Date) as MaxDate
                                                    from meeting where  YEAR(Date) = YEAR(CURRENT_DATE())
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company");
                                        
                                                echo '<table class="table table-striped table-bordered mb-none" >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            /*echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';*/
                                                            echo '<th>Last Meeting</th>';
                                                            echo '<th>Status</th>';
                                                            echo '<th>Requirement</th>';

                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tmi=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tmi;
                                                            $tmi=$tmi+1;
                                                            $company=$frow['Company'];
                                                            $contactperson=$frow['ContactPerson'];
                                                            
                                                            $mobile=$frow['Mobile'];
                                                            $mailid=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                            $pro=$frow['ProjectName'];
                                                            $campaigntype=$frow['CampaignType'];
                                                            $requirement=$frow['Requirement'];
                                                            $sector=$frow['Sector'];
                                                            
                                                            $companytype=$frow['CompanyType'];
                                                            $designation=$frow['Designation'];


                                                            $st=mysqli_query($dbc,"select Stage from funnel where Company='$company' and MeetingDate='$lastmeeting' ");
                                                            while ($str=mysqli_fetch_assoc($st)) {
                                                                $status=$str['Stage'];
                                                            }

                                                            if($status == null)
                                                            {
                                                                echo '<tr>';
                                                            echo "<td>".$tmi."</td>";
                                                            /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                            echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                                        <a href="proscat.php?c='.$company.'">View Profile</a>
                                                                    </div>
                                                                </div></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";*/
                                                            echo "<td>{$frow['Date']}</td>";
                                                            echo "<td>Meeting Stage</td>";
                                                            echo '<td><span class="more">'.$requirement.'</span></td>';
                                                            
                                                            echo '</tr>';
                                                            $status=null;

                                                            }
                                                            else
                                                            {

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tmi."</td>";
                                                            /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                            echo '<td><div class="dropdown">
                                                                <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                                    <div class="dropdown-content">
                                                                        <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                                        <a href="proscat.php?c='.$company.'">View Profile</a>
                                                                    </div>
                                                                </div></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                           /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";*/
                                                            echo "<td>{$frow['Date']}</td>";
                                                            echo "<td>".$status."</td>";
                                                            echo '<td><span class="more">'.$requirement.'</span></td>';
                                                            
                                                        
                                                        
                                                            echo '</tr>';
                                                            $status=null;
                                                            }
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>
                                <div class="col-md-12 col-lg-6 col-xl-6">
                                    <section class="panel panel-featured-left panel-featured-tertiary">
                                        <div class="panel-body">
                                            <div class="widget-summary">
                                                <div class="widget-summary-col widget-summary-col-icon">
                                                    <div class="summary-icon bg-tertiary">
                                                        <i class="fa fa-reply"></i>
                                                    </div>
                                                </div>
                                                <div class="widget-summary-col">
                                                    <div class="summary">
                                                        <h4 class="title">Prospect Response</h4>
                                                        <div class="info">
                                                            <?php 
                                                                $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` where `RMail`='$id' and `Response`='Positive' ");
                                                                $rc=mysqli_fetch_assoc($cust);
                                                                $res=$rc['count_column'];
                                                                
                                                            ?>
                                                            <strong class="amount"><?php echo $res; ?></strong>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="summary-footer">
                                                        <a class="text-muted text-uppercase">(statement)</a>
                                                    </div> -->
                                                    <div class="summary-footer">
                                                    <a id="showresponse" class="text-muted text-uppercase btn bg-tertiary"  ><font color="white">Total</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showresponse").click(function(){
                                                                $("#response").show();
                                                                });
                                                            $("#hideresponse").click(function(){
                                                                $("#response").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                    <a id="showresponsemonthly" class="text-muted text-uppercase btn bg-tertiary"  ><font color="white">Monthly</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showresponsemonthly").click(function(){
                                                                $("#responsemonthly").show();
                                                                });
                                                            $("#hideresponsemonthly").click(function(){
                                                                $("#responsemonthly").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                    <a id="showresponseannually" class="text-muted text-uppercase btn bg-tertiary"  ><font color="white">Annually</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showresponseannually").click(function(){
                                                                $("#responseannually").show();
                                                                });
                                                            $("#hideresponseannually").click(function(){
                                                                $("#responseannually").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>

                                <div id="response" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Total Response</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hideresponse" class=" text-muted text-uppercase btn bg-tertiary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.Company, m.ContactPerson, m.Designation, m.Mobile, m.Mail, m.Date   from `meeting` m inner join (
                                                    select Company, max(date) as MaxDate
                                                    from meeting where  `Response`='Positive'
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate  ");
                                        
                                                echo '<table class="table table-striped mb-none">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>Last Meeting</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tcri=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tcri;
                                                            $tcri=$tcri+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tcri."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['Date']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="responsemonthly" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Response Monthly</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hideresponsemonthly" class=" text-muted text-uppercase btn bg-tertiary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.Company, m.ContactPerson, m.Designation, m.Mobile, m.Mail, m.Date   from `meeting` m inner join (
                                                    select Company, max(date) as MaxDate
                                                    from meeting where  `Response`='Positive' and MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' 
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate  ");
                                        
                                                echo '<table class="table table-striped mb-none">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>Last Meeting</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tcrmi=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tcrmi;
                                                            $tcrmi=$tcrmi+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tcrmi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['Date']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="responseannually" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Response Annually</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hideresponseannually" class=" text-muted text-uppercase btn bg-tertiary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                                $fetfunnel=mysqli_query($dbc,"select m.Company, m.ContactPerson, m.Designation, m.Mobile, m.Mail, m.Date   from `meeting` m inner join (
                                                    select Company, max(date) as MaxDate
                                                    from meeting where  `Response`='Positive' and  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' 
                                                    group by Company
                                                ) tm on m.Company = tm.Company and m.Date = tm.MaxDate  ");
                                        
                                                echo '<table class="table table-striped mb-none">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>Last Meeting</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tcrai=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tcrai;
                                                            $tcrai=$tcrai+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $lastmeeting=$frow['Date'];
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tcrai."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['Date']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                                <div class="col-md-12 col-lg-6 col-xl-6">
                                    <section class="panel panel-featured-left panel-featured-quartenary">
                                        <div class="panel-body">
                                            <div class="widget-summary">
                                                <div class="widget-summary-col widget-summary-col-icon">
                                                    <div class="summary-icon bg-quartenary">
                                                        <i class="fa fa-link"></i>
                                                    </div>
                                                </div>
                                                <div class="widget-summary-col">
                                                    <div class="summary">
                                                        <h4 class="title">FollowUp</h4>
                                                        <div class="info">
                                                            <?php 
                                                                $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` where `RMail`='$id' and `FollowUp`='Yes' ");
                                                                $rc=mysqli_fetch_assoc($cust);
                                                                $res=$rc['count_column'];
                                                                
                                                            ?>
                                                            <strong class="amount"><?php echo $res; ?></strong>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="summary-footer">
                                                        <a class="text-muted text-uppercase">(report)</a>
                                                    </div> -->
                                                    <div class="summary-footer">
                                                    <a id="showfollowup" class="text-muted text-uppercase btn bg-quartenary"  ><font color="white">Total</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfollowup").click(function(){
                                                                $("#followup").show();
                                                                });
                                                            $("#hidefollowup").click(function(){
                                                                $("#followup").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>
                                                    <a id="showfollowuptoday" class="text-muted text-uppercase btn bg-quartenary"  ><font color="white">Today</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfollowuptoday").click(function(){
                                                                $("#followuptoday").show();
                                                                });
                                                            $("#hidefollowuptoday").click(function(){
                                                                $("#followuptoday").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>
                                                    <a id="showfollowuptomorrow" class="text-muted text-uppercase btn bg-quartenary"  ><font color="white">Tomorrow</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfollowuptomorrow").click(function(){
                                                                $("#followuptomorrow").show();
                                                                });
                                                            $("#hidefollowuptomorrow").click(function(){
                                                                $("#followuptomorrow").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>
                                                    <a id="showfollowupweek" class="text-muted text-uppercase btn bg-quartenary"  ><font color="white">Week</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfollowupweek").click(function(){
                                                                $("#followupweek").show();
                                                                });
                                                            $("#hidefollowupweek").click(function(){
                                                                $("#followupweek").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>
                                                    <a id="showfollowupmonth" class="text-muted text-uppercase btn bg-quartenary"  ><font color="white">Month</font></a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            $("#showfollowupmonth").click(function(){
                                                                $("#followupmonth").show();
                                                                });
                                                            $("#hidefollowupmonth").click(function(){
                                                                $("#followupmonth").hide();
                                                                    });
                                                                
                                                        });
                                                    </script>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <div id="followup" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Total FollowUp</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidefollowup" class=" text-muted text-uppercase btn bg-quartenary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                            echo '<table class="table table-striped mb-none">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>FollowUp Date</th>';
                                                        echo '<th>FollowUp Time</th>';
                                                        echo '<th>Project</th>';
                                                        echo '<th>Requirement</th>';
                                                        echo '<th>Remarks</th>';
                                                        echo '<th>Infrastructure</th>';
                                                        echo '<th>Contact Person</th>';
                                                        echo '<th>Designation</th>';
                                                        echo '<th>Mobile</th>';
                                                        echo '<th>Mail</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $tfi=0;
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        global $tfi;
                                                        $tfi=$tfi+1;
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        

                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));
                                                        if($followupdate == $today)
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$tfi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['FollowUpDate']}";
                                                            echo '</div>';
                                                            echo '</div></td>';
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            /*echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";*/
                                                            echo '<td> <span class="more">'.$requirement.'</span></td>';
                                                            echo '<td> <span class="more">'.$remark.'</span></td>';
                                                            echo '<td> <span class="more">'.$infrastructure.'</span></td>';
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } else
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$tfi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['FollowUpDate']}</td>";
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } 
                                                        

                                                    }
                                                echo '</tbody>';
                                            echo '</table>';
                                        ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="followuptoday" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Today's FollowUp</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidefollowuptoday" class=" text-muted text-uppercase btn bg-quartenary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                            echo '<table class="table table-striped mb-none">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>FollowUp Date</th>';
                                                        echo '<th>FollowUp Time</th>';
                                                        echo '<th>Project</th>';
                                                        echo '<th>Requirement</th>';
                                                        echo '<th>Remarks</th>';
                                                        echo '<th>Infrastructure</th>';
                                                        echo '<th>Contact Person</th>';
                                                        echo '<th>Designation</th>';
                                                        echo '<th>Mobile</th>';
                                                        echo '<th>Mail</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $tdfi=0;
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        global $tdfi;
                                                        $tdfi=$tdfi+1;
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($followupdate == $today)
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$tdfi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['FollowUpDate']}";
                                                            echo '</div>';
                                                            echo '</div></td>';
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    /*$datetime = new DateTime($date);
                                                        $datetime->add(new DateInterval("P2D"));
                                                            echo $datetime->format('Y-m-d H:i:s');*/
                                            
                                                echo '</tbody>';
                                            echo '</table>';
                                        ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="followuptomorrow" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">Tomorrow FollowUp</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidefollowuptomorrow" class=" text-muted text-uppercase btn bg-quartenary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                            echo '<table class="table table-striped mb-none">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>FollowUp Date</th>';
                                                        echo '<th>FollowUp Time</th>';
                                                        echo '<th>Project</th>';
                                                        echo '<th>Requirement</th>';
                                                        echo '<th>Remarks</th>';
                                                        echo '<th>Infrastructure</th>';
                                                        echo '<th>Contact Person</th>';
                                                        echo '<th>Designation</th>';
                                                        echo '<th>Mobile</th>';
                                                        echo '<th>Mail</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $tmfi=0;
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        global $tmfi;
                                                        $tmfi=$tmfi+1;
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        

                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        $tomorrowdate=(new DateTime($today))->add(new DateInterval("P1D"))->format('Y-m-d');
                                                        //echo $tomorrowdate;
                                                        if($followupdate == $tomorrowdate)
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$tmfi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['FollowUpDate']}";
                                                            echo '</div>';
                                                            echo '</div></td>';
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['FollowUpDate']}</td>";
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }

                                                echo '</tbody>';
                                            echo '</table>';
                                        ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="followupweek" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">This Week FollowUp</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidefollowupweek" class=" text-muted text-uppercase btn bg-quartenary pull-right"  ><font color="white">hide</font></a>


                                            <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                            echo '<table class="table table-striped mb-none">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>FollowUp Date</th>';
                                                        echo '<th>FollowUp Time</th>';
                                                        echo '<th>Project</th>';
                                                        echo '<th>Requirement</th>';
                                                        echo '<th>Remarks</th>';
                                                        echo '<th>Infrastructure</th>';
                                                        echo '<th>Contact Person</th>';
                                                        echo '<th>Designation</th>';
                                                        echo '<th>Mobile</th>';
                                                        echo '<th>Mail</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $twfi=0;
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        global $twfi;
                                                        $twfi=$twfi+1;
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        

                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        $monday = strtotime("last monday");
                                                        $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;

                                                        $satday = strtotime(date("Y-m-d",$monday)." +5 days");

                                                        $this_week_sd = date("Y-m-d",$monday);
                                                        $this_week_ed = date("Y-m-d",$satday);

                                                        if($followupdate >=$this_week_sd and $followupdate<=$this_week_ed)
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$twfi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['FollowUpDate']}";
                                                            echo '</div>';
                                                            echo '</div></td>';
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['FollowUpDate']}</td>";
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                echo '</tbody>';
                                            echo '</table>';
                                        ?>


                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div id="followupmonth" class="col-lg-12 col-md-12" style="display:none">
                                <section class="panel">
                                    <header class="panel-heading panel-heading-transparent">
                                        <!-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->

                                        <h2 class="panel-title">This Month FollowUp</h2>
                                    </header>
                            
                                    <div class="panel-body">
                            
                                        <div class="table-responsive">
                                            <a id="hidefollowupmonth" class=" text-muted text-uppercase btn bg-quartenary pull-right"  ><font color="white">hide</font></a>
                                            <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                            echo '<table class="table table-striped mb-none">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>FollowUp Date</th>';
                                                        echo '<th>FollowUp Time</th>';
                                                        echo '<th>Project</th>';
                                                        echo '<th>Requirement</th>';
                                                        echo '<th>Remarks</th>';
                                                        echo '<th>Infrastructure</th>';
                                                        echo '<th>Contact Person</th>';
                                                        echo '<th>Designation</th>';
                                                        echo '<th>Mobile</th>';
                                                        echo '<th>Mail</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $tmfui=0;
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        global $tmfui;
                                                        $tmfui=$tmfui+1;
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        

                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        $fyear = date('Y', strtotime($followupdate));
                                                        $fmonth = date('F', strtotime($followupdate));

                                                        $cyear = date('Y', strtotime($today));
                                                        $cmonth = date('F', strtotime($today));
                                                        
                                                        if($fyear == $cyear and $fmonth == $cmonth)
                                                        {
                                                            echo '<tr>';
                                                            echo "<td>".$tmfui."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                            echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                echo "{$frow['FollowUpDate']}";
                                                            echo '</div>';
                                                            echo '</div></td>';
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['FollowUpDate']}</td>";
                                                            echo "<td>$cdate</td>";
                                                            echo "<td>{$frow['ProjectName']}</td>";
                                                            echo "<td>{$frow['Requirement']}</td>";
                                                            echo "<td>{$frow['Remarks']}</td>";
                                                            echo "<td>{$frow['Infrastructure']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                echo '</tbody>';
                                            echo '</table>';
                                        ?>


                                        </div>
                                    </div>
                                </section>
                            </div>
                    <!-- <div class="row">
						<div class="col-md-12">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
										<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
									</div>

									<h2 class="panel-title">Best Seller</h2>
									<p class="panel-subtitle">Customize the graphs as much as you want, there are so many options and features to display information using Porto Admin Template.</p>
								</header>
								<div class="panel-body">

									Flot: Basic
									<div class="chart chart-md" id="flotDashBasic"></div>
									<script>

										var flotDashBasicData = [{
											data: [
												[0, 170],
												[1, 169],
												[2, 173],
												[3, 188],
												[4, 147],
												[5, 113],
												[6, 128],
												[7, 169],
												[8, 173],
												[9, 128],
												[10, 128]
											],
											label: "Series 1",
											color: "#0088cc"
										}, {
											data: [
												[0, 115],
												[1, 124],
												[2, 114],
												[3, 121],
												[4, 115],
												[5, 83],
												[6, 102],
												[7, 148],
												[8, 147],
												[9, 103],
												[10, 113]
											],
											label: "Series 2",
											color: "#2baab1"
										}, {
											data: [
												[0, 70],
												[1, 69],
												[2, 73],
												[3, 88],
												[4, 47],
												[5, 13],
												[6, 28],
												[7, 69],
												[8, 73],
												[9, 28],
												[10, 28]
											],
											label: "Series 3",
											color: "#734ba9"
										}];

										

									</script>

								</div>
							</section>
						</div>
						
					</div> -->
                    
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
                <div class="nano">
                    <div class="nano-content">
                        <a href="#" class="mobile-close visible-xs">
                            Collapse <i class="fa fa-chevron-right"></i>
                        </a>
            
                        <div class="sidebar-right-wrapper">
            
                            <div class="sidebar-widget widget-calendar">
                                <h6>Upcoming Tasks</h6>
                                <div data-plugin-datepicker data-plugin-skin="dark" ></div>
            
                                <ul>
                                    <li>
                                        <!-- <time datetime="2014-04-19T00:00+00:00">04/19/2014</time> -->
                                        <span>Company</span>
                                    </li>
                                </ul>
                            </div>
            
                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Meeting</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `campaign` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($followupdate == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Contact Person: {$frow['ContactPerson']}</span>
                                            <span class=title>Mobile: <a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></span>
                                            <span class=title>Meeting Time: $cdate</span>
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                    
                                </ul>
                            </div>




                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Funnel Follow Up</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($dateofclosure == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Product: {$frow['Products']}</span>
                                            
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                   
                                </ul>
                            </div>

            
                        </div>
                    </div>
                </div>
            </aside>
		</section>

		<!-- Vendor -->
		<script src="../../../log/assets/vendor/jquery/jquery.js"></script>
		<script src="../../../log/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../../../log/assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../../../log/assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../../../log/assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../../../log/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="../../../log/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="../../../log/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="../../../log/assets/vendor/jquery-appear/jquery.appear.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="../../../log/assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.js"></script>
		<script src="../../../log/assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.pie.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.categories.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.resize.js"></script>
		<script src="../../../log/assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
		<script src="../../../log/assets/vendor/raphael/raphael.js"></script>
		<script src="../../../log/assets/vendor/morris/morris.js"></script>
		<script src="../../../log/assets/vendor/gauge/gauge.js"></script>
		<script src="../../../log/assets/vendor/snap-svg/snap.svg.js"></script>
		<script src="../../../log/assets/vendor/liquid-meter/liquid.meter.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/jquery.vmap.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../../../log/assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../../../log/assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../../../log/assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="../../../log/assets/javascripts/dashboard/examples.dashboard.js"></script>

    <!-- modal form -->
      <script src="../assets/javascripts/ui-elements/examples.modals.js"></script>
	</body>
</html>