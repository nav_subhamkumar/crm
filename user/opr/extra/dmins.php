<?php
session_start();
error_reporting(E_PARSE | E_ERROR);
$id=$_SESSION['id'];

if(!$_SESSION['id'])
{
	echo '<script>location.replace("../../index.php");</script>';
} 

require_once "../conn/conn.php";
$chk=mysqli_query($dbc,"select * from team where email='$id'");
while($fchk=mysqli_fetch_assoc($chk))
{
	$type=$fchk['EmployeeType'];
	if($type != "Admin")
	{
		echo '<script>location.replace("../../index.php");</script>';
	}
}

?>



<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Insert | Navikra CRM</title>
		<meta name="keywords" content="Navikra CRM" />
		<meta name="description" content="Navikra CRM">
		<meta name="author" content="navikra.com">

		<link rel="apple-touch-icon" sizes="180x180" href="../../../log/assets/images/apple-touch-icon.png">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-32x32.png" sizes="32x32">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-16x16.png" sizes="16x16">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap/css/bootstrap.css" />
		
				<link rel="stylesheet" href="../../../log/assets/vendor/font-awesome/css/font-awesome.css" />
				<link rel="stylesheet" href="../../../log/assets/vendor/magnific-popup/magnific-popup.css" />
				<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />
		
				<!-- Specific Page Vendor CSS -->
				<link rel="stylesheet" href="../../../log/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
				<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
				<link rel="stylesheet" href="../../../log/assets/vendor/morris/morris.css" />

				
		
				<!-- Theme CSS -->
				<link rel="stylesheet" href="../../../log/assets/stylesheets/theme.css" />
		
				<!-- Skin CSS -->
				<link rel="stylesheet" href="../../../log/assets/stylesheets/skins/default.css" />
		
				<!-- Theme Custom CSS -->
				<link rel="stylesheet" href="../../../log/assets/stylesheets/theme-custom.css">
		
				<!-- Head Libs -->
				<script src="../../../log/assets/vendor/modernizr/modernizr.js"></script>

    <!-- fa-product icon -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet" />

		<!-- search -->
		<!-- <script type="text/javascript" src="assets/ajax/search.js" ></script> -->
		
		<script type="text/javascript" src="../../../log/assets/ajax/jquery-1.8.0.min.js"></script>
		<script type="text/javascript">
$(function(){
$(".search").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
if(searchid!='')
{
    $.ajax({
    type: "POST",
    url: "../auth/search.php",
    data: dataString,
    cache: false,
    success: function(html)
    {
    $("#result").html(html).show();
    }
    });
}return false;    
});

jQuery("#result").live("click",function(e){ 
    var $clicked = $(e.target);
    var $name = $clicked.find('.name').html();
    var decoded = $("<div/>").html($name).text();
    $('#searchid').val(decoded);
});
jQuery(document).live("click", function(e) { 
    var $clicked = $(e.target);
    if (! $clicked.hasClass("search")){
    jQuery("#result").fadeOut(); 
    }
});
$('#searchid').click(function(){
    jQuery("#result").fadeIn();
});
});
</script>
		

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="dashboard.php" class="logo">
						<img src="../../../log/assets/images/logo.png" height="35" alt="Navikra CRM" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
			
				
                    
                    <?php
                        $ad=mysqli_query($dbc,"select * from team where email='$id'");
                        while($r=mysqli_fetch_assoc($ad))
                        {
                            $name=$r['name'];
                            $role=$r['desig'];
                        }
                    ?>
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="../../../log/assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="<?php echo $id; ?>">
								<span class="name"><?php echo $name; $_SESSION['n']=$name; ?></span>
								<span class="role"><?php echo $role; ?></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="profile.php"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<!-- <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a> -->
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="../../index.php"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li >
										<a href="dashboard.php">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<!-- <li>
										<a href="mailbox-folder.html">
											<span class="pull-right label label-primary">182</span>
											<i class="fa fa-envelope" aria-hidden="true"></i>
											<span>Mailbox</span>
										</a>
                                    </li> -->
                                    <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-users" aria-hidden="true"></i>
                                       <span>Customers</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="custdash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addcust.php">
                                                 Add Customer
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="customerprofile.php">
                                                 Add Customer Profile
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="campcust.php">
                                                 Campaign Customer
                                                </a>
                                        
                                           </li>
                                    
                                          <!--  <li >
                                              <a href="targetset.php">
                                                 Set Target
                                              </a>
                                        
                                           </li> -->
                                    

                                    </ul>
                                 </li>
                                    <li class="nav-parent nav-expanded nav-active" >
										<a >
											<!-- <span class="pull-right label label-primary">182</span> -->
											<i class="fa fa-plus" aria-hidden="true"></i>
											<span>Campaign</span>
										</a>

										<ul class="nav nav-children">
											
											<li >
												<a href="create-campaign.php">
													Create 
												</a>
											</li>

											<li class="nav-parent" >
												<a >
													Direct 
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="dirdash.php.php">
													 		Show
														</a>
													</li>
													<li >
														<a href="direct.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>
											
											<li class="nav-parent nav-active nav-expanded">
												<a >
													 Digital
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="digdash.php">
													 		Show
														</a>
													</li>
													<li class="nav-active">
														<a href="digital.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>

											<li class="nav-parent">
												<a >
													 Telecalling
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="teldash.php">
													 		Show
														</a>
													</li>
													<li>
														<a href="telecalling.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>
											<li class="nav-parent">
												<a >
													 Reference
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="refdash.php">
													 		Show
														</a>
													</li>
													<li>
														<a href="reference.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>

											
										</ul>

                                    </li>


									<li class="nav-parent">
										<a >
											<!-- <span class="pull-right label label-primary">182</span> -->
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Meeting</span>
										</a>

										<ul class="nav nav-children">


											<li >
												<a href="meetingdash.php">
													Show 
												</a>
												
											</li>
											
											<li >
												<a href="meeting.php">
													 Update
												</a>
												
											</li>

										</ul>

                                    </li>

									<li class="nav-parent">
										<a >
										
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Funnel</span>
										</a>
										<ul class="nav nav-children">


											<li >
												<a href="funneldash.php">
													Show 
												</a>
											
											</li>
										
											<li >
												<a href="funnel.php">
													 Update
												</a>
											
											</li>

										</ul>
									</li>

									<li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <span>Order</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Quotation 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="quotdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotation-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 PO
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="podash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="po-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="pogen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Invoice
                                            </a>
                                            <ul class="nav nav-children">
                                               <li>
                                                    <a href="invdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invoice-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>
                                 

									
                                    <li class="nav-parent">
									<a >
										
										<i class="fa fa-check" aria-hidden="true"></i>
										<span>Target Vs Achievement</span>
									</a>
									<ul class="nav nav-children">
	
	
										<!-- <li >
											<a href="funneldash.php">
												View 
											</a>
											
										</li> -->
										<li >
											<a href="achievement.php">
												 Achievement
											</a>
											
										</li>
										
										<li >
											<a href="targetset.php">
												Set Target
											</a>
											
										</li>
										
	
									</ul>
								</li>
								<li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                        <span>HR</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Employee 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="emplist.php">
                                                         Employee List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addemp.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Holiday
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="holidaylist.php">
                                                         Holiday List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addholiday.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Leave
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="leavedash.php">
                                                         Applications
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>

                                 <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                       <span>Products</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="proddash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addprod.php">
                                                 Add
                                                </a>
                                        
                                           </li>
                                    
                                          

                                    </ul>
                                 </li>

                                 
									<li>
										<a href="#">
											
											<i class="fa fa-book" aria-hidden="true"></i>
											<span>Reports</span>
										</a>
                                    </li>

									<!-- <li>
										<a href="upd.php">
											
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Update</span>
										</a>
                                    </li>
									<li>
										<a href="csv.php">
											
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Update CSV</span>
										</a>
                                    </li> -->


									<!-- <li class="nav-parent">
										<a>
											<i class="fa fa-copy" aria-hidden="true"></i>
											<span>Pages</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="pages-signup.html">
													 Sign Up
												</a>
											</li>
											<li>
												<a href="meeting-ins.php">
													 Sign In
												</a>
											</li>
											<li>
												<a href="pages-recover-password.html">
													 Recover Password
												</a>
											</li>
											<li>
												<a href="pages-lock-screen.html">
													 Locked Screen
												</a>
											</li>
											<li>
												<a href="pages-user-profile.html">
													 User Profile
												</a>
											</li>
											<li>
												<a href="pages-session-timeout.html">
													 Session Timeout
												</a>
											</li>
											<li>
												<a href="pages-calendar.html">
													 Calendar
												</a>
											</li>
											<li>
												<a href="pages-timeline.html">
													 Timeline
												</a>
											</li>
											<li>
												<a href="pages-media-gallery.html">
													 Media Gallery
												</a>
											</li>
											<li>
												<a href="pages-invoice.html">
													 Invoice
												</a>
											</li>
											<li>
												<a href="pages-blank.html">
													 Blank Page
												</a>
											</li>
											<li>
												<a href="pages-404.html">
													 404
												</a>
											</li>
											<li>
												<a href="pages-500.html">
													 500
												</a>
											</li>
											<li>
												<a href="pages-log-viewer.html">
													 Log Viewer
												</a>
											</li>
											<li >
												<a href="pages-search-results.html">
													 Search Results
												</a>
											</li>
										</ul>
									</li> -->
									
									
								</ul>
							</nav>
				
							<!-- <hr class="separator" />
				
							<div class="sidebar-widget widget-tasks">
								<div class="widget-header">
									<h6>Projects</h6>
									<div class="widget-toggle">+</div>
								</div>
								<div class="widget-content">
									<ul class="list-unstyled m-none">
										<li><a href="#">Porto HTML5 Template</a></li>
										<li><a href="#">Tucson Template</a></li>
										<li><a href="#">Porto Admin</a></li>
									</ul>
								</div>
							</div>
 -->				
                           
							<hr class="separator" />
				
							<div class="sidebar-widget widget-stats">
								<div class="widget-header">
									<h6>Company Stats</h6>
									<div class="widget-toggle">+</div>
								</div>
								<div class="widget-content">
									<ul>
										<li>
											<span class="stats-title">Stat 1</span>
											<span class="stats-complete">85%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">
													<span class="sr-only">85% Complete</span>
												</div>
											</div>
										</li>
										<li>
											<span class="stats-title">Stat 2</span>
											<span class="stats-complete">70%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
													<span class="sr-only">70% Complete</span>
												</div>
											</div>
										</li>
										<li>
											<span class="stats-title">Stat 3</span>
											<span class="stats-complete">2%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
													<span class="sr-only">2% Complete</span>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Digital Campaign Insert</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<!-- <li><span>Forms</span></li> -->
								<li><span>Insert</span></li>
							</ol>

							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
					
							<!-- <a class="sidebar-right-toggle" ><i class="fa fa-chevron-down"></i></a> -->
						</div>
					</header>

					<!-- start: page -->
						
						<div class="row">
							<div class="col-xs-12">
								<section class="panel form-wizard" id="w4">
									<header class="panel-heading">
										<!-- <div class="panel-actions">
											<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
											<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
										</div> -->
						
										<h2 class="panel-title">Input Details</h2>
									</header>
									<div class="panel-body">
										<!-- <div class="wizard-progress wizard-progress-lg">
											<div class="steps-progress">
												<div class="progress-indicator"></div>
											</div>
											<ul class="wizard-steps">
												<li class="active">
													<a href="#w4-account" data-toggle="tab"><span>1</span>Company Info</a>
												</li>
												<li>
													<a href="#w4-profile" data-toggle="tab"><span>2</span>Contact Info</a>
												</li>
												<li>
													<a href="#w4-billing" data-toggle="tab"><span>3</span>Billing Info</a>
												</li>
												<li>
													<a href="#w4-confirm" data-toggle="tab"><span>2</span>Contact Info</a>
												</li>
											</ul>
										</div> -->

										<?php
										$t=$_GET['t'];
										$company=$_GET['c'];
										$fname=$_GET['f'];
										$lname=$_GET['l'];
										$email=$_GET['e'];

										$q=mysqli_query($dbc,"select * from `$t` where Company='$company' or Mail='$email'");
										while($dat=mysqli_fetch_assoc($q))
										{
											$fname=$dat['FirstName'];
											$lname=$dat['LastName'];
											$desig=$dat['Designation'];
											$mob=$dat['Mobile'];
											$mail=$dat['Mail'];
											$sector=$dat['Sector'];

											
										}
										?>
						
										<form action="../auth/campaign/diri.php" method="post" class="form-horizontal" novalidate="novalidate"  >
										
											


												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Campaign Type</label>
													<div class="col-sm-3">
														<!-- <select class="form-control" name="campaigntype" >
															<option selected value="Direct Campaign">Direct Campaign</option>
															<option value="Digital Campaign">Digital Campaign</option>
															<option value="TeleCalling Campaign">TeleCalling Campaign</option>
															<option value="Reference Campaign">Reference Campaign</option>
															
														</select> -->
														<select class="form-control" name="campaigntype" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
													
														<option value="Direct">Direct</option>
														<option selected value="Digital">Digital</option>
														<option value="TeleCalling">TeleCalling</option>
														<option value="Reference">Reference</option>
													
												</select>
													</div>
												
												
													<label class="col-sm-2 control-label" for="w4-username">Projects</label>
													<div class="col-sm-3" style="align:right">
													<select class="form-control" name="project"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6" required >
														<?php
															$project=mysqli_query($dbc,"select * from `campaign-projects`");
															echo '<option  value="'.$t.'">'.$t.'</option>';
															while($row=mysqli_fetch_assoc($project))
															{
																//$pro=$row['ProjectName'];
														
																 echo '<option  value="'.$row['ProjectName'].'">'.$row['ProjectName'].'</option>';
														
															}
														?>
													</select>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Campaign Date</label>
													<div class="col-sm-9">
														<div class="input-group">
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" name="campaigndate" data-plugin-datepicker class="form-control" required>
														</div>
													</div>
												</div>


										
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-username">Company</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="company" value="<?php echo $company; ?>" id="w4-username" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-username">Sector</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="sector" value="<?php echo $sector; ?>" id="w4-username" required>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-email">Contact Person</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="contactperson" value="<?php echo $fname." ".$lname; ?>" id="w4-f" onKeyUp="fname()" >
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-email">Designation</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="designation" value="<?php echo $desig ?>" id="w4-email" >
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-email">Mobile</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="mobile" value="<?php echo $mob; ?>" id="w4-mob" onKeyUp="mob()" maxlength="12">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-email">Email</label>
												<div class="col-sm-9">
													<input type="email" class="form-control" name="email" value="<?php echo $mail; ?>" id="w4-email" >
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-username">Response</label>
												<div class="col-sm-3">
												<select class="form-control" name="response"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
														<option selected value="Positive">Positive</option>
														<option value="Negative">Negative</option>
													</select>
												</div>
											<!-- </div>
											<div class="form-group"> -->
												
												<label class="col-sm-2 control-label " for="w4-username">Follow Up</label>
												<div class="col-sm-3 ">
												<select class="form-control" name="followup"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
														<option selected value="Yes" >Yes</option>
														<option value="No">No</option>
													</select>
												</div>
												
											</div>
											
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-username">FollowUp Date</label>
												<div class="col-sm-9">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</span>
														<input type="text" name="followupdate" data-plugin-datepicker class="form-control" >
													</div>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-email">Requirements</label>
												<div class="col-sm-9">
												<textarea name="requirements" rows="5" title="Input Requirements." class="form-control" placeholder="Enter customer requirements" ></textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-email">Remarks</label>
												<div class="col-sm-9">
												<textarea name="remarks" rows="5" title="Input Remarks." class="form-control" placeholder="Enter customer Remarks" ></textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-email">Infrastructure</label>
												<div class="col-sm-9">
												<textarea name="infrastructure" rows="5" title="Input Infrastructure." class="form-control" placeholder="Enter customer Infrastructure" ></textarea>
												</div>
											</div>

												<div class="form-group">
													<div class="col-sm-2"></div>
													<div class="col-sm-9">
														<div class="checkbox-custom">
														<input type="submit" name="submit" value="Submit" class="btn btn-success pull-right" />
															
														</div>
													</div>
												</div>
												
									</form>	
												
												<!-- <div id="w4-profile" class="tab-pane">
													<div class="form-group">
														<label class="col-sm-2 control-label" for="w4-first-name">First Name</label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="first-name" id="w4-first-name" >
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="w4-last-name">Last Name</label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="last-name" id="w4-last-name" >
														</div>
													</div>
												</div>
												<div id="w4-billing" class="tab-pane">
													<div class="form-group">
														<label class="col-sm-2 control-label" for="w4-cc">Card Number</label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="cc-number" id="w4-cc" >
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="inputSuccess">Expiration</label>
														<div class="col-sm-5">
															<select class="form-control" name="exp-month" >
																<option>January</option>
																<option>February</option>
																<option>March</option>
																<option>April</option>
																<option>May</option>
																<option>June</option>
																<option>July</option>
																<option>August</option>
																<option>September</option>
																<option>October</option>
																<option>November</option>
																<option>December</option>
															</select>
														</div>
														<div class="col-sm-4">
															<select class="form-control" name="exp-year" >
																<option>2014</option>
																<option>2015</option>
																<option>2016</option>
																<option>2017</option>
																<option>2018</option>
																<option>2019</option>
																<option>2020</option>
																<option>2021</option>
																<option>2022</option>
															</select>
														</div>
													</div>
												</div> -->
												
									</div>
									
								</section>
							</div>
						</div>
						
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
                <div class="nano">
                    <div class="nano-content">
                        <a href="#" class="mobile-close visible-xs">
                            Collapse <i class="fa fa-chevron-right"></i>
                        </a>
            
                        <div class="sidebar-right-wrapper">
            
                            <div class="sidebar-widget widget-calendar">
                                <h6>Upcoming Tasks</h6>
                                <div data-plugin-datepicker data-plugin-skin="dark" ></div>
            
                                <ul>
                                    <li>
                                        <!-- <time datetime="2014-04-19T00:00+00:00">04/19/2014</time> -->
                                        <span>Company</span>
                                    </li>
                                </ul>
                            </div>
            
                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Meeting</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($followupdate == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Contact Person: {$frow['ContactPerson']}</span>
                                            <span class=title>Mobile: <a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></span>
                                            <span class=title>Meeting Time: $cdate</span>
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                    
                                </ul>
                            </div>




                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Funnel Follow Up</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND YEAR(ModificationDetail) = YEAR(CURRENT_DATE()) ");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($dateofclosure == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Product: {$frow['Products']}</span>
                                            
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                   
                                </ul>
                            </div>

            
                        </div>
                    </div>
                </div>
            </aside>
		</section>

		<!-- Vendor -->
		<script src="../../../log/assets/vendor/jquery/jquery.js"></script>
		<script src="../../../log/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../../../log/assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../../../log/assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../../../log/assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../../../log/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="../../../log/assets/vendor/jquery-validation/jquery.validate.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
		<script src="../../../log/assets/vendor/pnotify/pnotify.custom.js"></script>

		<script src="../../../log/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="../../../log/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="../../../log/assets/vendor/select2/select2.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		
		
		
	
		
		
		
		
		<script src="../../../log/assets/vendor/ios7-switch/ios7-switch.js"></script>
		
		
		<!-- Theme Base, Components and Settings -->
		<script src="../../../log/assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../../../log/assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../../../log/assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="../../../log/assets/javascripts/forms/examples.wizard.js"></script>
		<script src="../../../log/assets/javascripts/forms/examples.advanced.form.js"></script>
		<script>
		function pins()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-pin")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-pin")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function stdc()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-std")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-std")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function mob()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-mob")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-mob")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function cn()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-cn")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-cn")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function landl()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-land")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-land")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function fname()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-f")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-f")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}

		function lname()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-l")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-l")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}

		function cpt()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-cp")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-cp")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}
		</script>
	</body>
</html>