<script type="text/javascript">
   $(function()
{
    $(document).on('click', '.btn-add-basic', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls-basic '),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add-basic')
            .removeClass('btn-add-basic').addClass('btn-remove-basic')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="icon md-minus"></span>');
    }).on('click', '.btn-remove-basic', function(e)
    {
      $(this).parents('.entry:first').remove();

      e.preventDefault();
      return false;
   });
});

   
</script>
<script type="text/javascript">
   $(function()
{
    $(document).on('click', '.btn-add-advance', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls-advance '),
            currentEntry = $(this).parents('.entrya:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entrya:not(:last) .btn-add-advance')
            .removeClass('btn-add-advance').addClass('btn-remove-advance')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="icon md-minus"></span>');
    }).on('click', '.btn-remove-advance', function(e)
    {
      $(this).parents('.entrya:first').remove();

      e.preventDefault();
      return false;
   });
});

   
</script>


<script type="text/javascript">
   $(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls '),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="icon md-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

      e.preventDefault();
      return false;
   });
});

   
</script>